<?php

namespace Tests\API\v0;

use App\AppstoreReceipt;
use App\FailedMobileSubscription;
use App\Gateway;
use App\Offer;
use App\Plan;
use App\Purchase;
use App\Subscription;
use App\Transaction;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LegacyAppstoreNotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Plan */
    private $plan;

    /** @var \App\Offer */
    private $offer;
    
    /** @var \App\User */
    private $user;

    /** @var \App\Transaction */
    private $transaction;

    /** @var \App\Purchase */
    private $purchase;

    /** @var \App\Subscription */
    private $subscription;

    /** @var \App\FailedMobileSubscription */
    private $failed_subscription;

    private
        $user_id = '13371706',
        $user_name = 'john doe',
        $user_email = 'john.doe.unique.123@example.org',
        $offer_duration = 1,
        $offer_token = 'tse1month',
        $subscription_gateway_token = '320000638174806',
        $previous_transaction_gateway_token = '320000638174806',
        $new_transaction_gateway_token = '340000638174999',
        $vendor_token = '382C9717-E277-46EB-BF53-AA5D7AB38240';

    private function createTestModels()
    {
        Model::withoutEvents(function()
        {
            $this->user = User::forceCreate([
                'id' => $this->user_id,
                'surname' =>  $this->user_name,
                'email' => $this->user_email,
                'username' => $this->user_email,
                'agreed_terms' => true,
                'password' => bcrypt('password')
            ]);

            $this->plan = Plan::create([
                'name'=>'TEST PLAN'
            ]);
    
            $this->offer = Offer::create([
                'plan_id'=>$this->plan->id,
                'name'=>'TEST OFFER',
                'title'=>'TEST OFFER',
                'description'=>'This is a test offer. You should not see this.',
                'price'=>'14.99',
                'tax_percent'=>'20',
                'duration'=>$this->offer_duration,
                'is_recurring'=>true,
                'appstore_id'=>$this->offer_token
            ]);
        });
    }

    private function createFailedSubscription()
    {
        Model::withoutEvents(function()
        {
            $this->failed_subscription = FailedMobileSubscription::create([
                'user_id' => $this->user->id,
                'gateway_id' => Gateway::APPSTORE,
                'offer_id' => $this->offer->id,
                'vendor_token' => $this->vendor_token,
                'error' => 'test error',
            ]);

            $this->failed_subscription = FailedMobileSubscription::first();
        });
    }

    private function createSuccessfulSubscription()
    {
        Model::withoutEvents(function()
        {
            $this->subscription = Subscription::createActive(
                $this->user,
                $this->offer,
                $this->subscription_gateway_token,
                Gateway::APPSTORE
            );

            $this->transaction = Transaction::createSucceededOfferSubscription(
                $this->subscription,
                $this->new_transaction_gateway_token,
                bcmul($this->offer->price,100),
                0,
                'EUR',
                now()->timestamp
            );

            $this->purchase = Purchase::createFromTransaction(
                $this->transaction
            );
        });
    }

    /**
     * Test the appstore subscription initial buy server notification
     * without failed subscription existing
     *
     * @return void
     */
    public function testAppstoreInitialBuy()
    { 
        $this->createTestModels();

        $payload = json_decode('{
            "latest_receipt":"ewoJInNpZ25hdHVyZSIgPSAiQXlDOEQ2R3dNdTJVS0Jta29tVEl5VTFQK2xheFluZ0xITkVBd3ZaWUN1REhGRmZsZUNhQkxBaC9tdnNBRkRmcEh4WkIwM2paaURybFdlcmZhZGlFM21vQzA4VitaeEg0MGhUZEV4WXZvY1RpTWh3bkpLajcySEdUcXd5cEk3Um9oeUJ2TmZXUXJTYlZ5MnV5azU1RUNZV25WU25QRDVhQ1BSeERmMWEzUkN1Unp5MnNRUngvUlA2ZXhjdjI3eFMwNCs4T0JaS1UvdU5qVTJCMDVFT2RZOUZlVkhxSmVPOXROMG95S0lONm5yRzQzdExrbjBOVEp0T2xPZWNtSDZyU2xYU2pTcDhGazdQT2hhclJVZGloN3JocU9MTW1maWMwM2VvWFpocXdkOU9CYWZCQWJWcFk4VUlzb3hVTFF3MG9SZGJXbjFMd08veURMbmE2R2lWNlR5WUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ESXdMVEF4TFRJNElERTRPalE0T2pVMElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5GMVlXNTBhWFI1SWlBOUlDSXhJanNLQ1NKemRXSnpZM0pwY0hScGIyNHRaM0p2ZFhBdGFXUmxiblJwWm1sbGNpSWdQU0FpTWpBMU5qZ3dPREFpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlNemd5UXprM01UY3RSVEkzTnkwME5rVkNMVUpHTlRNdFFVRTFSRGRCUWpNNE1qUXdJanNLQ1NKdmNtbG5hVzVoYkMxd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhOVGd3TWpZMk1UTTBNREF3SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREl3TFRBeUxUSTVJREF5T2pRNE9qVXpJRVYwWXk5SFRWUWlPd29KSW1sekxXbHVMV2x1ZEhKdkxXOW1abVZ5TFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0YlhNaUlEMGdJakUxT0RBeU5qWXhNek13TURBaU93b0pJbVY0Y0dseVpYTXRaR0YwWlMxbWIzSnRZWFIwWldRdGNITjBJaUE5SUNJeU1ESXdMVEF5TFRJNElERTRPalE0T2pVeklFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW1sekxYUnlhV0ZzTFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkltbDBaVzB0YVdRaUlEMGdJakUwT0RjM05URXpNakFpT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0kyTkRBMU9XRTBOakppTVdJd1lXUmpOREE1WkdWak5qSXpOemxtWmpjNU4yTXhOV1ppT0RGaUlqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTXpJd01EQXdOak00TVRjME9EQTJJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVWlJRDBnSWpFMU9ESTVORFExTXpNd01EQWlPd29KSW1Gd2NDMXBkR1Z0TFdsa0lpQTlJQ0l4TkRneU1EZ3dOVEF4SWpzS0NTSjBjbUZ1YzJGamRHbHZiaTFwWkNJZ1BTQWlNekl3TURBd05qTTRNVGMwT0RBMklqc0tDU0ppZG5KeklpQTlJQ0l4SWpzS0NTSjNaV0l0YjNKa1pYSXRiR2x1WlMxcGRHVnRMV2xrSWlBOUlDSXpNakF3TURBeU1qY3dNRFk1TmpJaU93b0pJblpsY25OcGIyNHRaWGgwWlhKdVlXd3RhV1JsYm5ScFptbGxjaUlnUFNBaU9ETTBORFl6TXpRMUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dmFHMWpiMjVqWlhCMGFXOXVMblJ6WlNJN0Nna2ljSEp2WkhWamRDMXBaQ0lnUFNBaWRITmxNVzF2Ym5Sb0lqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURJd0xUQXhMVEk1SURBeU9qUTRPalV6SUVWMFl5OUhUVlFpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0Y0hOMElpQTlJQ0l5TURJd0xUQXhMVEk0SURFNE9qUTRPalV6SUVGdFpYSnBZMkV2VEc5elgwRnVaMlZzWlhNaU93b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVaUlEMGdJakl3TWpBdE1ERXRNamtnTURJNk5EZzZOVFFnUlhSakwwZE5WQ0k3Q24wPSI7CgkicG9kIiA9ICIzMiI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9",
            "auto_renew_status_change_date":"2020-02-08 00:15:17 Etc/GMT",
            "environment":"PROD",
            "auto_renew_status":"false",
            "auto_renew_status_change_date_pst":"2020-02-07 16:15:17 America/Los_Angeles",

            "latest_receipt_info":{
                "original_purchase_date_pst":"2020-01-28 18:48:54 America/Los_Angeles",
                "quantity":"1",
                "subscription_group_identifier":"20568080",
                "unique_vendor_identifier":"'.$this->vendor_token.'",
                "original_purchase_date_ms":"1580266134000",
                "expires_date_formatted":"2020-02-29 02:48:53 Etc/GMT",
                "is_in_intro_offer_period":"false",
                "purchase_date_ms":"1580266133000",
                "expires_date_formatted_pst":"2020-02-28 18:48:53 America/Los_Angeles",
                "is_trial_period":"false",
                "item_id":"1487751320",
                "unique_identifier":"64059a462b1b0adc409dec62379ff797c15fb81b",
                "original_transaction_id":"'.$this->subscription_gateway_token.'",
                "expires_date":"1582944533000",
                "app_item_id":"1482080501",
                "transaction_id":"'.$this->new_transaction_gateway_token.'",
                "bvrs":"1",
                "web_order_line_item_id":"320000227006962",
                "version_external_identifier":"834463345",
                "bid":"com.ohmconception.tse",
                "product_id":"'.$this->offer->appstore_id.'",
                "purchase_date":"2020-01-29 02:48:53 Etc/GMT",
                "purchase_date_pst":"2020-01-28 18:48:53 America/Los_Angeles",
                "original_purchase_date":"2020-01-29 02:48:54 Etc/GMT"
            },

            "auto_renew_status_change_date_ms":"1581120917000",
            "auto_renew_product_id":"tse1month",
            "notification_type":"INITIAL_BUY"
         }');

        $endpoint = '/'.config('webhook.appstore_legacy_endpoint');

        $response = $this->call(
            'POST',
            $endpoint,
            [],
            [],
            [],
            [],
            json_encode($payload)
        );

        $response->assertOk();

        $response->assertJson([
            'message' => 'success'
        ]);

        $this->assertDatabaseHas(
            'appstore_receipts',
            [
                'id' => 1,
                'vendor_token' => $this->vendor_token,
                'original_transaction_token' =>
                    $this->subscription_gateway_token,
                'transaction_token' => $this->new_transaction_gateway_token,
            ]
        );

        $receipt_content = AppstoreReceipt::first()->content;

        $inflated_content = json_decode(gzinflate($receipt_content));

        $this->assertEquals(
            $payload,
            $inflated_content,
            'stored content is not identical to payload'
        );
    }

    /**
     * Test the appstore subscription initial buy server notification
     * with failed subscription existing and recovered
     *
     * @return void
     */
    public function testAppstoreInitialBuyRecovered()
    {
        $this->createTestModels();
        $this->createFailedSubscription();

        $payload = json_decode('{
            "latest_receipt":"ewoJInNpZ25hdHVyZSIgPSAiQXlDOEQ2R3dNdTJVS0Jta29tVEl5VTFQK2xheFluZ0xITkVBd3ZaWUN1REhGRmZsZUNhQkxBaC9tdnNBRkRmcEh4WkIwM2paaURybFdlcmZhZGlFM21vQzA4VitaeEg0MGhUZEV4WXZvY1RpTWh3bkpLajcySEdUcXd5cEk3Um9oeUJ2TmZXUXJTYlZ5MnV5azU1RUNZV25WU25QRDVhQ1BSeERmMWEzUkN1Unp5MnNRUngvUlA2ZXhjdjI3eFMwNCs4T0JaS1UvdU5qVTJCMDVFT2RZOUZlVkhxSmVPOXROMG95S0lONm5yRzQzdExrbjBOVEp0T2xPZWNtSDZyU2xYU2pTcDhGazdQT2hhclJVZGloN3JocU9MTW1maWMwM2VvWFpocXdkOU9CYWZCQWJWcFk4VUlzb3hVTFF3MG9SZGJXbjFMd08veURMbmE2R2lWNlR5WUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ESXdMVEF4TFRJNElERTRPalE0T2pVMElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5GMVlXNTBhWFI1SWlBOUlDSXhJanNLQ1NKemRXSnpZM0pwY0hScGIyNHRaM0p2ZFhBdGFXUmxiblJwWm1sbGNpSWdQU0FpTWpBMU5qZ3dPREFpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlNemd5UXprM01UY3RSVEkzTnkwME5rVkNMVUpHTlRNdFFVRTFSRGRCUWpNNE1qUXdJanNLQ1NKdmNtbG5hVzVoYkMxd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhOVGd3TWpZMk1UTTBNREF3SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREl3TFRBeUxUSTVJREF5T2pRNE9qVXpJRVYwWXk5SFRWUWlPd29KSW1sekxXbHVMV2x1ZEhKdkxXOW1abVZ5TFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0YlhNaUlEMGdJakUxT0RBeU5qWXhNek13TURBaU93b0pJbVY0Y0dseVpYTXRaR0YwWlMxbWIzSnRZWFIwWldRdGNITjBJaUE5SUNJeU1ESXdMVEF5TFRJNElERTRPalE0T2pVeklFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW1sekxYUnlhV0ZzTFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkltbDBaVzB0YVdRaUlEMGdJakUwT0RjM05URXpNakFpT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0kyTkRBMU9XRTBOakppTVdJd1lXUmpOREE1WkdWak5qSXpOemxtWmpjNU4yTXhOV1ppT0RGaUlqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTXpJd01EQXdOak00TVRjME9EQTJJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVWlJRDBnSWpFMU9ESTVORFExTXpNd01EQWlPd29KSW1Gd2NDMXBkR1Z0TFdsa0lpQTlJQ0l4TkRneU1EZ3dOVEF4SWpzS0NTSjBjbUZ1YzJGamRHbHZiaTFwWkNJZ1BTQWlNekl3TURBd05qTTRNVGMwT0RBMklqc0tDU0ppZG5KeklpQTlJQ0l4SWpzS0NTSjNaV0l0YjNKa1pYSXRiR2x1WlMxcGRHVnRMV2xrSWlBOUlDSXpNakF3TURBeU1qY3dNRFk1TmpJaU93b0pJblpsY25OcGIyNHRaWGgwWlhKdVlXd3RhV1JsYm5ScFptbGxjaUlnUFNBaU9ETTBORFl6TXpRMUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dmFHMWpiMjVqWlhCMGFXOXVMblJ6WlNJN0Nna2ljSEp2WkhWamRDMXBaQ0lnUFNBaWRITmxNVzF2Ym5Sb0lqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURJd0xUQXhMVEk1SURBeU9qUTRPalV6SUVWMFl5OUhUVlFpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0Y0hOMElpQTlJQ0l5TURJd0xUQXhMVEk0SURFNE9qUTRPalV6SUVGdFpYSnBZMkV2VEc5elgwRnVaMlZzWlhNaU93b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVaUlEMGdJakl3TWpBdE1ERXRNamtnTURJNk5EZzZOVFFnUlhSakwwZE5WQ0k3Q24wPSI7CgkicG9kIiA9ICIzMiI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9",
            "auto_renew_status_change_date":"2020-02-08 00:15:17 Etc/GMT",
            "environment":"PROD",
            "auto_renew_status":"false",
            "auto_renew_status_change_date_pst":"2020-02-07 16:15:17 America/Los_Angeles",

            "latest_receipt_info":{
                "original_purchase_date_pst":"2020-01-28 18:48:54 America/Los_Angeles",
                "quantity":"1",
                "subscription_group_identifier":"20568080",
                "unique_vendor_identifier":"'.$this->vendor_token.'",
                "original_purchase_date_ms":"1580266134000",
                "expires_date_formatted":"2020-02-29 02:48:53 Etc/GMT",
                "is_in_intro_offer_period":"false",
                "purchase_date_ms":"1580266133000",
                "expires_date_formatted_pst":"2020-02-28 18:48:53 America/Los_Angeles",
                "is_trial_period":"false",
                "item_id":"1487751320",
                "unique_identifier":"64059a462b1b0adc409dec62379ff797c15fb81b",
                "original_transaction_id":"'.$this->subscription_gateway_token.'",
                "expires_date":"1582944533000",
                "app_item_id":"1482080501",
                "transaction_id":"'.$this->new_transaction_gateway_token.'",
                "bvrs":"1",
                "web_order_line_item_id":"320000227006962",
                "version_external_identifier":"834463345",
                "bid":"com.ohmconception.tse",
                "product_id":"'.$this->offer->appstore_id.'",
                "purchase_date":"2020-01-29 02:48:53 Etc/GMT",
                "purchase_date_pst":"2020-01-28 18:48:53 America/Los_Angeles",
                "original_purchase_date":"2020-01-29 02:48:54 Etc/GMT"
            },

            "auto_renew_status_change_date_ms":"1581120917000",
            "auto_renew_product_id":"tse1month",
            "notification_type":"INITIAL_BUY"
         }');

        $endpoint = '/'.config('webhook.appstore_legacy_endpoint');

        $response = $this->call(
            'POST',
            $endpoint,
            [],
            [],
            [],
            [],
            json_encode($payload)
        );

        $this->user = User::first();
        $appstore_receipt = AppstoreReceipt::first();
        $new_subscription = Subscription::first();
        $new_transaction = Transaction::first();
        $this->failed_subscription = FailedMobileSubscription::first();

        $response->assertOk();

        $response->assertJson([
            'message' => 'success',
        ]);

        $this->assertDatabaseHas(
            'appstore_receipts',
            [
                'id' => 1,
                'vendor_token' => $this->vendor_token,
                'original_transaction_token' =>
                    $this->subscription_gateway_token,
                'transaction_token' => $this->new_transaction_gateway_token,
            ]
        );

        $receipt_content = $appstore_receipt->content;

        $inflated_content = json_decode(gzinflate($receipt_content));

        $this->assertEquals(
            $payload,
            $inflated_content,
            'stored content is not identical to payload'
        );

        $this->assertEquals(
            true,
            $this->failed_subscription->is_recovered,
            'failed subscription was not recovered'
        );

        $this->assertEquals(
            $appstore_receipt->id,
            $this->failed_subscription->appstore_receipt_id,
            'appstore receipt was not assigned to the failed subscription'
        );

        $this->assertNotEquals(
            null,
            $new_subscription,
            'new subscription does not exist'
        );

        $this->assertEquals(
            $this->user->id,
            $new_subscription->user_id,
            'subscription was not assigned to the user'
        );

        $this->assertEquals(
            $this->offer->id,
            $new_subscription->offer_id,
            'user was not subscribed to the offer'
        );

        $this->assertEquals(
            $this->subscription_gateway_token,
            $new_subscription->gateway_token,
            'subscription was not assigned the correct gateway_token'
        );

        $this->assertNotEquals(
            null,
            $new_transaction,
            'new transaction does not exist'
        );

        $this->assertEquals(
            $this->user->id,
            $new_transaction->user_id,
            'transaction was not assigned to the user'
        );

        $this->assertEquals(
            $this->new_transaction_gateway_token,
            $new_transaction->gateway_token,
            'transaction was not assigned the correct gateway_token'
        );
    }

    /**
     * Test the appstore subscription initial buy server notification
     * with successful subscription and no failed subscription
     *
     * @return void
     */
    public function testAppstoreInitialBuyNoFailed()
    {
        $this->createTestModels();
        $this->createSuccessfulSubscription();

        $payload = json_decode('{
            "latest_receipt":"ewoJInNpZ25hdHVyZSIgPSAiQXlDOEQ2R3dNdTJVS0Jta29tVEl5VTFQK2xheFluZ0xITkVBd3ZaWUN1REhGRmZsZUNhQkxBaC9tdnNBRkRmcEh4WkIwM2paaURybFdlcmZhZGlFM21vQzA4VitaeEg0MGhUZEV4WXZvY1RpTWh3bkpLajcySEdUcXd5cEk3Um9oeUJ2TmZXUXJTYlZ5MnV5azU1RUNZV25WU25QRDVhQ1BSeERmMWEzUkN1Unp5MnNRUngvUlA2ZXhjdjI3eFMwNCs4T0JaS1UvdU5qVTJCMDVFT2RZOUZlVkhxSmVPOXROMG95S0lONm5yRzQzdExrbjBOVEp0T2xPZWNtSDZyU2xYU2pTcDhGazdQT2hhclJVZGloN3JocU9MTW1maWMwM2VvWFpocXdkOU9CYWZCQWJWcFk4VUlzb3hVTFF3MG9SZGJXbjFMd08veURMbmE2R2lWNlR5WUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ESXdMVEF4TFRJNElERTRPalE0T2pVMElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5GMVlXNTBhWFI1SWlBOUlDSXhJanNLQ1NKemRXSnpZM0pwY0hScGIyNHRaM0p2ZFhBdGFXUmxiblJwWm1sbGNpSWdQU0FpTWpBMU5qZ3dPREFpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlNemd5UXprM01UY3RSVEkzTnkwME5rVkNMVUpHTlRNdFFVRTFSRGRCUWpNNE1qUXdJanNLQ1NKdmNtbG5hVzVoYkMxd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhOVGd3TWpZMk1UTTBNREF3SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREl3TFRBeUxUSTVJREF5T2pRNE9qVXpJRVYwWXk5SFRWUWlPd29KSW1sekxXbHVMV2x1ZEhKdkxXOW1abVZ5TFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0YlhNaUlEMGdJakUxT0RBeU5qWXhNek13TURBaU93b0pJbVY0Y0dseVpYTXRaR0YwWlMxbWIzSnRZWFIwWldRdGNITjBJaUE5SUNJeU1ESXdMVEF5TFRJNElERTRPalE0T2pVeklFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW1sekxYUnlhV0ZzTFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkltbDBaVzB0YVdRaUlEMGdJakUwT0RjM05URXpNakFpT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0kyTkRBMU9XRTBOakppTVdJd1lXUmpOREE1WkdWak5qSXpOemxtWmpjNU4yTXhOV1ppT0RGaUlqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTXpJd01EQXdOak00TVRjME9EQTJJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVWlJRDBnSWpFMU9ESTVORFExTXpNd01EQWlPd29KSW1Gd2NDMXBkR1Z0TFdsa0lpQTlJQ0l4TkRneU1EZ3dOVEF4SWpzS0NTSjBjbUZ1YzJGamRHbHZiaTFwWkNJZ1BTQWlNekl3TURBd05qTTRNVGMwT0RBMklqc0tDU0ppZG5KeklpQTlJQ0l4SWpzS0NTSjNaV0l0YjNKa1pYSXRiR2x1WlMxcGRHVnRMV2xrSWlBOUlDSXpNakF3TURBeU1qY3dNRFk1TmpJaU93b0pJblpsY25OcGIyNHRaWGgwWlhKdVlXd3RhV1JsYm5ScFptbGxjaUlnUFNBaU9ETTBORFl6TXpRMUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dmFHMWpiMjVqWlhCMGFXOXVMblJ6WlNJN0Nna2ljSEp2WkhWamRDMXBaQ0lnUFNBaWRITmxNVzF2Ym5Sb0lqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURJd0xUQXhMVEk1SURBeU9qUTRPalV6SUVWMFl5OUhUVlFpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0Y0hOMElpQTlJQ0l5TURJd0xUQXhMVEk0SURFNE9qUTRPalV6SUVGdFpYSnBZMkV2VEc5elgwRnVaMlZzWlhNaU93b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVaUlEMGdJakl3TWpBdE1ERXRNamtnTURJNk5EZzZOVFFnUlhSakwwZE5WQ0k3Q24wPSI7CgkicG9kIiA9ICIzMiI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9",
            "auto_renew_status_change_date":"2020-02-08 00:15:17 Etc/GMT",
            "environment":"PROD",
            "auto_renew_status":"false",
            "auto_renew_status_change_date_pst":"2020-02-07 16:15:17 America/Los_Angeles",

            "latest_receipt_info":{
                "original_purchase_date_pst":"2020-01-28 18:48:54 America/Los_Angeles",
                "quantity":"1",
                "subscription_group_identifier":"20568080",
                "unique_vendor_identifier":"'.$this->vendor_token.'",
                "original_purchase_date_ms":"1580266134000",
                "expires_date_formatted":"2020-02-29 02:48:53 Etc/GMT",
                "is_in_intro_offer_period":"false",
                "purchase_date_ms":"1580266133000",
                "expires_date_formatted_pst":"2020-02-28 18:48:53 America/Los_Angeles",
                "is_trial_period":"false",
                "item_id":"1487751320",
                "unique_identifier":"64059a462b1b0adc409dec62379ff797c15fb81b",
                "original_transaction_id":"'.$this->subscription_gateway_token.'",
                "expires_date":"1582944533000",
                "app_item_id":"1482080501",
                "transaction_id":"'.$this->new_transaction_gateway_token.'",
                "bvrs":"1",
                "web_order_line_item_id":"320000227006962",
                "version_external_identifier":"834463345",
                "bid":"com.ohmconception.tse",
                "product_id":"'.$this->offer->appstore_id.'",
                "purchase_date":"2020-01-29 02:48:53 Etc/GMT",
                "purchase_date_pst":"2020-01-28 18:48:53 America/Los_Angeles",
                "original_purchase_date":"2020-01-29 02:48:54 Etc/GMT"
            },

            "auto_renew_status_change_date_ms":"1581120917000",
            "auto_renew_product_id":"tse1month",
            "notification_type":"INITIAL_BUY"
         }');

        $endpoint = '/'.config('webhook.appstore_legacy_endpoint');

        $response = $this->call(
            'POST',
            $endpoint,
            [],
            [],
            [],
            [],
            json_encode($payload)
        );

        $this->user = User::first();
        $appstore_receipt = AppstoreReceipt::first();
        $new_subscription = Subscription::first();
        $new_transaction = Transaction::first();
        $this->subscription = Subscription::first();

        $response->assertOk();

        $response->assertJson([
            'message' => 'success',
        ]);

        $this->assertDatabaseHas(
            'appstore_receipts',
            [
                'id' => 1,
                'vendor_token' => $this->vendor_token,
                'original_transaction_token' =>
                    $this->subscription_gateway_token,
                'transaction_token' => $this->new_transaction_gateway_token,
            ]
        );

        $receipt_content = $appstore_receipt->content;

        $inflated_content = json_decode(gzinflate($receipt_content));

        $this->assertEquals(
            $payload,
            $inflated_content,
            'stored content is not identical to payload'
        );

        $this->assertEquals(
            $this->transaction->id,
            $appstore_receipt->transaction_id,
            'appstore receipt was not assigned to the transaction'
        );
    }
}