var register_app= new Vue({
	el:'#register-app',
	data:{
		loading:false,
		errors:[],
		user:{
			email:'',
			password:'',
			password_confirmation:'',
			agreed_terms:false
		},
	},
	methods:{
		getNextRoute(){
			chosen_offer_id=window.findRouteParameter('next')
			if (chosen_offer_id)
				return '/payment_method/'+chosen_offer_id

			return '/selectoffers'
				
		},
		submitRegister(){
			if (this.loading)
				return
			this.loading=true

			grecaptcha.ready(()=> {
				grecaptcha.execute('6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon', {action: 'register'})
				.then((token)=>{
					
					return axios.post('/api/register',Object.assign({token},this.user))
					.then(()=>{
						window.routePush(this.getNextRoute())
					})
					.catch(error=>{
						this.loading=false
						let error_content=error.response.data.errors
						if (!error_content)
							return
						this.errors=(Object.values(error_content).flat())
					})
				})
				
			});

		}
	},
	mounted(){
		document.getElementById('register-app').className="registration-wrapper"
	},
	created(){
	}
})