<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_id')->nullable();
                $table->foreign('resource_id')->references('id')->on('resources');
            $table->unsignedBigInteger('category_id');
                $table->foreign('category_id')->references('id')->on('categories');
            $table->string('name')->index();
            $table->string('title')->index()->nullable();
            $table->string('description')->nullable();
            $table->integer('order_index')->default(0)->index();
            $table->string('image_background_web')->nullable();
            $table->string('image_content_web')->nullable();
            $table->string('image_background_mobile')->nullable();
            $table->string('image_content_mobile')->nullable();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
