<?php

/**
 * Stripe Handler for one-off payments, and recurring payments using the Stripe PaymentIntents API
 * 
 */

namespace App\Handlers;

use Illuminate\Http\UploadedFile;

use \Carbon\Carbon;

use Stripe\Exception\CardException;

use Stripe\{
	Stripe,
	WebhookEndpoint,
	PaymentIntent,
	SetupIntent,
	BalanceTransaction,
	Invoice,
	Charge,
	Product as StripeProduct,
	Plan as StripePlan,
	Subscription as StripeSubscription,
	Customer as StripeCustomer,
	Coupon as StripeCoupon
};

use App\{
	Plan,
	GatewayPlan,
	Offer,
	GatewayOffer,
	User,
	GatewayUser,
	Subscription,
	Transaction,
	Purchase,
	Coupon
};
use Illuminate\Support\Collection;
/**
 * Creates Stripe Customers, Products, Plans, PaymentIntents, and 
	* Subscriptions.
 * 
 */
class StripeHandler extends AbstractPaymentHandler
{
	/**
		* @var \App\Handlers\BailHandler
	 */
	private $bail_handler;

	public function __construct(array $options=[])
	{
		Stripe::setApiKey(
			config('api.stripe_secret_key')
		);
		$this->bail_handler = new BailHandler;
	}

	public function handleCardException(CardException $e)
	{
		$message=$e->getMessage();

			if (preg_match('/declined/',$message))
			{
				$this->bail_handler->bailCardDeclined();
			}
			elseif (preg_match('/processing/',$message))
			{
				$this->bail_handler->bailProcessingError();
			}
			else
			{
				$this->bail_handler->bailCardError();
			}
	}

	public function setupNewPaymentIntent(
		User $user,
		string $token,
		Offer $offer
	): PaymentIntent {
		$customer = $this->getUpdatedCustomerWithPaymentMethod($user,$token);
		$amount = bcmul($offer->price, 100, 0);

		$payload = [
			'customer' => $customer->id,
			'amount' => $amount,
			'currency' => 'eur',
			'confirm' => true
		];

		$payment_intent = PaymentIntent::create($payload);

		return $payment_intent;
	}

	public function setupNewSubscription(
		User $user,
		string $token,
		Offer $offer,
		Carbon $start_time = null
	): StripeSubscription {
		$customer = $this->getUpdatedCustomerWithPaymentMethod($user,$token);
		$plan_id = $offer->stripe_token;

		$payload = [
			'customer'=>$customer->id,
			'items'=>[
				[
					'plan'=>$plan_id
				]
			]
		];

		if ($start_time)
		{
			$payload['trial_end'] = $start_time->getTimestamp();
		}

		$stripe_subscription = StripeSubscription::create($payload);

		return $stripe_subscription;
	}

	/**
	 * Create a new GatewayOffer.
	 * 
	 * Creates a new GatewayOffer in the local database that can either be 
		* recurring or not. Either;
	 * a new Stripe\Plan is created and attached to the new GatewayOffer 
		* (recurring), or;
	 * the GatewayOffer is simply given a token of 'one_time_invoice'
		* (non-recurring).
	 */
	public function createGatewayOffer(Offer $offer): GatewayOffer
	{
		if ($offer->is_recurring)
		{
			$stripe_plan = $this->createStripePlan($offer);
			return $offer->createStripeGateway($stripe_plan->id);
		}
		else
		{
			return $offer->createStripeGateway('one_time_invoice');
		}
	}

	/**
	 * Create a new GatewayPlan
	 * 
	 * Creates a new GatewayPlan in the local database that is linked to
		* a Stripe\Product.
	 */
	public function createGatewayPlan(Plan $plan): GatewayPlan
	{
		$name = $plan->name;
		$type = 'service';

		$payload = compact('name','type');

		$stripe_product = StripeProduct::create($payload);

		return $plan->createGateway(1, $stripe_product->id);
	}

	public function createStripePlan(Offer $offer): StripePlan
	{
		$nickname = $offer->name;
		$metadata = [
			'tax_percent'=>$offer->tax_percent,
		];
		$amount = (int)($offer->price*100);
		$currency = 'eur';
		$interval = 'month';
		$interval_count = $offer->duration; 
		$product = $offer->plan->stripe_token;

		$payload = compact(
			'nickname',
			'metadata',
			'amount',
			'currency',
			'interval',
			'interval_count',
			'product'
		);

		$stripe_plan = StripePlan::create($payload);

		return $stripe_plan;
	}

	public function createCustomer($customer): GatewayUser
	{
		$address = [
			'line1'=>$customer->address,
			'line2'=>$customer->address2,
			'city'=>$customer->city,
			'country'=>$customer->country,
			'postal_code'=>$customer->postal_code,
		];
		$email = $customer->email;
		$name = $customer->full_name;
		$phone = $customer->phone;

		$payload = compact(
			'address',
			'email',
			'name',
			'phone'
		);

		$stripe_customer = StripeCustomer::create($payload);

		return GatewayUser::create([
			'user_id'=>$customer->id,
			'gateway_id'=>1,
			'gateway_token'=>$stripe_customer->id,
		]);
	}

	public function getUpdatedCustomerWithPaymentMethod(
		User $user,
		string $token
	): ?StripeCustomer {
		try
		{
			$stripe_customer = StripeCustomer::update(
				$user->stripe_token,
				['source'=>$token]
			);
		}
		catch(CardException $e)
		{
			$this->handleCardException($e);
		}

		return $stripe_customer;
	}

	public function getSubscriptionTransactions(
		string $subscription_id
	): Collection {
		$invoices = collect([]);
		$invoices_page = Invoice::all([
			'subscription' => $subscription_id,
			'status' => 'paid',
			'expand' => ['data.charge.balance_transaction'],
		]);
		
		do {
			$invoices->push(...($invoices_page->data));
			$invoices_page = $invoices_page->nextPage();
		} while ($invoices_page->has_more);

		return $invoices;
	}

	public function getStripeSubscription(
		string $stripe_subscription_id
	): StripeSubscription {
		$stripe_subscription = StripeSubscription::retrieve([
			'id' => $stripe_subscription_id,
			'expand' => ['latest_invoice.payment_intent']
		]);

		return $stripe_subscription;
	}

	public function updateNextPaymentDate(
		StripeSubscription $stripe_subscription
	): void {
		$subscription = Subscription::getFromToken($stripe_subscription->id);
		$subscription->update([
			'next_payment_date'=>Carbon::parse(
				$stripe_subscription->current_period_end
			)
		]);
	}

	public function createSubscription(
		User $user,
		string $token,
		Offer $offer,
		Carbon $start_time = null,
		Subscription $upgrade_from_subscription = null
	){
		$stripe_subscription = $this->setupNewSubscription(
			$user,
			$token,
			$offer,
			$start_time
		);

		$subscription = Subscription::createInactive(
			$user,
			$offer,
			$stripe_subscription->id,
			1
		);

		if ($upgrade_from_subscription)
		{
			$subscription->update([
				'is_upgrade'=>true,
				'upgrade_from_subscription_id'=>$upgrade_from_subscription->id
			]);
		}

		return $this->confirmSubscription($user, $subscription);
	}

	public function confirmSubscription(
		User $user,
		Subscription $subscription
	){
		if ($subscription->is_active)
		{
			return ['status'=>'succeeded'];
		}

		if ($subscription->is_upgrade)
		{
			$current_subscription = Subscription::find(
				$subscription->upgrade_from_subscription_id
			);
			return $this->confirmSubscriptionUpgrade(
				$user,
				$subscription,
				$current_subscription
			);
		}

		$stripe_subscription_id = $subscription->gateway_token;
		$stripe_subscription = $this->getStripeSubscription(
			$stripe_subscription_id
		);

		$stripe_invoice=$stripe_subscription->latest_invoice;

		$payment_intent=$stripe_invoice ?
			$stripe_invoice->payment_intent :
			null;

		if ($stripe_invoice && $payment_intent)
		{
			switch ($payment_intent->status)
			{
				case 'succeeded':
					$balance_transaction=BalanceTransaction::retrieve($payment_intent->charges->data[0]->balance_transaction);

					$subscription->activate();

					$transaction = Transaction::getFromToken($payment_intent->id);
					if ($transaction) {
						$transaction->updateSucceeded(
							$payment_intent->amount_received,
							$balance_transaction->fee,
							$balance_transaction->currency
						);
					}
					else
					{
						$transaction=Transaction::createSucceededOfferSubscription(
							$subscription,
							$payment_intent->id,
							$payment_intent->amount_received,
							$balance_transaction->fee,
							$balance_transaction->currency,
							$balance_transaction->created
						);
					}
					
					Purchase::createFromTransaction($transaction);

					$this->updateNextPaymentDate($stripe_subscription);

					return ['status'=>'succeeded'];
					break;

				case 'requires_action':
					$transaction=Transaction::createPendingOfferSubscription(
						$user,
						$subscription->offer,
						$subscription,
						$payment_intent->id
					);

					return [
						'status'=>'requires_action',
						'subscription_token'=>$subscription->gateway_token,
						'transaction_token'=>$payment_intent->client_secret,
					];
					break;
				
				case 'requires_payment_method':
					//payment failed. use a different payment method.
					$charge_attempted=$payment_intent->charges->data[0];

					$subscription->cancel();
					$transaction=Transaction::createFailedOffer(
						$user, 
						$subscription->offer,
						$payment_intent->id,
						1,
						$charge_attempted->amount,
						0,
						$payment_intent->currency,
						$charge_attempted->created
					);
					$stripe_subscription->delete();

					return [
						'status'=>'failed',
						'message'=>'Le paiement a échoué. Veuillez utiliser une autre carte.'
					];

				default:
				//unknown status?
					http_response_code(500);
					return $stripe_subscription;
					break;
			}

		}else{

			// trial, use setupIntents

		}
	}

	public function upgradeSubscription(
		User $user,
		string $token,
		Offer $offer,
		Purchase $current_purchase,
		Subscription $current_subscription
	){
		$start_time = Carbon::parse($current_purchase->expires_on);

		return $this->createSubscription(
			$user,
			$token,
			$offer,
			$start_time,
			$current_subscription
		);
	}

	public function confirmSubscriptionUpgrade(
		User $user,
		Subscription $subscription,
		Subscription $current_subscription
	){
		$stripe_subscription_id = $subscription->gateway_token;
		$stripe_subscription = $this->getStripeSubscription(
			$stripe_subscription_id
		);

		$setup_intent_id = $stripe_subscription->pending_setup_intent;

		if ($setup_intent_id)
		{
			StripeSubscription::update(
				$stripe_subscription_id,
				['metadata'=>['setup_intent_id'=>$setup_intent_id]]
			);
		}
		else if ($stripe_subscription->metadata['setup_intent_id'])
		{
			$setup_intent_id = $stripe_subscription->metadata['setup_intent_id'];
		}
		else
		{
			$setup_intent = (object)[
				'status'=>'succeeded'
			];
		}

		$setup_intent = isset($setup_intent) ?
			$setup_intent :
			SetupIntent::retrieve($setup_intent_id);

		if ($setup_intent)
		{
			switch ($setup_intent->status)
			{
				case 'succeeded':
					$subscription->activateInFuture(
						Carbon::parse($stripe_subscription->trial_end)
					);
			
					$this->updateNextPaymentDate($stripe_subscription);
			
					app('App\Http\Controllers\Web\PaymentsController')
					->cancelSubscription($current_subscription->id);
			
					return ['status'=>'succeeded'];
					break;

				case 'requires_action':
					return [
						'status'=>'requires_action',
						'subscription_token'=>$subscription->gateway_token,
						'transaction_token'=>$setup_intent->client_secret,
					];
					break;
				
				case 'requires_payment_method':
					$subscription->cancel();
					$stripe_subscription->delete();
					return [
						'status'=>'failed',
						'message'=>'Le paiement a échoué. Veuillez utiliser une autre carte.'
					];

				default:
				//unknown status?
					http_response_code(500);
					return $stripe_subscription;
					break;
			}
		}
	}

	public function cancelSubscription(
		Subscription $subscription
	){
		$subscription->cancel();

		$stripe_subscription = StripeSubscription::retrieve(
			$subscription->gateway_token
		);

		$stripe_subscription->delete();

		return ['status' => 'succeeded'];
	}

	public function createPurchase(
		User $user,
		string $token,
		Offer $offer,
		Carbon $start_time = null,
		Subscription $upgrade_from_subscription = null
	){
		$payment_intent = $this->setupNewPaymentIntent(
			$user,
			$token,
			$offer
		);

		$transaction = Transaction::createPendingPurchase(
			$user,
			$offer,
			$payment_intent->id,
			1
		);

		return $this->confirmPurchase(
			$user,
			$transaction
		);
	}

	public function confirmPurchase(
		User $user,
		Transaction $transaction
	){
		if ($transaction->succeeded)
		{
			return ['status'=>'succeeded'];
		}

		$payment_intent = PaymentIntent::retrieve($transaction->gateway_token);

		switch ($payment_intent->status)
		{
			case 'succeeded':
				$balance_transaction=BalanceTransaction::retrieve($payment_intent->charges->data[0]->balance_transaction);

				$transaction = Transaction::getFromToken($payment_intent->id);

				$transaction->updateSucceeded(
					$payment_intent->amount_received,
					$balance_transaction->fee,
					$balance_transaction->currency
				);
				
				Purchase::createFromTransaction($transaction);

				return ['status'=>'succeeded'];
				break;

			case 'requires_action':

				return [
					'status'=>'requires_action',
					'transaction_token'=>$payment_intent->client_secret,
				];
				break;
			
			case 'requires_payment_method':
				//payment failed. use a different payment method.
				$charge_attempted=$payment_intent->charges->data[0];

				$transaction->updateFailed($payment_intent->amount);

				return [
					'status'=>'failed',
					'message'=>'Le paiement a échoué. Veuillez utiliser une autre carte.'
				];

			default:
			//unknown status?
				http_response_code(500);
				return $payment_intent;
				break;
		}
	}

	public function fetchWebhooks()
	{
		
		return WebhookEndpoint::all();
		
	}

	public function getWebhook(string $webhook_id)
	{

		return WebhookEndpoint::retrieve($webhook_id);
	}

	public function createWebhook(
		string $endpoint,
		array $events=['*']
	){

		return WebhookEndpoint::create([
			'url'=>$endpoint,
			'enabled_events'=>$events
		]);

	}

	public function deactivateWebhook(string $webhook_id)
	{

		return WebhookEndpoint::retrieve(
			$webhook_id
		)->delete();

	}

	public function getAllPlans(): object
	{
		return StripePlan::all(['limit'=>100]);
	}

	public function handleInvoice($invoice)
	{
		if (config('webhook.test_mode') && property_exists($invoice,'test'))
		{
			error_log('test mode');
			$payment_intent = $invoice->payment_intent;
			$balance_transaction = $invoice->charge->balance_transaction;
		}
		else
		{
			$invoice = Invoice::retrieve([
				'id' => $invoice->id,
				'expand' => [
					'charge.balance_transaction'
				]
			]);
			
			$invoice->payment_intent = $payment_intent =
				PaymentIntent::retrieve($invoice->payment_intent);

			$balance_transaction = $invoice->charge->balance_transaction;
		};
			
		$transaction =
			Transaction::getFromToken($payment_intent->id) ?:
			Transaction::getFromToken($invoice->charge->id); // legacy compatibility

		$subscription = Subscription::getFromToken($invoice->subscription);

		error_log($invoice->billing_reason);

		// If create subscription, check if there is a pending subscription and
		// transaction and activate
		if ($invoice->billing_reason == 'subscription_create')
		{
			error_log('subscription_create');

			if (!$subscription)
			{
				error_log('no subscription, not pending');
				return;
			}

			if (!$transaction)
			{
				error_log('no transaction found');
				return;
			}

			// payment was successful
			if ($invoice->status == 'paid')
			{
				error_log('payment was successful');

				// If transaction already recorded as succeeded, ignore
				if ($transaction->payment_status=='succeeded')
				{
					error_log('transaction already succeeded');
					return;
				}
				
				error_log('updating_transaction');
				// update transaction
				$transaction->updateSucceeded(
					$payment_intent->amount_received,
					$balance_transaction->fee,
					$balance_transaction->currency
				);
				
				error_log('activating_subscription');
				// activate the subscription
				if ($transaction->has_subscription)
					$transaction->subscription->activate();

				error_log('creating_purchase');
				// create the purchase
				if ($transaction->has_offer)
					Purchase::createFromTransaction($transaction);
			}
			else
			{
				error_log('payment was not succesful');
			}
			
			return;
		}

		// Not create subscription, renew
		error_log('renew subscription');

		// If there's an existing transaction, handle updating the transaction
		if ($transaction)
		{
			error_log('transaction exists');

			if ($transaction->payment_status == 'succeeded')
			{
				error_log('already succeded');
				return;
			}

			// If the invoice was paid, set transaction to successful, create purchase
			if ($invoice->status == 'paid')
			{
				error_log('paid invoice and pending transaction');

				// update transaction
				$transaction->updateSucceeded(
					$payment_intent->amount_received,
					$balance_transaction->fee,
					$balance_transaction->currency
				);

				if ($transaction->has_subscription)
					$transaction->subscription->reactivate();

				if ($transaction->has_offer)
					Purchase::createFromTransaction($transaction);

				return;
				
			// If the invoice was not paid, nothing to do
			}
			elseif ($invoice->status == 'open')
			{

				error_log('failed invoice');
				return;
			}

		} // Else if there is no existing transaction, handle creating a new transaction
		else
		{
			error_log('no transaction yet, let\'s make one');

			// Find the subscription this relates to
			$subscription = Subscription::getFromToken($invoice->subscription);

			// If there is no subscription, abort
			if (!$subscription)
			{
				error_log('no subscription! aborting');
				return response('No subscription for this invoice.',400);
			}

			// Get the user subscribed
			$user = $subscription->user;

			// If payment successful, create transaction and purchase
			if ($invoice->status == 'paid')
			{
				error_log('payment successful, creating transaction and purchase');

				$transaction = Transaction::createSucceededOfferSubscription(
					$subscription,
					$payment_intent->id,
					$payment_intent->amount_received,
					$balance_transaction->fee,
					$balance_transaction->currency,
					$invoice->created
				);
				
				Purchase::createFromTransaction($transaction);

			// If payment not successful, create failed transaction
			}
			elseif ($invoice->status == 'open')
			{
				error_log('payment failed, creating pending transaction');

				$transaction = Transaction::createPendingOfferSubscription(
					$user,
					$subscription->offer,
					$subscription,
					$payment_intent->id
				);
			}

		}

	}

	public function verifySubscription(string $subscription_token): bool
	{
		
		try{
			 $subscription = $this->getStripeSubscription($subscription_token);
			 return true;
		}
		catch(\Exception $e)
		{
			$message = $e->getMessage();
			if (preg_match('/No such subscription/',$message))
				return false;
			else
				throw $e;
		}
	}

	public function createLocalSubscription(
		User $user,
		Offer $offer,
		Object $request): Subscription
	{
		return Subscription::create([
			'user_id'=>$user->id,
			'offer_id'=>$offer->id,
			'gateway_id'=>$request->gateway_id,
			'gateway_token'=>$request->token,
			'is_active'=>true,
			'is_paused'=>false,
			'price'=>$offer->price,
			'tax_percent'=>$offer->tax_percent,
			'subscribed_on'=>now(),
			'is_legacy' => $request->is_legacy
		]);
	}

	public function createLocalTransactions(Subscription $new_subscription): void
	{
		$transactions = $this->getSubscriptionTransactions($new_subscription->gateway_token);

		foreach ($transactions as $transaction) {
			if ($transaction->status == "paid" && $transaction->amount_due != 0) {
				$new_transaction = Transaction::createSucceededOfferSubscription(
					$new_subscription,
					$transaction->payment_intent,
					$transaction->amount_paid,
					$transaction->charge->balance_transaction->fee,
					$transaction->charge->balance_transaction->currency,
					$transaction->charge->balance_transaction->created
				);
			}else {
				$new_transaction = Transaction::createFailedOffer(
					User::find($new_subscription->user_id),
					Offer::find($new_subscription->offer_id),
					$transaction->payment_intent,
					$new_subscription->gateway_id,
					$transaction->amount_due,
					$transaction->charge->balance_transaction->fee,
					$transaction->charge->balance_transaction->currency,
					$transaction->charge->balance_transaction->created
				);
			}
		}
	}

	public function getTransaction(string $transaction_id): Collection
	{
		try{
			$transaction = PaymentIntent::retrieve(['id'=>$transaction_id, 'expand' =>  ['charges.data.balance_transaction']]);
			return collect($transaction);
		}
		catch(\Exception $e)
		{
			$message = $e->getMessage();
			return collect([]);
		}
	}
}