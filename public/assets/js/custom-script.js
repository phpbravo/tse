jQuery(function($) {

 $('.choose_payment input[type="radio"]').change(function(event) {
		var payment_method = $(this).val();
		if (payment_method == 'stripe')
		{
			$('.stripe').fadeIn('slow');
			$('.paypal').fadeOut('slow');
		}
		else if (payment_method == 'paypal')
		{
			$('.paypal').fadeIn('slow');
			$('.stripe').fadeOut('slow');
		}
	});

	$(window).on('scroll',function(){
		if($(this).scrollTop() > 50 ) {
			$('.header').addClass('header-fixed');
		}else{
			$('.header').removeClass('header-fixed');
		}
	})

	$('.about .nav.nav-tabs li').click(function(event) {
		var active_about_tabs = $(this).find('a').attr('href');
		$(this).addClass('active').siblings('li').removeClass('active')
	});

	$('.header-wrapper .navbar-toggler').click(function(event) {
		header_collapse();
	});
	
	owl();

	method_available();
	});

function owl(){
	var testimonials = $('.owl-testimonials');
	if (!testimonials.length) return
		testimonials.owlCarousel({
			margin: 40,
			nav: true,
			loop: true,
			dots: false,
			autoplay: true,
			touchDrag  : true,
			mouseDrag  : true,
			responsive: {
				0: {
					items: 1,
				}
			}
		});
}

function logout(){
	axios.post('/api/logout')
	.then(()=>{
		window.location.replace("/")
	})
}

function method_available(){
	var offers_id = $('.registration-wrapper').data('offer_id');
	axios.get('/api/offers/'+offers_id)
	.then(response=>{
		if (response.data.is_paypal_available == 1 && response.data.is_stripe_available == 1) {
			$('.stripe-option').css('display','block');
			$('.paypal-option').css('display','block');
			$('.stripe').css('display','flex');
			$('.paypal').css('display','none');
		} else if (response.data.is_paypal_available == 0 && response.data.is_stripe_available == 1) {
			$('.stripe-option').css('display','block');
			$('.paypal-option').css('display','none');
			$('.paypal-option input').removeAttr('checked');
			$('.stripe-option input').attr('checked','checked');
			$('.stripe').css('display','flex');
			$('.paypal').css('display','none');
		} else if (response.data.is_paypal_available == 1 && response.data.is_stripe_available == 0) {
			$('.paypal-option').css('display','block');
			$('.stripe-option').css('display','none');
			$('.stripe-option input').removeAttr('checked');
			$('.paypal-option input').attr('checked','checked');
			$('.paypal').css('display','flex');
			$('.stripe').css('display','none');
		}
	})
}

function sameHeight(element) {
	var max = -1;
	jQuery(element).each(function() {
	    var h = jQuery(this).height(); 
	    max = h > max ? h : max;
	    
	});
 	$(element).each(function() {
      	$(this).height(max+201) ;
  	});
}

// HEADER COLLAPSE
function header_collapse() {
	if ($('.header-wrapper .navbar-toggler').hasClass('collapsed')) {
		$('.header-wrapper .navbar-toggler').addClass('open');
	} else {
		$('.header-wrapper .navbar-toggler').removeClass('open');
	}
}

// back to top //

jQuery(document).ready(function(){
	
	jQuery(window).on('scroll',function(){
		if(jQuery(this).scrollTop() > 200){
			jQuery("#mybtntop").fadeIn();
			
		}else{
			jQuery("#mybtntop").fadeOut();
		}


	});

	jQuery("#mybtntop").click(function(){
		jQuery("html, body").animate({scrollTop	:jQuery("#top").offset().top}, 1000);
		return false;
	})
});
