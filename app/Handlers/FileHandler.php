<?php

namespace App\Handlers;

use Illuminate\Http\UploadedFile;

class FileHandler{

	private static function recursive_mkdir($dest, $permissions=0755, $create=true){

		if(!is_dir(dirname($dest))){

			self::recursive_mkdir(dirname($dest), $permissions, $create);
		}  
		elseif(!is_dir($dest)){

			mkdir($dest, $permissions, $create);
		}
		else{

			return true;
		}
	}

	public function store(UploadedFile $file, string $path, $rename=false){

		$path=trim($path,'/');

		self::recursive_mkdir(base_path('/storage/app/'.$path.'/'));

		$filename=$file->getClientOriginalName();

		$file_extension=(array_reverse(explode('.',$filename))[0]);

		$file->move(
			base_path('/storage/app/'.$path),
			($rename?
				'0_'.$rename.'.'.$file_extension:
				time().'_'.$filename
			)
		);

	}

	public function fetch(string $subpath){
		$path=base_path('storage/app/resources/'.$subpath);
		if (!file_exists($path) || is_dir($path))
			return response('Fichier non trouvé',404);

		return response()->file($path);
	}

	public static function getVersion($path){

		$mtime=filemtime(base_path($path));
		$version=base_convert($mtime,10,36);

		return $version;
	}
	  

}