<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\
{
	Offer,
	Recipe,
	Page,
	Product,
	Exercise
};

class PagesController extends Controller
{

	private function checkProduct(int $product_id)
	{
		return (
			auth()->check() &&
			auth()->user()->hasProductById($product_id)
		);
	}

	public function homepage(){
		return view('frontend.homepage');
	}

	public function newhomepage(){
		$offers=app('App\Http\Controllers\Web\OffersController')->getAvailableOffers()->toArray();
		return view('frontend/newhomepage',['offers'=>$offers]);
	}

	public function register(){
		return view('frontend.register');
	}

	public function selectoffers(){
		$offers=app('App\Http\Controllers\Web\OffersController')->getAvailableOffers()->toArray();
		return view('frontend.selectoffers',['offers'=>$offers]);
	}

	public function payment_method($id){
		return view('frontend.payment_method',['offers_id'=>$id,'offer_name'=>Offer::findOrFail($id)->title]);
	}

	public function ebook(){
		return view('frontend.ebook');
	}

	public function login(){
		return view('frontend.login');
	}

	public function terms_and_conditions(){
		return view('frontend.conditionsgenerales');
	}

	public function program(){
		if ($this->checkProduct(Product::PROGRAMS_PRODUCT))
			return view('frontend.program');
		else
			return view('frontend/we-train/training-du-jour-public');
	}

	public function temoignages(){
		return view('frontend/temoignages');
	}

	public function moncopte(){
		return view('frontend/access/mon-copte');
	}

	public function toutsavoir(){
		return view('frontend/concept/tout-savoir');
	}
	
	public function weTrainVideos(){
		if ($this->checkProduct(Product::EXERCISES_PRODUCT))
			return view('frontend/log-in/we-train/videos');
		else
			return view('frontend/we-train/videos-public');
	}

	public function weTrainBonus(){
		if ($this->checkProduct(Product::SESSIONS_PRODUCT))
			return view('frontend/log-in/we-train/seances-bonus');
		else
			return view('frontend/we-train/les-seances-bonus-public');
	}

	public function definition(){
		return view('frontend/we-train/definitions-public');
	}

	public function weTrainFaq(){
		return view('frontend/we-train/we-talk');
	}

	public function concept(){
		return view('frontend/we-train/le-concept-we-train');
	}

	public function seancesvideos(){
		if ($this->checkProduct(Product::EXERCISES_PRODUCT))
			return view('frontend/log-in/we-sweat/videos');
		else
			return view('frontend.we-sweat.tous-les-trainings-public');
	}

	public function wetalk(){
		return view('frontend.we-sweat.we-talk');
	}

	public function leconcept(){
		return view('frontend.we-sweat.le-concept');
	}

	public function recipes(){
		if ($this->checkProduct(Product::RECIPES_PRODUCT))
			return view('frontend/log-in/we-eat/mes-recettes-fit');
		else
			return view('frontend.recipes');
	}

	public function getRecipe($id){
		if ($this->checkProduct(Product::RECIPES_PRODUCT))
		{
			$recipe = Recipe::with('sections','recipe_category','tags')->find($id);
			return view('frontend/log-in/we-eat/item/view-recipe',['recipe'=>$recipe]);
		}
		else
			return view('frontend/we-eat/mes-recettes-fit-public');
	}

	public function sissy(){
		return view('frontend.nous.sissy');
	}

	public function tini(){
		return view('frontend.nous.tini');
	}

	public function recontre(){
		return view('frontend.nous.notre-rencontre');
	}

	public function getPageBySlug($slug){
		$page=Page::where('slug',$slug)->where('is_published',true)->where('is_public',true)->first();
		if (!$page)
			abort(404);
		else
			return $page;
	
	}

	public function getSingleVideo($id)
	{
		$exercise = Exercise::with('video')->find($id);	
		return view('frontend.log-in.we-train.video', ['exercise'=>$exercise]);
	}
}