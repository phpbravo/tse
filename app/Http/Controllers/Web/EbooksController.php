<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Recipe, Product};

class EbooksController extends Controller
{

	public function downloadEbook($id,$version){
		if (auth()->user()->hasProductById(Product::EBOOK_PRODUCT))
			return app('App\Http\Controllers\FilesController')->fetchGuarded("books/{$id}/{$version}.pdf");
		else
			abort(403);
	}

}
