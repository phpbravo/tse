<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;


/**
 * Class Taxes
 *
 * Tax details
 *
 * @package PayPal\Api2
 *
 * @property string percentage
 * @property bool inclusive
 */
class Taxes extends PayPalModel
{
    /**
     * Percentage.
     * 
     * The tax percentage on the billing amount.
     */
    public function setPercentage(string $percentage): self
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * Percentage.
     * 
     * The tax percentage on the billing amount.
     */
    public function getGivenName(): string
    {
        return $this->percentage;
    }

    /**
     * Inclusive.
     * 
     * Indicates whether the tax was already included in the billing 
     * amount.
     */
    public function setInclusive(bool $inclusive): self
    {
        $this->inclusive = $inclusive;
        return $this;
    }

    /**
     * Inclusive.
     * 
     * Indicates whether the tax was already included in the billing 
     * amount.
     */
    public function getInclusive(): bool
    {
        return $this->inclusive;
    }

}
