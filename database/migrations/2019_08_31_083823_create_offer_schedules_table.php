<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('offer_id');
                $table->foreign('offer_id')->references('id')->on('offers');
            $table->boolean('is_published')->default(false)->index();
            $table->date('from')->index();
            $table->date('to')->index();
            $table->boolean('is_custom')->default(false)->index();
            $table->string('title')->nullable()->index();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_schedules');
    }
}
