<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, StripeHandler, PaypalHandler};

class GatewaysController extends Controller
{

	private $stripe_handler;
	private $paypal_handler;

	public function __construct(){

		$this->stripe_handler=new StripeHandler;
		$this->paypal_handler=new PaypalHandler;
	}

	public function getEndpoints(){
		return [
			'stripe'=>'/'.config('webhook.stripe_endpoint'),
			'paypal'=>'/'.config('webhook.paypal_endpoint')
		];
	}

	public function getGatewayWebhooks(){

		$stripe=$this->stripe_handler->fetchWebhooks()->data;
		$paypal=$this->paypal_handler->fetchWebhooks();

		return compact('stripe','paypal');
	}

	public function createWebhook(){

		request()->validate([
			'gateway_id'=>'required|between:1,2',
			'url'=>'required|url'
		]);

		$gateway_id=request()->get('gateway_id');
		$url=request()->get('url');

		$handler=$gateway_id==1?
			new StripeHandler:
			new PaypalHandler;

		$response=$handler->createWebhook($url);

		return json_encode($response);

	}

	public function deleteWebhook(){

		request()->validate([
			'gateway_id'=>'required|between:1,2',
			'webhook_id'=>'required'
		]);

		$gateway_id=request()->get('gateway_id');
		$webhook_id=request()->get('webhook_id');

		$handler=$gateway_id==1?
			new StripeHandler:
			new PaypalHandler;

		$handler->deactivateWebhook($webhook_id);

		return (new ObjectHandler)->newSuccess();

	}

	public function getEvents(){
		return \App\WebhookEvent::orderBy('id','desc')->get();
	}

}