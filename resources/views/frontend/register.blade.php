@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')

	<section class="registration-wrapper d-none" id="register-app">
		<transition name="fade">
			<div v-if="loading" class="loading-overlay full-overlay"><div class="loader4"></div></div>
		</transition>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1">
					<div class="registration-inner-wrapper">
						<div class="warning-alert">
							<div v-for="error in errors" class="alert alert-warning alert-dismissible fade show position-relative" role="alert">
								<strong style="text-transform:Capitalize;">Erruer : </strong>
								@{{error}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
						<form autocomplete="on" id="reg-form" class="reg-form" method="POST" @submit.prevent="submitRegister">
							<h2>Inscription</h2>
							<div class="form-group">
									<label for="Email">E-mail</label>
									<input v-model="user.email" autocomplete="email" name="email" id="email" class="form-control">
									
							</div>
							<div class="form-group">
									<label for="Mot de passe">Mot de passe</label>
									<input v-model="user.password" autocomplete="new-password" name="password" id="password" type="password" class="form-control">
							</div>
							<div class="form-group">
									<label for="Mot de passe">Confirmer le mot passe</label>
									<input v-model="user.password_confirmation" autocomplete="new-password" name="password_confirmation" id="password_confirmation" type="password" class="form-control">
							</div>
							<div class="form-group form-row checkcheck">
								<div class="col-md-12">
									<label for="terms">
										<input v-model="user.agreed_terms" type="checkbox" name="agreed_terms" id="terms" required>
										<span class="checkmark"></span>
										J'accepte les <a href="{{ route('front.terms') }}">termes et conditions</a>.<span class="star">&nbsp;*</span>
									</label>
								</div>
							</div>
							<hr>
							<button type="submit" class="btn btn-primary float-right">
								<span>&nbsp;Prochaine étape</span>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

@endsection
@section('footer')
<script src="https://www.google.com/recaptcha/api.js?render=6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon"></script>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/register.js?v={{filever('public/assets/js/register.js')}}"></script>
@endsection