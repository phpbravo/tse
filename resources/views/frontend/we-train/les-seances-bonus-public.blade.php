@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
		<section class="same-wrapper bonussession" id="top">
			<div class="container">
				<div class="row pb-5">
					
						<div class="col-lg-12">
							<h1 class="pages-heading">LES SÉANCES BONUS</h1>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="inner-samecontent inner-bonussession">
								<h3 class="inner-contentheader">LES SEANCES BONUS</h3>
								
								<p>
									Tu ne peux pas effectuer le training du jour ? Pas de problème ! Nous te proposons ici des séances bonus en remplacement.
								</p>

								<p>
									<br>
								</p>

							@if(!auth('web')->check())
								<p class="text-center">
									<a href="{{ route('front.login') }}" class="btn btn-primary custombtn"><span>CONNECTE TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								<p class="text-center font-weight-bold">
									OU
								</p>
							@endif
								<p class="text-center">
									<a href="{{ !auth('web') ? route('front.login') : route('front.select-offer') }}" class="btn btn-primary custombtn"><span>INSCRIS TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								
							</div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="img-container">
								<img src="/assets/img/cc-seancesbonus.jpg" class="img-fluid bg-fitnessimg">
							</div>
						</div>
					
				</div>
			</div>
		
		</section>
@include('frontend.includes.parallax-last')	
	<a href="#" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection