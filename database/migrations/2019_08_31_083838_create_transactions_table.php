<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('gateway_id');
                $table->foreign('gateway_id')->references('id')->on('gateways');
            $table->string('gateway_token')->index();
            $table->string('payment_status')->index();
            $table->boolean('has_offer')->index();
            $table->unsignedBigInteger('offer_id')->nullable();
                $table->foreign('offer_id')->references('id')->on('offers');
            $table->boolean('has_subscription')->index();
            $table->unsignedBigInteger('subscription_id')->nullable();
                $table->foreign('subscription_id')->references('id')->on('subscriptions');
            $table->boolean('has_coupon')->default(false)->index();
            $table->unsignedBigInteger('coupon_id')->nullable();
                $table->foreign('coupon_id')->references('id')->on('coupons');
            $table->integer('gross_amount')->nullable()->index();
            $table->integer('discount')->nullable()->index();
            $table->integer('gateway_fee')->nullable()->index();
            $table->integer('tax_amount')->nullable()->index();
            $table->decimal('tax_percent')->nullable()->index();
            $table->integer('net_amount')->nullable()->index();
            $table->char('currency',3)->nullable()->index();
            $table->longText('payment_details')->nullable();
            $table->dateTime('transaction_date')->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
