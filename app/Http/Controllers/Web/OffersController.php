<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Offer, OfferSchedule};

class OffersController extends Controller
{

	public function getAvailableOffers(){

		$available_offers=Offer::with('plan.products')
			->available()
			->public();

		$user=auth('web')->user();

		if (!$user || !$user->active_plans->count())
		{
			$available_offers=$available_offers->get();
		}	
		else
		{
			$active_plan_ids=$user->active_plans->pluck('id');
			$active_offer_ids=$user->active_purchases()->pluck('offer_id');
			
			$available_offers=$available_offers
				->where(function($query) use ($active_plan_ids, $active_offer_ids){
					$query
						->whereDoesntHave('plan',function($query) use ($active_plan_ids){
							$query->whereIn('id',$active_plan_ids);
						})
						->orWhere(function($query) use ($active_offer_ids){
							$query
								->where('is_for_upgrade',true)
								->whereNotIn('id',$active_offer_ids);
						});
				})
				->get();
		}

		return $available_offers;

	}

	public function getUpgradeOffers(){

		$user=auth('web')->user();

		if (!$user)
			abort(403);
		
		$active_plan_ids=$user->active_plans->pluck('id');
		$active_purchases=$user->active_purchases;
		$active_offer_ids=$active_purchases->pluck('offer_id');
		$active_offers=Offer::findMany($active_offer_ids);
		$active_subscription_offer_ids=
			$user
			->active_subscriptions()
			->pluck('offer_id');


		$upgrade_offers=Offer::with('plan')
			->available()
			->forUpgrade()
			->whereNotIn('id',
			$active_offer_ids
				->concat($active_subscription_offer_ids)
			)
			->whereIn('plan_id',$active_plan_ids)
			->get();

		$upgrade_offers=$upgrade_offers->filter(function($offer) use ($active_offers){
			$current_offer = $active_offers->where('plan_id',$offer->plan_id)->first();
			return (int)$offer->duration>(int)$current_offer->duration;
		});
 
		$upgrade_offers->map(function ($offer) use ($active_offers, $active_purchases){
			$current_offer = $active_offers->where('plan_id',$offer->plan_id)->first();
			$current_purchase = $active_purchases->where('offer_id',$current_offer->id)->first();
			$offer->discount=$current_purchase->refundable_amount;
			return $offer;
		});

		return $upgrade_offers;

	}

	public function getOffer($id){

		return Offer::find($id);

	}

	public function getOfferPaypalToken($id){

		$offer=Offer::find($id);

		if (!($offer->is_paypal_available)) return null;

		return $offer->paypal_token;
	}

}
