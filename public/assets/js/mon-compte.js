var mon_compte_app= new Vue({
	el:'#mon-compte-app',
	data:{
		loading:false,
		errors:[],
		user:{
			id:'',
			email:'',
			username:'',
			password:'',
			password_confirmation:'',
			name:'',
			surname:'',
			organization:'',
			address:'',
			address2:'',
			city:'',
			postal_code:'',
			country:'',
			phone:'',
			homepage_url:'',
			accepted_terms:false,
		},
		upgrade_offers:[],
		selected_offer:null,
	},
	methods:{
		getPrice(offer){
			numberFormatter=new Intl.NumberFormat('fr',{minimumFractionDigits:2})

			return numberFormatter.format(offer.price)+'€ / '+parseInt(offer.duration)+' mois'
		},
		getSubscriptionStatus(subscription){
			if (subscription.is_active){
				return 'Actif'
			} else if (subscription.is_paused){
				return 'En pause'
			} else if (subscription.cancelled_on){
				return 'Annulé'
			} else {
				return 'En attente'
			}
		},
		getPurchaseStatus(purchase){
			if (!purchase.is_active){
				if (purchase.transaction=="pending"){
					//purchase is inactive and never been active
					return 'En attente'
				}
				else {
					return 'Inactif'
				}
			}else{
				if (!purchase.subscription || purchase.subscription.is_active){
					return 'Actif'
				}
				else{
					return 'Actif (Abonnement annulé)'
				}
			}
		},
		postProfile(){
			if (this.loading)
				return Promise.reject('Debounce')
			this.loading=true;

			return axios.post('/api/updateProfile',this.user)
		.then(response=>{
			this.user=response.data
		})
		.catch(error=>{
			error_messages=Object.values(error.response.data.errors).map(error_category=>{
				return error_category.join("\n")
			})
			.join("\n")
			window.alert(error_messages)
		})
		.finally(()=>{
			console.log('wow')
			this.loading=false
		})

		},
		postUpgrade(){
			if (!this.selected_offer)
				window.alert('Veuillez sélectionner un abonnement pour passer à')
			else
				window.routePush('/payment_method/'+this.selected_offer)
		},
		getSelf(){

			return axios.get('/api/self')
			.then(response=>{
				this.user=response.data
			})

		},
		getUpgradeOffers(){

			return axios.get('/api/offers/upgrade')
			.then(response=>{
				this.upgrade_offers=response.data
			})

		},
		loadData(){
			this.loading=true
			Promise.allSettled([
				this.getSelf(),
				this.getUpgradeOffers()
			])
			.finally(()=>{
				this.loading=false
			})
		}
	},
	mounted(){
		document.getElementById('mon-compte-app').className="login-wrapper"
	},
	created(){
		this.loadData()
	}
})