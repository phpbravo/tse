<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseSet extends Model
{
    protected $fillable=[
        'session_id',
        'name',
        'order_index',
        'description',
    ];

    public function session(){
        return $this->belongsTo('App\Session');
    }

    public function exercise_assignments(){
        return $this->hasMany('App\ExerciseAssignment');
    }

    public function exercises(){
        return $this->hasManyThrough('App\Exercise','App\ExerciseAssignment');
    }
}
