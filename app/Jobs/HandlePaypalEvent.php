<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use PayPal\Api\WebhookEvent;

use App\Handlers\PaypalHandler;

use App\{
	Subscription,
	Purchase,
	Transaction
};

class HandlePaypalEvent implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $webhook_event;

	public $tries = 3;
	public $retryAfter = 3;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($webhook_event)
	{
		$this->webhook_event=$webhook_event;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(PaypalHandler $handler)
	{
		$webhook_event = WebhookEvent::get($this->webhook_event->data['id']);
		switch ($webhook_event->event_type)
		{
			case 'PAYMENT.SALE.COMPLETED':
				$handler->handleRenewSubscriptionWebhookEvent($webhook_event);
				break;

			case 'BILLING.SUBSCRIPTION.CANCELLED':
				$handler->handleCancelSubscriptionWebhookEvent($webhook_event);
				break;

			default:
				$this->fail(new \Exception('Unsupported event type'));
				break;
		}
	}
 
}
