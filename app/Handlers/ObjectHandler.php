<?php

namespace App\Handlers;

use Illuminate\Support\Str;

class ObjectHandler{

    const ERROR_TYPES=[
        'general_error'=>400,
        'unauthorized'=>401,
        'not_found'=>404,
        'invalid_request'=>422,
        'connection_error'=>504,
    ];

    protected $object_structures=[
        'page_request'=>[

        ]
    ];

    private $modelClass;
    private $modelName;
    private $tableName;

    public function __construct($modelClass=null){

        if ($modelClass){

            $this->modelClass=$modelClass;
            $this->modelName=(new \ReflectionClass($modelClass))->getShortName();
            $this->tableName=with(new $modelClass)->getTable();
            
        }
    }


    public function verifyObject($object_type,...$arguments){

    }

    public function newPage($contents,$current_page,$last_page,$total_item_count){

        return json_encode((object)[
            'object'=>'page',
            'content_type'=>Str::snake($this->modelName),
            'contents'=>$contents,
            'current_page'=>$current_page,
            'last_page'=>$last_page,
            'total_item_count'=>$total_item_count,
        ]);
    }

    public function newError($error_type='general_error',$message=null){
        
        return json_encode((object)[
            'object'=>'error',
            'status_code'=>self::ERROR_TYPES[$error_type],
            'error_type'=>$error_type,
            'error_content'=>[
                'error'=>__('objects.error.'.$error_type.'.error'),
                'message'=>$message?:__('objects.error.'.$error_type.'.message'),
            ]
        ]);
    }

    public function newSuccess($message="Success"){

        return json_encode((object)[
            'object'=>'success',
            'status_code'=>200,
            'message'=>$message,
        ]);
    }

}