<?php

namespace App\Http\Controllers\API\v0;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};
use App\Offer;
use App\Purchase;
use App\User;
use Carbon\Carbon;

class SelfController extends Controller
{
	public function getSelf()
	{
		/** @var \App\User */
		$user = auth()->user();

		$purchases =
			$user
			->purchases()
			->with(['offer', 'transaction'])
			->get();

		$access = $user->hasPlanById(1);

		$history = array_map(function($purchase)
			{
				return [
					'id' => $purchase['id'],
					'title' => $purchase['offer']['title'],
					'payment_amount' => bcdiv(
							$purchase['transaction']['net_amount'] ?? 0,
							100,
							2
						),
					'lifetime_membership' => $purchase['is_lifetime'],
					'subscription_status' =>
						(new Carbon($purchase['expires_on']))
						->lt(now()) ?
							2 :
							(
								$purchase['is_paused'] ?
									3 :
									(
										$purchase['is_active'] ?
											1 :
											0
									)
							),
					'validity' => [
						'from' => $purchase['created_at'],
						'to' => $purchase['expires_on'] ?:
							now()
							->addMonthsNoOverflow(99)
							->format('Y-m-d H:m:s'),
					],
				];
			},
			$purchases->toArray()
		);
		
		$last_transaction = count($history) > 0 ? 
			[
				'id' => (string) $history[0]['id'],
				'title' => $history[0]['title'],
				'transactionId' => null,
				'payment_amount' => $history[0]['payment_amount'],
				'lifetime_membership' => $history[0]['lifetime_membership'],
				'subscription_status' => $history[0]['subscription_status'],
				'validity' => $history[0]['validity']
			] :
			null;

		$active_plan = array_map(function($purchase)
			{
				return [
					'id' => strval($purchase['id']),
					'title' => $purchase['offer']['title'],
					'payment_amount' => bcdiv(
							$purchase['transaction']['net_amount'] ?? 0,
							100,
							2
						),
					'lifetime_membership' => $purchase['is_lifetime'],
					'subscription_status' =>
						(new Carbon($purchase['expires_on']))->lt(now()) ?
							2 :
							(
								$purchase['is_paused'] ?
									3 :
									(
										$purchase['is_active'] ?
											1 :
											0
									)
							),
						'validity'=>[
							'from'=>$purchase['created_at'],
							'to'=>$purchase['expires_on'],
						],
					];
			},
			$purchases->where('is_active',true)->toArray()
		);

		return [
			'success' => true,
			'data' => [
				'user' => [
					'id' => $user->id,
					'email' => $user->email,
					'name' => $user->full_name,
				],
				'profile' => [
					'membership_id' => null,
					'profile_id' => null,
					'first_name' => $user->name,
					'last_name' => $user->surname,
					'email' => $user->email,
					'address' => $user->address,
					'city' => $user->city,
					'zip' => $user->postal_code,
					'country' => $user->country,
					'created_date' => $user->created_at,
				],
				'lastTransaction' => $last_transaction,
				'history' => $history,
				'active_plan' => $active_plan,
				'access' => $access,
			]
		];
	}

	public function register()
    {
		$email = request()->get('email');
		if ($email) {
			$user = User::where('email', $email)->first();
			if (!$user) {
				$new_user = User::create([
					'username' => request()->get('email'),
					'email' => request()->get('email'),
					'password' => bcrypt(request()->get('password')), 
					'name' => request()->get('firstname'),
					'surname' => request()->get('lastname'),
					'agreed_terms' => false,
					'agreed_newsletter' => false,
				], 200);

				$offer = Offer::find(24);
				Purchase::createFromOffer($offer, $new_user);

				return response()->json([
					'success' => true,
					'data' => 'Success'
				]);
			}else {
				return response()->json([
					'success' => false
				], 403);
			}
		}else {
			return response()->json([
				'data' => 'Email is required'
			], 422);
		}
    }
}