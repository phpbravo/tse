<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GatewayPlan extends Pivot
{
    protected $fillable=[
        'plan_id',
        'gateway_id',
        'gateway_token',
    ];
}
