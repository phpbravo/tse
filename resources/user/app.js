require('./bootstrap');

window.routePush = (url)=>{
	let a;
	a = document.createElement('a');
	a.href = url;
	document.body.appendChild(a);
	a.style = 'display: none';
	a.click();
	a.remove();
}

window.downloadUrl = (url,filename)=>{
	let a;
	a = document.createElement('a');
	a.href = url;
	a.download = filename;
	document.body.appendChild(a);
	a.style = 'display: none';
	a.click();
	a.remove();
}

window.findRouteParameter=(parameterName)=>{
	let result = null,
		tmp = [];
	location.search
		.substr(1)
		.split("&")
		.forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		});
	return result;
}

window.findUrlComponent = (index)=>{
	let current_url=window.location.pathname.substr(1)
	let url_components=current_url.split('/')

	// negative index traverses array backwards
	if (index<0)
		index=url_components+index

	// index out of range check
	if (index >= url_components.length || index<0)
		return null
		
	return url_components[index]
}