<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePageGroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_page_group', function (Blueprint $table) {
            $table->unsignedBigInteger('page_group_id');
                $table->foreign('page_group_id')->references('id')->on('page_groups')->onDelete('cascade');
            $table->unsignedBigInteger('page_id');
                $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_page_group');
    }
}
