@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')

	<div class="d-flex" style="position:absolute; top:100px; width:100%; justify-content:center;" id="loading-icon">
		<div class="loader4"></div>
	</div>
	<section class="program-wrapper d-none" id="program-app" >
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="text-center programs-intro">
						<h1>@{{program.title}}</h1>
						<h3
							v-if="program.active_schedule.from"
						>@{{'('+program_date_range+')'}}</h3>
						<div>
							<pre
								style="display:block; word-wrap: break-word; font-family:'Raleway', sans-serif; font-size: 14px; line-height:1; font-weight: 400; color: #666; text-align:center; overflow:hidden;"
							>@{{program.content}}
							</pre>
						</div>
					</div>
				</div>

				<div
					v-for="session in program.sessions"
					:key="session.id"
					class="col-lg-6 col-md-12"
				>
					<div class="grid-card">
						<div class="card programs">
							<div class="card-body">
								<h3 class="text-wrap">@{{session.title}}</h3>
								<div
									v-for="exercise_set in session.exercise_sets"
									:key="exercise_set.id"
									class="random-set"
								>
									<p> 
										<template v-if="exercise_set.name!=''">
											<span class="text-uppercase">@{{exercise_set.name}}</span><br>
										</template>
										<span
											v-for="(exercise_assignment,index) in exercise_set.exercise_assignments"
											:key="exercise_assignment.id"
										>
											<br v-if="index!=0">
											<a href="#">@{{exercise_assignment.exercise.name}} :</a>
											@{{exercise_assignment.reps}}
											<template v-if="exercise_assignment.tempo">
												&nbsp;(@{{exercise_assignment.tempo}})
											</template>
										</span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	<section class="planning-section mt-5">
		<div class="container">
			<div class="row pb-5">
				<div class="col-lg-6 col-md-12">
				</div>

				<div class="col-lg-6 col-md-12 ">
					<div class="planright-section">
						<p>
							<strong>
								N’oublie pas que les vidéos illustrant nos exercices sont disponibles dans la rubrique <a href="#">« vidéos ».</a>
							</strong>
						</p>
						<div class="hidemeimg">
							<img src="/assets/img/dailytraining.jpg" class="img-fluid ">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	

	<a href="#asd" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/program.js?v={{filever('public/assets/js/program.js')}}"></script>

@endsection