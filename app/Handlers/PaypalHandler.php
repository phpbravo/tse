<?php

namespace App\Handlers;

use Illuminate\Http\UploadedFile;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;

use PayPal\Api\Webhook;
use PayPal\Api\WebhookEvent;
use PayPal\Api\WebhookEventType;

use PayPal\Api2\ApplicationContext;
use PayPal\Api2\Product as PaypalProduct;
use PayPal\Api2\Plan as PaypalPlan;
use PayPal\Api2\Order as PaypalOrder;
use PayPal\Api2\Transaction as PaypalTransaction;
use PayPal\Api2\BillingCycle;
use PayPal\Api2\Frequency;
use PayPal\Api2\TransactionsList;
use PayPal\Api2\PricingScheme;
use PayPal\Api2\PaymentPreferences;
use PayPal\Api2\Taxes;
use PayPal\Api2\Money;
use PayPal\Api2\Subscription as PaypalSubscription;

use Carbon\Carbon;

use App\{
	User,
	Plan,
	Transaction,
	Purchase,
	GatewayPlan,
	Offer,
	GatewayOffer,
	Subscription
};
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Void_;

class PaypalHandler extends AbstractPaymentHandler
{
	/**
		* @var \PayPal\Rest\ApiContext $api_context
	 */
	public $api_context;

	/**
		* @var \App\Handlers\BailHandler
	 */
	private $bail_handler;

	public function __construct()
	{
		$this->api_context = new ApiContext(
			new OAuthTokenCredential(
				config('api.paypal_id'),
				config('api.paypal_secret_key')
			)
		);
		$this->api_context->setConfig(config('paypal.settings'));
		$this->bail_handler = new BailHandler;
	}

	public function setupFrequency(int $interval_count): Frequency
	{
		$frequency = new Frequency;

		$frequency
			->setIntervalUnit(Frequency::MONTH)
			->setIntervalCount($interval_count);

		return $frequency;
	}

	public function setupPricingScheme(
		string $value,
		string $currency_code
	): PricingScheme {
		$pricing_scheme = new PricingScheme;

		$pricing_scheme
		->setVersion(1)
			->setFixedPrice(new Money(
				$value,
				$currency_code
			));

		return $pricing_scheme;
	}

	/**
		* @return \PayPal\Api2\BillingCycle[] $billing_cycles
	 */
	public function setupBillingCycles(Offer $offer): array
	{
		$billing_cycles = [];
		$current_cycle_sequence = 1;
		// Need to implement trial 

		$main_cycle = new BillingCycle;
		$main_frequency = $this->setupFrequency($offer->duration);
		$main_pricing_scheme = $this->setupPricingScheme(
			$offer->price,
			'EUR'
		);


		$main_cycle
			->setFrequency($main_frequency)
			->setTenureType(BillingCycle::REGULAR)
			->setSequence($current_cycle_sequence)
			->setTotalCycles(0)
			->setPricingScheme($main_pricing_scheme);

		$billing_cycles[] = $main_cycle;

		return $billing_cycles;
	}

	public function setupPaymentPreferences(): PaymentPreferences
	{
		$payment_preferences = new PaymentPreferences;

		return $payment_preferences;
	}

	public function setupTaxes(
		string $percentage,
		bool $inclusive = true
	): Taxes {
		$taxes = new Taxes;

		$taxes
			->setPercentage($percentage)
			->setInclusive($inclusive);

		return $taxes;
	}

	public function setupNewOrder(
		string $value,
		Offer $offer
	): PaypalOrder {
		$order = new PaypalOrder();
		$application_context = $this->setupNewApplicationContext();
		$amount = (object)[
			'currency_code' => 'EUR',
			'value' => (string)$value
		];
		
		$order->setIntent('CAPTURE');
		$order->purchase_units = [
			(object)[
				'amount'=>$amount,
				'custom_id'=>$offer->id
			]
		];
		$order->application_context = $application_context;

		return $order;
	}

	public function setupNewSubscription(
		string $start_time,
		string $plan_id,
		ApplicationContext $application_context
	): PaypalSubscription {
		$subscription = new PaypalSubscription();
		$subscription->setStartTime($start_time)
		->setPlanId($plan_id)
		->setApplicationContext($application_context);

		return $subscription;
	}

	public function setupNewApplicationContext(
		string $return_url = null,
		string $cancel_url = null
	): ApplicationContext {
		$application_context = new ApplicationContext();

		$application_context->setBrandName(config('app.name'));
		$application_context->setLocale(config('app.locale'));
		$application_context->setShippingPreference('NO_SHIPPING');
		$application_context->setReturnURL($return_url ?: url()->previous());
		$application_context->setCancelURL($cancel_url ?: url()->previous());

		return $application_context;
	}

	public function createGatewayOffer(Offer $offer): GatewayOffer
	{
		if ($offer->is_recurring)
		{
			$paypal_plan = $this->createPaypalPlan($offer);
			return $offer->createPaypalGateway($paypal_plan->id);
		}
		else
		{
			return $offer->createPaypalGateway('one_time_invoice');
		}
	}

	public function createGatewayPlan(Plan $plan): GatewayPlan
	{
		$paypal_product = new PaypalProduct;

		$name=$plan->name;
		$type="DIGITAL";  
		$category="HEALTH_AND_NUTRITION";

		$paypal_product
			->setName($name)
			->setType($type)
			->setCategory($category);

		$paypal_product->create($this->api_context);

		return $plan->createGateway(2, $paypal_product->id);
	}
 
	public function getPaypalSubscription(
		string $paypal_subscription_id
	): PaypalSubscription {
		$paypal_subscription = PaypalSubscription::get(
			$paypal_subscription_id,
			$this->api_context
		);

		return $paypal_subscription;
	}

	public function getPaypalPlan(string $paypal_plan_id): PaypalPlan
	{
		$plan = PaypalPlan::get($paypal_plan_id, $this->api_context);

		return $plan;
	}

	public function getPaypalSubscriptionTransactions(
		PaypalSubscription $paypal_subscription
	): TransactionsList {
		$transactions = $paypal_subscription->transactions(
			[
				'start_time'=>'2000-01-01T00:00:00.001Z',
				'end_time'=>now()->format('Y-m-d\TH:i:s\Z')
			],
			$this->api_context
		);

		return $transactions;
	}

	public function updateNextPaymentDate(
		PaypalSubscription $paypal_subscription
	): void {
		$subscription = Subscription::getFromToken($paypal_subscription->id);
		$subscription->update([
			'next_payment_date'=>Carbon::parse(
				$paypal_subscription->billing_info['next_billing_time']
			)
		]);
	}

	public function createPaypalPlan(Offer $offer): PaypalPlan
	{
		$paypal_plan = new PaypalPlan;

		$name = $offer->title;
		$product_id = $offer->plan->paypal_token;
		$billing_cycles = $this->setupBillingCycles($offer);
		$payment_preferences = $this->setupPaymentPreferences();
		$taxes = $this->setupTaxes($offer->tax_percent);

		$paypal_plan
			->setName($name)
			->setProductId($product_id)
			->setBillingCycles($billing_cycles)
			->setPaymentPreferences($payment_preferences)
			->setTaxes($taxes);
		
		$paypal_plan->create($this->api_context);

		return $paypal_plan;
	}

	public function createSubscription(
		User $user,
		Offer $offer,
		Carbon $start_time = null,
		Subscription $upgrade_from = null
	){
		$plan_id = $offer->paypal_token;

		$start_time = $start_time ?
			$start_time->format('Y-m-d\TH:i:s\Z') :
			now()->addSeconds(10)->format('Y-m-d\TH:i:s\Z');

		$application_context = $this->setupNewApplicationContext();

		$paypal_subscription = $this->setupNewSubscription(
			$start_time,
			$plan_id,
			$application_context);

		$paypal_subscription=$paypal_subscription->create($this->api_context);
		$approval_url=$paypal_subscription->getApprovalLink();

		$subscription = Subscription::createInactive(
			$user,
			$offer,
			$paypal_subscription->id,
			2
		);

		if ($upgrade_from)
		{
			$subscription->update([
				'is_upgrade'=>true,
				'upgrade_from_subscription_id'=>$upgrade_from->id
			]);
		}

	 return response()->json([
			'status'=>'succeeded',
			'redirect'=>$approval_url
		],200);
	}

	public function confirmSubscription(
		User $user,
		Subscription $subscription
	){
		if ($subscription->is_active)
		{
			return [
				'status'=>'succeeded'
			];
		}

		if ($subscription->is_upgrade)
		{
			$current_subscription = Subscription::find(
				$subscription->upgrade_from_subscription_id
			);
			return $this->confirmSubscriptionUpgrade(
				$user,
				$subscription,
				$current_subscription
			);
		}
		
		$paypal_subscription_id = $subscription->gateway_token;
		$paypal_subscription = $this->getPaypalSubscription(
			$paypal_subscription_id
		);


		$paypal_transactions = $this->getPaypalSubscriptionTransactions(
			$paypal_subscription
		);

		$first_paypal_transaction = $paypal_transactions->transactions[0];

		if (!$first_paypal_transaction)
		{
			$this->bail_handler->bail([
				'status'=>'transaction_not_found',
				'message'=>'paypal subscription first transaction not found'
			], 404);
		}

		if ($first_paypal_transaction->status == PaypalTransaction::COMPLETED)
		{
			$subscription->activate();

			$payment_amounts = $first_paypal_transaction
				->amount_with_breakdown;

			$transaction = Transaction::createSucceededOfferSubscription(
				$subscription,
				$first_paypal_transaction->id,
				bcmul($payment_amounts->gross_amount->value,100,2),
				bcmul($payment_amounts->fee_amount->value,100,2),
				$payment_amounts->gross_amount->currency_code,
				$payment_amounts->time
			);

			Purchase::createFromTransaction($transaction);

			$paypal_subscription = $this->getPaypalSubscription(
				$paypal_subscription->id
			);

			$this->updateNextPaymentDate($paypal_subscription);

			return ['status'=>'succeeded'];
		}
	}

	public function upgradeSubscription(
		User $user,
		Offer $offer,
		Purchase $current_purchase,
		Subscription $current_subscription
	){
		$start_time = Carbon::parse($current_purchase->expires_on);

		return $this->createSubscription(
			$user,
			$offer,
			$start_time,
			$current_subscription
		);
	}

	public function confirmSubscriptionUpgrade(
		User $user,
		Subscription $subscription,
		Subscription $current_subscription
	){
		$paypal_subscription = PaypalSubscription::get(
			$subscription->gateway_token,
			$this->api_context
		);

		if ($paypal_subscription->status == PaypalSubscription::ACTIVE)
		{
			$subscription->activateInFuture(
				Carbon::parse($paypal_subscription->start_time)
			);

			$this->updateNextPaymentDate($paypal_subscription);

			app('App\Http\Controllers\Web\PaymentsController')
			->cancelSubscription($current_subscription->id);

			return [
				'status'=>'succeeded'
			];
		}
		else
		{
			http_response_code(500);
		}
	}

	public function cancelSubscription(
		Subscription $subscription
	){
		$subscription->cancel();

		$paypal_subscription = $this->getPaypalSubscription(
			$subscription->gateway_token
		);

		$paypal_subscription->cancel('cancel', $this->api_context);

		return ['status' => 'succeeded'];
	}

	public function createPurchase(
		User $user,
		Offer $offer,
		Carbon $start_time = null,
		Subscription $upgrade_from_subscription = null
	){
		$order = $this->setupNewOrder($offer->price,$offer);

		$order->create($this->api_context);
		$approval_url = $order->getApprovalLink();

	 return response()->json([
			'status'=>'succeeded',
			'redirect'=>$approval_url
		],200);
	}

	public function confirmPurchase(
		User $user,
		string $paypal_order_id
	){
		$paypal_order = PaypalOrder::get(
			$paypal_order_id,
			$this->api_context
		);

		$captured_paypal_order = $paypal_order->capture($this->api_context);

		$paypal_order = $captured_paypal_order;

		$paypal_transaction = 
			$paypal_order->purchase_units[0]['payments']['captures'][0];

		$existing_transaction = Transaction::getFromToken(
			$paypal_transaction['id']
		);

		if ($existing_transaction && $existing_transaction->succeeded)
		{
			return ['status'=>'succeeded'];
		}

		$offer_id = $paypal_transaction['custom_id'];

		$offer = Offer::findOrFail($offer_id);

		if ($paypal_transaction['status'] == 'COMPLETED')
		{
			$transaction = Transaction::createSucceededOfferPurchase(
				$user,
				$offer,
				$paypal_transaction
					['seller_receivable_breakdown']['gross_amount']['value'],
				$paypal_transaction
					['seller_receivable_breakdown']['paypal_fee']['value'],
				$paypal_transaction
					['seller_receivable_breakdown']['gross_amount']['currency_code'],
				$paypal_transaction['id'],
				2
			);

			Purchase::createFromTransaction($transaction);

			return ['status'=>'succeeded'];
		}
		else
		{
			http_response_code(500);
		}
	}
	
	public function handleRenewSubscriptionWebhookEvent(
		WebhookEvent $webhook_event
	){
		$paypal_subscription_id = $webhook_event->resource->billing_agreement_id;
		$paypal_transaction_id = $webhook_event->resource->id;

		$subscription = Subscription::getFromToken($paypal_subscription_id);
		$transaction = Transaction::getFromToken($paypal_transaction_id);

		if (!$subscription)
			return;

		if ($transaction && $transaction->is_active)
			return;

		$paypal_subscription = $this->getPaypalSubscription($paypal_subscription_id);

		$paypal_subscription_transactions = $this->getPaypalSubscriptionTransactions(
			$paypal_subscription
		);

		$successful_transaction_count = collect(
			$paypal_subscription_transactions->transactions
		)
			->where('status','COMPLETED')
			->count();

		if ($successful_transaction_count <= 1)
			return;

		$user = $subscription->user;
		$offer = $subscription->offer;
		$amount = bcmul($webhook_event->amount->total,100);
		$fee = bcmul($webhook_event->transaction_fee->value,100);
		$currency = $webhook_event->amount->currency;
		$transaction_date = $webhook_event->update_time;

		$transaction = Transaction::createSucceededOfferSubscription(
			$subscription,
			$paypal_transaction_id,
			$amount,
			$fee,
			$currency,
			$transaction_date
		);

		Purchase::createFromTransaction($transaction);
	}

	public function handleCancelSubscriptionWebhookEvent(
		WebhookEvent $webhook_event
	){
		$paypal_subscription_id = $webhook_event->resource->id;

		$subscription = Subscription::getFromToken($paypal_subscription_id);

		$subscription->cancel();
	}

	const WEBHOOK_SUBSCRIBED_EVENTS = [
		'PAYMENT.SALE.COMPLETED',
		'PAYMENT.SALE.DENIED',
		'PAYMENT.SALE.REFUNDED',
		'PAYMENT.SALE.REVERSED',
		'BILLING.SUBSCRIPTION.CANCELLED',
		'BILLING.SUBSCRIPTION.SUSPENDED',
		'BILLING.SUBSCRIPTION.RE-ACTIVATED',
	];

	public function fetchWebhookEventTypes(): array
	{
		$event_types = [];

		foreach (self::WEBHOOK_SUBSCRIBED_EVENTS as $event_type_name)
		{
			$event_types[] = new WebhookEventType(
				"{\"name\" : \"{$event_type_name}\" }"
			);
		}

		$event_names=array_map(function($event){
			return $event->name;
		},$event_types);

		return $event_names;
	}

	public function fetchWebhooks(): array
	{
		$webhooks = Webhook::getAll($this->api_context)->toArray()['webhooks'];

		return $webhooks;
	}

	public function getWebhook(string $webhook_id): Webhook
	{
		$webhook = Webhook::get($webhook_id, $this->api_context);
			
		return $webhook;
	}

	public function createWebhook(
		string $url,
		array $events=[]
	): Webhook {
		if (!count($events))
			$events=$this->fetchWebhookEventTypes();

		$event_types=array_map(function($event){
			$webhook_event = new WebhookEventType;
			$webhook_event->setName($event);
			return $webhook_event;
		},$events);

		$webhook = new Webhook;
		$webhook->setUrl($url)
			->setEventTypes($event_types);

		$webhook->create($this->api_context);

		return $webhook;
	}

	public function deactivateWebhook(
		string $webhook_id
	): void {
		$webhook=$this->getWebhook($webhook_id);

		$webhook->delete($this->api_context);
	}

	public function verifySubscription(string $subscription_token): bool
	{
		try{
			$subscription = $this->getPaypalSubscription($subscription_token);
			return true;
	    }
	    catch(\Exception $e)
	    {
			$message = $e->getMessage();
		    if (preg_match('/No such subscription/',$message))
			   return false;
		  	else
			   throw $e;
	    }
	}

	public function createLocalSubscription(
		User $user,
		Offer $offer,
		Object $request): Subscription
	{
		return Subscription::create([
			'user_id'=>$user->id,
			'offer_id'=>$offer->id,
			'gateway_id'=>$request->gateway_id,
			'gateway_token'=>$request->token,
			'is_active'=>true,
			'is_paused'=>false,
			'price'=>$offer->price,
			'tax_percent'=>$offer->tax_percent,
			'subscribed_on'=>now(),
			'is_legacy' => $request->is_legacy
		]);
	}

	public function createLocalTransactions(Subscription $new_subscription): void
	{
		$subscription = $this->getPaypalSubscription($new_subscription->gateway_token);
		$transaction_object = $this->getPaypalSubscriptionTransactions($subscription);

		if ($transaction_object->transactions) {
			foreach ($transaction_object->transactions as $transaction) {
				if ($transaction->status == "COMPLETED") {
					$new_transaction = Transaction::createSucceededOfferSubscription(
						$new_subscription,
						$new_subscription->id,
						$transaction->amount_with_breakdown->gross_amount->value,
						$transaction->amount_with_breakdown->fee_amount->value,
						$transaction->amount_with_breakdown->gross_amount->currency_code,
						$transaction->time
					);
				}else {
					$new_transaction = Transaction::createFailedOffer(
						$new_subscription->user,
						$new_subscription->offer,
						$transaction->id,
						$new_subscription->gateway_id,
						$transaction->amount_with_breakdown->gross_amount->value,
						$transaction->amount_with_breakdown->fee_amount->value,
						$transaction->amount_with_breakdown->gross_amount->currency_code,
						$transaction->time
					);
				}
			}
		}
	}

	public function getTransaction(string $transaction_id): Collection
	{
		return collect([]);
	}
}
