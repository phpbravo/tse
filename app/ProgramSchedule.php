<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProgramSchedule extends Model
{
    protected $fillable=[
        'program_id',
        'from',
        'to',
        'title',
        'description',
    ];

    public function program(){
        return $this->belongsTo('App\Program');
    }

    public static function getActiveSchedule(){
        return self::where('to','>=',now())
        ->where('from','<=',now())
        ->orderBy('from','DESC')
        ->first();
    }

}
