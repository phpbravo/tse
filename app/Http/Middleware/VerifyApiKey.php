<?php

namespace App\Http\Middleware;

use Closure;

class VerifyApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!isset($_SERVER['HTTP_X_API_TOKEN']) || $_SERVER['HTTP_X_API_TOKEN']!=config('api.key')) {
            abort(498,'Invalid Request');
        }

        return $next($request);
    }
}
