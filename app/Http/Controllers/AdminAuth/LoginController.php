<?php

namespace App\Http\Controllers\AdminAuth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Handlers\ObjectHandler;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    protected $guard = 'admin';

    protected $username = 'username';

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function apiLogin()
    {
        $login_attempt = auth('admin')
        ->attempt(request()->only('username','password'));
        if ($login_attempt)
        {
            auth('admin')->user()->update([
                'last_login'=>now()
            ]);
            return auth('admin')->user();
        }else {
            return response(json_encode(['message'=>'Login failed.']),401);
        }
    }
 
    public function login(Request $request)
    {
        $this->validator($request);
        
        if(Auth::guard('admin')->attempt($request->only('username','password'),$request->filled('remember'))){
            auth('admin')->user()->update([
                'last_login'=>now()
            ]);
            return redirect()->intended();
        }
        //Authentication failed...
        return $this->loginFailed();
    }

    public function getConfig(){
        return json_encode(auth('admin')->user()->config);
    }

    public function updateConfig(){
        
        $config_item=request()->get('config_item');
        
        if (!$config_item)
            return (new ObjectHandler)->newError('invalid_request');

        $value=request()->get('value');

        $config=auth('admin')->user()->config;

        $config->$config_item=$value;

        auth('admin')->user()->update([
            'config'=>$config
        ]);

        return (new ObjectHandler)->newSuccess();
    }

    private function loginFailed(){
        return redirect()->back()->withInput();
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'username' => 'required|exists:admins|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];
        //validate the request.
        $request->validate($rules);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
}
