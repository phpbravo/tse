<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Product;

class ProductsController extends Controller
{

	private function validateProduct($fields){
		$objectHandler=new ObjectHandler(Product::class);

		$resource=\App\Resource::find($fields->resource_id);

		if (!$resource){
			return $objectHandler->newError(
				'invalid_request',
				'La ressource n\'est pas valide'
			);
		}

		if ($resource->product){
			return $objectHandler->newError(
				'invalid_request',
				'La ressource a déjà un produit.'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Product::class);
		$pageHandler=new PageHandler(Product::class);
		$objectHandler=new ObjectHandler(Product::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('resource');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateProduct($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$resource=\App\Resource::find($fields->resource_id);

		$product=Product::create([
			'resource_id'=>$resource->id,
			'name'=>$fields->name??$resource->name,
			'description'=>$fields->description??'',
		]);

		return $product;
	}

	public function update($id){

		$fields=(object)request()->all();

		$product=Product::find($id);

		if(!$product){
			return (new ObjectHandler(Product::class))->newError('not_found');
		}

		$product->update([
			'name'=>$fields->name,
			'description'=>$fields->description,
		]);

		return $product;
	}

	public function show($id){

		$product=Product::with(['sessions'])->find($id);

		if(!$product){
			return (new ObjectHandler(Product::class))->newError('not_found');
		}

		return $product;
	}

	public function destroy($id){

		$product=Product::find($id);

		if(!$product){
			return (new ObjectHandler(Product::class))->newError('not_found');
		}

		$product->delete();

		return (new ObjectHandler(Product::class))->newSuccess();
	}

	public function list(){

		return Product::all();

	}

}
