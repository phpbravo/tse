<?php

namespace App\Http\Controllers\Admin;

use App\Exercise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Vimeo\Vimeo;

use App\Video;
use Illuminate\Support\Collection;

class VideosController extends Controller
{

	private function validateVideo($fields){
		$objectHandler=new ObjectHandler(Exercise::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Video::class);
		$pageHandler=new PageHandler(Video::class);
		$objectHandler=new ObjectHandler(Video::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page->items->makeHidden('content');

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateExercise($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$exercise=Exercise::create([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_link'=>$fields->video_link,
			'api_video_link'=>$fields->api_video_link,
		]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$exercise->tag($tag);
			}
		}

		return $exercise;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateExercise($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$exercise=Exercise::find($id);

		if(!$exercise){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		$exercise->update([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_link'=>$fields->video_link,
			'api_video_link'=>$fields->api_video_link,
		]);

		$exercise->tags()->sync([]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$exercise->tag($tag);
			}
		}

		return $exercise;
	}

	public function show($id){

		$video=Video::find($id);

		if(!$video){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		return $video;
	}

	public function destroy($id){

		$exercise=Exercise::with(['tags'])->find($id);
		
		if(!$exercise){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		$exercise->delete();

		return (new ObjectHandler(Exercise::class))->newSuccess();
	}

	public function tag($id,$name){

		$exercise=Exercise::findOrFail($id);

		$exercise->tag($name);

		return $exercise;

	}

	public function untag($id,$name){

		$exercise=Exercise::findOrFail($id);

		$exercise->untag($name);

		return $exercise;

	}
	
	// returns an array of autocomplete matches
	public function autocomplete(){
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Video::where('vimeo_name','like','%'.$search.'%')->orderBy(DB::raw('length(vimeo_name)'))->whereNotIn('id',$ignore)->take(15)->get();

		return $tags_matched;

		}

	private function getBestLink($links){
		return array_reduce(array_splice($links,1),function($link_a,$link_b){
			$link_a_result=null;
			$link_b_result=null;
			
			preg_match('/profile_id=(.*)\&/',$link_a,$link_a_result);
			preg_match('/profile_id=(.*)\&/',$link_b,$link_b_result);

			if (!array_key_exists(1,$link_a_result)){
				return $link_b;
			}
			elseif (!array_key_exists(1,$link_b_result)){
				return $link_a;
			}

			$link_a_id=$link_a_result[1];
			$link_b_id=$link_b_result[1];
			
			return ((int)$link_a_id<(int)$link_b_id)?
				$link_b:
				$link_a;

		},$links[0]);
	}

	private function getAccessDetails(array $links): Collection
	{

		$links = collect($links)->filter(function ($link){
			return preg_match('/\.(sd|hd)\./',$link);
		});
		
		return $links->map(function($link)
		{
			$quality = '';
			$token = '';
			$profile_id = '';
			preg_match('/\.(sd|hd)\./',$link,$quality);
			preg_match('/\?s=(.*?)&/',$link,$token);
			preg_match('/&profile_id=(.*?)&/',$link,$profile_id);


			return (object)[
				'quality'=>$quality[1],
				'token'=>$token[1],
				'profile_id'=>$profile_id[1],
			];

		})->keyBy('profile_id');
	}

		
	public function fetchVimeoVideos(){
		$vimeo=new Vimeo(
			config('vimeo.client_id'),
			config('vimeo.client_secret'),
			config('vimeo.access_token')
		);

		$headers=[];

		if ($date=Video::max('last_modified_date')){

			$date=new \Carbon\Carbon($date);

			$date=$date->locale('en')->isoFormat('ddd, D MMM YYYY hh:mm:ss Z');

			$headers=[
				'If-Modified-Since'=>$date
			];
		}

		$params=
		'?'.
		'page=1'.
		'&'.
		'per_page=100'.
		'&'.
		'fields='.
			'uri,'.
			'name,'.
			'pictures.sizes.link_with_play_button,'.
			'files.link,'.
			'created_time,'.
			'modified_time'.
		'&'.
		'sizes='.
			'640x360,'.
			'1280x720';

		$video_data=[];

		do {
			
			$tries_left=3;

			do{

				try{
					if (!isset($videos)){
		
						$videos=$vimeo->request(
							'/users'
							.'/'
							.config('vimeo.user_id')
							.'/videos'
							.$params,
							[],
							'GET',
							true,
							$headers
						);
					}else{
		
						$videos=$vimeo->request(
							$videos['body']['paging']['next']
						);
					}
					$tries_left=0;
				}catch(\Exception $e){
					$tries_left--;

					if ($tries_left==0){
						return with(new ObjectHandler)->newError();
					}
				}

			} while ($tries_left>0);

			$video_data=array_merge(

				//old array
				$video_data,
				
				//new array
				array_map(
					function($video){

						$access_details=null;

						if (array_key_exists('files',$video) && count($video['files'])>0){
			
							$links=array_map(function($file){
								return $file['link'];
							},$video['files']);

							// NEED TO SORT BY profile_id= AND GET THE HIGHEST

							$access_details=$this->getAccessDetails($links);
						}

						$video_uri_array=explode('/',$video['uri']);

						$result=[
							'vimeo_id'=>array_pop($video_uri_array),
							'vimeo_name'=>$video['name']??'',
							'thumbnails'=>[
								'small'=>$video['pictures']['sizes'][0]['link_with_play_button']??'',
								'large'=>$video['pictures']['sizes'][1]['link_with_play_button']??'',
							],
							'access_details'=>$access_details,
							'vimeo_created_date'=>new \Carbon\Carbon($video['created_time']),
							'last_modified_date'=>new \Carbon\Carbon($video['modified_time']),
						];

						return $result;
					},
					$videos['body']['data']
				)

			);
		} while ($videos['body']['paging']['next']);

		foreach ($video_data as $video_details){

			Video::updateOrCreate(
				['vimeo_id'=>$video_details['vimeo_id']],
				$video_details
			);

		}

		return response()->json([
			'status'=>'success',
			'message'=>'refreshed'
		],200);

	}

}
