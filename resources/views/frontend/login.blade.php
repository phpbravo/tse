@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="login-wrapper d-none" id="login-app">
			<transition name="fade">
			<div v-if="loading" class="loading-overlay full-overlay"><div class="loader4"></div></div>
		</transition>
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<h1  class="pages-heading">CONNEXION</h1>
					<div class="warning-alert">
						<div v-for="error in errors" class="alert alert-warning alert-dismissible fade show position-relative" role="alert">
							<strong style="text-transform:Capitalize;">Erruer : </strong>
							@{{error}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>


				<div class="col-lg-6 col-md-12 ">
					<div class="services-section ">
						<h3 class="text-center mb-5">ACCÉDEZ À NOS <strong>SERVICES</strong></h3>
						<p class="text-center">Merci de saisir votre <strong>e-mail</strong> et <strong>mot de passe</strong> afin d<strong>'accéder à nos services</strong>.</p>
					<p class="text-center px-3">Si vous n’en disposez pas, merci de vous rendre sur la page<strong> <a href="{{ route('front.register') }}">«&nbsp;Inscription&nbsp;»</a></strong>.</p>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<form @submit.prevent="login" class="login-form p-0 m-0">
						<div class="form-group">
								<label for="Email">E-mail</label>
								<input v-model="user.login" autocomplete="email" name="login" class="form-control" id="Email">
								
						</div>
						<div class="form-group">
								<label for="Mot de passe">Mot de passe</label>
								<input v-model="user.password" autocomplete="current-password" name="password" type="password" class="form-control" id="Mot de passe">
						</div>
						<div class="form-check pt-3">
									
						</div>
						<button type="submit" class="btn btn-primary">Connexion</button>
						<div class="form-group">
							<a href="{{route('password.request')}}">Mot de passe oublié ?</a>
						</div>
							
						</form>
					</div>
				
				</div>
		</div>
	
	</section>
</div>

@endsection
@section('footer')
<script src="https://www.google.com/recaptcha/api.js?render=6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon', {action: 'homepage'}).then(function(token) {
    	// console.log(token);
    });
});
</script>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/login.js?v={{filever('public/assets/js/login.js')}}"></script>
@endsection