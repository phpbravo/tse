<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vimeo_id')->index();
            $table->string('vimeo_name')->nullable()->index();
            $table->text('thumbnails')->nullable();
            $table->text('access_details')->nullable();
            $table->dateTime('last_modified_date')->nullable()->index();
            $table->dateTime('vimeo_created_date')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
