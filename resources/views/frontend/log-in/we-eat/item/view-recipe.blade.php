@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section loginrecettes" id="top">
			<div class="container">
			</div>
	</section>
	
	

	<section class="faq-section" id="recettesfit">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">MES RECETTES FIT</h1>
					<hr>
				</div>
				
				
			</div>

			<!---- DISPLAY  GET item ----->
			
			<div class="row getItemContent">
				<div class="col-lg-12 px-0">
					<div class="display-img">
						<img src="{{"/images?path=recipes/{$recipe->id}/image/0_main_image.jpg"}}" class="img-fluid">
					</div>
					<div class="itemname">
						<h2>{{$recipe->name}}</h2>
					</div>

					<div class="recentsactivity">
						<span>
							<i class="fa fa-link"></i>
							<a href="#">{{$recipe->recipe_category->name}}</a>
						</span>

						<span>
							<i class="fa fa-heart"></i>
							@foreach($recipe->tags as $tag)
								<span>{{$tag->name}}</span>
							@endforeach
						</span>
						<span class="preparation">
							<i class="fa fa-clock"></i>
							{{now()->diffInMinutes(now()->addSeconds($recipe->preparation_time))}} min
						</span>
						<span>
							<i class="fa fa-fire"></i>
							{{now()->diffInMinutes(now()->addSeconds($recipe->cook_time))}} min
						</span>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="card itemboxes">
						<div class="card-body">
							<h3 class="card-text">
								<img src="/assets/img/picto-ingredients.png">
								INGRÉDIENTS
							</h3>
							{!! $recipe->sections[0]->pivot->content !!}
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="card itemboxes">
						<div class="card-body">
							<h3 class="card-text">
								<img src="/assets/img/picto-preparation.png">
								PRÉPARATION
							</h3>
							{!! $recipe->sections[1]->pivot->content !!}
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="card itemboxes">
						<div class="card-body">
							<h3 class="card-text">
								<i class="fa fa-heart"></i>
								ASTUCE DE SISSY
							</h3>
							{!! $recipe->sections[2]->pivot->content !!}
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="card itemboxes" >
						<div class="card-body" id="tabledisplay">
							<h3 class="card-text">
								<i class="fa fa-table"></i>
								INFORMATIONS NUTRITIONNELLES
							</h3>
							<div class="table-responsive">
								{!! $recipe->sections[3]->pivot->content !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	

	

	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection