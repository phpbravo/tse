@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section wetrainseancesbonus" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section" id="wetrainvids">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">SÉANCES BONUS</h1>
					<hr>	
				</div>
			</div>

	

			<div class="row categoriesbtn d-none">
				
				<div class="col-lg-12 text-center" id="recettesfitcol">
					<a href="#" class="btn btn-secondary recettesfitbtn">
						<h4>Toutes les séances bonus</h4>
					</a>
				</div>

				<div class="col-xl-2 offset-xl-1 col-lg-2 offset-lg-1  col-md-3 col-sm-4 col-6 recettescol">
					<a href="#">
						<div class="categoriesbtn-item">
							<h4>Bras</h4>
							<img src="/assets/img/bras.png">
						</div>
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a href="#">
						<div class="categoriesbtn-item">
							<h4>Dos</h4>
							<img src="/assets/img/dos.png">
						</div>
						
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a href="#">
						<div class="categoriesbtn-item">
							<h4>Jambes</h4>
							<img src="/assets/img/jambes.png">
						</div>
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a href="#">
						<div class="categoriesbtn-item">
							<h4>Pecs</h4>
							<img src="/assets/img/pecs.png">
						</div>
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a href="#">
						<div class="categoriesbtn-item">
							<h4>Épaules</h4>
							<img src="/assets/img/epaules.png">
						</div>
					</a>
				</div>


			</div>


			<div class="row">
				<div class="col-lg-12 px-0">
					<div class="materialouter">
						<div class="materialinner">
							<h3>SÉANCES BONUS</h3>

							<div>
								<p>Tu ne peux pas faire la séance du jour ? Pas de panique ! Nous te proposons quelques séances bonus en remplacement.</p>

								
							</div>
						</div>
					</div>
				</div>
			</div>

			<div v-if="loading" class="d-flex" style="position:relative; width:100%; justify-content:center;" id="loading-icon">
				<div class="loader4"></div>
			</div>
			<!---- seance list ----->

			<div class="row seancelist">
				<div
					v-for="session in sessions"
					:key="session.id"
					class="col-lg-6"
				>
					<article class="seancelist-wrapper">
						
						<div class="seancelist-inner">
							<h4>@{{session.title}}</h4>
							<p>
								<img src="/assets/img/pt-picto-haltere-rose.jpg"><br>
								<strong>@{{session.description}}</strong>
							</p>
							<br>
							<div
								v-for="exercise_set in session.exercise_sets"
								:key="exercise_set.id"
								class="random-set"
							>
								<p> 
									<template v-if="exercise_set.name!=''">
										<span class="text-uppercase">@{{exercise_set.name}}</span><br>
									</template>
									<span
										v-for="(exercise_assignment,index) in exercise_set.exercise_assignments"
										:key="exercise_assignment.id"
									>
										<br v-if="index!=0">
										<a href="#">@{{exercise_assignment.exercise.name}} :</a>
										@{{exercise_assignment.reps}}
										<template v-if="exercise_assignment.tempo">
											&nbsp;(@{{exercise_assignment.tempo}})
										</template>
									</span>
								</p>
							</div>
						</div>
					</article>
				</div>
		</div>
		
	</section>

	

	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/we-train-bonus.js?v={{filever('public/assets/js/we-train-bonus.js')}}"></script>

@endsection