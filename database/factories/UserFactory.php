<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(User::class, function (Faker $faker) {
    $email=$faker->unique()->safeEmail;
    $is_oauth=$faker->boolean;
    $detailed=0.2+($faker->boolean($chanceOfGettingTrue = 20)*0.7);
    $date_created=$faker->dateTime($max = 'now');
    return [
        'email' => $email,
        'username' => ($is_oauth?$email:$faker->boolean)?$email:$faker->unique()->userName,
        'password' => $is_oauth?null:'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'name' => $faker->firstName,
        'surname' => $faker->optional(0.9)->lastName,
        'organization' => $faker->optional($weight=$detailed)->company,
        'address' => $faker->optional($weight=$detailed)->streetAddress,
        'address2' => $faker->optional($weight=$detailed)->secondaryAddress,
        'city' => $faker->optional($weight=$detailed)->city,
        'postal_code' => $faker->optional($weight=$detailed)->postcode,
        'country' => $faker->optional($weight=$detailed)->country,
        'phone' => $faker->optional($weight=$detailed)->phoneNumber,
        'homepage_url' => $faker->randomElement(['/']),
        'agreed_terms' => true,
        'agreed_newsletter' => $faker->boolean($changeOfGettingTrue=40),
        'email_verified_at' => $faker->dateTime($min = $date_created),
        'remember_token' => Str::random(10),
        'created_at' => $date_created,
        'updated_at' => $date_created,
    ];
});
