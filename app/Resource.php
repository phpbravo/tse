<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
	protected $fillable=[
		'name',
		'resource_object_type',
		'resource_object_id',
		'created_at',
		'updated_at',
	];

	protected $casts=[
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

	// columns that can be searched
	public $searchable_columns = [
		'name'
	];

	public function product(){
		return $this->hasOne('App\Product');
	}
	
}
