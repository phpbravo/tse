<?php

namespace App\Http\Controllers\API\v0;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Exercise, Subcategory};

class ExercisesController extends Controller
{

	public function getVideos($category_id){

		$exercises=Exercise::whereHas('subcategory')->where('category_id',$category_id)->with(['video','subcategory'])->get()
		->map(function($exercise,$index)  use($category_id){
			$details = collect($exercise->video->access_details);
			return [
				'item'=>[
					'id'=> (string) $exercise->id,
					'title'=>$exercise->name,
					'description'=>'',
					'descIntro'=>$category_id == 4 ? '' : '<p class="excerpt-blog">'.$exercise->name.'</p>',
					'categoryname'=>$category_id == 4 ? $exercise->subcategory->name : 'Trainings',
					'videoid'=>$exercise->video->vimeo_id,
					'extrafieldsearch'=>'',
					'ingredient'=>'',
					'ctr'=>$index,
					'time'=>$exercise->duration?bcdiv($exercise->duration,60).' min':'',
					'thumbnail'=>$exercise->video->thumbnails,
					't'=> $details->sortByDesc('profile_id')->first()['token'],
					'q'=> $details->sortByDesc('profile_id')->first()['quality'],
					'pid'=> (string) $details->sortByDesc('profile_id')->first()['profile_id']
				],
				'tags'=>[
					[
						'name'=>$exercise->subcategory->name??'',
						'id'=>(string)$exercise->subcategory->id,
						'published'=>1
					]
				]
			];
		});

		$subcategories=Subcategory::where('category_id',$category_id)->get()
		->map(function($subcategory){
			return [
				'name'=>$subcategory->name,
				'id'=>(string)$subcategory->id,
				'published'=>1
			];
		});

		return response()->json([
			'success'=>true,
			'data'=>[
					'tags'=>$subcategories,
					'list'=>$exercises
				]
		],200);

	}

	public function getWeTrainVideos(){
		return $this->getVideos(1);
	}

	public function getWeSweatVideos(){
		return $this->getVideos(2);
	}

	public function getWeFlow()
	{
		return $this->getVideos(4);
	}

	public function getBikiniVideos(){
		$category_id = request()->get('catid');
		$subcategory = Subcategory::with('exercises.video', 'category')->find($category_id);
		$exercises=$subcategory->exercises->map(function($exercise, $index){
			$details = collect($exercise->video->access_details);
			return [
				'item' => [
					'id' => (string) $exercise->id,
					'title' => $exercise->name,
					'descIntro' => '',
					'categoryname' => $exercise->category->name,
					'videoid' => $exercise->video->vimeo_id,
					'extrafieldsearch' => '',
					'ingredient' => '',
					'ctr' => $index,
					'time' => $exercise->duration?bcdiv($exercise->duration,60).' min':'',
					'thumbnail' => $exercise->video->thumbnails,
					't'=>$details->sortByDesc('profile_id')->first()['token'],
					'q'=>$details->sortByDesc('profile_id')->first()['quality'],
					'pid'=> $details->sortByDesc('profile_id')->first()['profile_id'],
					'tags' => []
				]
			];
		});

		return response()->json([
			'success'=>true,
			'data'=>[
					'tags'=>[],
					'list'=>$exercises
				]
		],200);

	}
}