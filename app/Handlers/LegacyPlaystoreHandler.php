<?php

namespace App\Handlers;

use App\Subscription;
use App\Transaction;

class LegacyPlaystoreHandler
{
    public function generateRandomId()
    {
        return 'legacy_'.uniqid('',true);
    }

    public function createTransactionForSubscription(Subscription $subscription)
    {
        return Transaction::createSucceededOfferSubscription(
            $subscription,
            $this->generateRandomId(),
            bcmul($subscription->offer->price,100),
            0,
            'EUR',
            now()->timestamp
        );
    }

    public function renewSubscription(Subscription $subscription)
    {
        $transaction = $this->createTransactionForSubscription($subscription);
        $subscription->renew($transaction);
    }   

    public function handleEvent($event)
    {
        $token = $event['purchaseToken'];
        
        $event_type = $event['event_type'];

        switch($event_type)
        {
            case 'legacy_SUBSCRIPTION_RENEWED':
                $subscription = Subscription::getFromToken($token);
                $this->renewSubscription($subscription);
                break;

            case 'legacy_SUBSCRIPTION_CANCELED':
            case 'legacy_SUBSCRIPTION_REVOKED':
                $subscription = Subscription::getFromToken($token);
                $subscription->cancel();
                break;

            default:
                throw(new \Exception("Unsupported event: $event_type"));
                break;
        }

    }

}