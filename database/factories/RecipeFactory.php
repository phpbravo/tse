<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Recipe;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Recipe::class, function (Faker $faker) {
    return [
        'recipe_category_id'=>$faker->numberBetween($min=1,$max=6),
        'name'=>$faker->words($nb=$faker->numberBetween($min=1,$max=4), $asText=true),
        'description'=>$faker->sentence($nb=$faker->numberBetween($min=3,$max=8), $asText=true),
    ];
});