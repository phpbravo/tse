<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Converter\FormatConverter;
use PayPal\Validation\NumericValidator;

/**
 * Class Money
 *
 * Base object for all financial value related fields (balance, payment 
	* due, etc.)
 *
 * @package PayPal\Api2
 *
 */
class Money extends PayPalModel
{
	public function __construct(
		?string $value = null,
		?string $currency_code = null
	){
		if ($currency_code != null && $value != null)
		{
			$this->setCurrencyCode($currency_code);
			$this->setValue($value);
		}
	}

	/**
		* 3 letter currency code as defined by ISO 4217.
		*/
	public function setCurrencyCode(string $currency_code): self
	{
		$this->currency_code = $currency_code;
		return $this;
	}

	/**
		* 3 letter currency code as defined by ISO 4217.
		*/
	public function getCurrencyCode(): string
	{
		return $this->currency_code;
	}
	
	/**
		* amount up to N digit after the decimals separator as defined in ISO 
		* 4217 for the appropriate currency code.
		*
		* @param string|double $value
		*/
	public function setValue($value): self
	{
		NumericValidator::validate($value, "Value");
		$value = FormatConverter::formatToPrice(
			$value,
			$this->getCurrencyCode()
		);
		$this->value = $value;
		return $this;
	}

	/**
		* amount up to N digit after the decimals separator as defined in ISO 
		* 4217 for the appropriate currency code.
		*/
	public function getValue(): string
	{
		return $this->value;
	}

}
