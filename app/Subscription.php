<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\{
	User,
	Offer
};

class Subscription extends Model
{
	protected $fillable=[
		'user_id',
		'offer_id',
		'gateway_id',
		'upgrade_from_subscription_id',
		'gateway_token',
		'is_active',
		'is_paused',
		'is_upgrade',
		'price',
		'tax_percent',
		'last_pause',
		'paused_total',
		'subscribed_on',
		'cancelled_on',
		'next_payment_date',
	];

	protected $hidden = [

	];

	protected $casts = [

	];

	// columns that can be searched
	public $searchable_columns = [
		'gateway_token',
	];

	// Simple Relations

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function gateway()
	{
		return $this->belongsTo('App\Gateway');
	}

	public function offer()
	{
		return $this->belongsTo('App\Offer');
	}

	public function transactions()
	{
		return $this->hasMany('App\Transaction');
	}

	public function upgradeFromSubscription()
	{
		return $this->hasOne(
			'App\Subscription',
			'upgrade_from_subscription_id'
		);
	}

	// Scoped Relations

		// none

	// Attributes

	public function getLastSuccessfulPurchaseAttribute()
	{
		return $this->transactions()
			->where('payment_status','succeeded')
			->with('purchase')
			->get()
			->map(function($transaction)
			{ return $transaction->purchase; })
			->sortByDesc('effective_on')
			->first();
	}

	// Scopes

	public function scopeActive($query)
	{
		return $query->where('is_active',true);
	}

	public function scopeInactive($query)
	{
		return $query->where('is_active',false);
	}

	// Instance Methods

	public function renew(Transaction $transaction)
	{
		$new_purchase = Purchase::createFromTransaction($transaction);

		$new_purchase->reschedule(Carbon::parse($this->next_payment_date));

		$this->recalculateNextPaymentDate();
	}

	public function recalculateNextPaymentDate()
	{
		$last_purchase = $this->last_successful_purchase;
		$last_purchase_expiry_date = $last_purchase->expires_on;

		$this->update([
			'next_payment_date' => $last_purchase_expiry_date,
		]);
	}

	public function reactivate()
	{
		if ($this->is_active)
			return;

		$this->update([
			'is_active'=>true
		]);
	}

	public function activate()
	{
		if ($this->is_active)
			return;
		
		$this->update([
			'is_active'=>true,
			'subscribed_on'=>now(),
		]);
	}

	public function activateInFuture(Carbon $activation_date)
	{
		if ($this->is_active)
			return;
		
		$this->update([
			'is_active'=>true,
			'subscribed_on'=>$activation_date,
			'next_payment_date' => $activation_date,
		]);
	}

	public function cancel()
	{
		$this->update([
			'is_active'=>false,
			'is_paused'=>false,
			'cancelled_on'=>now(),
			'next_payment_date'=>null,
		]);
	}

	// Static Methods

	public static function getFromToken(
		string $subscription_token
	): ?Subscription {
		return self::where('gateway_token',$subscription_token)->first();
	}

	public static function createInactive(
		User $user,
		Offer $offer,
		string $gateway_token,
		int $gateway_id
	): Subscription {

		return self::create([
			'user_id'=>$user->id,
			'offer_id'=>$offer->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
			'is_active'=>false,
			'is_paused'=>false,
			'price'=>$offer->price,
			'tax_percent'=>$offer->tax_percent,
		]);
	}

	public static function createActive(
		User $user,
		Offer $offer,
		string $gateway_token,
		int $gateway_id
	): Subscription {

		return self::create([
			'user_id'=>$user->id,
			'offer_id'=>$offer->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
			'is_active'=>true,
			'is_paused'=>false,
			'price'=>$offer->price,
			'tax_percent'=>$offer->tax_percent,
			'subscribed_on'=>now(),
			'next_payment_date' => now()->addMonthsNoOverflow($offer->duration),
		]);
	}
}
