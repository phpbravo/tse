<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GatewayOffer extends Pivot
{
    public function scopeStripe($query)
    {
        $query->where('gateway_id',1);
    }

    public function scopePaypal($query)
    {
        $query->where('gateway_id',2);
    }

    public function scopeMobile($query)
    {
        $query->where('gateway_id',3);
    }

    public function scopeFree($query)
    {
        $query->where('gateway_id',4);
    }
}
