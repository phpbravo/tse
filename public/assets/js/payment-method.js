var payment_app= new Vue({
	el:'#payment-app',
	data:{
		loading:false,
		payment_options:{
			stripe_offer_token:null,
			card_payment_available:false,
			paypal_payment_available:false,
		},
		selected_offer:0,
		selected_payment_method:0,
		errors:[],
		stripe_instance:null,
		stripe_elements:null,
		stripe_card_element:null,
		stripe_elements_style:{
			base: {
				color: '#32325d',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		}
	},
	methods:{
		secureStripePayment(transaction_token, subscription_token){
			return (()=>{
				if (transaction_token.substring(0,2) == 'pi')
					return this.stripe_instance.confirmCardPayment(transaction_token)
				else if (transaction_token.substring(0,4) == 'seti')
					return this.stripe_instance.confirmCardSetup(transaction_token)
				else
					return Promise.reject('invalid transaction token')
			})()
			.then(response=>{
				if (response.paymentIntent || response.setupIntent){
					if (subscription_token)
						return this.verifyStripePayment(subscription_token,'subscription')
					else
						console.log(response.paymentIntent)
						return this.verifyStripePayment(response.paymentIntent.id,'purchase')
				} else if (response.error) {
					console.log(response.error)
					return Promise.reject('payment security failed')
				}
			})
		},
		handleStripeConfirmation(response){
			if (response.data.status == 'succeeded'){
				window.routePush('/access/moncompte')
				return Promise.resolve()
			} else if (response.data.status == 'requires_action'){
				transaction_token = response.data.transaction_token
				subscription_token = response.data.subscription_token
				return this.secureStripePayment(
					transaction_token,
					subscription_token
				)
			} else {
				return Promise.reject(response.data.message)
			}
		},
		payWithStripe(){
			this.loading = true;
			this.stripe_instance.createToken(this.stripe_card_element)
			.then(response=>{
				if (response.error){
					this.errors.push(response.error.message)
					return Promise.reject(response.error.status)
				} else {
					return axios.post('/api/handlePayment',{
						offer_id: this.selected_offer,
						token: response.token.id,
						gateway:'stripe'
					})
					.then(response=>{
						return this.handleStripeConfirmation(response)
					})
					.catch(error=>{
						return Promise.reject(
							error.response ?
							error.response.data.message :
							error
						)
					})
				}
			})
			.catch(error=>{
				console.log(error)
				this.errors.push(error)
				this.loading = false
			})
		},
		payWithPaypal(){
			this.loading = true;
			axios.post('/api/handlePayment',{
				offer_id: this.selected_offer,
				gateway: 'paypal'
			})
			.then(response=>{
				window.routePush(response.data.redirect)
			})
			.catch(error=>{
				console.log(error)
				this.loading = false
			})
		},
		getOfferId(){
			offer_id=window.findUrlComponent(1)
			return offer_id
		},
		verifyStripePayment(token, payment_type){
			return axios.post('/api/confirmPayment',{
				payment_type,
				token,
				gateway: 'stripe'
			})
			.then(response=>{
				return this.handleStripeConfirmation(response)
			})
			.catch(error=>{
				return Promise.reject(error.response.data.message)
			})
		},
		verifyPaypalPayment(){
			let subscription_token=window.findRouteParameter('subscription_id')
			let order_token=window.findRouteParameter('token')
			if (subscription_token)
			{
				return axios.post('/api/confirmPayment',{
					payment_type: 'subscription',
					token: subscription_token,
					gateway: 'paypal'
				})
				.then(response=>{
					window.routePush('/access/moncompte');
				})
				.catch((error=>{
					if (error.response.data.status == 'transaction_not_found')
					{
						return this.verifyPaypalPayment()
					}
					return Promise.reject();
				}))
			}
			else if(order_token)
			{
				return axios.post('/api/confirmPayment',{
					payment_type: 'purchase',
					token: order_token,
					gateway: 'paypal'
				})
				.then(response=>{
					window.routePush('/access/moncompte');
				})
			}
			else
			{
				return Promise.reject()
			}
		},
		getStripeKey(){
			return axios.get('/api/stripe_public_key')
			.catch(()=>this.getStripeKey())
		},
		mountStripeElements(){
			return this.getStripeKey()
			.then(response=>response.data)
			.then(stripe_key=>{
				this.stripe_instance = window.Stripe(stripe_key,{locale:'fr'})
				this.stripe_elements = this.stripe_instance.elements()
				this.stripe_card_element = this.stripe_elements.create(
					'card',
					{style:this.stripe_elements_style}
				)
				this.stripe_card_element.mount('#card-element')
				this.stripe_card_element.addEventListener('change', function(event) {
  			var displayError = document.getElementById('card-errors');
  			if (event.error) {
    			displayError.textContent = event.error.message;
  			} else {
    			displayError.textContent = '';
  			}
		});
			})
		},
		loadData(){
			this.loading=true
			this.selected_offer = this.getOfferId()

			this.verifyPaypalPayment()
			.catch(()=>{
				this.mountStripeElements()
				.then(()=>{
					this.loading=false
				})
				.catch(()=>{
					console.log(error)
					// window.location.reload()
				})
			})
		}
	},
	mounted(){
		document.getElementById('payment-app')
		.className="registration-wrapper"
	},
	created(){
		this.loadData()
	}
})