<?php

namespace App\Http\Controllers\API\v0;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Category, Exercise, Program, ProgramSchedule, Session, SuperProgram, Video};

class ProgramsController extends Controller
{

	public function getCurrentProgram(){

		$active_schedule=ProgramSchedule::getActiveSchedule();

		if (!$active_schedule)
			return response()->json(['message'=>['Not found']],404);

		$program=$active_schedule->program;

		$program->load('sessions');

		$sessions=$program->sessions->map(function($session){
			return [
				'id'=>$session->id,
				'name'=>$session->title,
				'description'=>$session->description,
				'image'=>config('app.url').'/'.'image/sessions/'.$session->id.'/image/0_main_image.jpg'
			];
		});

		return response()->json([
			'success'=>true,
			'data'=>$sessions
		],200);

	}

	public function getSession($id){
		$session=Session::with('exercise_sets.exercise_assignments.exercise.video')->find($id);

		$exercises_assignments=$session->exercise_sets->reduce(function($exercises,$exercise_set){
			$items=$exercise_set->exercise_assignments->map(function($exercise_assignment, $index) use ($exercises,$exercise_set){
				$alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
				return ['item'=>[
					'title'=>'Exercice '.($exercises->count()+$index+1).(
						$exercise_set->name!=''?
							(' : '.strtoupper($exercise_set->name).' Partie '.$alpha[$index]):
							''
					),
					'description'=>$exercise_assignment->description,
					'q'=>$exercise_assignment->exercise->video->quality,
					'reps'=>$exercise_assignment->reps.' ('.$exercise_assignment->tempo.')',
					'name'=>$exercise_assignment->exercise->name,
					'instruction'=>'',
					'video'=>$exercise_assignment->exercise->video->vimeo_id,
					'thumbnail'=>$exercise_assignment->exercise->video->thumbnails,
					't'=>$exercise_assignment->exercise->video->token,
					'pid'=>$exercise_assignment->exercise->video->profile_id
				],'tags'=>[]];
			});
			return $exercises->merge($items);
		},collect([]));

		return response()->json([
			'success'=>true,
			'data'=>[
				'tags'=>[],
				'list'=>$exercises_assignments,
			]
		],200);
	}

	public function getWeSweatCategories()
	{
		$super_programs = SuperProgram::with('programs.sessions.exercise_sets.exercise_assignments.exercise.video')->get();
		$exercises_videos = collect([]);
		$categories = $super_programs->map(function($super_program) use($exercises_videos) {
			return [
				'id' => (string) $super_program->id,
				'name' => $super_program->name,
				'description' => $super_program->content ,
				'image'=>config('app.url').'/'.'image/super_programs/'.$super_program->id.'/image/0_main_image.jpg',
				'child' => $super_program->programs->map(function($program) use($super_program, $exercises_videos) {
					return [
						'id' =>  (string) $program->id,
						'name' => $program->name,
						'description' => '',
						'image'=>config('app.url').'/'.'image/programs/'.$program->id.'/image/0_main_image.jpg',
						'child' => 0,
						'video' => [
							'list' => $program->sessions->map(function($session) use($program, $super_program, $exercises_videos) {
								return [
									'item' => [
										'id' =>  (string) $session->id,
										'title' => $session->title,
										'description' => '',
										'categoryname' => $program->name,
										'videos' => $session->exercise_sets->map(function($exercise_set) use($program, $session, $super_program, $exercises_videos) {
											$exercise_assignment = $exercise_set->exercise_assignments[0];
											$exercise = $exercise_assignment->exercise;
											$video = $exercise->video;
											$details = collect($exercise->video->access_details);
											$exercises_video = [
												'name' => $exercise->name,
												'video' => [
													'id' =>  (string) $video->vimeo_id,
													'thumbnail' => $video->thumbnails,
													'thumbnail' => $video->thumbnails,
													't'=> $details->filter(function($detail, $key){ return $detail['quality'] == 'sd'; })->sortByDesc('profile_id')->first()['token'],
													'q'=> $details->filter(function($detail, $key){ return $detail['quality'] == 'sd'; })->sortByDesc('profile_id')->first()['quality'],
													'pid'=> (string) $details->filter(function($detail, $key){ return $detail['quality'] == 'sd'; })->sortByDesc('profile_id')->first()['profile_id']
												],
												'duration' => $exercise->duration?bcdiv($exercise->duration,60).' min':'',
												'cat' =>  (string) $program->id,
												'item' =>  (string) $session->id,
												'parent' =>  (string) $super_program->id,
											];
											$exercises_videos->push($exercises_video);
											return $exercises_video;
										})
									]
								];
							})
						],
					];
				})
			];
		});
		return response()->json([
			'success' => true,
			'data' => [
				'category' => $categories,
				'videos' => $exercises_videos
			]
			], 200);
	}

	public function getWeFlowCategories()
	{
		$category = Category::with('subcategories')->find(4);

		
		return response()->json([
			'success' => true,
			'data' => [
				'main' => [
					'image'=>config('app.url').'/'.'image/categories/'.$category->id.'/image/0_main_image.jpg',
				],
				'categories' => $category->subcategories->map(function($sub_category){
					return [
						'id' => (string) $sub_category->id,
						'name' => $sub_category->name,
						'description' => $sub_category->description ?: "",
						'image' => [
							'large'=>config('app.url').'/'.'image/subcategories/'.$sub_category->id.'/image/0_main_image.jpg',
							'wide'=>config('app.url').'/'.'image/subcategories/'.$sub_category->id.'/image/0_main_image.jpg',
						]
					];
				})
			]
		]);
	}


	public function getBikiniCategories()
	{
		$category = Category::with('subcategories')->find(3);
		return response()->json([
			'success' => true,
			'data' => $category->subcategories->map(function($sub_category){
				return [
					'id' => (string) $sub_category->id,
					'name' => $sub_category->name,
					'description' => $sub_category->description,
					'image'=>config('app.url').'/'.'image/subcategories/'.$sub_category->id.'/image/0_main_image.jpg',
				];
			})
		]);
	}
}