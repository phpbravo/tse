<?php

namespace App\Http\Controllers\API\v0;

use App\FailedMobileSubscription;
use App\Handlers\LegacyAppstoreHandler;
use App\Http\Controllers\Controller;
use App\Offer;
use App\Purchase;
use App\Subscription;
use App\Transaction;
use App\User;

class MobileController extends Controller
{
    public function notify()
    {
        $task = request()->get('task');

        if ($task == 'api.subscribeuser')
        {
            return $this->subscription_success();
        }
        elseif ($task == 'api.subscriptionfail')
        {
            return $this->subscription_failure();
        }
        elseif ($task == 'api.syncsubscription')
        {
            return $this->subscription_sync();
        }
    }

    public function subscription_sync()
    {
        $handler = new LegacyAppstoreHandler();
        $receipt_data = $this->getReceiptData();
        $user = $this->getUser();

        $handler->syncSubscription($receipt_data, $user->id);

        return response()->json([
            'status' => 'subscription_recovered'
        ]);
    }

    public function subscription_success()
    {
        $user = $this->getUser();

        $offer = $this->getOffer();

        $subscription_token = request()->get('transactionId');
        $gateway_id = $this->getGatewayId();

        $transaction_token = request()->get('purchaseToken');

        $transaction_amount = $this->getTransactionAmount();

        $transaction_date = request()->get('startDate');

        $subscription = Subscription::createActive(
            $user,
            $offer,
            $subscription_token,
            $gateway_id
        );

        $transaction = Transaction::createSucceededOfferSubscription(
            $subscription,
            $transaction_token,
            $transaction_amount,
            0,
            'EUR',
            $transaction_date
        );

        $purchase = Purchase::createFromTransaction($transaction);

        return response()->json([
            'success' => 1,
            'data'=> $purchase
        ]);
    }

    public function subscription_failure()
    {
        $user = $this->getUser();
        $gateway_id = $this->getGatewayId();
        $offer = $this->getOffer();
        $vendor_token = $this->getVendorToken();
        $error_message = $this->getErrorMessage();

        FailedMobileSubscription::create([
            'user_id' => $user->id,
            'gateway_id' => $gateway_id,
            'offer_id' => $offer->id,
            'vendor_token' => $vendor_token,
            'error' => $error_message,
        ]);

        return response()->json([
            'success' => 1,
            'data' => 'Subscription reported to failed'
        ]);
    }

    public function getUser()
    {
        $user_id =
            request()->get('userId') ?:
            request()->get('userid') ?:
            request()->get('user_id');

        $user = User::find($user_id);

        return $user;
    }

    public function getVendorToken()
    {
        $vendor_token = request()->get('vid');

        return $vendor_token;
    }

    public function getErrorMessage()
    {
        $error_message = request()->get('error');

        return $error_message;
    }

    public function getOffer()
    {
        $offer_token =
            request()->get('productId') ?:
            request()->get('pid');

        $is_android = request()->get('isAndroid') == '1';

        if ($is_android)
        {
            $offer = Offer::fromPlaystoreToken($offer_token);
        }
        else
        {
            $offer = Offer::fromAppstoreToken($offer_token);
        }

        return $offer;
    }

    public function getGatewayId()
    {
        $is_android = request()->get('isAndroid') == '1';

        if ($is_android)
        {
            $gateway_id = 4;
        }
        else
        {
            $gateway_id = 3;
        }

        return $gateway_id;
    }

    public function getTransactionAmount()
    {
        $gross_amount_string = request()->get('grossAmount');

        $gross_amount_int = (int) preg_replace(
            '/\D/',
            '',
            $gross_amount_string
        );

        return $gross_amount_int;
    }

    public function getReceiptData(): ?string
    {
        $receipt_data = request()->get('receipt_data');

        return $receipt_data;
    }
}