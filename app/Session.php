<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
	];

	protected $fillable=[
		'name',
		'title',
		'description',
		'content',
		'is_bonus',
	];

	public $filterable_columns=[
		'name',
		'title',
		'is_bonus'
	];
	
	public function exercise_sets(){
		return $this->hasMany('App\ExerciseSet');
	}

	public function exercise_assignments(){
		return $this->hasMany('App\ExerciseAssignment');
	}

	public function programs(){
		return $this->belongsToMany('App\Program');
	}


}
