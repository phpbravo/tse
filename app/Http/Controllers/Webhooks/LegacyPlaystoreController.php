<?php

namespace App\Http\Controllers\Webhooks;

use App\Gateway;
use App\WebhookEvent;

class LegacyPlaystoreController
{

    public function handle()
    {
        $pub_sub_payload =
            json_decode(@file_get_contents('php://input') ?:
            request()->getContent());

        $payload = json_decode(base64_decode($pub_sub_payload->message->data));

        $payload->event_type = 'legacy_'.$this->getEventType($payload);

        $url=request()->url();

        WebhookEvent::create([
            'gateway_id' => Gateway::PLAYSTORE,
            'url' => $url,
            'data' => $payload
        ]);

        return response()->json([
            'message' => 'success',
            'event' => $payload->event_type
        ]);
    }

    public function getEventType(object $payload): string
    {
        switch($payload->notificationType)
        {
            case 1:
                return 'SUBSCRIPTION_RECOVERED';

            case 2:
                return 'SUBSCRIPTION_RENEWED';

            case 3:
                return 'SUBSCRIPTION_CANCELED';

            case 4:
                return 'SUBSCRIPTION_PURCHASED';

            case 5:
                return 'SUBSCRIPTION_ON_HOLD';

            case 6:
                return 'SUBSCRIPTION_IN_GRACE_PERIOD';

            case 7:
                return 'SUBSCRIPTION_RESTARTED';

            case 8:
                return 'SUBSCRIPTION_PRICE_CHANGE_CONFIRMED';

            case 9:
                return 'SUBSCRIPTION_DEFERRED';

            case 10:
                return 'SUBSCRIPTION_PAUSED';

            case 11:
                return 'SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED';

            case 12:
                return 'SUBSCRIPTION_REVOKED';

            case 13:
                return 'SUBSCRIPTION_EXPIRED';

            default:
                return 'UNKNOWN_EVENT';
        }
    }

}