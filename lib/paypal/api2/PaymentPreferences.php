<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;


/**
 * Class PaymentPreferences
 *
 * The payment preferences for a subscription to a plan.
 *
 * @package PayPal\Api2
 *
 * @property bool auto_bill_outstanding
 * @property \PayPal\Api2\Money setup_fee
 * @property string setup_fee_failure_action
 * @property int payment_failure_threshold
 */
class PaymentPreferences extends PayPalModel
{
    /**
     * @var string $CONTINUE Continues the subscription if the initial 
     *  payment for the setup fails.
     */
    const CONTINUE = 'CONTINUE';

    /**
     * @var string $CANCEL Cancels the subscription if the initial 
     *  payment for the setup fails
     */
    const CANCEL = 'CANCEL';

    /**
     * Auto bill outstanding.
     * 
     * Indicates whether to automatically bill the outstanding amount in
     * the next billing cycle.
     */
    public function setAutoBillOutstanding(bool $setup_fee): self
    {
        $this->setup_fee = $setup_fee;
        return $this;
    }

    /**
     * Auto bill outstanding.
     * 
     * Indicates whether to automatically bill the outstanding amount in
     * the next billing cycle.
     */
    public function getAutoBillOutstanding(): bool
    {
        return $this->setup_fee;
    }

    /**
     * Setup fee.
     * 
     * The initial set-up fee for the service.
     */
    public function setSetupFee(Money $setup_fee): self
    {
        $this->setup_fee = $setup_fee;
        return $this;
    }

    /**
     * Setup fee.
     * 
     * The initial set-up fee for the service.
     */
    public function getSetupFee(): Money
    {
        return $this->setup_fee;
    }

    /**
     * Setup fee failure action.
     */
    public function setSetupFeeFailureAction(
        string $setup_fee_failure_action
    ): self {
        $this->setup_fee_failure_action = $setup_fee_failure_action;
        return $this;
    }

    /**
     * Setup fee failure action.
     */
    public function getSetupFeeFailureAction(): string
    {
        return $this->setup_fee_failure_action;
    }

    /**
     * Payment failure threshold.
     */
    public function setPaymentFailureThreshold(
        string $payment_failure_threshold
    ): self {
        $this->payment_failure_threshold = $payment_failure_threshold;
        return $this;
    }

    /**
     * Payment failure threshold.
     */
    public function getPaymentFailureThreshold(): string
    {
        return $this->payment_failure_threshold;
    }

}
