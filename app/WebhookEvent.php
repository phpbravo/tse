<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebhookEvent extends Model
{
	protected $dispatchesEvents = [
		'created' => Events\WebhookEventCreated::class,
  ];
  
  protected $fillable=[
    'gateway_id',
    'url',
    'data'
  ];

  protected $casts=[
    'data'=>'json'
  ];
  
}
