<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ACCUEIL

Route::get('/images','FilesController@fetch');


// TERMS AND CONDITIONS

Route::get('/conditions-generales-d-utilisation-et-de-vente', 'Web\PagesController@terms_and_conditions')
->name('front.terms');

// CONCEPT

Route::get('/concept/tout-savoir', 'Web\PagesController@toutsavoir')
->name('front.concept');

// TÈMOIGNAGES

Route::get('/temoignages', 'Web\PagesController@temoignages')
->name('front.testimonies');


// WE TRAIN

Route::get('/programme', 'Web\PagesController@program')
->name('front.program');

Route::get('/we-train/videos', 'Web\PagesController@weTrainVideos')
->name('front.we-train.videos');

Route::get('/we-train/videos-bonus', 'Web\PagesController@weTrainBonus')
->name('front.we-train.bonus');

Route::get('/we-train/faq', 'Web\PagesController@weTrainFaq')
->name('front.we-train.faq');

Route::get('/we-train/le-concept-we-train', 'Web\PagesController@concept')
->name('front.we-train.concept');

// WE SWEAT

Route::get('/we-sweat/tous-les-training', 'Web\PagesController@seancesvideos')
->name('front.we-sweat.videos');

Route::get('/we-sweat/we-talk', 'Web\PagesController@wetalk')
->name('front.we-sweat.faq');

Route::get('/we-sweat/le-concept', 'Web\PagesController@leconcept')
->name('front.we-sweat.concept');

// WE EAT

Route::get('/we-eat/mes-recettes-fit','Web\PagesController@recipes')
->name('front.we-eat.recipes');

Route::get('/we-eat/mes-recettes-fit/{id}', 'Web\PagesController@getRecipe')
->name('front.we-eat.show-recipe');

Route::get('/ebook', 'Web\PagesController@ebook')
->name('front.ebook');

// NOUS

Route::get('/nous/sissy', 'Web\PagesController@sissy')
->name('front.nous.sissy');

Route::get('/nous/tini', 'Web\PagesController@tini')
->name('front.nous.tini');

Route::get('/nous/notre-rencontre', 'Web\PagesController@recontre')
->name('front.nous.notre-recontre');

//VIDEO 
Route::get('/video/{id}', 'Web\PagesController@getSingleVideo')
->name('front.single_video');

// ACCES

Route::group(['middleware'=>['guest']],function(){

    Route::get('/acces/inscription','Web\PagesController@register')
    ->name('front.register');
    
    Route::get('/acces/connexion','Web\PagesController@login')
    ->name('front.login');
    
    Route::post('/acces/motddepasse/email','Auth\ForgotPasswordController@sendResetLinkEmail')
    ->name('password.email');
    
    Route::get('/acces/motddepasse/reset','Auth\ForgotPasswordController@showLinkRequestForm')
    ->name('password.request');

    Route::post('/acces/motddepasse/reset','Auth\ResetPasswordController@reset')
    ->name('password.update');

    Route::get('/acces/motddepasse/reset/{token}','Auth\ResetPasswordController@showResetForm')
    ->name('password.reset');

});

Route::group(['middleware'=>['auth']],function(){

    
    Route::get('/access/moncompte', 'Web\PagesController@moncopte')
    ->name('front.account-subscriptions');

    // PAYMENT

    Route::get('/selectoffers','Web\PagesController@selectoffers')
    ->name('front.select-offer');

    Route::get('/payment_method/{id}','Web\PagesController@payment_method')
    ->name('front.payment');

    Route::get('/ebook/{id}/download/{version}','Web\EbooksController@downloadEbook')
    ->name('front.download-ebook');

});

