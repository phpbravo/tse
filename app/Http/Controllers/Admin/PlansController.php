<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Plan;

class PlansController extends Controller
{

	private function validatePlan($fields){
		$objectHandler=new ObjectHandler(Plan::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Plan::class);
		$pageHandler=new PageHandler(Plan::class);
		$objectHandler=new ObjectHandler(Plan::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validatePlan($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$offer=Plan::create([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
		]);

		if (isset($fields->sessions) && is_array($fields->sessions)){

			$sessions=array_map(function($session){
				return $session['id'];
			},$fields->sessions);

			$offer->sessions()->sync($sessions);

		}

		return $offer;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validatePlan($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$offer=Plan::find($id);

		if(!$offer){
			return (new ObjectHandler(Plan::class))->newError('not_found');
		}

		$offer->update([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
		]);

		if (isset($fields->sessions) && is_array($fields->sessions)){

			$sessions=array_map(function($session){
				return $session['id'];
			},$fields->sessions);

			$offer->sessions()->sync($sessions);

		}

		return $offer;
	}

	public function show($id){

		$offer=Plan::with(['sessions'])->find($id);

		if(!$offer){
			return (new ObjectHandler(Plan::class))->newError('not_found');
		}

		return $offer;
	}

	public function destroy($id){

		$offer=Plan::find($id);

		if(!$offer){
			return (new ObjectHandler(Plan::class))->newError('not_found');
		}

		$offer->delete();

		return (new ObjectHandler(Plan::class))->newSuccess();
	}

	public function list(){

		return Plan::with('products')->get();

	}

}
