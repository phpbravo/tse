<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Train Sweat Eat</title>
	<meta name="description" content="Sculpte et muscle toi avec Trainsweateat et Bikini avec Sissy, Le programme N°1 en France !">
	<script rel="preload" src="/js/webp_detect.js"></script>
	<link rel="stylesheet" media="screen, print" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" media="screen, print" type="text/css" href="/assets/css/all.css">
	<link rel="stylesheet" media="screen, print" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" media="screen, print" type="text/css" href="/assets/css/custom-style.css?v={{filever('public/assets/css/custom-style.css')}}">  
	<link rel="stylesheet" media="screen, print" type="text/css" href="/assets/css/custom-style1.css?v={{filever('public/assets/css/custom-style1.css')}}">
	<link rel="stylesheet" media="screen, print" type="text/css" href="/assets/css/ebook.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="cache-control" content="no-cache">
	@yield('header')
</head>
<body>
@yield('content')
@include('frontend.includes.footer')
<script type="text/javascript" src="{{ mix('web/app.js', config('app.manifest_path')) }}"></script>
<script type="text/javascript" src="/assets/js/custom-script.js?v={{filever('public/assets/js/custom-script.js')}}"></script>
@yield('footer')
</body>
</html>