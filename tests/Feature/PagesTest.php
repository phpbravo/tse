<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test the home page
     *
     * @return void
     */
    public function testHomepageWorks()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText("L'application");
        $response->assertSeeText("We train");
        $response->assertSeeText("We sweat");
        $response->assertSeeText("We eat");
    }

    /**
     * Test the concept page
     * 
     * @return void
     */
    public function testConceptPage(){
        $response = $this->get('/concept/tout-savoir');

        $response->assertStatus(200);
        $response->assertSeeText("LE CONCEPT");
        $response->assertSeeText("TRAIN SWEAT EAT");
        $response->assertSeeText("WE TRAIN");
        $response->assertSeeText("WE SWEAT");
        $response->assertSeeText("WE EAT");
        $response->assertSeeText("S'INSCRIRE");
    }
}
