<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$accepted_endpoints=[
    'subscriptions',
    'purchases',
    'transactions',
    'publishing',
    'offers',
    'products',
    'programs',
    'sessions',
    'categories',
    'exercises',
    'recipes',
    'videos',
    'pages',
    'users',
    'gateways',
    'blogs'
];

$accepted_endpoints=implode('|',$accepted_endpoints);

Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'AdminAuth\LoginController@login');

Route::get('/files','FilesController@fetch')
->middleware('admin.auth');

Route::get('/app.js',function(){
    return response()->file(base_path('storage/app/resources/admin/app.js'),['Content-Type'=>'application/javascript']);
})
->middleware('admin.auth')
->name('admin.js');
Route::get('/app.css',function(){
    return response()->file(base_path('storage/app/resources/admin/app.css'),['Content-Type'=>'text/css']);
})
->middleware('admin.auth')
->name('admin.css');

Route::get('/{url?}', function () {
    return view('admin.app');
})->where(['url' => $accepted_endpoints ])->middleware('admin.auth');
