<?php

namespace App\Http\Controllers\API\v0;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};
use App\LegacyRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class Router extends Controller
{

	private function subRoute($route,$params=[]){
		return preg_replace('/\/index.php/','',route($route,$params));
	}

	public function route(){
		$task=request()->get('task');

		auth()->shouldUse('api');

		$this->monitorRequest(request()->fullUrl());

		switch ($task) {
			case 'test':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.test'),
					'GET'
				));
				break;

			case 'api.subscribeuser':
			case 'api.subscriptionfail':
			case 'api.syncsubscription':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.mobile.notify'),
					'GET'
				));
				break;

			case 'api.login':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.self'),
					'GET'
				));
				break;

			case 'api.registerUsers':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.register'),
					'GET'
				));
				break;

			case 'api.sessions':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.current_program'),
					'GET'
				));
			break;

			case 'api.sessionvideos':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.session',['id'=>request()->get('catid')]),
					'GET'
				));
			break;

			case 'api.weflow':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.we_flow_categories'),
					'GET'
				));
			break;

			case 'api.videos':
				$catid=request()->get('catid');

				if ($catid=='6'){
					return Route::dispatch(Request::create(
						$this->subRoute('api.v0.we_train.index'),
						'GET'
					));
				}else if($catid=='15'){
					return Route::dispatch(Request::create(
						$this->subRoute('api.v0.we_sweat.index'),
						'GET'
					));
				}else if($catid=='92'){
					return Route::dispatch(Request::create(
						$this->subRoute('api.v0.we_flow'),
						'GET'
					));
				}else{
					return Route::dispatch(Request::create(
						$this->subRoute('api.v0.bikini.videos'),
						'GET'
					));
				}
			break;

			case 'api.getCategories':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.we_sweat.categories'),
					'GET'
				));
			break;

			case 'api.getNutrition':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.we_eat_nutritions'),
					'GET'
				));
			break;

			case 'api.recipies': //intentional misspell
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.we_eat.index'),
					'GET'
				));
			break;

			case 'api.bikini':
				return Route::dispatch(Request::create(
					$this->subRoute('api.v0.bikini.index'),
					'GET'
				));
			break;
			
			default:
				# code...
				break;
		}

	}

	private function monitorRequest(string $endpoint)
	{
		LegacyRequest::create([
			'endpoint' => $endpoint,
			'ip' => request()->ip()
		]);
	}
}
