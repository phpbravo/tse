<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_id')->nullable();
                $table->foreign('resource_id')->references('id')->on('resources');
            $table->unsignedBigInteger('category_id');
                $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('subcategory_id')->nullable();
                $table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('set null');
            $table->unsignedBigInteger('video_id')->nullable();
                $table->foreign('video_id')->references('id')->on('videos')->onDelete('set null');
            $table->string('name')->index();
            $table->string('description')->nullable();
            $table->text('content')->nullable();
            $table->integer('duration')->nullable()->index();
            $table->integer('difficulty')->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
