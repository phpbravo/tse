var program_app= new Vue({
	el:'#program-app',
	data:{
		program:{
			title:'',
			description:'',
			content:'',
			sessions:[],
			active_schedule:{
				from:'',
				to:''
			},
		}
	},
	computed:{
		program_date_range(){
			from_date=new Date(Date.parse(this.program.active_schedule.from))
			.toLocaleDateString('fr-FR',{
				formatMatcher:'basic',
				day:'numeric',
				month:'short'
			})
			to_date=new Date(Date.parse(this.program.active_schedule.to))
			.toLocaleDateString('fr-FR',{
				formatMatcher:'basic',
				day:'numeric',
				month:'short'
			})
			return 'du '+from_date+' au '+to_date
		}
	},
	methods:{
		getCurrentProgram(){
			this.loading=true
			axios.get('/api/programs/published/current')
			.then(res=>{
				this.program=res.data
			})
			.catch(error=>{
				this.program.title="Pas de programme aujourd'hui"
			})
			.finally(()=>{
				document.getElementById('loading-icon').remove()
			})
		}
	},
	mounted(){
		document.getElementById('program-app').className="program-wrapper"
	},
	created(){
		this.getCurrentProgram();
	}
})