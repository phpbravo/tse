<section class="parallax-section" id="faqparallax">
		<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="inner-parallax-section text-center">
							<div class="parallax-content">
								<h1>ABONNE <span>TOI !</span></h1>
								<h4>ET REJOINS-NOUS !</h4>

								<div class="parallax-checklist">
									<ul class="list-group">
										<li><i class="fa fa-check" aria-hidden="true"></i>Accès au training du jour we train
										</li>
										<li><i class="fa fa-check" aria-hidden="true"></i>Accès aux vidéos live we sweat
										</li>
										<li><i class="fa fa-check" aria-hidden="true"></i>Accès aux recettes fit we eat
										</li>
										<li><i class="fa fa-check" aria-hidden="true"></i>Et à toutes les prochaines surprises !
										</li>
									</ul>
								</div>

								<a href="{{ !auth('web') ? route('front.login') : route('front.select-offer') }}" class="btn btn-primary custombtn"><span>S'INSCRIRE !</span><i class="fa fa-user-plus" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
</section>