<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
	];

	// attributes that are mass-assignable
	public $fillable = [
		'name',
		'title',
		'content',
	];

	// attributes that can be searched and filtered
	public $filterable_columns = [
		'name',
		'title',
	];

	public function tags(){
		return $this->belongsToMany('App\Tag');
	}

	public function tag($tag_name){
		
		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first()?:Tag::create(['name'=>$tag_name]);

		$this->tags()->syncWithoutDetaching($tag);

		return $this;
	}

	public function untag($tag_name){

		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first();
		if(!$tag) return $this;

		$this->tags()->detach($tag);

		return $this;
	}
	
}
