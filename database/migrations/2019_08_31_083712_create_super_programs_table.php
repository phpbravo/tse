<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperProgramsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('super_programs', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('resource_id')->nullable();
				$table->foreign('resource_id')->references('id')->on('resources');
			$table->string('name')->index();
			$table->string('title')->nullable()->index();
			$table->string('description')->nullable();
			$table->text('content')->nullable();
			$table->timestamps();
			$table->index('created_at');
			$table->index('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('super_programs');
	}
}
