const vx_currentUser={
    namespaced:false,
    state:{
        logged_in:false,
        id:0,
        name:'',
        email:''
    },
    mutations:{
        login(state, user){
            state.logged_in=true;
            state.id=user.id;
            state.name=user.name;
            state.email=user.email;
        },
        logout(state){
            state.logged_in=false;
            state.id=0,
            state.name='',
            state.email=''
        }
    }
}

export default vx_currentUser;