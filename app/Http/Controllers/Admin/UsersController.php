<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\User;

class UsersController extends Controller
{
    public function index(){
        $filterHandler=new FilterHandler(User::class);
        $pageHandler=new PageHandler(User::class);
        $objectHandler=new ObjectHandler(User::class);

        $filters=request()->filters;
        $search=request()->search;
        $perPage=request()->perPage;
        $page=request()->page;
        $orderBy=request()->orderBy;
        $ascending=request()->ascending;

        $filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

        $page=$pageHandler->getPage($filtered_query,$perPage,$page);

        $page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

        return $page_object;
    
    }

	public function show($id){

        $pageHandler=new PageHandler(User::class);

        $user=User::with(['purchases'])->find($id);

        $subscriptions=$pageHandler->getPage($user->subscriptions()->with('offer'),5,request()->get('page_subscriptions'));
        $transactions=$pageHandler->getPage($user->transactions()->with('subscription','offer'),10,request()->get('page_transactions'));
        
        $user->subscriptions=$subscriptions;
        $user->transactions=$transactions;

		if(!$user){
			return (new ObjectHandler(User::class))->newError('not_found');
		}

		return $user;
    }
    
    // returns an array of autocomplete matches
    public function autocomplete()
    {
        $search=mb_strtolower(trim(request()->search));
        $ignore=request()->ignore?:[];

        if (!$search) return json_encode((object)[]);

        $tags_matched=User::where('email','like','%'.$search.'%')
            ->orWhere('username','like','%'.$search.'%')
            ->orderBy(DB::raw('length(email)'))
            ->whereNotIn('id',$ignore)
            ->take(5)
            ->get();

        return $tags_matched;
    }
}
