@extends('frontend.includes.layout')
@section('header')
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@endsection
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	<div class="registration-wrapper d-none" data-offer_id="{{$offers_id}} " id="payment-app">
		<transition name="fade">
			<div v-if="loading" class="loading-overlay full-overlay"><div class="loader4"></div></div>
		</transition>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="registration-inner-wrapper">
						<div class="warning-alert">
							<div v-for="error in errors" class="alert alert-warning alert-dismissible fade show position-relative" role="alert">
								<strong style="text-transform:Capitalize;">Erruer : </strong>
								@{{error}}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
						<form method="post" id="payment-method-form">
							
							<h2>Paiment de l'abonnement <span class="text-uppercase">{{$offer_name}}</span></h2>
							<div class="form-group form-row" id="field_email">
								<div class="col-md-3 form-control-label">
					    			<label>Méthode de paiement<span class="star">&nbsp;*</span></label>
					    		</div>
					    		<div class="col-md-9">
					    			<div class="choose_payment">
						    			<div class="option stripe-option">
					    					<input type="radio" name="payment_method" id="paiment_par_carte" required value="0" v-model="selected_payment_method">
					    					<label for="paiment_par_carte">Paiement par carte</label>
					    				</div>
					    				<div class="option paypal-option">
					    					<input type="radio" name="payment_method" id="paypal" required value="1" v-model="selected_payment_method">
					    					<label for="paypal">Paypal</label>
					    				</div>
					    			</div>
				    			</div>
							</div>
							<div v-show="selected_payment_method == 0" class="form-row stripe method_payment">
								<div class="col-md-3 form-control-label stripe-inner">
									<label for="card-element">
										Credit or debit card<span class="star">&nbsp;*</span>
									</label>
								</div>
								<div class="col-md-9">
									<div id="card-element">
									</div>
								</div>
								<div class="col-md-3">
								</div>
								<div class="col-md-9">
									<div id="card-errors" role="alert"></div>
								</div>
								<div class="col-12">
									<button @click="payWithStripe" type="button" class="btn btn-primary float-right btn-payment-method">
										<span>&nbsp;Procéder à l'inscription</span>
									</button>
								</div>
							</div>
									
							<div v-show="selected_payment_method == 1" class="form-row paypal method_payment">
								<div class="col-md-3 form-control-label stripe-inner">
									<label for="card-element">
										PayPal<span class="star">&nbsp;*</span>
									</label>
								</div>
								<div class="col-md-9">
										<button @click="payWithPaypal" type="button" class="mt-0 btn btn-primary float-left btn-payment-method">Payer avec PayPal</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')

</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon', {action: 'homepage'}).then(function(token) {
    	// console.log(token);
    });
});
</script>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/payment-method.js?v={{filever('public/assets/js/payment-method.js')}}"></script>
@endsection