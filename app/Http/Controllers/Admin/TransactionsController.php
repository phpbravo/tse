<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Transaction;

class TransactionsController extends Controller
{
    public function index()
    {
        $filterHandler = new FilterHandler(Transaction::class);
        $pageHandler = new PageHandler(Transaction::class);
        $objectHandler = new ObjectHandler(Transaction::class);

        $filters = request()->filters;
        $search = request()->search;
        $perPage = request()->perPage;
        $page = request()->page;
        $orderBy = request()->orderBy;
        $ascending = request()->ascending;

        $filtered_query = $filterHandler
            ->getFilteredQuery($filters, $search, $orderBy, $ascending);

        $filtered_query->with([
            'offer' => function ($query){
                $query->select(['id','name']);
            },
            'gateway' => function($query){
                $query->select(['id','name']);
            },
        ]);        

        $page=$pageHandler
            ->getPage($filtered_query, $perPage, $page);

        $page_object=$objectHandler
            ->newPage(
                $page->items,
                $page->current_page,
                $page->last_page,
                $page->total_item_count
            );

        return $page_object;
    }

    public function show($id)
    {
        $transaction=Transaction::with([
            'offer' => function ($query){
                $query->select(['id','name']);
            },
            'gateway' => function($query){
                $query->select(['id','name']);
            },
            'purchase' => function($query){
                $query->select(['id','transaction_id']);
            },
            'user' => function($query){
                $query->select(['id','email','name','surname','phone','created_at']);
            },
        ])->find($id);

		if(!$transaction){
			return (new ObjectHandler(Transaction::class))->newError('not_found');
		}

		return $transaction;
    }
}
