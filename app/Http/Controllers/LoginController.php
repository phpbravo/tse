<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{

    use AuthenticatesUsers {
        login as protected mainLogin;
    }

    public function login(Request $request){
        try{
            $this->mainLogin($request);
            return auth()->user();
        }catch(ValidationException $e){
            return json_encode($e->errors());
        }
    }
}
