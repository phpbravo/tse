<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * A base-64 encoded receipt from the app store
 * 
 * @property-read int $id
 * @property-read int $transaction_id
 * @property string|null $vendor_token
 * @property string $original_transaction_token
 * @property string $transaction_token
 * @property string $content
 * @property object $parsed_content attribute returned
 * by getParsedContentAttribute()
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class AppstoreReceipt extends Model
{
	// attributes that are mass-assignable
	public $fillable = [
		'user_id',
		'transaction_id',
		'vendor_token',
		'original_transaction_token',
		'transaction_token',
		'content',
	];

	// attributes that can be searched and filtered
	public $filterable_columns = [
		'user_id',
		'transaction_id',
		'original_transaction_token',
		'transaction_token',
	];

	// Simple Relations

	public function transaction(): BelongsTo
	{
		return $this->belongsTo(Transaction::class);
	}

	// Attributes

	public function getParsedContentAttribute()
	{
		return json_decode(
			gzinflate(
				$this->content
			)
		);
	}

	// Static Methods

	public static function getFromOriginalTransactionToken(
		string $original_transaction_token,
		string $transaction_token
	): ?self {
		return $this->query()
			->where(
				'original_transaction_token',
				$original_transaction_token
			)
			->where(
				'transaction_token',
				$transaction_token
			)
			->first();
	}

	public static function getFromTransactionToken(
		string $transaction_token
	): ?self {
		return $this->query()
			->where(
				'transaction_token',
				$transaction_token
			)
			->first();
	}
}
