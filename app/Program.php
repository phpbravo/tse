<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
	];

	protected $fillable=[
		'name',
		'title',
		'description',
		'content',
	];

	public $filterable_columns=[
		'name',
		'title',
	];
    
	public function sessions(){
		return $this->belongsToMany('App\Session')->withPivot('session_order');
	}

	public function program_schedules()
	{
			return $this->hasMany('App\ProgramSchedule');
	}

}
