export default {
 methods: {
  formatDateFR(date){
    try {
        if (date){
            let formatter = new Intl.DateTimeFormat('fr-FR')
            return formatter.format(new Date(date))
        } else {
            return '';
        }
    }catch {
        return "";
    }
  
  },
  formatNumberFR(number){
   if (number){
    let formatter = new Intl.NumberFormat('fr',{minimumFractionDigits:2})
    return formatter.format(number)
   } else {
    return '';
   }
  },
  formatHundredFR(number){
   return this.formatNumberFR(number/100)
  }
 }
}