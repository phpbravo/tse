<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Handlers\PaypalHandler;

class CreatePaypalOffer implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $offer;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($offer)
	{
		$this->offer=$offer;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(PaypalHandler $handler)
	{
		$handler->createGatewayOffer($this->offer);
	}
}
