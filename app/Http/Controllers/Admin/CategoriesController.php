<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Category;

class CategoriesController extends Controller
{

	private function validateCategory($fields){
		$objectHandler=new ObjectHandler(Category::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if (!isset($fields->category_id) || $fields->category_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Catégorie est obligatoire'
			);
		}

		if (!isset($fields->subcategory_id) || $fields->subcategory_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Sous-catégorie est obligatoire'
			);
		}

		if(!\App\Category::find($fields->category_id)){
			return $objectHandler->newError(
				'invalid_request',
				'La catégorie est invalide.'
			);
		}
		
		if(!\App\Subcategory::where('category_id',$fields->category_id)->find($fields->subcategory_id)){
			return $objectHandler->newError(
				'invalid_request',
				'La sous-catégorie est invalide.'
			);
		}
		
		return false;
	}

	public function list(){
		
		$with=request()->get('with');
		$query=with(new Category)->newQuery();

		switch (gettype($with)){
			case 'NULL':
				break;
			case 'string':
			case 'array':
				$query->with($with);
				break;
		}

		return $query->get();
	}

	public function index(){
		$filterHandler=new FilterHandler(Category::class);
		$pageHandler=new PageHandler(Category::class);
		$objectHandler=new ObjectHandler(Category::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('subcategories');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateCategory($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$category=Category::create([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_link'=>$fields->video_link,
			'api_video_link'=>$fields->api_video_link,
		]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$category->tag($tag);
			}
		}

		return $category;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateCategory($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$category=Category::find($id);

		if(!$category){
			return (new ObjectHandler(Category::class))->newError('not_found');
		}

		$category->update([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_link'=>$fields->video_link,
			'api_video_link'=>$fields->api_video_link,
		]);

		$category->tags()->sync([]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$category->tag($tag);
			}
		}

		return $category;
	}

	public function show($id){

		$category=Category::with(['tags'])->find($id);

		if(!$category){
			return (new ObjectHandler(Category::class))->newError('not_found');
		}

		return $category;
	}

	public function destroy($id){

		$category=Category::with(['tags'])->find($id);
		
		if(!$category){
			return (new ObjectHandler(Category::class))->newError('not_found');
		}

		$category->delete();

		return (new ObjectHandler(Category::class))->newSuccess();
	}

	public function tag($id,$name){

		$category=Category::findOrFail($id);

		$category->tag($name);

		return $category;

	}

	public function untag($id,$name){

		$category=Category::findOrFail($id);

		$category->untag($name);

		return $category;

	}
	
	// returns an array of autocomplete matches
	public function autocomplete(){
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Category::where('name','like','%'.$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('id',$ignore)->take(5)->get();

		return $tags_matched;

  }

}
