<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Handlers\ObjectHandler;

use App\User;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	protected $guard = 'web';

	protected $username = 'username';

	/**
	 * Get the login username to be used by the controller.
	 *
	 * @return string
	 */
	public function username()
	{
		return $this->username;
	}

	public function showLoginForm()
	{
		return view('admin.login');
	}

	public function login(Request $request)
	{

		if (auth()->check()) return auth()->user();

		$login = request()->input('login');
 
		$fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
 
		request()->merge([$fieldType => $login]);
		
		if(auth('web')->attempt($request->only($fieldType,'password'),$request->filled('remember'))){
			//Authentication passed...
			return auth()->user();
		}
		//Authentication failed...
		return $this->loginFailed();
	}

	private function loginFailed(){

		$objectHandler=new ObjectHandler(User::class);

		return response(
			$objectHandler->newError('unauthorized',__('auth.failed')),
			401
		);
	}

	public function logout(){

		$objectHandler=new ObjectHandler(User::class);

		auth('web')->logout();

		return response(
			$objectHandler->newSuccess(),
			200
		);

	}

	private function validator(Request $request)
	{
		//validation rules.
		$rules = [
			'username' => 'required|exists:admins|min:5|max:191',
			'password' => 'required|string|min:4|max:255',
		];
		//validate the request.
		$request->validate($rules);
	}

	public function deleteAdmin($user_id){
		$user=User::findOrFail($user_id);
		$user->delete();
	}

	public function editAdmin($user_id){
		$user=User::findOrFail($user_id);
		if (request()->input('email')==$user->email)
			request()->offsetUnset('email');
		$validated=request()->validate([
				'email'=>'sometimes|required|unique:users|email',
				'password'=>'sometimes|required|same:password_confirmation',
			]);

		if(request()->get('password'))
			request()->offsetSet('password',bcrypt(request()->get('password')));
			
		$user->update(request()->all());
	}

	public function registerAdmin(){
		request()->validate([
			'surname'=>'required|string',
			'email'=>'required|unique:users|email',
			'password'=>'required|same:password_confirmation',
		]);
		
		request()->merge(['password'=>bcrypt(request()->password)]);
		if (!request()->input('username')){
			request()->merge(['username'=>request()->input('email')]);
		}
		if (!request()->input('agreed_terms')){
			request()->offsetSet('agreed_terms',0);
		}
		$user=User::create(request()->all());
		return $user;
	}

	public function register(){
		request()->validate([
			'username'=>'sometimes|unique:users|regex:/^[a-zA-Z0-9]{3,12}$/',
			'email'=>'required|unique:users|email',
			'password'=>'required|same:password_confirmation',
			'agreed_terms'=>'required'
		]);
		request()->merge(['password'=>bcrypt(request()->password)]);
		if (!request()->input('username')){
			request()->merge(['username'=>request()->input('email')]);
		}
		$user=User::create(request()->all());
		auth()->login($user);
		return $user;
	}

	public function checkEmailAvailability(){
		$email=request()->input('email');
		if (!$email) return 'no email';
		if (User::where('email',$email)->first()) return 'not available';
		return 'available';
	}

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}
}
