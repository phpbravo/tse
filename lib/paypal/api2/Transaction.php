<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

/**
 * Class Transaction
 *
 * Billing frequency details for a billing cycle
 *
 * @package PayPal\Api2
	* 
 *	@property string $status
 *	@property string $id
 *	@property \PayPal\Api2\AmountWithBreakdown $amount_with_breakdown
 *	@property \PayPal\Api2\Name $payer_name
 *	@property string $payer_email
 *	@property string $time
	* 
 */
class Transaction extends PayPalResourceModel
{
	const
		/**
			* @var string The funds for this captured payment were credited to 
			* the payee's PayPal account.
			*/
		COMPLETED = 'COMPLETED',
		/**
			* @var string The funds could not be captured.
			*/
		DECLINED = 'DECLINED',
		/**
			* @var string An amount less than this captured payment's amount was
			* partially refunded to the payer.
			*/
		PARTIALLY_REFUNDED = 'PARTIALLY_REFUNDED',
		/**
			* @var string The funds for this captured payment was not yet 
			* credited to the payee's PayPal account.
			*/
		PENDING = 'PENDING',
		/**
			* @var string An amount greater than or equal to this captured
			* payment's amount was refunded to the payer.
			*/
		REFUNDED = 'REFUNDED';
	
 /**
		* The status of the captured payment.
	 */
	public function setStatus(string $status): self
	{
		$this->status = $status;
		return $this;
	}

 /**
		* The status of the captured payment.
	 */
	public function getStatus(): string
	{
		return $this->status;
	}
	
 /**
		* The PayPal-generated transaction ID.
	 */
	public function setId(string $id): self
	{
		$this->id = $id;
		return $this;
	}

 /**
		* The PayPal-generated transaction ID.
	 */
	public function getId(): string
	{
		return $this->id;
	}
	
 /**
		* The breakdown details for the amount. Includes the gross, tax, fee,
		* and shipping amounts.
	 */
	public function setAmountWithBreakdown(
		AmountWithBreakdown $amount_with_breakdown
	): self {
		$this->amount_with_breakdown = $amount_with_breakdown;
		return $this;
	}

 /**
		* The breakdown details for the amount. Includes the gross, tax, fee,
		* and shipping amounts.
		*
		*	@return \PayPal\Api2\AmountWithBreakdown
	 */
	public function getAmountWithBreakdown(): AmountWithBreakdown
	{
		return $this->amount_with_breakdown;
	}
	
 /**
		* The name of the customer.
	 */
	public function setPayerName(
		Name $payer_name
	): self {
		$this->payer_name = $payer_name;
		return $this;
	}

 /**
		* The name of the customer.
		*
		* @return \PayPal\Api2\Name
	 */
	public function getPayerName(): Name
	{
		return $this->payer_name;
	}
	
 /**
		* The email ID of the customer.
	 */
	public function setPayerEmail(string $payer_email): self
	{
		$this->payer_email = $payer_email;
		return $this;
	}

 /**
		* The email ID of the customer.
	 */
	public function getPayerEmail(): string
	{
		return $this->payer_email;
	}
	
 /**
		* The date and time when the transaction was processed.
	 */
	public function setTime(string $time): self
	{
		$this->time = $time;
		return $this;
	}

 /**
		* The date and time when the transaction was processed.
	 */
	public function getTime(): string
	{
		return $this->time;
	}

	/**
     * Retrieve details for a transaction by passing 
     * the ID of the agreement to the request URI.
     */
    public static function get(
        string $transaction_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate(
            $transaction_id,
            'transaction_id'
        );
		$payLoad = "";
		$query = ['transaction_id' => $transaction_id];
        $json = self::executeCall(
            "/v1/reporting/transactions?". http_build_query($query),
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $transaction = new self;
        $transaction->fromJson($json);
        return $transaction;
    }
}
