<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler, FileHandler};

use App\{Session, ExerciseSet, ExerciseAssignment};

class SessionsController extends Controller
{

	private function validateSession($fields){
		$objectHandler=new ObjectHandler(Session::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Session::class);
		$pageHandler=new PageHandler(Session::class);
		$objectHandler=new ObjectHandler(Session::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page->items->makeHidden('content');

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateSession($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$session=Session::create([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'is_bonus'=>$fields->is_bonus,
		]);

		if (isset($fields->exercise_sets) && is_array($fields->exercise_sets)){

			$exercise_sets=array_filter(
				$fields->exercise_sets,
				function($exercise_set){
					return(
						count($exercise_set['exercise_assignments'])
					);
				});

				foreach ($exercise_sets as $exercise_set) {
					$exercise_set_model=ExerciseSet::create([
						'session_id'=>$session->id,
						'name'=>$exercise_set['name'],
						'order_index'=>$exercise_set['order_index'],
						'description'=>$exercise_set['description'],
					]);

					foreach ($exercise_set['exercise_assignments'] as $exercise_assignment){
						$exercise_assignment_model=ExerciseAssignment::create([
							'exercise_id'=>$exercise_assignment['exercise_id'],
							'exercise_set_id'=>$exercise_set_model->id,
							'session_id'=>$session->id,
							'order_index'=>$exercise_set['order_index'],
							'reps'=>$exercise_assignment['reps'],
							'tempo'=>$exercise_assignment['tempo'],
							'description'=>$exercise_assignment['description'],
						]);
					}
				}


		}

		
		if (
			isset($fields->image) && !is_null($fields->image) && $fields->image!='null'
			&& with(new \ReflectionClass($fields->image))->getShortName()=='UploadedFile'
		){
			$files = glob('storage/app/resources/sessions/'.$session->id.'/image/*'); // get all file names
			foreach($files as $file){ // iterate files
				if(is_file($file))
					unlink($file); // delete file
			}
			with(new FileHandler)->store($fields->image,'sessions/'.$session->id.'/image','main_image');
		}

		return $session;
	}

	public function update($id){
		
		$fields=(object)request()->all();

		$validation=$this->validateSession($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$session=Session::find($id);

		if(!$session){
			return (new ObjectHandler(Session::class))->newError('not_found');
		}

		$session->update([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'is_bonus'=>$fields->is_bonus,
			'image'=>$fields->image ?? null
		]);

		$session->exercise_sets()->delete();

		if (isset($fields->exercise_sets) && is_array($fields->exercise_sets)){

			$exercise_sets=array_filter(
				$fields->exercise_sets,
				function($exercise_set){
					return(
						count($exercise_set['exercise_assignments'])
					);
				});

				foreach ($exercise_sets as $exercise_set) {
					$exercise_set_model=ExerciseSet::create([
						'session_id'=>$session->id,
						'name'=>$exercise_set['name'],
						'order_index'=>$exercise_set['order_index'],
						'description'=>$exercise_set['description'],
					]);

					foreach ($exercise_set['exercise_assignments'] as $exercise_assignment){
						$exercise_assignment_model=ExerciseAssignment::create([
							'exercise_id'=>$exercise_assignment['exercise_id'],
							'exercise_set_id'=>$exercise_set_model->id,
							'session_id'=>$session->id,
							'order_index'=>$exercise_set['order_index'],
							'reps'=>$exercise_assignment['reps'],
							'tempo'=>$exercise_assignment['tempo'],
							'description'=>$exercise_assignment['description'],
						]);
					}
				}


		}

		
		if (
			isset($fields->image) && !is_null($fields->image) && $fields->image!='null'
			&& with(new \ReflectionClass($fields->image))->getShortName()=='UploadedFile'
		){
			$files = glob(base_path('/storage/app/sessions/'.$session->id.'/image/*')); // get all file names
			foreach($files as $file){ // iterate files
				if(is_file($file))
					unlink($file); // delete file
			}
			with(new FileHandler)->store($fields->image,'sessions/'.$session->id.'/image','main_image');
		}

		return $session;
	}

	public function show($id){

		$session=Session::with([
			'exercise_sets.exercise_assignments.exercise'
		])->find($id);

		if(!$session){
			return (new ObjectHandler(Session::class))->newError('not_found');
		}

		return $session;
	}

	public function destroy($id){

		$session=Session::find($id);

		if(!$session){
			return (new ObjectHandler(Session::class))->newError('not_found');
		}

		$session->delete();

		return (new ObjectHandler(Session::class))->newSuccess();
	}
	
	// returns an array of autocomplete matches
	public function autocomplete(){
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Session::with('exercise_sets.exercise_assignments.exercise')->where('name','like','%'.$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('id',$ignore)->take(5)->get();

		return $tags_matched;

  }

}
