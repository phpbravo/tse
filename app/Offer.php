<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property-read int $id
 * @property int|null $plan_id
 * @property string $name
 * @property string|null $title
 * @property string|null $description
 * @property float $price
 * @property float $tax_percent
 * @property string $duration
 * @property int|null $subscriber_limit
 * @property bool is_limited
 * @property bool is_for_upgrade
 * @property bool is_recurring
 * @property bool is_lifetime
 * @property bool is_public
 * @property bool is_available
 * @property bool is_stripe_available
 * @property bool is_paypal_available
 * @property string|null appstore_id
 * @property string|null playstore_id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
class Offer extends Model
{
	protected $dispatchesEvents = [
		'created' => Events\OfferCreated::class,
	];

	protected $guarded = [

	];

	protected $appends = [
		'plan_name'
	];

	protected $hidden = [

	];

	protected $casts = [

	];

	// Simple Relations

	public function plan()
	{
		return $this->belongsTo(Plan::class);
	}

	public function offer_schedules()
	{
		return $this->hasMany(OfferSchedule::class);
	}

	public function gateway_offers()
	{
		return $this->hasMany(GatewayOffer::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(Subscription::class);
	}

	public function users()
	{
	 return $this->belongsToMany(
			User::class,
			'subscriptions'
		);
	}

	// Scoped Relations

	public function active_subscriptions()
	{
		return $this->subscriptions()->active();
	}

	public function inactive_subscriptions()
	{
		return $this->subscriptions()->inactive();
	}

	// Attributes

	public function getUserCountAttribute()
	{
		return $this->users()->count();
	}

	public function getSubscriptionCountAttribute()
	{
		return $this->active_subscriptions()->count();
	}

	public function getActiveSubscriptionCountAttribute()
	{
		return $this->active_subscriptions()->count();
	}

	public function getInactiveSubscriptionCountAttribute()
	{
		return $this->inactive_subscriptions()->count();
	}

	public function getPlanNameAttribute()
	{
		if ($this->plan)
			return $this->plan->name;
		else
			return '';
	}

	public function getWhitelistedUserIdsAttribute()
	{
		return $this->whitelisted_users()->pluck('id');
	}

	public function getStripeTokenAttribute()
	{
		return GatewayOffer::where('offer_id',$this->id)
			->where('gateway_id',1)
			->first()->gateway_token;
	}

	public function getPaypalTokenAttribute()
	{
		return GatewayOffer::where('offer_id',$this->id)
			->where('gateway_id',2)
			->first()->gateway_token;
	}

	// Scopes

	public function scopeAvailable($query)
	{
		return $query->where('is_available',true);
	}

	public function scopePublic($query)
	{
		return $query->where('is_public',true);
	}

	public function scopeForUpgrade($query)
	{
		return $query->where('is_for_upgrade',true);
	}

	public function scopeRecurring($query)
	{
		return $query->where('is_recurring', true);
	}

	public function scopeOneTime($query)
	{
		return $query->where('is_recurring', false);
	}

	// Instance Methods

	public function createGateway(
		int $gateway_id,
		string $gateway_token
	): GatewayOffer {
		return GatewayOffer::create([
			'offer_id'=>$this->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
		]);
	}

	public function createStripeGateway(
		string $gateway_token
	): GatewayOffer {
		return $this->createGateway(1, $gateway_token);
	}

	public function createPaypalGateway(
		string $gateway_token
	): GatewayOffer {
		return $this->createGateway(2, $gateway_token);
	}

	// Static methods

	public static function fromGatewayToken(string $gateway_token): ?self
	{
		return self::whereHas('gateway_offers',
			function($query) use ($gateway_token){
				$query->where('gateway_token',$gateway_token);
			}
		)
		->first();
	}

	public static function fromAppstoreToken(string $appstore_token): ?self
	{
		return self::where('appstore_id',$appstore_token)
			->first();
	}

	public static function fromPlaystoreToken(string $appstore_token): ?self
	{
		return self::where('playstore_id',$appstore_token)
			->first();
	}

}
