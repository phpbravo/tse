const files = require.context('./components/', true, /\.vue$/i);

const vx_routes={
	mode:'history',
	base:'/admin',
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: files('./pages/Dashboard.vue').default
		},
		{
			path: '/purchases',
			name: 'purchases',
			component: files('./pages/Purchases.vue').default
		},
		{
			path: '/subscriptions',
			name: 'subscriptions',
			component: files('./pages/Subscriptions.vue').default,
		},
		{
			path: '/transactions',
			name: 'transactions',
			component: files('./pages/Transactions.vue').default
		},
		{
			path: '/pages',
			name: 'pages',
			component: files('./pages/Pages.vue').default
		},
		{
			path: '/offers',
			name: 'offers',
			component: files('./pages/Offers.vue').default
		},
		{
			path: '/products',
			name: 'products',
			component: files('./pages/Products.vue').default
		},
		{
			path: '/programs',
			name: 'programs',
			component: files('./pages/Programs.vue').default
		},
		{
			path: '/sessions',
			name: 'sessions',
			component: files('./pages/Sessions.vue').default
		},
		{
			path: '/categories',
			name: 'categories',
			component: files('./pages/Categories.vue').default
		},
		{
			path: '/exercises',
			name: 'exercises',
			component: files('./pages/Exercises.vue').default
		},
		{
			path: '/recipes',
			name: 'recipes',
			component: files('./pages/Recipes.vue').default
		},
		{
			path: '/blogs',
			name: 'blogs',
			component: files('./pages/Blog.vue').default
		},
		{
			path: '/videos',
			name: 'videos',
			component: files('./pages/Videos.vue').default
		},
		{
			path: '/users',
			name: 'users',
			component: files('./pages/Users.vue').default
		},
		{
			path: '/gateways',
			name: 'gateways',
			component: files('./pages/Gateways.vue').default
		},
	],
};

export default vx_routes;