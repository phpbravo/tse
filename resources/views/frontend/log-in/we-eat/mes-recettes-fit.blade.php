@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section loginrecettes" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section" id="recettesfit">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">MES RECETTES FIT</h1>
					<hr>
				</div>
				
				
			</div>
			<div class="row categoriesbtn loginrecipes" id="">
				
				<div class="col-lg-12 text-center" id="recettesfitcol">
					<a
						@click.prevent="changeCategory(0)"
						href="#"
						class="btn btn-secondary recettesfitbtn"
					>
						<h4>Toutes les recettes</h4>
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(1)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Petits dej’ et brunchs</h4>
							<img src="/assets/img/16.png">
						</div>
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(2)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Les classiques de Sissy</h4>
							<img src="/assets/img/10.png">
						</div>
						
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(3)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>« Cheat meal » healthy</h4>
							<img src="/assets/img/11.png">
						</div>
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(4)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Les exofit’</h4>
							<img src="/assets/img/12.png">
						</div>
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(5)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Collations healthy fit’</h4>
							<img src="/assets/img/13.png">
						</div>
					</a>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(6)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Desserts pour le plaisir</h4>
							<img src="/assets/img/14.png">
						</div>
					</a>
				</div>
			</div>

			<div v-if="loading" class="d-flex" style="position:relative; width:100%; justify-content:center;" id="loading-icon">
				<div class="loader4"></div>
			</div>
			<!---- itemlist ----->
			<div class="row recettesfit-items">
				<div
					v-for="recipe in recipes"
					:key="recipe.id"
					class="col-xl-4 col-lg-6"
				>
					<article>
						
							<div class="row">
								<div class="col-md-6 col-sm-12">
								
								<figure>
									<a :href="'/we-eat/mes-recettes-fit/'+recipe.id">
										<img :src="`/images?path=recipes/${recipe.id}/image/0_main_image.jpg`" class="img-fluid">
									</a>
								</figure>
								
							</div>
							
							<div class="col-md-6 col-sm-12 recettesfit-itemsdesc">
								<a :href="'/we-eat/mes-recettes-fit/'+recipe.id">
									<h4>@{{recipe.name}}</h4>
								</a>
							</div>
							</div>
						
					</article>
				</div>

			</div>
			
		</div>
		
	</section>

	

	

	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/we-eat-recipes.js?v={{filever('public/assets/js/we-eat-recipes.js')}}"></script>

@endsection