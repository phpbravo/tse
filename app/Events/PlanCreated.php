<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Jobs\CreatePaypalPlan;
use App\Jobs\CreateStripePlan;

class PlanCreated
{
	use Dispatchable, SerializesModels;

	public $plan;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($plan)
	{
		
		dispatch(new CreatePaypalPlan($plan))->onConnection('sync');
		dispatch(new CreateStripePlan($plan))->onConnection('sync');

	}

}
