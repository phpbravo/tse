@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section leconcept" id="top">
			<div class="container"></div>
	</section>
	
	

	<section class="faq-section" id="le-concepintro">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">LE CONCEPT</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
						<div class="leconcept-innercontent">
							<p>
								« Comme beaucoup de femmes, j’ai commencé par le fitness à la maison. Habitant à l’époque à l’étranger, au Brésil, avec peu de temps à consacrer au sport, je me suis lancée dans mon salon, avec pour seul matériel un tapis et des petits haltères. J’ai découvert tout un tas d’exercices réalisables avec peu de matos, mais redoutables pour se raffermir et sécher. J’ai ainsi commencé à transformer mon corps : je le voyais se raffermir et se dessiner de semaines en semaines.
							</p>
							<p>
								Passionnée par ces trainings courts et intensifs que j’effectuais chaque soir après une journée de boulot, j’ai eu envie de les partager avec vous sur ma chaine YouTube. Une fois par an, je vous propose donc un mois de vidéos « Bikini avec Sissy ». Vous êtes extrêmement nombreux à avoir fait et refait les 5 éditions et à en avoir apprécié les résultats indéniables. Au point de m’en réclamer chaque semaine ! Réellement animée par l’envie d’aller plus loin, de vous en proposer davantage, et surtout de repousser chaque fois davantage la qualité du contenu proposé, WE SWEAT est enfin né !
							</p>
							<p>
								Avec WE SWEAT, j’ai souhaité me lancer dans une nouvelle aventure ! Vous proposer chaque mois de nouvelles vidéos de fitness à la maison, avec pas ou peu de matériel et d’une qualité inégalée en France. Je suis fière de vous présenter mon nouveau programme axé tonification, renforcement musculaire et sèche. Ces vidéos sont à réaliser chez soi, dehors ou encore à la salle. Seules, ou en complément de la musculation comme moi, WE SWEAT a pour objectif de nous faire transpirer, brûler des calories, sécher mais aussi de dessiner nos muscles et notre sangle abdominale.
							</p>
							<p>
								Chaque début de mois, vous aurez accès à de nombreux nouveaux trainings live tournés dans un cadre idyllique. 
							</p>
						</div>

						<div class="leconcept-innerimg" id="concept-bottomimg">
							<img src="/assets/img/we-sweat-le-concept-petite.jpg" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
							<div class="leconcept-innerimg">
								<img src="/assets/img/Wesweat-concept-2e.jpg" class="img-fluid">
							</div>
						</div>

						<div class="leconcept-innercontent" id="rightcontent">
							<p>
								Des dizaines de vidéos sont donc disponibles pour vous permettre de vous créer votre programme sur mesure, varier et ainsi ne jamais vous lasser ! Chaque séance est exclusive, et reste accessible dans la librairie de vidéos WE SWEAT. Ainsi, vous pourrez refaire autant que vous le souhaitez vos vidéos favorites.
							</p>
							<p>
								Les séances WE SWEAT sont de courts entrainements intensifs de 15min, qui mêlent à la fois le cardio et le renforcement musculaire. Ils constituent clairement pour moi les trainings idéaux pour sécher tout en gagnant en masse musculaire afin de se tonifier et dessiner, sans fondre musculairement. Pour la petite anecdote, je perds à chaque tournage plusieurs kilos de masse grasse, et ce sans voir fondre mes muscles ! Sur un format court mais ultra intensif, vous n’avez plus aucune excuse pour ne pas réussir à caser votre séance sur mesure dans votre emploi du temps surchargé ! Si j’y arrive, pourquoi pas vous ?
							</p>
							<p>
								Vous vous demandez surement si vous devez faire ces vidéos seules, ou en complément de la musculation. Les deux sont envisageables ! En les faisant seules, vous activerez votre perte de masse grasse, en conservant votre masse musculaire et en dessinant votre corps. Idéal pour celles et ceux qui souhaitent sécher, notamment à l’approche de l’été ou pour atteindre leurs poids de forme, tout en restant fins. Si vous choisissez cette option, je vous recommande d’assembler entre 2 et 4 vidéos afin de vous constituer une séance complète et intensive ! Vous manquez d’idées ? Pas de panique ! Je publie tous les jours en instastories des exemples de séances constituées de plusieurs vidéos. De quoi se composer son programme à la carte, même quand on débute ! Retrouvez tous ces exemples de training dans l’onglet à la une SWEAT.
							</p>

							<p>
								En associant WE SWEAT à WE TRAIN, vous aurez un training complet. Vous gagnerez significativement en masse musculaire (et donc en booty ou en épaules !) grâce à WE TRAIN, mais vous resterez sec toute l’année avec un 6 pack de plus en plus dessiné grâce à WE SWEAT ! C’est personnellement le programme que nous avons choisi ! Dans ce cas-là, je vous recommande de finir votre séance musculation par une séance de cardio ou de gainage WE SWEAT.
							</p>

							<p>
								Que vous travailliez énormément, que vous gardiez vos enfants, que vous n’ayez pas de salle de sport à proximité, ou tout simplement pas le budget d’y aller, WE SWEAT est la solution idéale ! Ultra abordable, rapide, et sans matériel, ces séances sont faites pour vous ! Perdez de la masse grasse, tout en gagnant de la masse musculaire, c’est possible. Faites comme moi et let’s sweat together !»
							</p>

						</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.includes.parallax')	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>WE</span> SWEAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box" >
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="Le training du mois">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>Le Training Du Mois</h2>
									<p>
										Je te partage dans We Sweat mes meilleurs entrainements pour transpirer, brûler et se sculpter. Ces trainings font moins de 15min, n'hésites pas à en assembler plusieurs pour te composer un training à la carte !
									</p>
									<br>
									<p id="flipcardOne"><a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fas fa-heart" data-headline="Le concept">&nbsp;</span>
								<div class="services-box-item-content fa fa-heart">
									<h2>Le Concept</h2>
									<p>
										Adepte depuis de longues années des circuit trainings quand il s'agit de travailler mon cardio et de perdre en masse grasse, je vous partage chaque mois, retrouve mes nouvelles séances à réaliser chez vous, à la salle de sport ou encore dehors ! Intensives et de moins de quinze minutes, vous n'avez plus d'excuses !
									</p>
									<br>
									<p><a href="{{ route('front.we-sweat.concept') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fa fa-calendar" data-headline="Tous les trainings">&nbsp;</span>
								<div class="services-box-item-content fa fa-calendar">
									<h2>Tous Les Trainings</h2>
									<p>
										Retrouves toutes mes séances dans l'onglet tous les trainings ! Tu peux refaire tes workouts favoris à l'infinis !
									</p>
									<br>
									<p id="flipcardlast"><a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="passion-section tse-last-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container inner-parallax-section">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
							</div>
							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE PASSION</h1>
								<h3>POURQUOI TRAIN SWEAT EAT ?</h3>
								<p class="notrep2">
									C'est l’aboutissement de notre volonté commune de partager notre motivation, notre savoir, et surtout, notre passion ! Nous avons ainsi uni nos compétences très complémentaires pour vous proposer un site communautaire jamais créé en France, d’une qualité à la hauteur de nos exigences. Très fiers que ce projet voit enfin le jour après plus de deux ans de travail et de réflexion, nous espérons que vous aimerez plus que tout partager ce lifestyle avec nous, et surtout que vous deviendrez la meilleure version de vous-même !
								</p>
								
								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<a href="#asd" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection