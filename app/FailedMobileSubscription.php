<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * A failed mobile subscription
 * 
 * @property-read int $id
 * @property int $user_id
 * @property int $gateway_id
 * @property int $offer_id
 * @property int|null $appstore_receipt_id
 * @property string|null $vendor_token
 * @property string|null $error
 * @property bool $is_recovered
 * @property \App\User|null $user relation returned by user()
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
*/
class FailedMobileSubscription extends Model
{
	protected $guarded = [
	];

	// attributes that can be searched and filtered
	public $filterable_columns = [
		'user_id',
		'gateway_id',
		'appstore_receipt_id',
		'vendor_token',
		'error',
		'is_recovered',
	];

	// Simple Relations

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	// Scopes

	public function scopeRecovered($query)
	{
		return $query->where('is_recovered', true);
	}

	public function scopeNotRecovered($query)
	{
		return $query->where('is_recovered', false);
	}

	// Static Methods
	
	public static function getFromVendorToken(
		string $vendor_token
	): ?self {
		return self::where('vendor_token',$vendor_token)->first();
	}

	public static function getFromVendorTokenNotRecovered(
		string $vendor_token
	): ?self {
		return self::query()
			->where('vendor_token',$vendor_token)
			->notRecovered()
			->first();
	}

}
