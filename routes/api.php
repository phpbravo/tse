<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post(
	'/login',
	'Auth\LoginController@login'
);

Route::post(
	'/register',
	'Auth\LoginController@register'
);

Route::get(
	'/isEmailAvailable',
	'Auth\LoginController@checkEmailAvailability'
);

Route::get(
	'/offers',
	'Web\OffersController@getAvailableOffers'
);

Route::get(
	'/offers/upgrade',
	'Web\OffersController@getUpgradeOffers'
);

Route::get(
	'/offers/{id}',
	'Web\OffersController@getOffer'
);

Route::get(
	'/offers/{id}/paypal_token',
	'Web\OffersController@getOfferPaypalToken'
);

Route::post(
	'/transactions/confirm',
	'Web\TransactionsController@confirmTransaction'
);

Route::get(
	'/stripe_public_key',
	'Web\KeysController@getStripePublicKey'
);

Route::get(
	'/pages/get_by_slug/{slug}',
	'Web\PagesController@getPageBySlug'
)->where('slug','.*');

Route::group(['middleware'=>['auth']],function() {

    Route::get(
        '/programs/published/current',
        'Web\ProgramsController@getCurrentPublishedProgram'
    );

    Route::get(
		'/we_train/subcategories',
		'Web\ExercisesController@getWeTrainSubcategories'
	);

    Route::get(
		'/we_train/bonus',
		'Web\ProgramsController@getBonusSessions'
	);

    Route::get(
		'/we_train',
		'Web\ExercisesController@getWeTrainVideos'
	);

    Route::get(
		'/we_eat',
		'Web\RecipesController@getRecipes'
	);

    // USER

    Route::post(
		'/logout',
		'Auth\LoginController@logout'
	);
    
    Route::get(
		'/self',
		'Web\UserController@getSelf'
	);

    Route::post(
		'/updateProfile',
		'Web\UserController@updateProfile'
	);

    Route::get(
		'/subscriptions',
		'Web\UserController@getSubscriptions'
	);

    Route::get(
		'/purchases',
		'Web\UserController@getPurchases'
	);

    Route::get(
		'/transactions',
		'Web\UserController@getTransactions'
	);

    // PAYMENTS

    Route::post(
		'/handlePayment',
		'Web\PaymentsController@handlePayment'
	);

    Route::post(
		'/confirmPayment',
		'Web\PaymentsController@confirmPayment'
	);

});