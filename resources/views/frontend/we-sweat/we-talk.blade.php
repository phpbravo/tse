@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section we-talk" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">FAQ</h1>
					<hr>
				</div>
				<div class="col-xl-6 col-lg-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358242889"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358242905"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358242953"></iframe>
					</div>
				</div>
				<div class="col-xl-6 col-lg-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358242969"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358243052"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/358243036"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	

	<section class="section-collapsingfaq"> 
		<div class="container collapsingfaq-content">
			<div class="row collapsingfaq-content-row">
				<div class="col-lg-7 col-md-12">
					<h3 class="title-about">FAQ</h3>
					<div id="accordion">
						<div class="card faq-cards">
							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse1" aria-expanded="true">
									QUEL TRAINING ET PROGRAMME ADOPTER ?
								</a>
								</h4>
							</div>
							<div id="collapse1" class="collapse show" data-parent="#accordion">
								<div class="card-body">
									<p>
										WE SWEAT est mon programme fitness à la maison avec peu ou pas de matériel, à réaliser n’importe où, n’importe quand, en moins de 15min par vidéo ! Que l’on soit à la salle, à la maison, ou en vacances, nous n’avons plus aucune excuse pour ne pas bouger et transpirer ! Ces séances intenses et rapides nous permettent de brûler un max de calories en un minimum de temps, et surtout, d’augmenter nôtre métabolisme basal ! Idéales pour sécher, se dessiner et enfin faire apparaître les abdominaux. Mais ce n’est pas tout ! WE SWEAT est également axé sur le renforcement musculaire grâce à ses séances inspirées des salles de musculation, afin de gagner en volume musculaire et ainsi en tonicité ! WE SWEAT est un programme varié, qui peut être réalisé seul, comme en complément de WE TRAIN. De nombreuses nouvelles séances par mois nous permettent de rester motivés et de toujours varier nos entrainements. Parfait pour celles et ceux qui recherchent une silhouette affinée, tonique et tracée.
									</p>
									<br>
									<p>
										WE TRAIN est quant à lui un programme de musculation avec matériel à réaliser en salle de sport. Chaque jour, nous publions notre workout of the day, toujours différent. Ces 25 nouvelles séances par mois (toutes entièrement différentes) nous permettent de progresser, de varier nos entrainements et de sortir de notre zone de confort pour enfin évoluer ! Ce programme nous permet de prendre en masse musculaire, tout en perdant en masse grasse grâce à l’augmentation du métabolisme basal. L’objectif est de prendre en galbe pour les femmes, en masse pour les hommes, tout en conservant une ligne tracée.
									</p>
									<br>
									<p>
										Si votre objectif est comme moi, d’augmenter masse musculaire tout en restant sec ou sèche, l’idéal est donc de faire à la fois WE TRAIN et WE SWEAT.
									</p>
									<br>
									<p>
										Si votre objectif est simplement de sécher et tonifier votre corps, vous pouvez rester fidèles à WE SWEAT. N’oubliez cependant pas qu’entretenir sa masse musculaire est très important, d’un point de vue santé mais aussi silhouette. Alors pourquoi ne pas introduire du petit matériel type haltères pour intensifier vos trainings WE SWEAT ?
									</p>
									<br>
									<p>
										Grâce à TRAINSWEATEAT, vous avez accès à nos deux programmes de sport, musculation et fitness à la maison, ainsi qu’à nos recettes fitness favorites, qui vous permettrons d’obtenir enfin résultats que vous espérez ! TRAINSWEATEAT étant quotidiennement mis à jour, il est désormais facile de s’entrainer avec plaisir et envie !
									</p>
								</div>
							</div>

							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse2" aria-expanded="false">
									QUAND S’ENTRAINER ? RESPECTER LE PROGRAMME
								</a>
								</h4>
							</div>

							<div id="collapse2" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Nous publions tous nos entrainements à 7h du matin. Mais quelle est le meilleur moment pour vous entrainer ? Le matin, l’après-midi ou le soir ? Je pourrais vous expliquer scientifiquement pourquoi tel ou tel moment est mieux pour vous, mais je préfère tout simplement vous dire que le meilleur moment est celui que vous préférez. Que vous soyez du matin, ou du soir, entrainez-vous quand ça vous plait, et surtout quand ça vous arrange ! Le parfait moment est celui qui s’insère facilement dans votre agenda et où vous vous sentez en forme ! Par exemple, j’aime personnellement m’entrainer le soir tard entre 20 et 22h ! Pourquoi ? Tout simplement car j’aime aller au sport quand tout mon travail est fini, mon esprit est libéré et que je me sens relativement en forme à cette heure-là, même si je dois parfois me faire violence pour sortir lorsqu’il fait nuit depuis des heures. Me lever à 5h pour aller à la salle avant ma journée ne correspond clairement pas à mes habitudes et mon rythme. Alors même si je me fais parfois violence, je sais que je ne pourrai tenir un tel rythme durant des années.
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse3" aria-expanded="false">
									QUELS RÉSULTATS ATTENDRE
								</a>
								</h4>
							</div>

							<div id="collapse3" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										WE SWEAT est un programme de cardio training qui allie cardio HIIT et renforcement musculaire. C’est ma méthode préférée pour perdre de la masse grasse tout en conservant et développant ma masse musculaire. Ces trainings redoutables me permettent chaque année de perdre les petites graisses superflues et d’éliminer ma rétention d’eau, notamment à l’approche de l’été. En réalisant WE SWEAT, on parvient enfin à s’affiner et à sculpter sa silhouette, sans s’affamer ! Le métabolisme est boosté, les muscles dessinés, et la shape clairement tonifiée et affinée.
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse4" aria-expanded="false">
									COMMENT PERDRE DU GRAS
								</a>
								</h4>
							</div>

							<div id="collapse4" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Vous souhaitez perdre du gras, mais ne savez pas vraiment comment vous y prendre. Tous les régimes que vous avez essayés n’ont que réussi à vous faire prendre encore plus de poids, et aucun programme de sport ne vous a suffisamment motivé ? Bienvenue sur WE SWEAT, mon ultime programme pour perdre de la masse grasse, sans aucun régime, et sans se dégouter du sport.
									</p>

									<p>
										En effet, perdre du poids n’est en réalité pas si compliqué. Lorsque vous ingérez plus de calories que vous n’en dépensez, vous stockez, lorsque vous en avalez moins que vous n’en perdez, vous mincissez. C’est mathématique. Il y a donc deux façons d’être sur une balance négative (en ingérer moins que n’en dépenser) : soit vous mangez moins de calories, soit vous en dépensez plus. Je suis profondément persuadée qu’atteindre son poids de forme durablement ne s’acquière pas grâce aux régimes draconiens. Evidemment il est primordial de manger équilibré (retrouvez toutes nos recettes fitness et plaisir favorites dans l’onglet WE EAT), mais pour stabiliser son poids, il est tout aussi important d’avoir la bonne activité sportive.
									</p>

									<p>
										Et c’est là que je me suis lancée dans WE SWEAT ! Des trainings courts et intenses sous format HIIT pour élever au maximum sa fréquence cardiaque, se faire transpirer, brûler des calories, et le plus important, élever son métabolisme basal ! Le MB est le nombre de calories que l’on dépense au repos, sans rien faire. Plus ce chiffre est élevé, plus la silhouette s’affine, durablement. Non seulement on brûle de l’énergie pendant la séance, mais on en brûle aussi en dehors. Avec mes séances WE SWEAT combinant cardio HIIT et renforcement musculaire, il n’y a aucun risque de voir la masse musculaire fondre ! Ma méthode permet de perdre du gras oui, mais tout en développant sa masse musculaire. C’est ainsi le gage pour moi d’obtenir une shape sèche et tracée ! En combinant mes séances WE SWEAT avec nos recettes WE EAT, toutes les composantes sont parfaitement réunies pour obtenir la meilleure version de soi même !
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse5" aria-expanded="false">
									OBTENIR ENFIN DES ABDOS
								</a>
								</h4>
							</div>

							<div id="collapse5" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Avoir des abdominaux visibles, le rêve de tout fitguy ou de toute fitgirl. Tini et moi-même avons la chance d’afficher des abdos visibles en toute saison, même si ils sont évidemment plus visibles en été qu’en hiver. Mais en réalité, si la génétique compte concernant la morphologie de vos abdos, je devrais plutôt remercier WE SWEAT! En effet, avoir des abdos n’est pas si compliqué. Il s’agit seulement que la couche de gras qui les recouvre au niveau de l’abdomen soit suffisamment fine pour qu’on puisse les apercevoir à travers.
									</p>
									<p>
										Comment faire ? C’est simple, il suffit de tonifier musculairement ses abdos, et avoir une masse grasse suffisamment faible pour les rendre apparents. Grace à WE SWEAT, nous combinons les deux ! On stimule notre sangle abdominale grâce aux différents exercices de cardio et renforcement musculaire, et on sèche grâce au HIIT! Le tout sans s'affamer et en se faisant plaisir !
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse6" aria-expanded="false">
									COMMENT SE CONSTITUER SON PROGRAMME?
								</a>
								</h4>
							</div>

							<div id="collapse6" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Chaque mois, de nombreuses séances vidéo inédites sont publiées sur TRAINSWEATEAT. Comme elles ne s’effacent jamais, vous avez donc à votre disposition un énorme catalogue de trainings. Comment composer ses séances ? Doit-on faire une seule séance par jour ? Plusieurs ? Comment les assembler ? It’s up to you ! WE SWEAT est un programme à la carte qui s’adapte à votre emploi du temps, à vos envies et à votre niveau ! Vous n’avez que très peu de temps ? Faite une des vidéos intensives proposées. Vous avez envie de faire une bonne séance d’une heure ? Composez dans ce cas-là votre séance sur mesure en assemblant 4 vidéos de 15min. Vous êtes fatiguée de votre semaine ? Terminez en beauté par une séance gainage full body. Le mot d’ordre lorsqu’on se compose un programme : VA-RI-E-TE ! Vous devez absolument varier les vidéos, que ce soit au sein de votre séance comme au sein de votre semaine ! Si vous manquez d’inspiration, sachez que je vous propose chaque jour un exemple de séance plus ou moins court en instastory. Vous les retrouverez également à la une dans l’onglet SWEAT.
									</p>
								</div>
							</div>
							

						</div>
					</div>
				</div>

				<div class="col-lg-5 rightsideimg" id="hidefaqimg">
					<div class="we-talk-rightimages">
						<img src="/assets/img/WESWEAT-FAQ-small.jpg" class="img-fluid">
					</div>

					<div class="we-talk-rightimages">
						<img src="/assets/img/wesweat-concept2.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.includes.parallax')	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>WE</span> SWEAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box" >
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="Le training du mois">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>Le Training Du Mois</h2>
									<p>
										Je te partage dans <strong>We Sweat</strong> mes <strong>meilleurs entrainements</strong> pour<strong> transpirer, brûler</strong> et se <strong>sculpter</strong>. Ces <strong>trainings</strong> font <strong>moins de 15min</strong>, n'hésite-pas à <strong>en assembler plusieurs</strong> pour te composer un<strong> training à la carte</strong> !
									</p>
									<br>
									<p id="flipcardOne"><a href="#">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fas fa-heart" data-headline="Le concept">&nbsp;</span>
								<div class="services-box-item-content fa fa-heart">
									<h2>Le Concept</h2>
									<p>
										Adepte depuis de longues années des <strong>circuits trainings</strong> quand il s'agit de travailler mon <strong>cardio</strong> et de<strong> perdre en masse grasse</strong>, je vous partage chaque mois, retrouve mes <strong>nouvelles séances</strong> à réaliser chez vous, à la <strong>salle de sport</strong> ou encore <strong>dehors</strong> ! <strong>Intensives</strong> et de<strong> moins de quinze minutes</strong>, vous n'avez plus d'excuses !
									</p>
									<br>
									<p><a href="#">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item leconcept-flipcards">
								<span class="services-box-item-cover fa fa-calendar" data-headline="Tous les trainings">&nbsp;</span>
								<div class="services-box-item-content fa fa-calendar">
									<h2>Tous Les Trainings</h2>
									<p>
										Retrouve toutes <strong>mes séances</strong> dans l'onglet tous <strong>les trainings</strong> ! Tu peux refaire tes <strong>workouts favoris</strong> à l'infini !
									</p>
									<br>
									<p id="flipcardlast"><a href="#">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="passion-section tse-last-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container inner-parallax-section">
						<div class="row">
							<div class="col-xl-5 col-lg-12">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
								
							</div>

							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE PASSION</h1>
								<h3>POURQUOI TRAIN SWEAT EAT ?</h3>
								<p class="notrep">
									C'est l’aboutissement de notre volonté commune de partager notre motivation, notre savoir, et surtout, notre passion ! Nous avons ainsi uni nos compétences très complémentaires pour vous proposer un site communautaire jamais créé en France, d’une qualité à la hauteur de nos exigences. Très fiers que ce projet voit enfin le jour après plus de deux ans de travail et de réflexion, nous espérons que vous aimerez plus que tout partager ce lifestyle avec nous, et surtout que vous deviendrez la meilleure version de vous-même !
								</p>

								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection