<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{FileHandler, ObjectHandler};

use Illuminate\Http\UploadedFile;

class FilesController extends Controller
{

	public function upload(){
		$file=request()->file;
		$path=request()->path;
		$rename=request()->rename;
		
		if (!$file)
			return (new ObjectHandler())->newError('invalid_request','Aucun fichier fourni.');

		if (!$path)
			return (new ObjectHandler())->newError('invalid_request','Aucun chemin d\'accès fourni');
		
		with(new FileHandler)->store($file,$path,$rename?:null);

		return (new ObjectHandler())->newSuccess('Fichier téléchargé.');

	}

}
