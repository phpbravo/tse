<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;

/**
 * Class Subscription
 *
 * A resource representing a subscription.
 *
 * @package PayPal\Api2
 *
 * @property string id
 * @property string plan_id
 * @property string status
 * @property string start_time
 * @property \PayPal\Api2\Subscriber subscriber
 * @property \PayPal\Api2\ApplicationContext application_context
 * @property string create_time
 * @property string update_time
 */
class Subscription extends PayPalResourceModel
{
    use Timestamps;

    const
        /** @var string The subscription is created but not yet approved
         * by the buyer. 
         */
        APPROVAL_PENDING = 'APPROVAL_PENDING',

        /** @var string The buyer has approved the subscription. */
        APPROVED = 'APPROVED',

        /** @var string The subscription is active. */
        ACTIVE = 'ACTIVE',

        /** @var string The subscription is suspended. */
        SUSPENDED = 'SUSPENDED',

        /** @var string The subscription is cancelled. */
        CANCELLED = 'CANCELLED',

        /** @var string The subscription is expired. */
        EXPIRED = 'EXPIRED';

    /**
     * Identifier of the agreement.
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Identifier of the agreement.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Plan ID for this agreement.
     */
    public function setPlanId(string $plan_id): self
    {
        $this->plan_id = $plan_id;
        return $this;
    }

    /**
     * Plan ID for this agreement.
     */
    public function getPlanId(): string
    {
        return $this->plan_id;
    }

    /**
     * Status of the agreement.
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Status of the agreement.
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Start time of the agreement. Date format yyyy-MM-dd z, as 
     * defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function setStartTime(string $start_time): self
    {
        $this->start_time = $start_time;
        return $this;
    }

    /**
     * Start time of the agreement. Date format yyyy-MM-dd z, as 
     * defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function getStartTime(): string
    {
        return $this->start_time;
    }

    /**
     * Details of the subscriber who is enrolling in this subscription. 
     * This information is gathered from execution of the approval URL.
     */
    public function setSubscriber(Subscriber $subscriber): self
    {
        $this->subscriber = $subscriber;
        return $this;
    }

    /**
     * Details of the subscriber who is enrolling in this subscription. 
     * This information is gathered from execution of the approval URL.
     * 
     * @return \PayPal\Api2\Subscriber
     */
    public function getSubscriber(): Subscriber
    {
        return $this->subscriber;
    }

    /**
     * Date and time that this resource was created. Date format 
     * yyyy-MM-dd z, as defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function setCreateTime(string $create_time): self
    {
        $this->create_time = $create_time;
        return $this;
    }

    /**
     * Date and time that this resource was created. Date format 
     * yyyy-MM-dd z, as defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function getCreateTime(): string
    {
        return $this->create_time;
    }

    /**
     * Date and time that this resource was updated. Date format 
     * yyyy-MM-dd z, as defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function setUpdateTime(string $update_time): self
    {
        $this->update_time = $update_time;
        return $this;
    }

    /**
     * Date and time that this resource was updated. Date format 
     * yyyy-MM-dd z, as defined in [ISO8601]
     * (http://tools.ietf.org/html/rfc3339#section-5.6).
     */
    public function getUpdateTime(): string
    {
        return $this->update_time;
    }

    /**
     * Application context for user experience
     */
    public function setApplicationContext(
        ApplicationContext $application_context
    ): self {
        $this->application_context = $application_context;
        return $this;
    }

    /**
     * DApplication context for user experience
     */
    public function getApplicationContext(): ApplicationContext
    {
        return $this->application_context;
    }

    /**
     * Get Approval Link
     */
    public function getApprovalLink(): ?string
    {
        return $this->getLink('approve');
    }

    /**
     * Create a new Subscription
     */
    public function create(
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        $payLoad = $this->toJSON();
        $json = self::executeCall(
            "/v1/billing/subscriptions/",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    /**
     * Execute a subscription after buyer approval by passing the 
     * payment token to the request URI.
     */
    public function capture(
        string $subscription_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate(
            $subscription_id,
            'subscription_id'
        );
        $payLoad = "";
        $json = self::executeCall(
            "/v1/billing/subscriptions/$subscription_id/capture",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    /**
     * Retrieve details for a subscription by passing 
     * the ID of the agreement to the request URI.
     */
    public static function get(
        string $subscription_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate(
            $subscription_id,
            'subscription_id'
        );
        $payLoad = "";
        $json = self::executeCall(
            "/v1/billing/subscriptions/$subscription_id",
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $subscription = new self;
        $subscription->fromJson($json);
        return $subscription;
    }

    /**
     * Suspend a subscription by passing the ID of the 
     * agreement to the request URI.
     */
    public function suspend(
        string $reason,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): bool{
        ArgumentValidator::validate($this->getId(), "Id");
        ArgumentValidator::validate($reason, 'reason');
        $payLoad = json_encode(['reason'=>$reason]);
        self::executeCall(
            "/v1/billing/subscriptions/{$this->getId()}/suspend",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        return true;
    }

    /**
     * Activate a suspended subscription by passing the ID of the
     * agreement to the appropriate URI.
     */
    public function activate(
        string $reason,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): bool {
        ArgumentValidator::validate($this->getId(), "Id");
        ArgumentValidator::validate($reason, 'reason');
        $payLoad = json_encode(['reason'=>$reason]);
        self::executeCall(
            "/v1/billing/subscriptions/{$this->getId()}/activate",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        return true;
    }

    /**
     * Cancel a subscription by passing the ID of the
     * agreement to the request URI.
     */
    public function cancel(
        string $reason,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): bool {
        ArgumentValidator::validate($this->getId(), "Id");
        ArgumentValidator::validate($reason, 'reason');
        $payLoad = json_encode(['reason'=>$reason]);
        self::executeCall(
            "/v1/billing/subscriptions/{$this->getId()}/cancel",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        return true;
    }

    /**
     * List transactions for a subscription by passing the start and end
     * times of the range of transactions to list, to the request URI.
     */
    public function transactions(
        array $params = array(),
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): TransactionsList {
        ArgumentValidator::validate($this->getId(), 'Id');
        ArgumentValidator::validate($params, 'params');

        $allowedParams = array(
            'start_time' => 1,
            'end_time' => 1,
        );

        $payLoad = "";
        $json = self::executeCall(
            "/v1/billing/subscriptions/{$this->getId()}/transactions?"
            . http_build_query(
                array_intersect_key($params, $allowedParams)
            ),
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $transactions_list = new TransactionsList;
        $transactions_list->fromJson($json);
        return $transactions_list;
    }

}
