<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseTagPivotTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exercise_tag', function (Blueprint $table) {
			$table->unsignedBigInteger('exercise_id');
				$table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
			$table->unsignedBigInteger('tag_id');
				$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::dropIfExists('exercise_tag');
		
	}
}
