@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
		<section class="concept-section" id="top">
			<div class="container">
				
			</div>
		</section>
	
	

	<section class="faq-section" id="le-concepintro2">
		<div class="container ">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">LE CONCEPT WE TRAIN</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
						<h3 class="title-about">LE <strong>CONCEPT</strong></h3>
						
						<div class="leconcept-innercontent">
							<p>
								Lorsque nous nous sommes rencontrés, une <strong>passion</strong> nous a vite réunis, le <strong>fitness</strong>. Etant deux accros à ce sport aussi passionnant qu’addictif, c’est tout naturellement que nous avons commencé à nous entrainer ensembles de temps à autres. Trois ans plus tard, nous sommes en couples et nous allons toujours à deux à la salle, faisons les mêmes séances et les mêmes exercices. Encore plus <strong>motivés</strong> qu’auparavant, nous avons néanmoins constaté que de nombreux fitgens se découragent rapidement dans ce <strong>sport</strong>. En effet, beaucoup d’inscrits ne savent pas toujours comment s’entrainer, comment constituer leur <strong>routine</strong>, quels<strong> exos</strong> choisir… Ne voyant pas de résultats, ils finissent souvent par baisser les bras. Recevant de plus en plus de messages de notre communauté nous demandant de partager nos <strong>entrainements</strong>, il nous est venu l’envie de créer <strong>We Train</strong>.
							</p>

							<p>
								<strong>We Train</strong> est un site communautaire où nous partageons chaque jour de la semaine notre<strong> workout journalier</strong>. Notre véritable entrainement, celui qui nous fait progresser, et qui vous donnera sans aucun doute autant de résultats. Tous les jours, vous avez accès à une <strong>nouvelle séance inédite</strong>, constituée d’exercices de bases, comme d’exercices plus pointus, formant une séance redoutable, elle même intégrée dans un programme structuré sur plusieurs mois.
							</p>

							<p>
								<strong>We Train</strong> nous permettra de toujours<strong> restés motivés ensembles</strong>, de nous <strong>challenger</strong> et surtout de <strong>ne jamais abandonner</strong>. Car la régularité et la variété sont sans aucun doute deux des facteurs les plus importants pour progresser et atteindre ses objectifs.
							</p>
						</div>

						<div class="leconcept-innerimg" id="concept-bottomimg">
							<img src="/assets/img/Wetrain-concept-eating.jpg" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
						<div class="leconcept-innerimg mt-3">
							<img src="/assets/img/concept01.jpg" class="img-fluid">
						</div>

						<div class="leconcept-innercontent" id="rightcontent">
							<p>
								Vous vous demandez sans doute si ce qui marche pour nous va forcément fonctionner pour vous. Sachez que je n’ai jamais eu autant de résultats que depuis que je m’entraine avec un homme, Tini. En effet, j’ai commencé la <strong>musculation</strong> comme la plupart des femmes, en faisant des impasses sur plusieurs muscles et en pensant que faire le booty à chaque séance me donnera le fessier de Jen Selter. J’ai vite compris en ne voyant aucun résultat qu’il était primordial pour les femmes comme pour les hommes de<strong> travailler le corps dans sa globalité</strong>. Oui, je parle aussi de vous, les<strong> fitguys</strong> qui faites les jambes aussi souvent que Booba. Notre programme commun, global et intensif apporte de véritables <strong>résultats visibles</strong> sur votre corps, pas seulement sur le papier !
							</p>
							<p>
								Notre<strong> training</strong> est constitué de séances toujours <strong>polymusculaires</strong>, pour permettre une fréquence plus élevée du recrutement de chaque muscle, chaque semaine. Ainsi nous travaillons environ <strong>deux fois par semaine</strong> chaque groupe musculaire. Notre programme est établi sur plusieurs mois, afin d’intégrer des séances axées force pour la progression et d’autres davantage sur l’hypertrophie musculaire. Nous varions les <strong>exercices</strong>, les <strong>séries</strong>, les <strong>répétitions</strong>, les <strong>tempos</strong> tout en intégrant des <strong>séances originales</strong> et <strong>fun</strong> pour se challenger et rester motivés ensembles.
							</p>
							<p>
								Si toi aussi tu as<strong> envie de te surpasser</strong> avec nous, de <strong>resté motivé(e)</strong>, et d’enfin atteindre tes <strong>objectifs</strong>, alors suis nous !
							</p>

							<p>
								<strong>
									Grâce à We Train, nous nous entrainerons ensembles, chaque jour, pour atteindre chacun de nos objectifs.
								</strong>
							</p>

						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	@include('frontend.includes.parallax')	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>WE</span> TRAIN</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="Training Du Jour">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>Training Du Jour</h2>
									<p>Quotidiennement, nous partageons avec vous nos <strong>entrainements</strong> en salle de musculation ! Ensemble, <strong>progressons chaque jour</strong> et restons <strong>surmotivés</strong> !</p>
									<br>
									<p><a href="{{ route('front.program') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-camera" data-headline="Vidéos">&nbsp;</span>
								<div class="services-box-item-content fa fa-camera">
									<h2>Vidéos</h2>
									<p>
										<strong>Tu ne connais pas un exercice</strong> ? Nous nous sommes filmés en train de <strong>le réaliser </strong> pour <strong>t'aider à le reproduire</strong> !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-calendar" data-headline="Scéances Bonus">&nbsp;</span>
								<div class="services-box-item-content fa fa-calendar">
									<h2>Scéances Bonus</h2>
									<p>
										Tu ne peux pas faire la <strong>séance du jour</strong> ? <strong>Pas de panique </strong>! Nous te proposons quelques<strong> séances bonus</strong> en <strong>remplacement.</strong>
									</p>
									<br>
									<p><a href="{{ route('front.we-train.bonus') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="passion-section tse-last-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container inner-parallax-section">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
								
							</div>

							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE PASSION</h1>
								<h3>POURQUOI TRAIN SWEAT EAT ?</h3>
								<p class="notrep">
									C'est l’aboutissement de notre volonté commune de partager notre motivation, notre savoir, et surtout, notre passion ! Nous avons ainsi uni nos compétences très complémentaires pour vous proposer un site communautaire jamais créé en France, d’une qualité à la hauteur de nos exigences. Très fiers que ce projet voit enfin le jour après plus de deux ans de travail et de réflexion, nous espérons que vous aimerez plus que tout partager ce lifestyle avec nous, et surtout que vous deviendrez la meilleure version de vous-même !
								</p>

								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<a href="#asd" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection