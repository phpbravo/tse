<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;


/**
 * Class BillingCycle
 *
 * A resource representing a plan's billing cycle.
 *
 * @package PayPal\Api2
 *
 * @property \PayPal\Api2\PricingScheme pricing_scheme
 * @property \PayPal\Api2\Frequency frequency
 * @property string tenure_type
 * @property int sequence
 * @property int total_cycles
 * @property string create_time
 * @property string update_time
 */
class BillingCycle extends PayPalModel
{
    use Timestamps;
    /**
     * @var string $REGULAR A regular billing cycle.
     */
    const REGULAR = 'REGULAR';

    /**
     * @var string $TRIAL A trial billing cycle.
     */
    const TRIAL = 'TRIAL';

    /**
     * Pricing scheme.
     * 
     * The active pricing scheme for this billing cycle. A free trial 
     * billing cycle does not require a pricing scheme.
     */
    public function setPricingScheme(
        PricingScheme $pricing_scheme
    ): self {
        $this->pricing_scheme = $pricing_scheme;
        return $this;
    }

    /**
     * Pricing scheme.
     * 
     * The active pricing scheme for this billing cycle. A free trial 
     * billing cycle does not require a pricing scheme.
     */
    public function getPricingScheme(): PricingScheme
    {
        return $this->pricing_scheme;
    }

    /**
     * Frequency.
     * 
     * The frequency details for this billing cycle.
     */
    public function setFrequency(Frequency $frequency): self
    {
        $this->frequency = $frequency;
        return $this;
    }

    /**
     * Frequency.
     * 
     * The frequency details for this billing cycle.
     */
    public function getFrequency(): Frequency
    {
        return $this->frequency;
    }

    /**
     * Tenure type.
     * 
     * The tenure type of the billing cycle. In case of a plan having 
     * trial period, only 1 trial period is allowed per plan. 
     */
    public function setTenureType(string $tenure_type): self
    {
        $this->tenure_type = $tenure_type;
        return $this;
    }

    /**
     * Tenure type.
     * 
     * The tenure type of the billing cycle. In case of a plan having 
     * trial period, only 1 trial period is allowed per plan. 
     */
    public function getTenureType(): string
    {
        return $this->tenure_type;
    }

    /**
     * Sequence.
     * 
     * The order in which this cycle is to run among other billing 
     * cycles.
     */
    public function setSequence(int $sequence): self
    {
        $this->sequence = $sequence;
        return $this;
    }

    /**
     * Sequence.
     * 
     * The order in which this cycle is to run among other billing 
     * cycles.
     */
    public function getSequence(): int
    {
        return $this->sequence;
    }

    /**
     * Total cycles.
     * 
     * The number of times this billing cycle runs. Trial billing cycles
     * can only have a value of 1 for total_cycles. Regular billing 
     * cycles can either have infinite cycles (value of 0 for 
     * total_cycles)
     */
    public function setTotalCycles(int $total_cycles): self
    {
        $this->total_cycles = $total_cycles;
        return $this;
    }

    /**
     * Total cycles.
     * 
     * The number of times this billing cycle runs. Trial billing cycles
     * can only have a value of 1 for total_cycles. Regular billing 
     * cycles can either have infinite cycles (value of 0 for 
     * total_cycles)
     */
    public function getTotalCycles(): int
    {
        return $this->total_cycles;
    }
}
