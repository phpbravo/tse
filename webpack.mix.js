const mix = require('laravel-mix');

if (process.env != 'production'){
    require('laravel-mix-bundle-analyzer');
    // mix.bundleAnalyzer();
}

mix.disableNotifications();

mix.webpackConfig({
    module: {
       rules: [
            {
                test: /\.pug$/,
                loader: 'pug-plain-loader'
            },
            {
                test: /\.tsx?$/,
                loader: "ts-loader" 
            }
        ],
    },
    resolve:{
        symlinks: false,
        modules:[
            'node_modules'
        ],
        extensions:['.vue', '.ts', '.tsx', '.js'],
        alias:{
            '@mixins': path.resolve(__dirname,'resources/mixins'),
        }
    },
 });

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
if (process.env.NODE_ENV == 'production'){

   mix.setPublicPath('public');
   mix.version();

} else if (process.env.NODE_ENV == 'development'){

   mix.setPublicPath('public/dev');

}

mix
.js('resources/user/app.js', 'web/app.js');

mix
.js('resources/admin/app.js', 'administrator/app.js')
.sass('resources/admin/app.scss', 'administrator/app.css');