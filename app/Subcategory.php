<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $dispatchesEvents=[
        'created' => Events\ResourceCreated::class,
    ];

    protected $fillable=[
        'category_id',
        'name',
        'title',
        'description',
        'order_index',
        'image_background_web',
        'image_content_web',
        'image_background_mobile',
        'image_content_mobile',
        'created_at',
        'updated_at'
    ];

    public function exercises(){
        return $this->hasMany('App\Exercise');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
