<footer class="footer">
	<div class="container top-footer">
		<div class="row">
			<div class="col-md-6">
				<div class="footer-logo">
					<img src="/assets/img/footer-tse.png" alt="footer-tse">
				</div>
				<div class="footer-apps">
					<a href="#"><img src="/optimized/img/app_store.svg" alt="app store"></a>
					<a href="#"><img src="/optimized/img/play_store.svg" alt="play store"></a>
				</div>
				<div class="footer-contact">
					<h3> Contact</h3>
					<p>Pour toute demande de support:</p>
					<p><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:contact@trainsweateat.com"><span>CONTACT@TRAINSWEATEAT.COM</span></a></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-4">
						<h4>WE TRAIN</h4>
						<div class="custom">
							<div class="menu">
								<ul>
									<li><a href="{{route('front.program')}}">Programme du jour</a></li>
									<li><a href="{{route('front.we-train.videos')}}">Vidéos</a></li>
									<li><a href="{{route('front.we-train.bonus')}}">Vidéos Bonus</a></li>
									<li><a href="{{route('front.we-train.faq')}}">Faq & Definitions</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4">
						<h4>WE SWEAT</h4>
						<div class="custom">
							<div class="menu">
								<ul>
									<li><a href="{{route('front.we-sweat.videos')}}">Tous les vidéos</a></li>
									<li><a href="{{route('front.we-sweat.faq')}}">Faq</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4">
						<h4>WE EAT</h4>
						<div class="custom">
							<div class="menu">
								<ul>
									<li><a href="{{route('front.we-eat.recipes')}}">Mes recettes Fit</a></li>
									<li><a href="{{route('front.ebook')}}">Ebook</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="custom">
							<div class="facts-footer">
								<div>
									<h5>2.300.000</h5>
									<span>followers à 2</span>
								</div>
								<div>
									<h5>8.400 </h5>
									<span>heures de sport à 2</span>
								</div>
							</div>
							<hr>
							@include('frontend.includes.social-icons')
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="custom">
						<a href="{{ route('front.terms') }}">Conditions générales d'utilisation et de vente</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>