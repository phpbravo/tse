<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	const EXERCISES_PRODUCT = 1;
	const RECIPES_PRODUCT = 2;
	const SESSIONS_PRODUCT = 3;
	const PROGRAMS_PRODUCT = 4;
	const EBOOK_PRODUCT = 5;

	protected $fillable=[
		'resource_id',
		'name',
		'description',
		'content',
	];

	public function resource(){
		return $this->belongsTo('App\Resource');
	}

}
