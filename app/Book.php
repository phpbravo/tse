<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
		'deleted' => Events\ResourceDeleted::class,
	];

	// attributes that are mass-assignable
	public $fillable = [
        'name',
        'description'
    ];

	protected $hidden = [
	];

	protected $casts = [
	];
    
    // Simple Relations
    
        // none

    // Scoped Relations

        // none

    // Attributes

        // none

    // Scopes

        // none

    // Instance Methods

        // none

    //Static Methods

        // none

}
