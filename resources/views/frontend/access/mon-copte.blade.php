@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="login-wrapper d-none" id="mon-compte-app">
		<transition name="fade">
			<div v-if="loading" class="loading-overlay full-overlay"><div class="loader4"></div></div>
		</transition>
		<div class="container">
			<div class="row pb-5">
				<div class="col-lg-12">
					<h1  class="pages-heading" id="profilehead">PROFIL DE L'UTILISATEUR</h1>
					
				</div>

				<div class="col-lg-12">
					<ul class="nav nav-tabs profile">
						<li class="nav-item">
							<a href="#modifiervotreprofil" class="nav-link active" data-toggle="tab">Modifier votre profil</a>
						</li>
						<li class="nav-item">
							<a href="#mesadhésions" class="nav-link" data-toggle="tab">Mes abonnements & adhésions</a>
						</li>
						<li class="nav-item">
							<a href="#historique" class="nav-link" data-toggle="tab">Historique des abonnements & adhésions</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="container tab-pane active" id="modifiervotreprofil">
							
							<form @submit.prevent="postProfile" autocomplete="off" class="profileform">

									<div class="form-group row profilerow">
										<label class="col-lg-3 col-form-label col-form-label-sm">E-mail <span>*</span></label>
										<div class="col-lg-9">
											<input v-model="user.email" type="email" class="form-control form-control-sm editprofile" id="colFormLabelSm">
										</div>
									</div>
							  <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Nom d'utilisateur</label>
							    <div class="col-lg-9">
							      <input v-model="user.username" readonly class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Mot de passe</label>
							    <div class="col-lg-9">
							      <input v-model="user.password" type="password" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Confirmer le mot de passe</label>
							    <div class="col-lg-9">
							      <input v-model="user.password_confirmation" type="password" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Numéro de membre</label>
							    <div class="col-lg-9">
												<input v-model="user.email" readonly class="form-control form-control-sm editprofile">
										</div>
							  </div> 

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Prénom</label>
							    <div class="col-lg-9">
							      <input v-model="user.name" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							  <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Nom</label>
							    <div class="col-lg-9">
							      <input v-model="user.surname" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							  <div class="form-group row profilerow">
							    <label  class="col-lg-3 col-form-label col-form-label-sm">Entreprise</label>
							    <div class="col-lg-9">
							      <input v-model="user.organization" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							  <div class="form-group row profilerow">
							    <label  class="col-lg-3 col-form-label col-form-label-sm">Adresse</label>
							    <div class="col-lg-9">
							      <input v-model="user.address" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label  class="col-lg-3 col-form-label col-form-label-sm">Adresse 2</label>
							    <div class="col-lg-9">
							      <input v-model="user.address2" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label  class="col-lg-3 col-form-label col-form-label-sm">Ville</label>
							    <div class="col-lg-9">
							      <input v-model="user.city" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Code postal</label>
							    <div class="col-lg-9">
							      <input v-model="user.postal_code" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>

							  <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Pays</label>
							    <div class="col-lg-9">
							       <select v-model="user.country" class="select" id="inlineFormCustomSelect">
															@include('frontend.includes.country-options')
							      </select>
							    </div>
							  </div>
							 
							  <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Téléphone</label>
							    <div class="col-lg-9">
							      <input v-model="user.phone" type="text" class="form-control form-control-sm editprofile">
							    </div>
							  </div>
							 
							  <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Page d'accueil</label>
							    <div class="col-lg-9">
							      <select v-model="user.homepage_url" type="text" class="form-control form-control-sm editprofile">
														<option value="{{route('front.home')}}">Accueil</option>
														<option value="{{route('front.program')}}">Programme du jour</option>
														<option value="{{route('front.we-train.videos')}}">We Train - vidéos</option>
														<option value="{{route('front.we-sweat.videos')}}">We Sweat - vidéos</option>
														<option value="{{route('front.we-eat.recipes')}}">We Sweat - recettes</option>
														<option value="{{route('front.account-subscriptions')}}">Mon Compte</option>
							      </select>
							    </div>
							  </div>

							   <div class="form-group row profilerow">
							    <label class="col-lg-3 col-form-label col-form-label-sm">Obligatoire <span>*</span></label>
							    <div class="col-lg-9">
												<input v-model="user.agreed_terms" type="checkbox" class="form-check-input" checked="true"><p id="dets">J'ai lu et j'accepte les <a href="{{ route('front.terms') }}">conditions générales d'utilisation et de vente.</a></p>
							    </div>
							  </div>
								<button type="submit" class="btn btn-primary float-right">Mettre à jour</button>
								<div class="clearfix"></div>
							</form>
							
						</div>

						<div class="container tab-pane fade" id="mesadhésions">
							<div class="table-responsive">
								<table class="table table-bordered profiletable">
									<thead>
										<th>Formule d'abonnement</th>
										<th>Prochaine date de paiement</th>
										<th>Prix</th>
										<th>Date de commencement</th>
										<th>Status</th>
									</thead>
									<tbody>
										<tr
											v-for="subscription in user.active_subscriptions"
											:key="subscription.id"
										>
											<td>@{{subscription.offer.name}}</td>
											<td>@{{new Intl.DateTimeFormat('fr-FR').format(new Date(subscription.next_payment_date))}}</td>
											<td>@{{new Intl.NumberFormat('fr',{minimumFractionDigits:2}).format(subscription.price)}}&nbsp;€</td>
											<td>@{{new Intl.DateTimeFormat('fr-FR').format(new Date(subscription.subscribed_on))}}</td>
											<td>Actif</td>
										</tr>
									</tbody>
								</table>
							</div>

							<hr style="border-top:1px solid gainsboro; transform: scaleX(1.01);">
							<hr>

							<div class="table-responsive">
								<table class="table table-bordered profiletable">
									<thead>
										<th>Formule d'adhésion</th>
										<th>Période valide</th>
										<th>Status</th>
									</thead>
									<tbody>
										<tr
											v-for="purchase in user.active_purchases"
											:key="purchase.id"
										>
											<td>@{{purchase.offer.name}}</td>
											<td>@{{new Intl.DateTimeFormat('fr-FR').format(new Date(purchase.effective_on))}} à @{{new Intl.DateTimeFormat('fr-FR').format(new Date(purchase.expires_on))}}</td>
											<td>Actif</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="container tab-pane fade" id="historique">
							<div class="table-responsive">
								<table class="table table-bordered profiletable">
									<thead>
										<th>Formule d'abonnement</th>
										<th>Prochaine date de paiement</th>
										<th>Prix</th>
										<th>Date de commencement</th>
										<th>Status</th>
									</thead>
									<tbody>
										<tr
											v-for="subscription in user.subscriptions"
											:key="subscription.id"
										>
											<td>@{{subscription.offer.name}}</td>
											<td>@{{subscription.is_active ? new Intl.DateTimeFormat('fr-FR').format(new Date(subscription.next_payment_date)) : ''}}</td>
											<td>@{{new Intl.NumberFormat('fr',{minimumFractionDigits:2}).format(subscription.price)}}&nbsp;€</td>
											<td>@{{subscription.subscribed_on ? new Intl.DateTimeFormat('fr-FR').format(new Date(subscription.subscribed_on)) : ''}}</td>
											<td>@{{getSubscriptionStatus(subscription)}}</td>
										</tr>
									</tbody>
								</table>
							</div>

							<hr style="border-top:1px solid gainsboro; transform: scaleX(1.01);">
							<hr>

							<div class="table-responsive">
								<table class="table table-bordered profiletable">
									<thead>
										<th>Formule d'adhésion</th>
										<th>Période valide</th>
										<th>Status</th>
									</thead>
									<tbody>
										<tr
											v-for="purchase in user.purchases"
											:key="purchase.id"
										>
											<td>@{{purchase.offer.name}}</td>
											<td>@{{new Intl.DateTimeFormat('fr-FR').format(new Date(purchase.effective_on))}} à @{{new Intl.DateTimeFormat('fr-FR').format(new Date(purchase.expires_on))}}</td>
											<td>@{{getPurchaseStatus(purchase)}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
				

					<div class="container tab-pane active">
						
						<form
							v-if="user.active_purchases && user.active_purchases.length>0 && upgrade_offers.length>0"
							@submit.prevent="postUpgrade"
							class="subsform"
						>
							<h2>Changer d'adhésion</h2>
							<hr>
							<div
								v-for="offer in upgrade_offers"
								:key="offer.id"
								class="form-check"
							>
									<label class="form-check-label">
										<input
											v-model="selected_offer"
											:value="offer.id"
											type="radio"
											class="form-check-input"
											name="optradio1"
										>
										Changer votre formule d'adhésion @{{user.active_purchases[0].offer.title}} pour la formule @{{offer.title}}. Prix @{{getPrice(offer)}}.
									</label>
							</div>
							<hr>

								<button type="submit" class="btn btn-primary float-right">Mettre à niveau</button>
								<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>	
	
@endsection
@section('footer')


<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/mon-compte.js?v={{filever('public/assets/js/mon-compte.js')}}"></script>

@endsection