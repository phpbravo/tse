<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Exercise, Category};

class ExercisesController extends Controller
{

	public function getWeTrainSubcategories()
	{
		$subcategories=Category::find(1)->subcategories;

		return $subcategories;
	}

	public function getWeSweatSubcategories()
	{
		$subcategories=Category::find(2)->subcategories;

		return $subcategories;
	}

	public function getWeTrainVideos()
	{
		$filterHandler=new FilterHandler(Exercise::class);
		$pageHandler=new PageHandler(Exercise::class);
		$objectHandler=new ObjectHandler(Exercise::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=10;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page->items->makeHidden('content');

		$page->items->each->append('video_name');

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	}

}
