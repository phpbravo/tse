@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="concept-section sissy" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section sissy-section">
		<div class="container">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">SISSY</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="sissy-inner">
						<h3 class="title-about">
							<strong>SISSY</strong>
						</h3>
						<p>
							Durant trop longtemps, on m’a imposé les diktats d’un corps parfait. Mince mais pas maigre, pulpeux mais pas rond, tonique mais pas musclé. On a tenté de me faire croire que la musculation était un sport d’homme et que je ne pouvais être ni musclée, ni puissante. Aujourd’hui, on me dit parfois que j’ai plus de cuisses qu’un homme et que les épaules musclées ce n’est « pas beau chez une femme ». Et si ma féminité n’était pas proportionnelle à la taille de mon soutien-gorge, mais à ma motivation, à ma détermination et à ma confiance en moi ? Vous levez vous le matin en cherchant vos défauts, ou plutôt en admirant vos qualités ? Personnellement, je me lève chaque jour en étant fière de moi, de ce que j’ai accompli la veille, et en étant motivée à atteindre de nouveaux objectifs. Pourtant, je ne suis pas la femme parfaite. Je deviens seulement la meilleure version de moi-même. Physiquement, mais surtout mentalement. Cette force, je la puise dans le sport.
						</p>

						<p>
							Sportive depuis ma plus jeune enfance, j’ai poussé les portes d’une salle de muscu il y a 10 ans déjà, dans le but de me dépasser, et de montrer que je n’étais plus une petite adolescente fragile. J’ai adoré cet ambiance masculine et testostéronée. J’ai oublié les body-goals que je pouvais avoir, et je m’en suis créé un nouveau : le miens. J’ai vite oublié mes « grosses cuisses » et je me suis forgée des quads en béton. Je n’ai pas honte d’avoir les cuisses galbées, alors que la mode prône les jambes de gazelles. Je suis fière de pousser plus lourd à la presse que certains hommes de ma salle.
						</p>

						<p>
							Grâce au fitness, chaque jour est un nouveau challenge pour moi. Dans le sport, mais aussi professionnellement ou personnellement. Ma volonté est sans faille. Je repousse sans cesse mes limites et je me sens invincible. Je ne prends jamais une défaite comme un échec, mais toujours comme un nouveau challenge. Ma muscu est une muscu positive. Intensité et régularité oui, évidemment, mais aussi plaisir pour ne jamais se lasser et ainsi obtenir de véritables résultats. Ce sont toutes ces notions que j’ai aujourd’hui envie de vous partager grâce à yeswetrain.com. Vous permettre à vous aussi d’avoir de nouveaux objectifs et surtout, de les atteindre. Contrairement à ce que l’on retrouve beaucoup sur les réseaux sociaux, je ne vous vends pas de la poudre de perlimpinpin exorbitante et surtout inefficace. Ce que je vous partage, ce sont nos véritables entrainements, à Tini et moi, ceux que nous faisons chaque jour ! Ceux qui nous permettent de toujours progresser après 10 ans, et d’obtenir le corps que nous nous forgeons. Je ne suis pas une espèce incroyable, je suis juste une femme avec une volonté sans limite. Ce que j’ai accompli, vous pouvez le faire également. Ensembles, We Train !
						</p>
					</div>
				</div>
				<div class="col-lg-6 sissy-img-hide">
					<div class="sissy-rightimg">
						<img src="/assets/img/Sissy.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section class="passion-section sissy-notre">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
								
							</div>

							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE RENCONTRE</h1>
								<h3>TINI ET MOI</h3>
								<p class="notrep">La première fois que Tini et moi nous sommes rencontrés, c’était bel et bien dans une salle de sport, pendant les vacances ! Très cliché, mais pourtant vrai. Manque de pot pour moi, je finissais ma séance toute transpirée lorsqu’ il est arrivé. Je ne me suis, comme vous vous en doutez, pas attardée ! En revanche, nous nous sommes rapidement revus en France dès notre retour. Nous avions énormément de points communs et plein de choses à nous raconter !
								</p>

								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	
	

	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>TRAIN </span> SWEAT EAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="WE TRAIN">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>WE TRAIN</h2>
									<p>
										Quotidiennement, nous partageons avec vous nos <strong>entrainements</strong> en salle de musculation ! Ensemble, <strong>progressons chaque jour</strong> et restons <strong>surmotivés</strong> !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-heart" data-headline="WE SWEAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-heart">
									<h2>WE SWEAT</h2>
									<p>
										Retrouvez chaque mois en<strong> vidéo live</strong>, mes <strong>entrainements</strong> poids du corps favoris pour <strong>brûler un max de calories</strong> et <strong>sculpter son corps</strong> ! Transpirons ensembles dans votre salon !
									</p>
									<br>
									<p><a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-utensils" data-headline="WE EAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-utensils">
									<h2>WE EAT</h2>
									<p>
										Chaque mois, cuisinons ensembles de<strong> nouvelles recettes</strong> aussi fit que gourmandes ! Rapides et simples à réaliser, vous n’aurez plus d’excuses !
									</p>
									<br>
									<p><a href="{{ route('front.we-eat.recipes') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@include('frontend.includes.parallax-last')	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection