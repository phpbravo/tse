<?php

namespace App\Handlers;

class BailHandler
{
	public function bail($message, int $status = 400): void
	{
		header('Content-Type: application/json; charset=UTF-8');
		http_response_code($status);
		echo json_encode($message);
		exit;
	}

	public function bailInvalidGateway(): void
	{
		$this->bail([
			'status'=>'invalid_gateway',
			'message'=>'invalid gateway'
		]);
	}

	public function bailGatewayNotAvailableForOffer(): void
	{
		$this->bail([
			'status'=>'invalid_gateway',
			'message'=>'Le mode de paiement sélectionné n\'est pas'
				.' disponible pour cette offre.'
		]);
	}

	public function bailInvalidPaymentType(): void
	{
		$this->bail([
			'status'=>'invalid_payment_type',
			'message'=>'invalid payment type'
		]);
	}

	public function bailUserAlreadySubscribed(): void
	{
		$this->bail([
			'status'=>'already_subscribed',
			'message'=>'Vous êtes déjà abonné à cette offre.'
		]);
	}

	public function bailOfferNotAvailable(): void
	{
		$this->bail([
			'status'=>'invalid_offer',
			'message'=>'L\'offre sélectionnée n\'est pas disponible.'
		]);
	}

	public function bailOfferNotForUpgrade(): void
	{
		$this->bail([
			'status'=>'invalid_offer',
			'message'=>'L\'offre sélectionnée n\'est pas disponible'
				.' pour une mise à niveau.'
		]);
	}

	public function bailSubscriptionNotFound(): void
	{
		$this->bail([
			'status'=>'subscription_not_found',
			'message'=>'subscription not found'
		],404);
	}

	public function bailTransactionNotFound(): void
	{
		$this->bail([
			'status'=>'transaction_not_found',
			'message'=>'transaction not found'
		],404);
	}

	public function bailCardDeclined(): void
	{
		$this->bail([
			'status'=>'card_declined',
			'message'=>'Votre carte a été refusée, essayez une autre carte ou'
				.' contactez votre banque.'
		]);
	}

	public function bailProcessingError(): void
	{
		$this->bail([
			'status'=>'processing_error',
			'message'=>'Une erreur s\'est produite lors du traitement de votre carte.'
				.' Veuillez réessayer plus tard.'
		]);
	}

	public function bailCardError(): void
	{
		$this->bail([
			'status'=>'card_error',
			'message'=>'Les détails de votre carte ne sont pas valides, veuillez'
				.' les corriger et réessayer.'
		]);
	}
}