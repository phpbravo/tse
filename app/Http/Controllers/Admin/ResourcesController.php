<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Resource;

class ResourcesController extends Controller
{

	private function validateResource($fields){
		$objectHandler=new ObjectHandler(Resource::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Resource::class);
		$pageHandler=new PageHandler(Resource::class);
		$objectHandler=new ObjectHandler(Resource::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('product');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function show($id){

		$product=Resource::with(['sessions'])->find($id);

		if(!$product){
			return (new ObjectHandler(Resource::class))->newError('not_found');
		}

		return $product;
	}

	public function list(){

		return Resource::all();

	}

}
