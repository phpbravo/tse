@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	

	<section class="temoignages-section conditions-generalestemp">
		<div class="container ">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">CONDITIONS GÉNÉRALES D'UTILISATION ET DE VENTE</h1>
					<hr>
				</div>
				
				<div class="col-lg-12 conditions-generales">
					<p>Date de mise en ligne : 15/04/2019</p>
					<p>Date de dernière mise à jour : 16/09/2019</p>
					<p>Voir ou télécharger les Conditions Générales d’Utilisation et de Vente sur support durable.</p>
					<p>Le Site www.trainsweateat.com est édité par la Société SAS TISSY. Les présentes conditions générales d’utilisation et de vente, ainsi que les mentions légales sont en permanence accessibles depuis le site à l’adresse :</p>
					<p><strong>Société responsable</strong> : Société TISSY - SASU immatriculée au RCS de Nice sous le numéro 847869963</p>
					<p><strong>Adresse du siège social</strong> : 455, promenade des Anglais</p>
					<p><strong>Numéro intracommunautaire : FR47847869963</strong></p>
					<p><strong>Email de contact</strong> : support@trainsweateat.com</p>
					<p><strong>Éditeur et directeur de la publication </strong>: Société TISSY</p>
					<p><strong>DPO et adresse de contact : <a href="#">contact@trainsweateat.com</a> </p>
					<p><strong>Coordonnées de l’hébergeur du site : TAS France</strong> 15, traverse des Brucs <br>06560 Valbonne Sophia Antipolis</p>
					<p><strong>Conception : <a href="#">www.coteweb.fr</strong></a></p>
				</div>

				<div class="col-lg-12">
					<p>TRAINSWEATEAT est une marque déposée auprès de l’INPI.</p>

					<ol class="list-parent">
						<li class="list-child"><p>PREAMBULE</p>
							<ol class="preambule-parent">
								<li >La Société TISSY (ci-après désignée par « TISSY ») exerce sur internet et sur les réseaux sociaux (Facebook, Instagram, Twitter, Snapchat, YouTube) une activité d’influenceurs dans les domaines du sport, du coaching personnel, de la nutrition, des compléments alimentaires et de la beauté.</li>
								<li>Dans ce contexte, la société propose au travers du présent Site (ci-après, « le Site ») la vente d’abonnements à des contenus sportifs en ligne, regroupant des programmes de cardio (ci-après désigné par <strong>« We Sweat »</strong>) et des programmes de musculation (ci-après désigné par <strong>« We Train »</strong>) ainsi que des informations gratuites de recettes de cuisine et conseil en nutrition (ci-après désigné par <strong>« We Eat »</strong>) afin d’accompagner les deux contenus sportifs.</li>
								<li>Les présentes Conditions Générales d’Utilisation et de Vente (ci-après « CGUV ») ont pour objet de régir de manière exclusive l’ensemble des relations commerciales issues de l’Utilisation du Site et de ses Services par les Utilisateurs ayant la qualité de Consommateurs (ci-après, « l’Utilisateur »).</li>
								<li>L’Utilisation du site est strictement réservée à un usage privé et personnel.</li>
								<li>Le Site et les Services qu’il propose sont ouverts à tous les pays du monde qui n’interdisent, ni ne réglementent, de manière générale, l’activité de fourniture de contenu sportif en ligne. A défaut, il appartient à l’Utilisateur de renoncer à l’utilisation du Site et de ses Services d’abonnement aux contenus sportifs.</li>
								<li>Les présentes CGUV sont régies par les dispositions du Code civil et du Code de la consommation français.</li>
								<li>Les CGUV constituent avec la commande en ligne les documents contractuels opposables aux parties, à l’exclusion de tous autres documents, prospectus, catalogues et photographies des Services qui n’ont qu’une valeur indicative.</li>
								<li>La Société TISSY se réserve le droit de modifier les présentes CGUV à sa discrétion et à tout moment sans en informer au préalable l’Utilisateur, selon l’évolution de ses Services, ainsi que l’évolution de la législation en vigueur. L’utilisation du Site, des plateformes, des systèmes et de toute autre fonctionnalité proposée par TISSY est toujours soumise à la version la plus récente des CGUV disponibles sur le Site.</li>
								<li>Les équipements permettant l’accès au Site et aux Services dispensés sur le Site sont à la charge exclusive de l’Utilisateur, de même que les frais de connexion induits par l’utilisation.</li>
								<li>Le Site met en garde l’Utilisateur sur la nécessité de disposer d’une connexion internet sécurisée, permettant l’accès aux contenus depuis le support de son choix (ordinateur, tablette, téléphone portable) :</li>
								<li>Tous les termes commençant par une majuscule sont définis à l’article 2.</li>
							</ol>
						</li>

						<li class="list-child"><p>DEFINITIONS</p>
							<ul class="definition-parent">
								<li><strong>Site </strong>: Désigne le Site internet trainsweateat.com édité par la Société TISSY et l’ensemble des Services dispensés par ce dernier à un Utilisateur.</li>
								<li><strong>Utilisateur</strong> : Toute personne physique capable majeure ou ayant plus de 16 ans avec l’accord de ses responsables légaux, agissant à des fins personnelles et privée qui n’entrent pas dans le cadre de son activité commerciale, industrielle, artisanale ou libérale.</li>
								<li><strong>Abonnement</strong> : Offre mensuelle (1 mois), trimestrielle (3 mois), semestrielle (6 mois), annuelle (1 an) ou trisannuelle (3 ans) d’abonnement comprenant obligatoirement et de manière indissociable l’accès à deux programmes « We Train » et « We Sweat » accompagnés de recettes de cuisine et de conseil en nutrition « We Eat ». L’adhésion nécessite la création par l’Utilisateur d’un Compte Abonné.<br><br>
									<ul class="definition-grandchild">
										<li><strong>Programme « We Train »</strong>: Programme de musculation qui comprend des vidéos journalières de training à la salle de musculation, des exercices détaillés avec matériel à réaliser obligatoirement en salle de sport et des séances bonus.</li>
										<li><strong>Programme « We Sweat »</strong>: Programme de cardio qui comprend cinq vidéos mensuelles en direct (live) de training intensif mêlant des exercices de cardio et de renforcement musculaire sans matériel.</li>
										<li><strong>Programme « We Eat »</strong> : Programme de recettes de cuisine et de conseil en nutrition. </li>
									</ul>
								</li>
								<li><strong>Compte Abonné </strong>: Formalité d’inscription préalable pour la création d’un compte dédié et personnel nécessaire à l’Utilisateur afin d’accéder aux Services « We Sweat » et « We Train » et « We Eat ».</li>
							</ul>
						</li>

						<li class="list-child"><p>OBJET ET CONDITIONS D’UTILISATION</p>
							<ol class="objet-child">
								<li>Les présentes CGUV régissent l’ensemble des relations commerciales issues de l’Utilisation des Services du Site par les Utilisateurs.</li>
								<li>Par l’utilisation des Services proposés par la Société TISSY l’Utilisateur reconnait être sain d’esprit, majeur capable ou avoir plus de 16 ans et obtenu l’accord de ses responsables légaux, ayant la capacité de contracter et ne faire l’objet d’aucune mesure de protection juridique des majeurs tels que défini aux articles 425 et suivants du Code civil.</li>
								<li>L’Abonnement proposé sur le Site est exclusivement réservé aux personnes physiques justifiant d’un état de santé ne présentant aucune contre-indication à la pratique sportive, et notamment aux trainings et exercices proposés dans le cadre desdits programmes d’entrainement « We Sweat » et « We Train ».</li>
								<li>Dans ce contexte, l’Utilisateur déclare, préalablement à toute création de Compte Abonné et souscription de l’Abonnement, s’être assuré de son aptitude physique à suivre programmes d’entrainements proposés par le Site.</li>
								<li>Ce dernier s’engage à avoir préalablement procédé à une visite médicale ayant pour but la délivrance d’un certificat médical indiquant aucune contre-indication quant à la pratique des exercices de musculation et de cardio proposés par le Site.</li>
								<li>Si l’Utilisateur n’approuve pas pleinement les CGUV, ce dernier n’est pas autorisé à utiliser le Site.</li>
								<li><strong>L’UTILISATEUR DÉCLARE AVOIR PRIS CONNAISSANCE DES PRÉSENTES CGUV DANS LEUR INTÉGRALITÉ ET ACCEPTER PLEINEMENT ET SANS RÉSERVE LES OBLIGATIONS QUI LUI INCOMBENT. L’UTILISATION DU SITE ET LA SOUSCRIPTION A l’ABONNEMENT IMPLIQUE L’ACCEPTATION SANS RESTRICTION, NI RESERVE DES CGUV.</strong></li>
								<li>Toute création d’un Compte Abonné ou validation d’une souscription à un Abonnement vaut adhésion et acceptation sans réserve par l’Utilisateur des CGUV en vigueur au jour de la commande dont la conservation et la reproduction sont assurées par la Société TISSY conformément aux dispositions de l’article 1127-2 du Code civil.</li>
								<li>Dans le cadre de l’utilisation du Site et de ses Services, il est strictement interdit notamment de :
									<ul class="dans-parent">
										<li>Copier, modifier ou altérer tout ou partie du Site ;</li>
										<li>Utiliser des services d’une manière qui n’est pas loyale et sincère ;</li>
										<li>Utiliser tout ou partie des fonctionnalités du Site d’une façon et/ou dans un but contraire aux lois et règlements en vigueur ;</li>
										<li>Collecter ou recueillir sous quelque forme et dans quelque but que ce soit des données, personnelles ou non ;</li>
										<li>Porter atteinte de quelque façon que ce soit aux droits des Utilisateurs ou des tiers ;</li>
										<li>Tenir ou proférer des propos ou diffuser sous quelque forme que ce soit des contenus portant atteinte de quelque manière que ce soit aux droits d’autrui et de manière générale tout contenu contraire aux lois en vigueur en France ;</li>
										<li>Contrevenir à une disposition légale ou règlementaire en vigueur. Le Site se réserve le droit de contrôler à tout moment le respect des CGUV par les Utilisateurs. Le non-respect de l’une ou l’autre des stipulations des présentes CGUV met automatiquement fin à l’autorisation d’utiliser les Services qui y sont proposés et peut entrainer la suppression du Compte Abonné</li>
									</ul>

								</li>
							</ol>

						</li>

						<li class="list-child"><p>ABONNEMENT</p>
							<ul class="lasociete">
								<li> La société propose sur son site un Abonnement qui comprend l’accès à trois programmes d’entrainement de musculation « We Train »,  de cardio « We Sweat » et de programmes de recettes « We Eat ». <br><br> L’Abonnement est  au choix de l’Utilisateur :
									<ul class="lasociete-child">
										<li>mensuel (1 mois) par mois renouvelé par tacite reconduction.</li>
										<li>trimestriel (3 mois) pour trois mois, payable en une seule fois renouvelé par tacite reconduction..</li>
										<li>semestriel (6 mois) pour six mois, payable en une seule fois renouvelé par tacite reconduction.</li>
										<li>annuel (1 an) pour douze mois, payable en une seule fois renouvelé par tacite reconduction.</li>
										<li>trisannuel (3 ans) pour trente six mois, payable en une seule fois renouvelé par tacite reconduction.</li>
									</ul>
								</li>
								<li> La Société se réserve le droit de supprimer, modifier ou remplacer un ou plusieurs des programmes et/ou Offre proposés sur le Site, à tout moment, étant entendu que ces modifications ne seront pas opposables aux Abonnements en cours d’exécution</li>
							</ul>
						</li>

						<li class="list-child">
							<p>MODALITES D’INSCRIPTION, DE SOUSCRIPTION ET DE RENOUVELLEMENT DE L’OFFRE</p>
							<ol class="objet-child modalités">
								<li><strong>Création d’un Compte Abonné</strong>
									<ol>
										<li>L’accès et l’utilisation des Services de mise en ligne de contenu sportif nécessitent obligatoirement (1) la création d’un Compte Abonné, (2) l’acceptation pleine et expresse des Conditions Générales d’Utilisation et de Vente et (3) le paiement des sommes dues au titre de l’Abonnement choisi par l’Utilisateur.</li>
										<li>Tout utilisateur peut facilement et gratuitement créer son Compte Abonné en cliquant sur le lien « S’inscrire à l’Abonnement ».</li>
										<li>La création du Compte Abonné requiert une inscription préalable de l’Utilisateur se traduisant par le renseignement de certaines informations obligatoires à savoir : nom, prénom,  date de naissance, adresse e-mail, adresse postale, numéro de téléphone, e-mail et mot de passe.</li>
										<li>L’inscription nécessite la connaissance et l’acceptation par l’Utilisateur des Conditions Générales d’Utilisation et de Vente du Site, laquelle se traduit par une case à cocher lors de la confirmation par l’Utilisateur de la création du Compte Abonné.</li>
										<li>L’Utilisateur s’engage à ne communiquer que des informations exactes, actuelles et complètes dont il garantit en tout temps l’exactitude, la sincérité et la fiabilité, lors de la création d’un Compte Abonné. L’Utilisateur s’engage à procéder sans délai à tout changement des informations le concernant.</li>
										<li>L’Utilisateur est seul responsable de la gestion et de la confidentialité de ses moyens d’authentification (e-mail et mot de passe), lesquels sont personnels et confidentiels. Il supporte seul les conséquences pouvant résulter de la perte, la divulgation ou de l’utilisation frauduleuse ou illicite des moyens d’authentification, le Site ne pouvant en aucun cas être tenu responsable. L’Utilisateur s’engage à informer sans délai le Site de toute perte ou divulgation éventuelle de ses moyens d’authentification, et à procéder à la modification par renouvellement desdits moyens d’authentification.</li>
										<li>La création d’un Compte Abonné implique, après les étapes d’inscription et d’acceptation des CGUV par l’Utilisateur, le paiement des sommes dues au titre de l’abonnement choisi.</li>
									</ol>
								</li>
								<li>
									<strong> Choix de l’Abonnement mensuel, trimestriel, semestriel, annuel, ou trisannuel</strong>
									 <ol>
									 	<li>L’Utilisateur a la possibilité de choisir soit un Abonnement mensuel payable tous les mois ou un Abonnement trimestriel, semestriel, annuel ou trisannuel payable en une fois lors de sa souscription (puis tacitement renouvellé).</li>
									 	<li>L’Utilisateur reconnaît expressément qu’en souscrivant à un Abonnement - il s’oblige au paiement de celui-ci dans son intégralité et pour la durée choisie.</li>
									 	<li>Après confirmation des détails de la commande, de la renonciation ou non par l’Utilisateur à son droit de rétractation (Article 7) et validation du paiement, l’Utilisateur reçoit un e-mail de confirmation de souscription lui rappelant les modalités d’utilisation de l’abonnement souscrit.</li>
									 </ol>
								</li>
								<li><strong>Renouvellement tacite de l’Abonnement</strong>
									<ol>
										<li> <strong>Abonnement mensuel</strong> <br><br>
											Sauf résiliation par l’Utilisateur avant la fin de la période d’abonnement en cours selon les modalités prévues à l’article 9 « Conditions de résiliation d’un abonnement », tout Abonnement mensuel sera tacitement reconduit pour une nouvelle durée identique à celle initialement souscrite.
										</li>

										<li><strong>Abonnement trimestriel, semestriel, annuel ou trisannuel</strong> <br><br>
											Conformément aux dispositions des articles L.215-1 et suivants du Code de la consommation, dans le cas où l’Utilisateur a souscrit un Abonnement trimestriel (3 mois), semestriel (6 mois), annuel (1 an) ou trisannuel (3 ans) ce dernier recevra un e-mail de la part du Site, au plus tard un (1) mois avant la fin de l’Abonnement, lui notifiant sa faculté de résilier ledit Abonnement à son terme.<br>
											Si l’Utilisateur ne résilie pas l’Abonnement trimestriel, semestriel, annuel ou trisannuel dans le délai indiqué dans l’e-mail, celui-ci sera reconduit pour une nouvelle durée de trois, six mois, 12 mois ou 36 mois (suivant Abonnement souscrit).
										</li>

										<li>
											A défaut de paiement total ou partiel d’une seule échéance à la date convenue, l’accès aux programmes seront suspendus jusqu’à régularisation de la situation. Le Site informera immédiatement l’Utilisateur de la suspension de l’accès aux programmes lui indiquant qu’il dispose d’un délai d’un (1) mois pour régulariser la situation. Faute de régularisation dans le délai prévu, l’Abonnement sera résilié de plein droit par le Site, nonobstant le droit pour le Site de demander des dommages et intérêts du fait du non-paiement de la résiliation fautive.
										</li>

										<li>
											L’Utilisateur ne pourra plus accéder à Son Compte d’Abonné en cas d’incident de paiement non résolu.
										</li>
									</ol>
								</li>
							</ol>
						</li>

						<li class="list-child">
							<p>PRIX ET MODALITES DE PAIEMENT</p>

							<ol class="objet-child prix">
								<li>Le prix de l’Abonnement sur le Site est indiqué en Euros, toutes taxes comprises (TTC).</li>
								<li>Le Site se réserve le droit de modifier le prix de l’Abonnement, et s’engage dans cette hypothèse à communiquer à l’Utilisateur toute modification tarifaire à l’avance, ainsi que le cas échéant les modalités d’acceptation de ces changements. Les modifications tarifaires prendront effet à compter de la nouvelle période d’abonnement consécutive à la date de modification tarifaire.</li>
								<li>En cas de non-acceptation des nouveaux tarifs, l’Utilisateur se réserve le droit de refuser la modification en résiliant son Abonnement avant l’entrée en vigueur de la modification tarifaire, dans les conditions prévues à l’article 9.1.</li>
								<li><strong>Concernant l’Abonnement mensuel</strong><br><br>
									Le paiement s’effectue au moment de la souscription, par carte bancaire (CB, Visa, MasterCard, American Express) sur la plateforme sécurisée de paiement d’un tiers certificateur Stripe, sur laquelle l’Utilisateur renseigne directement ses coordonnées bancaires. En aucun cas, le Site et/ou la Société TISSY ont et auront accès aux coordonnées bancaires de l’Utilisateur.<br>
									<br>
									Sauf résiliation par l’Utilisateur, le paiement des mensualités suivantes sera automatiquement effectué par prélèvement sur la carte bancaire dont l’Utilisateur aura renseigné les coordonnées précédemment.
								</li>
								<li><strong>Concernant l’Abonnement trimestriel, semestriel, annuel ou trisannuel</strong> <br>
									<br>
									Le paiement s’effectue dans sa totalité au moment de la souscription, par carte bancaire (CB, Visa, MasterCard, American Express) la plateforme sécurisée de paiement d’un tiers certificateur Stripe sur laquelle l’Utilisateur renseigne directement ses coordonnées bancaires. En aucun cas, le Site et/ou la Société TISSY ont et auront accès aux coordonnées bancaires de l’Utilisateur.<br><br>
									Sauf résiliation par l’Utilisateur dans la date limite de non-reconduction indiquée dans l’e-mail, l’Abonnement trimestriel, semestriel, annuel ou trisannuel sera tacitement renouvelé pour une nouvelle période (3, 6, 12 ou 36 mois suivant durée de l'abonnement choisi). En conséquence, le paiement de l’intégralité de la souscription, soit des trois(3), six (6), douze (12) ou trente six (36) mensualités sera automatiquement effectué par prélèvement sur la carte bancaire dont l’Utilisateur aura renseigné les coordonnées précédemment.
								</li>
								<li>Il appartient à l’Utilisateur de mettre à jour ses coordonnées bancaires, en temps utile, et d’en informer le Site, au même titre que tout éventuel incident de paiement.</li>
								<li>L’UTILISATEUR GARANTIT QU’IL DISPOSE DE TOUTES LES AUTORISATIONS NECESSAIRES A UNE UTILISATION PERSONNELLE DU MOYEN DE PAIEMENT SELECTIONNE.</li>

							</ol>
						</li>

						<li class="list-child"><p>DROIT DE RETRACTATION</p>
							<ol class="objet-child droit">
								<li>Conformément aux dispositions de l’article L.221-18 du Code de la consommation,<strong> l’Utilisateur dispose d’un délai de quatorze (14) jours à compter de la souscription de l’Abonnement pour exercer son droit de rétractation, sans avoir à motiver sa décision ni à supporter d’autres coûts que ceux prévus aux articles L.221-23 à L.221-25 du même Code.</strong></li>
								<li>Pour ce faire, ce dernier doit notifier l’exercice en contactant par email <a href="#">contact@trainsweateat.com</a></li>
								<li>En cas d’exercice du droit de rétractation dans les délais suivant les formes requises ci-avant, l’Utilisateur sera remboursé de l’intégralité des sommes versées au plus tard dans les quatorze jours suivant la notification de la décision de se rétracter. A défaut de demande expresse autre, le remboursement s’effectuera par le biais du même moyen de paiement que celui utilisé par l’Utilisateur</li>
								<li><strong>L’Utilisateur n’aura accès au contenu numérique des programmes « We Train » et « We Sweat » qu’à l’expiration du délai de rétractation de quatorze jours.</strong></li>
								<li><strong>INFORMATION EN CAS D’UTILISATION DE l’ABONNEMENT AVANT LA FIN DU DELAI DE RETRACTATION </strong>:<br><br>
									L’article L.221-28 du Code de la consommation écarte l’exercice du droit de rétractation pour les contrats de fourniture d’un contenu numérique non fourni sur un support matériel dont l’exécution a commencé après accord préalable exprès du consommateur et renoncement exprès de son droit de rétractation.<br><br>

									En principe, l’Utilisateur ayant souscrit un Abonnement doit attendre l’expiration du délai de rétractation de quatorze jours pour avoir accès au contenu numérique souscrit, qu’il s’agisse des programmes « We Train », « We Sweat » ou « We Eat »
								</li>
							</ol>
						</li>

						<li class="list-child">
							<p>CONDITIONS DE RESILIATION D’UN ABONNEMENT</p>
							Les Abonnements demeureront en vigueur jusqu’à leur résiliation à l’initiative de l’Utilisateur ou du Site.
							<ol class="objet-child conditions">
								<li><strong>Résiliation du fait du Client</strong><br><br>
									L’Utilisateur a toujours la faculté de résilier son abonnement depuis son Compte Abonné, sans motif ni frais, en cliquant sur la rubrique « Résilier mon abonnement ». La prise d’acte de la résiliation sera effective au terme de la période d’abonnement en cours, sous réserve que l’Utilisateur ait notifié sa demande au moins quarante-huit (48) heures avant son terme.<br><br>

									La demande de résiliation n’entraine en aucun cas le remboursement à l’Utilisateur de la période restant à courir jusqu’à l’échéance du terme de l’abonnement. 
								</li>

								<li><strong>Résiliation du fait du Site</strong><br><br>
									Le Site se réserve le droit de résilier à tout moment, un Abonnement et/ou de clôturer un Compte Abonné en cas d’utilisation frauduleuse ou illicite des moyens d’authentification (utilisateur et/ou mot de passe) d’un Utilisateur, en cas de violation des présentes CGUV, ainsi qu’en cas de défaut de paiement d’une des mensualités, dans l’hypothèse prévue à l’article 5.3.3.

									Dans cette hypothèse, l’Utilisateur reconnaît avoir été informé que l’Abonnement sera résilié de plein droit par le Site, sans possibilité pour l’Utilisateur de prétendre à une quelconque indemnité ni remboursement de la période d’abonnement restant à courir au jour de la prise d’acte de la résiliation.

								</li>
							</ol>
						</li>

						<li class="list-child">
							<p>ENGAGEMENT DE L’UTILISATEUR</p>
							<ol class="objet-child engagement">
								<li>ATTENTION : Compte tenu de la nature sportive des programmes « We Train » et « We Sweat » mis en ligne dans le cadre de l’Offre Unique, l’Utilisateur s’engage à adopter un comportement raisonnable, adéquat et réfléchi lors du visionnage et de la reproduction des exercices et trainings.</li>
								<li>L’Utilisateur reconnaît être informé que les trainings et exercices mis en ligne ne sauraient s’analyser en une prestation de coaching sportif mais constituent une prestation de mise en ligne de programmes et d’entrainements sportifs journaliers standards et en aucun cas personnalisée à la personne qui l’utilise.</li>
								<li>En conséquence, l’Utilisateur s’engage à suivre les recommandations énoncées par le Site et à respecter toutes les règles de sécurité, d’hygiène et de comportement raisonnablement attendues d’un Utilisateur dans la pratique d’une activité sportive.</li>
								<li>L’Utilisateur reconnait que les trainings et exercices mis en ligne dans le cadre des programmes « We Train » et « We Sweat » sont proposés dans un perspective de divertissement sportif.</li>
								<li>Il appartient à l’Utilisateur d’adapter les exercices, le poids des charges, la fréquence des exercices et le temps de repos entre les séries, en fonction de son état de santé et de ses capacités tant physiques que respiratoires en fonction de son rythme, sa force et sa morphologie.</li>
								<li>Il est expressément précisé que les exercices du programme « We Train » sont exclusivement destinés à un visionnage et un usage dans une salle de musculation, sous la surveillance et/ou la responsabilité d’un professionnel, à la différence des exercices du programme « We Sweat » lesquels peuvent également faire l’objet d’une pratique à domicile ou à l’extérieur sous réserve de respecter les règles de prudence et de sécurité qui s’imposent.</li>
								<li>L’Utilisateur doit s’assurer qu’il dispose d’une assurance responsabilité civile couvrant tous dommages corporels et/ou matériels qu’il est susceptible de causer à lui-même ou à autrui, dans le cadre du visionnage des vidéos et de la reproduction des exercices et trainings mis en ligne.</li>
							</ol>
						</li>

						<li class="list-child">
							<p>RESPONSABILITE</p>
							<ol class="objet-child responsabilité">
								<li>La Société dispense des programmes sportifs non individualisés et est soumise à une obligation de moyen. Toutes les informations et données diffusées dans les Programmes et/ou sur le Site  sont fournies à titre d’information. La fourniture de ces données ne saurait être assimilée, de quelque façon que ce soit, à un conseil spécifique. Le Site ne saurait être tenu pour responsable d’un quelconque dommage, direct ou indirect. L’Utilisateur utilise donc le Site à ses risques et périls et ne Site ne donne aucune garantie sur l’adéquation des Programmes aux besoins de l’Utilisateur.</li>

								<li><strong>L’UTILSATEUR EST SEUL RESPONSABLE DE L’UTILISATION DES INFORMATIONS, PROGRAMMES ET SERVICES PROPOSES, ET IL LUI APPARTIENT DE VERIFIER PREALABLEMENT A TOUTE UTILISATION DE L’ABONNEMENT SOUSCRIT LA CONFORMITE ET L’ADEQUATION DES PROGRAMMES D’EXERCICES PROPOSES A SES BESOINS, Y COMPRIS EN SOLLICITANT SI NECESSAIRE L’AVIS D’UN PROFESSIONNEL DE SANTE ET/OU DU SPORT</strong>. En aucun cas, le Site ne saurait être tenu pour responsable de l’inexécution du contrat conclu, en cas de rupture des Programmes, de force majeure, de dysfonctionnement, perturbation ou grève totale ou partielle, notamment des moyens de télécommunications. Le Site n’encourra aucune responsabilité pour tous dommages indirects du fait des présentes, perte d’exploitation, perte de profit, perte de chance, dommages ou frais.</li>

								<li>L’utilisation du Site et de ses Services par l’Utilisateur implique la connaissance et l’acceptation par celui-ci des caractéristiques et limites afférentes au réseau Internet, et notamment en ce qui concerne sa fiabilité.</li>
								<li>L’Utilisateur est informé que la Société TISSY peut être amenée à interrompre momentanément l’accès au Site pour des raisons techniques, et notamment pour en effectuer la maintenance. L’Utilisateur accepte expressément ses interruptions et renonce à exercer une quelconque réclamation à ce sujet, quelques soient les circonstances de ladite interruption.</li>
								<li>Le Site ne saurait en aucune circonstance être tenu responsable de tout dysfonctionnement du réseau empêchant le bon fonctionnement du Site, toute suppression ou perte de donnée, toute conséquence d’un virus, bug, anomalie ou défaillance informatique, ainsi que plus généralement de tout dommage causé à l’ordinateur ou autre équipement utilisé par l’Utilisateur pour accéder au Site - étant entendu que la présente liste n’est pas limitative.</li>
								<li>Force majeure : sera considéré comme un cas de force majeure tout fait ou circonstance irrésistible, extérieur aux parties, imprévisible, inévitable, indépendant de la volonté des parties et qui ne pourra être empêché par ces dernières, malgré tous les efforts raisonnablement possibles. La partie touchée par de telles circonstances en avisera l’autre dans les dix jours ouvrables suivant la date à laquelle elle en aura eu connaissance. Les deux parties se rapprocheront alors, dans un délai de trois mois, sauf impossibilité due au cas de force majeure, pour examiner l’incidence de l’événement et convenir des conditions dans lesquelles l’exécution du contrat sera poursuivie. Si le cas de force majeur a une durée supérieure à une durée d'un mois, les présentes CGV pourront être immédiatement résiliées par la partie lésée. De façon non exhaustive, sont considérés comme cas de force majeure ou cas fortuits, outre ceux qui sont habituellement retenus par la jurisprudence des cours et des tribunaux français : le blocage des moyens de transports ou de La Poste, tremblement de terre, incendies, tempêtes, inondation, foudre, l’arrêt des réseaux de télécommunication ou difficultés propres aux réseaux de télécommunication externes aux Utilisateurs.</li>
							</ol>
						</li>

						<li class="list-child">
							<p>PROPRIETE INTELLECTUELLE</p>
							<ol class="objet-child propriété">
								<li>TRAINSWEATEAT est une marque protégée par un enregistrement auprès de l’INPI, et est par conséquent la propriété exclusive de la Société TISSY</li>
								<li>Tous les textes, commentaires, ouvrages, illustrations, œuvres, images, photographies, vidéos et tous autres éléments graphiques ou visuels reproduits ou représentés sur le Site sont strictement réservés au titre du droit d'auteur ainsi qu'au titre de la Propriété Intellectuelle pour le monde entier.</li>
								<li>A ce titre et conformément aux dispositions du Code de la Propriété Intellectuelle, seule l'utilisation pour un usage privé sous réserve de dispositions différentes voire plus restrictives du Code de la Propriété Intellectuelle est autorisée.</li>
								<li>Toute reproduction ou représentation totale ou partielle du Site y compris des vidéos et de tous autres éléments graphiques ou visuels ou de tout ou partie des éléments se trouvant directement sur le Site ou indirectement lié au Site est strictement interdite.</li>
								<li>Les dénominations sociales, marques et signes distinctifs reproduits sur le Site sont protégés au titre du droit des marques. La reproduction ou la représentation de tout ou partie d'un des signes précités est strictement interdite et doit faire l'objet d'une autorisation écrite préalable du Site</li>
								<li class="bullet-listp">L'Utilisateur s'interdit également :
									<ul class="utilisateur-child">
										<li>L'extraction par transfert permanent ou temporaire de la totalité ou d'une partie qualitativement ou quantitativement substantielle du contenu du Site et des vidéos sur tout autre support, par tout moyen et sous toute forme que ce soit ;</li>
										<li>La réutilisation, par la mise à la disposition du public de la totalité ou d'une partie qualitativement ou quantitativement substantielle du contenu du Site Internet et des vidéos, quelle qu'en soit la forme.</li>
									</ul>
								</li>
								<li>La création d'un lien hypertexte, même simple ne peut se faire qu’avec l’autorisation du Site, et sous réserve qu'aucune confusion ne puisse exister dans l'esprit des internautes sur l'identité du site ou la provenance des informations.</li>
							</ol>
						</li>

						<li class="list-child">
							<p>DONNEES PERSONNELLES</p>
							<ol class="objet-child données">
								<li>Les données personnelles concernant l’Utilisateur sont nécessaires à la gestion du Compte Abonné, des abonnements et aux relations commerciales du Site.</li>
								<li>Elles peuvent être transmises aux sociétés (France) qui contribuent à ces relations telles que celles chargées de l’exécution des Services, de la gestion du Site, l’exécution, le traitement et le paiement. Ces informations et données sont également conservées à des fins de sécurité, afin de respecter les obligations légales et réglementaires et ainsi que pour permettre au Site d’améliorer et personnaliser les Services proposés.</li>
								<li>Conformément à la Loi Informatique et Libertés du 6 janvier 1978 dument modifiée et le RGPD, l’Utilisateur, justifiant de son identité, peut exiger du responsable d'un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données à caractère personnel le concernant, qui sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite.</li>
								<li>Dans ce cas, il suffit de faire la demande en ligne à l’adresse 455, promenade des Anglais  06000 Nice ou par courrier en indiquant nom, prénom, e-mail et adresse postale. Le responsable de traitement du Site est la Société TISSY.</li>
								<li><strong>Conformément à la réglementation en vigueur, la demande doit être signée et accompagnée de la photocopie d’un titre d’identité valide portant la signature de l’Utilisateur et préciser l’adresse à laquelle doit parvenir la réponse. Une réponse vous sera alors adressée dans un délai de 2 mois suivant la réception de la demande.</strong></li>
								<li>De la même manière, les héritiers d'une personne décédée justifiant de leur identité peuvent, si des éléments portés à leur connaissance leur laissent présumer que les données à caractère personnel la concernant faisant l'objet d'un traitement n'ont pas été actualisées, exiger du responsable de ce traitement qu'il prenne en considération le décès et procède aux mises à jour qui doivent en être la conséquence. Lorsque les héritiers en font la demande, le responsable du traitement doit justifier, sans frais pour le demandeur, qu'il a procédé aux opérations exigées en vertu de l'alinéa précédent.</li>
								<li> A des fins de sécurité et de fiabilité de l’utilisation du Site, les logs de connexion liés aux Comptes Abonnés seront conservés de manière anonyme durant la durée de l’abonnement 30 jours.</li>
							</ol>
						</li>
						<li class="list-child">
							<p>CONVENTION DE PREUVE</p>
							<ol>
								<li> Conformément à l’article 1316-2 du Code Civil, les parties entendent fixer, dans le cadre des prestations, les règles relatives aux preuves recevables entre eux en cas de litige et à leur force probante. Les dispositions qui suivent constituent ainsi la convention de preuve passée entre les parties, lesquelles s’engagent à respecter le présent article.</li>
								<li>Le Site et l’Utilisateur s’engagent à accepter qu’en cas de litige, les données issues de tout enregistrement informatique, numérique, e-mail ainsi que tout élément transmis par le client constituent la preuve de l’acceptation des présentes Conditions Générales d’Utilisation et de Vente. Les Parties acceptent irrévocablement qu’en cas de litige, la portée de ces documents, informations et enregistrements est celle accordée à un original, au sens d’un document écrit papier, signé de manière manuscrite.</li>
							</ol>
						</li>

						<li class="list-child">
							<p>LOI APPLICABLE ET ATTRIBUTION DE COMPETENCE</p>
							<ol class="objet-child loi-application">
								<li>Les présentes CGUV sont soumises au droit français.</li>
								<li>En cas de litige entre le professionnel et le consommateur, ceux-ci s’efforceront de trouver une solution amiable.<br>
									A défaut d’accord amiable, le consommateur a la possibilité de saisir gratuitement le médiateur de la consommation dont relève le professionnel, à savoir l’Association des Médiateurs Européens (AME CONSO), dans un délai d’un an à compter de la réclamation écrite adressée au professionnel.<br>
									<br>
									La saisine du médiateur de la consommation devra s’effectuer :<br><br>
									- soit en complétant le formulaire prévu à cet effet sur le site internet de l’AME CONSO : www.mediationconso-ame.com<br><br>
									- soit par courrier adressé à l’AME CONSO, 11 Place Dauphine – 75001 PARIS.<br>
									<br>
									Plateforme de Règlement en Ligne des Litiges : Conformément à l’article 14 du Règlement (UE) n°524/2013, la Commission Européenne a mis en place une plateforme de Règlement en Ligne des Litiges, facilitant le règlement indépendant par voie extrajudiciaire des litiges en ligne entre consommateurs et professionnels de l’Union européenne. Cette plateforme est accessible au lien suivant : <a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=FR" target="_blank">https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=FR</a>
								</li>

								<li> Tout différend relatif à la validité, l’interprétation, l’exécution, la non-exécution des présentes Conditions Générales d’Utilisation et de Vente régissant les rapports entre le Site et l’Utilisateur sera soumis aux Tribunaux du ressort de la Cour d’Appel d’Aix en Provence.</li>
							</ol>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</section>

	


	
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection