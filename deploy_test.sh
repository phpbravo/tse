#!/bin/bash

CONFIG_FILE=./.env
if test -f "$CONFIG_FILE"; then

    # config file exists, do nothing
    :

else

    # no config file, make one and generate a key
    cp .env.example .env
    php artisan key:generate

    # set environment to production
    sed -i 's/APP_ENV=local/APP_ENV=production/g' .env

fi

    # install production dependencies
    composer install --no-ansi --no-dev --no-interaction --no-plugins --no-progress --no-scripts --no-suggest --optimize-autoloader

    # clean up cache
    rm -rf bootstrap/cache/*.php

    # run migrations
    php artisan migrate --force