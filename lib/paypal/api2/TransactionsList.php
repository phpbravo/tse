<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

/**
 * Class TransactionsList
 *
 * Billing frequency details for a billing cycle
 *
 * @package PayPal\Api2
	* 
 *	@property \PayPal\Api2\Transaction[] transactions
 *	@property int total_items
 *	@property int total_pages
	* 
 */
class TransactionsList extends PayPalResourceModel
{
	
	/**
		* An array of transactions.
		* 
		* @param \PayPal\Api2\Transaction[] $transactions
		*/
	public function setTransactions(array $transactions): self
	{
		$this->transactions = $transactions;
		return $this;
	}
	
	/**
		* An array of transactions.
		* 
		* @return \PayPal\Api2\Transaction[]
		*/
	public function getTransactions(): array
	{
		return $this->transactions;
	}
	
 /**
		* The total number of items.
	 */
	public function setTotalItems(int $total_items): self
	{
		$this->total_items = $total_items;
		return $this;
	}

 /**
		* The total number of items.
	 */
	public function getTotalItems(): int
	{
		return $this->total_items;
	}
	
 /**
		* The total number of pages.
	 */
	public function setTotalPages(int $total_pages): self
	{
		$this->total_pages = $total_pages;
		return $this;
	}

 /**
		* The total number of pages.
	 */
	public function getTotalPages(): int
	{
		return $this->total_pages;
	}
}
