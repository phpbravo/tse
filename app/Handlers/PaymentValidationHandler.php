<?php

namespace App\Handlers;

use App\{
	Transaction,
	Subscription,
	User,
	Offer
};

class PaymentValidationHandler
{
	/**
		* @var BailHandler
	 */
	private $bail_handler;
	
	public function __construct()
	{
		$this->bail_handler = new BailHandler;
	}

	public function validateGatewayAvailableForOffer(
		string $gateway,
		Offer $offer
	): void {
		switch($gateway)
		{
			case 'stripe':
				if (!$offer->is_stripe_available)
					$this->bail_handler->bailGatewayNotAvailableForOffer();
				break;
			
			case 'paypal':
				if (!$offer->is_paypal_available)
					$this->bail_handler->bailGatewayNotAvailableForOffer();
				break;

			default:
				$this->bail_handler->bailInvalidGateway();
				break;
		}

		return;
	}

	public function validateUserCanAvailOffer(
		User $user,
		Offer $offer
	): void {
		if ($offer->is_available)
			return;
		else
			$this->bail_handler->bailOfferNotAvailable();
	}

	public function validateUserDoesNotHaveOffer(
		User $user,
		Offer $offer
	): void {
		if ($user->hasOfferById($offer->id))
			$this->bail_handler->bailUserAlreadySubscribed();

		return;
	}

	public function validatePaymentRequest(
		User $user,
		Offer $offer,
		string $gateway
	): void {
		$this->validateGatewayAvailableForOffer($gateway, $offer);
		$this->validateUserCanAvailOffer($user, $offer);

		return;
	}

	public function validatePaymentConfirmationRequest(
		string $payment_type,
		string $token,
		string $gateway
	): void {
		switch($payment_type)
		{
			case 'subscription':
				$subscription = Subscription::getFromToken($token);
				if (!$subscription)
					$this->bail_handler->bailSubscriptionNotFound();
				break;
			case 'purchase':
				$transaction = Transaction::getFromToken($token);
				if (!$transaction && $gateway == 'stripe')
					$this->bail_handler->bailTransactionNotFound();
				break;
			default:
				$this->bail_handler->bailInvalidPaymentType();
				break;
		}

		switch($gateway)
		{
			case 'stripe':
			case 'paypal':
				break;
			default:
				$this->bail_handler->bailInvalidGateway();
				break;
		}

		return;
	}
}