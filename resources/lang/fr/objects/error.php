<?php

return [

    'general_error' => [
        'error'=>'Erreur inconnue',
        'message'=>'Il y a eu une erreur.',
    ],

    'unauthorized' => [
        'error'=>'Non autorisé',
        'message'=>'Vous n\'êtes pas autorisé à y accéder.',
    ],

    'invalid_object' => [
        'error'=>'Objet non valide',
        'message'=>'L\'objet fourni n\'était pas valide',
    ],

    'invalid_request' => [
        'error'=>'Objet non valide',
        'message'=>'L\'objet fourni n\'était pas valide',
    ],

    'not_found' => [
        'error'=>'Objet non valide',
        'message'=>'L\'objet fourni n\'était pas valide',
    ],

    'connection_error' => [
        'error'=>'Erreur de connexion',
        'message'=>'Le serveur n\'a pas été en mesure d\'établir une connexion avec le fournisseur distant.',
    ]

];