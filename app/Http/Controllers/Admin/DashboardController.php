<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Purchase;
use App\User;
use App\Subscribers;
use App\LegacyRequest;
use Carbon\Carbon;
class DashboardController extends Controller
{
    //get the data for users
    public function getUserStats()
    {
        $year = request()->get('year');
        $month = request()->get('month');
        if ($month != 'All') {
           return $this->getDataInMonth($month, $year, 'App\User');
        }else {
            return $this->getDataInYear($year, 'App\User');
        }
    }
    //get the data for purchases
    public function getPurchaseStats()
    {
        $year = request()->get('year');
        $month = request()->get('month');
        if ($month != 'All') {
            return $this->getDataInMonth($month, $year, 'App\Purchase');
        }else {
            return $this->getDataInYear($year, 'App\Purchase');
        }
    }
    //get the data for subscriptions
    public function getSubscriptionStats()
    {
        $year = request()->get('year');
        $month = request()->get('month');
        if ($month != 'All') {
            return $this->getDataInMonth($month, $year, 'App\Subscription');
        }else {
            return $this->getDataInYear($year, 'App\Subscription');
        }
    }
    //get the data for subscriptions
    public function getTransactionStats()
    {
        $year = request()->get('year');
        $month = request()->get('month');
        if ($month != 'All') {
            return $this->getDataInMonth($month, $year, 'App\Transaction');
        }else {
            return $this->getDataInYear($year, 'App\Transaction');
        }
    }
    //get the data for users in old api
    public function getOldApiUsageStats()
    {
        $year = request()->get('year');
        $month = request()->get('month');
        if ($month != 'All') {
            return $this->getDataInMonth($month, $year, 'App\LegacyRequest');
        }else {
            return $this->getDataInYear($year, 'App\LegacyRequest');
        }
    }

    private function getDataInYear(string $year, string $model)
    {
        $datas = $model::whereYear('created_at', $year)
                    ->pluck('created_at')
                    ->groupBy(function($query) {
                        return Carbon::parse($query)->month;
                    });
        $datas_year = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($datas as $data) {
            $datas_year[date('n', strtotime($data[0])) - 1] = count($data);
        }
        return $datas_year;
    }

    private function getDataInMonth(string $month,string $year, string $model)
    {
        $datas = $model::whereYear('created_at', $year)
                    ->whereMonth('created_at', $month)
                    ->pluck('created_at')
                    ->groupBy(function($query) {
                        return Carbon::parse($query)->day;
                    });
        $days_count =  Carbon::parse((string)$year.'-'.(string)$month)->daysInMonth;

        $datas_month = [];
        for ($i=1; $i <= $days_count ; $i++) { 
            array_push($datas_month, 0);
        }
       
        foreach ($datas as $data) {
            $datas_month[date('d', strtotime($data[0])) - 1] = count($data);
        }
        return $datas_month;
    }

}
