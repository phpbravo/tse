<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

/**
 * Class Frequency
 *
 * Billing frequency details for a billing cycle
 *
 * @package PayPal\Api2
	* 
 *	@property string interval_unit
 *	@property string interval_count
	* 
 */
class Frequency extends PayPalModel
{
	/**
		* @var string $DAY A daily billing cycle.
	 */
	const DAY = 'DAY';

	/**
		* @var string $WEEK A weekly billing cycle.
	 */
	const WEEK = 'WEEK';

	/**
		* @var string $MONTH A monthly billing cycle.
	 */
	const MONTH = 'MONTH';
	
	/**
		* @var string $YEAR A yearly billing cycle.
	 */
	const YEAR = 'YEAR';
	
	/**
		* The interval at which the subscription is charged or billed.
		* 
		* Possible values are:
		* Frequency::DAY
		* Frequency::WEEK
		* Frequency::MONTH
		* Frequency::YEAR
		*/
	public function setIntervalUnit(string $interval_unit): self
	{
		$this->interval_unit = $interval_unit;
		return $this;
	}
	
	/**
		* The interval at which the subscription is charged or billed.
		* setIntervalUnit
		* Possible values are:
		* Frequency::DAY
		* Frequency::WEEK
		* Frequency::MONTH
		* Frequency::YEAR
		*/
	public function getIntervalUnit(): string
	{
		return $this->interval_unit;
	}
	
 /**
		* The number of intervals after which a subscriber is billed
	 */
	public function setIntervalCount(int $interval_count): self
	{
		$this->interval_count = $interval_count;
		return $this;
	}

 /**
		* The number of intervals after which a subscriber is billed
	 */
	public function getIntervalCount(): int
	{
		return $this->interval_count;
	}
}
