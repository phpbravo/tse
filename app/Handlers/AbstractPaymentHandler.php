<?php

namespace App\Handlers;

use App\{
    Offer,
    Subscription,
    User,
};

abstract class AbstractPaymentHandler
{

    abstract public function verifySubscription(string $subscription_token);
    abstract public function createLocalSubscription(
        User $user,
        Offer $offer,
        Object $request);
    abstract public function createLocalTransactions(Subscription $new_subscription);
    abstract public function getTransaction(string $transaction_id);


    public static function get_handler(int $gateway_id): self
    {
        /** @var App\Handlers\AbstractPaymentHandler */
        $handler = null;

        switch($gateway_id)
        {
            case 1:
                $handler = new StripeHandler;
                break;
            case 2:
                $handler = new PaypalHandler;
                break;
            case 3:
                // TODO create AppstoreHandler
            case 4:
                // TODO create PlaystoreHandler
            default:
                throw new \Exception('Handler not found');
                break;
        }
        return $handler;
    }
}
