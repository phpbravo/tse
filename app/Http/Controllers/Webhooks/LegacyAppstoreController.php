<?php

namespace App\Http\Controllers\Webhooks;

use App\AppstoreReceipt;
use App\Gateway;
use App\WebhookEvent;

class LegacyAppstoreController
{
    public function handle()
    {
        $raw_payload =
            @file_get_contents('php://input') ?:
            request()->getContent();

        $payload =
            json_decode($raw_payload);

        $compressed_payload =
            gzdeflate($raw_payload);

        $vendor_token =
            $payload->latest_receipt_info->unique_vendor_identifier ?? null;

        $original_transaction_token = 
            $payload->latest_receipt_info->original_transaction_id ?? null;

        $transaction_token = 
            $payload->latest_receipt_info->transaction_id ?? null;

        $payload->event_type = 'legacy_'.$this->getEventType($payload);

        $url=request()->url();

        $receipt = AppstoreReceipt::updateOrCreate(
            [
                'original_transaction_token' => $original_transaction_token,
                'transaction_token' => $transaction_token
            ],
            [
                'content' => $compressed_payload,
                'vendor_token' => $vendor_token,
                'original_transaction_token' => $original_transaction_token, 
                'transaction_token' => $transaction_token, 
            ]
        );

        $payload->receipt_id = $receipt->id;

        WebhookEvent::create([
            'gateway_id' => Gateway::APPSTORE,
            'url' => $url,
            'data' => $payload
        ]);

        return response()->json([
            'message' => 'success',
            'event' => $payload->event_type
        ]);
    }

    public function getEventType(object $payload): string
    {
        return $payload->notification_type;
    }
}