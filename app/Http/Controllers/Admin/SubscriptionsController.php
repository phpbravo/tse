<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{
     ObjectHandler,
     FilterHandler, 
     PageHandler, 
     PaypalHandler, 
     StripeHandler,
     AbstractPaymentHandler
};
use App\{
    Gateway,
    Offer,
    Purchase,
    Subscription,
    Transaction,
    User
};

class SubscriptionsController extends Controller
{
    public function index(){
        $filterHandler=new FilterHandler(Subscription::class);
        $pageHandler=new PageHandler(Subscription::class);
        $objectHandler=new ObjectHandler(Subscription::class);

        $filters=request()->filters;
        $search=request()->search;
        $perPage=request()->perPage;
        $page=request()->page;
        $orderBy=request()->orderBy;
        $ascending=request()->ascending;

        $filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

        $filtered_query->with('user','offer','gateway');

        $page=$pageHandler->getPage($filtered_query,$perPage,$page);

        $page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

        return $page_object;
    }

	public function show($id){

        $pageHandler=new PageHandler(Subscription::class);

        $subscription=Subscription::with(['user','offer.plan','gateway'])->find($id);

        $transactions=$pageHandler
            ->getPage($subscription->transactions(),10,request()->get('page_transactions'));
        
        $subscription->transactions=$transactions;

		if(!$subscription){
			return (new ObjectHandler(Subscription::class))->newError('not_found');
		}

		return $subscription;
    }
    
    public function store(Request $request)
    {
        $handler = AbstractPaymentHandler::get_handler($request->gateway_id);
        $existing_subcription = Subscription::getFromToken($request->token);
        $offer = Offer::find($request->offer_id);
        $user = User::find($request->user_id);

        $verified_subscription = $handler->verifySubscription($request->token);
        $id = null;
        if ($verified_subscription && !$request->is_legacy) {
            if (!$existing_subcription) {
                $new_subscription = $handler->createLocalSubscription(
                    $user,
                    $offer,
                    $request
                );
                $handler->createLocalTransactions($new_subscription);
                $id = $new_subscription->id;
            }
        }elseif($request->gateway_id == Gateway::PAYPAL) {
            $new_subscription = $handler->createLocalSubscription(
                $user,
                $offer,
                $request->token,
                $request->gateway_id
            );
            $handler->createLocalTransactions($new_subscription);
            $id = $new_subscription->id;
        }
        return $id;
    }

    public function edit($subscription_id): Subscription
    {
        return Subscription::with('user', 'gateway', 'offer', 'transactions.purchase')
                            ->find($subscription_id);
    }

    public function update(Request $request, $id)
    {
        $subscription = Subscription::findOrFail($id);
        $subscription->update([
            'user_id' => $request->user_id,
            'offer_id' => $request->offer_id,
            'gateway_id' => $request->gateway_id,
            'gateway_token' => $request->token,
        ]);
        //adding purchases
        foreach ($subscription->transactions as $key => $transaction) {
            if ($transaction->purchase) {
                $transaction->purchase()->delete();
            }
        }
        foreach ($request->purchases as $key => $purchase) {
            $transaction = Transaction::where('gateway_token', $purchase['transaction_number'])->first();
            if ($transaction) {
                $new_purchase = Purchase::create([
                    'user_id'=>$subscription->user_id,
                    'offer_id'=>$subscription->offer_id,
                    'plan_id'=>$subscription->offer->plan_id,
                    'transaction_id'=>$transaction->id,
                    'is_active'=>true,
                    'is_paused'=>false,
                    'is_lifetime'=>$subscription->offer->is_lifetime,
                    'paused_total'=>'00000000',
                    'duration'=>$subscription->offer->duration,
                    'effective_on'=>$purchase['start_date'],
                    'expires_on'=>$purchase['end_date'],
                ]);
            }
        }
        return $subscription;
    }
}
