<?php

use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Seed Subcategories
        $arms = \App\Subcategory::create([
			'category_id'=>1,
			'name'=>'Bras',
			'title'=>'Bras',
        ]);
        
        $back = \App\Subcategory::create([
			'category_id'=>1,
			'name'=>'Dos',
			'title'=>'Dos',
        ]);

        $legs = \App\Subcategory::create([
			'category_id'=>1,
			'name'=>'Jambes',
			'title'=>'Jambes',
        ]);

        $pecs = \App\Subcategory::create([
			'category_id'=>1,
			'name'=>'Pecs',
			'title'=>'Pecs',
        ]);

        $shoulders = \App\Subcategory::create([
			'category_id'=>1,
			'name'=>'Épaules',
			'title'=>'Épaules',
        ]);


        $abs = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Abdos',
			'title'=>'Abdos',
        ]);

        $lower_body = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Bas du corps',
			'title'=>'Bas du corps',
        ]);

        $cardio = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Cardio',
			'title'=>'Cardio',
        ]);

        $fitboxing = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Fitboxing',
			'title'=>'Fitboxing',
        ]);

        $full_body = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Full body',
			'title'=>'Full body',
        ]);

        $upper_body = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Haut du corps',
			'title'=>'Haut du corps',
        ]);

        $hot_pilates = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Hot pilates',
			'title'=>'Hot pilates',
        ]);

        $gym = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Salle',
			'title'=>'Salle',
        ]);

        $yoga = \App\Subcategory::create([
			'category_id'=>2,
			'name'=>'Yoga Energy',
			'title'=>'Yoga Energy',
        ]);
        
    }
}
