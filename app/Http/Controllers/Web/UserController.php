<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

class UserController extends Controller
{

	public function getSelf(){
		$user=auth()->user()->load([
			'active_subscriptions.offer',
			'subscriptions.offer',
			'purchases.offer',
			'purchases.transaction.subscription',
			'active_purchases.offer',
			'active_purchases.transaction'
			]);

		$user->purchases=$user->purchases();
		$user->active_purchases=$user->active_purchases();

		return $user;
	}

	public function updateProfile(){
		$user = auth()->user();

		if (request()->get('email')==$user->email)
			request()->offsetUnset('email');
		
		if (request()->get('username')==$user->email){

			if (request()->get('email'))
				request()->offsetSet('username',request()->get('email'));
			else
				request()->offsetUnset('username');

		}

		if (!request()->get('password'))
			request()->offsetUnset('password');

		$validated=request()->validate([
			'email'=>'sometimes|email|unique:users',
			'username'=>'sometimes|same:email',
			'password'=>'sometimes|confirmed',
			'name'=>'string|nullable',
			'surname'=>'string|nullable',
			'organization'=>'string|nullable',
			'address'=>'string|nullable',
			'address2'=>'string|nullable',
			'city'=>'string|nullable',
			'postal_code'=>'string|nullable',
			'country'=>'string|nullable',
			'phone'=>'string|nullable',
			'homepage_url'=>'string|nullable',
			'agreed_terms'=>'required|boolean|accepted'
		]);

		if (isset($validated['password']))
			$validated['password']=bcrypt($validated['password']);

		$user->update($validated);

		return $user;
		
	}

	public function getSubscriptions(){

		$subscriptions=auth()->user()->subscriptions();

		$with=request('with',[]);
		$where=request('where',[]);

		if ($with){
			foreach ($with as $arg) {
				$subscriptions=$subscriptions->with($arg);
			}
		}


		if ($where){
			foreach ($where as $arg) {

				$arg=json_decode($arg);

				$subscriptions=$subscriptions->where($arg->field,$arg->value);

			}
		}

		return $subscriptions->get();
	}

	public function getPurchases(){

		$purchases=auth()->user()->purchases();

		$with=request('with',[]);
		$where=request('where',[]);

		if ($with){
			foreach ($with as $arg) {
				$purchases=$purchases->with($arg);
			}
		}


		if ($where){
			foreach ($where as $arg) {

				$arg=json_decode($arg);

				$purchases=$purchases->where($arg->field,$arg->value);

			}
		}

		return $purchases->get();
	}

	public function getTransactions(){

		$purchases=auth()->user()->transactions();

		$with=request('with',[]);
		$where=request('where',[]);

		if ($with){
			foreach ($with as $arg) {
				$purchases=$purchases->with($arg);
			}
		}


		if ($where){
			foreach ($where as $arg) {

				$arg=json_decode($arg);

				$purchases=$purchases->where($arg->field,$arg->value);

			}
		}

		return $purchases->get();
	}

}
