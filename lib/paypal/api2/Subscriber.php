<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;


/**
 * Class Subscriber
 *
 * A resource representing a subscriber.
 *
 * @package PayPal\Api2
 *
 * @property \PayPal\Api2\Name name
 * @property string email_address
 */
class Subscriber extends PayPalModel
{
    /**
     * Name of the subscriber.
     */
    public function setName(Name $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Name of the subscriber.
     * 
     * @return \PayPal\Api2\Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * Email address of the subscriber.
     */
    public function setEmailAddress(string $email_address): self
    {
        $this->email_address = $email_address;
        return $this;
    }

    /**
     * Email address of the subscriber.
     */
    public function getEmailAddress(): string
    {
        return $this->email_address;
    }

}
