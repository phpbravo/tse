<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Program, ProgramSchedule, Offer, Session};

class ProgramsController extends Controller
{

	public function getCurrentPublishedProgram(){

		$active_schedule=ProgramSchedule::getActiveSchedule();

		if (!$active_schedule)
			return response()->json(['message'=>['Not found']],404);

		$program=$active_schedule->program;

		$program->load('sessions.exercise_sets.exercise_assignments.exercise');

		unset($active_schedule->program);

		$program->active_schedule=$active_schedule;

		return $program;

	}

	public function getBonusSessions(){
		$filterHandler=new FilterHandler(Session::class);
		$pageHandler=new PageHandler(Session::class);
		$objectHandler=new ObjectHandler(Session::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=99;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('exercise_sets.exercise_assignments.exercise');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	}

}
