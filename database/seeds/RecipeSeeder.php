<?php

use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		// Seed Exercises
		factory(App\Recipe::class,1)->create()->each(function($recipe){
			$recipe->sections()->attach([
				1=>[
					'content'=>'<h3><i class="fa fa-shopping-cart"></i> Ingrédients</h3> <p><strong>Ingrédients pour 2 gros pots de confiture</strong></p> <ul class="ingredients-list"> <li> <p>400g de compote de fraise (ou pomme fraise)</p> </li> <li> <p>1 barquette de fraises ou autres fruits rouges (env. 300g)</p> </li> <li> <p>4 cas de graines de chia</p> </li> </ul> <style> '
				],
				2=>[
					'content'=>'<h3><i class="fa fa-spoon"></i> Préparation</h3> <!-- Item fulltext --> <div class="btitemFullText"> <p>Dans un bol, mélangez les graines de chia avec la compote de fraise.<br>En parallèle, coupez grossièrement les fraises et les mettre dans une casserole. <br>Ajouter le mélange compote graines de chia et faire cuire quelques minutes en remuant régulièrement jusqu’à obtenir la consistance d’une confiture. <br>Laisser refroidir et déguster.</p> </div>'
				],
				3=>[
					'content'=>'<h3><i class="fa fa-heart"></i> Astuce de Sissy</h3> <p>Absolument divine avec les pancakes gourmands ou express, cette confiture sans sucre relèvera tous vos desserts, cakes, biscuits, sans aucun impact sur votre ligne&nbsp;!</p>'
				],
				4=>[
					'content'=>'<h3><i class="fa fa-table"></i> Informations nutritionnelles</h3> <table> <tbody> <tr> <td> <p>Calories pour 100g&nbsp; &nbsp;</p> </td> <td> <p>&nbsp;72,7 Kcal</p> </td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td> <p>Macro nutriments</p> </td> <td> <p>Pour 100g</p> </td> </tr> <tr> <td> <p>Protéines</p> </td> <td> <p>&nbsp;3,2 g</p> </td> </tr> <tr> <td> <p>Glucides</p> </td> <td> <p>&nbsp;6 g</p> </td> </tr> <tr> <td> <p>Lipides</p> </td> <td> <p>3,3 g</p> </td> </tr> </tbody> </table>'
				],
			]);
		});
			
	}
}
