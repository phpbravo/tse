<?php

namespace Tests\API\v0;

use App\FailedMobileSubscription;
use App\Gateway;
use App\Offer;
use App\Plan;
use App\Purchase;
use App\Subscription;
use App\Transaction;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LegacyPlaystoreNotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Plan */
    private $plan;

    /** @var \App\Offer */
    private $offer;
    
    /** @var \App\User */
    private $user;

    /** @var \App\Transaction */
    private $previous_transaction;

    /** @var \App\Purchase */
    private $previous_purchase;

    /** @var \App\Subscription */
    private $subscription;

    private
        $user_id = '13371706',
        $user_name = 'john doe',
        $user_email = 'john.doe.unique.123@example.org',
        $offer_duration = 1,
        $subscription_gateway_token = 'GPA.abcdef',
        $previous_transaction_gateway_token = '123',
        $new_transaction_gateway_token = '456';


    private function createTestModels()
    {
        Model::withoutEvents(function()
        {
            $this->user = User::forceCreate([
                'id' => $this->user_id,
                'surname' =>  $this->user_name,
                'email' => $this->user_email,
                'username' => $this->user_email,
                'agreed_terms' => true,
                'password' => bcrypt('password')
            ]);

            $this->plan = Plan::create([
                'name'=>'TEST PLAN'
            ]);
    
            $this->offer = Offer::create([
                'plan_id'=>$this->plan->id,
                'name'=>'TEST OFFER',
                'title'=>'TEST OFFER',
                'description'=>'This is a test offer. You should not see this.',
                'price'=>'14.99',
                'tax_percent'=>'20',
                'duration'=>$this->offer_duration,
                'is_recurring'=>true,
                'appstore_id'=>'tse1month',
                'playstore_id'=>'tse1month'
            ]);

            $this->subscription = Subscription::createActive(
                $this->user,
                $this->offer,
                $this->subscription_gateway_token,
                Gateway::PLAYSTORE
            );

            $this->previous_transaction =
                Transaction::createSucceededOfferSubscription(
                    $this->subscription,
                    $this->previous_transaction_gateway_token,
                    $this->offer->price,
                    0,
                    'EUR',
                    now()->timestamp
                );
            
            $this->previous_purchase = Purchase::createFromTransaction(
                $this->previous_transaction
            );
        });
    }

    /**
     * Test the playstore subscription renewal server notification
     *
     * @return void
     */
    public function testPlaystoreRenewSubscription()
    { 
        $this->createTestModels();

        $new_purchase_effective_on = $this->previous_purchase->expires_on;
        $new_purchase_expires_on = 
            $new_purchase_effective_on
            ->copy()
            ->addMonthsNoOverflow($this->offer_duration);

        $notification = base64_encode(json_encode((object)
            [
                'version' => '0.0.1',
                'notificationType' => 2,
                'purchaseToken' => $this->subscription_gateway_token,
                'subscriptionId' => 'tse1month'
            ]
        ));

        $payload = json_decode('{
            "message": {
              "attributes": {
                "key": "value"
              },
              "data": "'.$notification.'",
              "messageId": "136969346945"
            },
            "subscription": "projects/myproject/subscriptions/mysubscription"
         }');

        $endpoint = '/'.config('webhook.playstore_legacy_endpoint');

        $response = $this->call('POST',$endpoint,[],[],[],[],json_encode($payload));

        $response->assertOk();

        $response->assertJson([
            'message' => 'success'
        ]);

        $this->assertDatabaseHas(
            'transactions',
            [
                'id' => 2
            ]
        );

        $this->assertDatabaseHas(
            'purchases',
            [
                'id' => 2,
                'effective_on' => $new_purchase_effective_on,
                'expires_on' => $new_purchase_expires_on,
                'is_active' => true
            ]
        );

        $this->assertDatabaseHas(
            'subscriptions',
            [
                'id' => 1,
                'gateway_id' => Gateway::PLAYSTORE,
                'next_payment_date' =>$new_purchase_expires_on
            ]
        );

        $response->json();
    }

    /**
     * Test the playstore subscription cancel server notification
     *
     * @return void
     */
    public function testPlaystoreCancelSubscription()
    { 
        $this->createTestModels();

        $new_purchase_effective_on = $this->previous_purchase->expires_on;
        $new_purchase_expires_on = 
            $new_purchase_effective_on
            ->copy()
            ->addMonthsNoOverflow($this->offer_duration);

        $notification = base64_encode(json_encode((object)
            [
                'version' => '0.0.1',
                'notificationType' => 3,
                'purchaseToken' => $this->subscription_gateway_token,
                'subscriptionId' => 'tse1month'
            ]
        ));

        $payload = json_decode('{
            "message": {
              "attributes": {
                "key": "value"
              },
              "data": "'.$notification.'",
              "messageId": "136969346945"
            },
            "subscription": "projects/myproject/subscriptions/mysubscription"
         }');

        $endpoint = '/'.config('webhook.playstore_legacy_endpoint');

        $response = $this->call('POST',$endpoint,[],[],[],[],json_encode($payload));

        $response->assertOk();

        $response->assertJson([
            'message' => 'success'
        ]);

        $this->assertDatabaseHas(
            'subscriptions',
            [
                'id' => 1,
                'gateway_id' => Gateway::PLAYSTORE,
                'next_payment_date' => null,
                'is_active' => false,
            ]
        );

        $response->json();
    }

    /**
     * Test the playstore subscription revoke server notification
     *
     * @return void
     */
    public function testPlaystoreRevokeSubscription()
    { 
        $this->createTestModels();

        $new_purchase_effective_on = $this->previous_purchase->expires_on;
        $new_purchase_expires_on = 
            $new_purchase_effective_on
            ->copy()
            ->addMonthsNoOverflow($this->offer_duration);

        $notification = base64_encode(json_encode((object)
            [
                'version' => '0.0.1',
                'notificationType' => 12,
                'purchaseToken' => $this->subscription_gateway_token,
                'subscriptionId' => 'tse1month'
            ]
        ));

        $payload = json_decode('{
            "message": {
              "attributes": {
                "key": "value"
              },
              "data": "'.$notification.'",
              "messageId": "136969346945"
            },
            "subscription": "projects/myproject/subscriptions/mysubscription"
         }');

        $endpoint = '/'.config('webhook.playstore_legacy_endpoint');

        $response = $this->call('POST',$endpoint,[],[],[],[],json_encode($payload));

        $response->assertOk();

        $response->assertJson([
            'message' => 'success'
        ]);

        $this->assertDatabaseHas(
            'subscriptions',
            [
                'id' => 1,
                'gateway_id' => Gateway::PLAYSTORE,
                'next_payment_date' => null,
                'is_active' => false,
            ]
        );

        $response->json();
    }
}