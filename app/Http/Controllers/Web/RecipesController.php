<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Recipe};

class RecipesController extends Controller
{

	public function getRecipes(){
		$filterHandler=new FilterHandler(Recipe::class);
		$pageHandler=new PageHandler(Recipe::class);
		$objectHandler=new ObjectHandler(Recipe::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=999;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('sections');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	}

	public function getImage(){
		return null;
	}

}
