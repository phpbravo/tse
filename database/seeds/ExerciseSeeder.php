<?php

use Illuminate\Database\Seeder;

class ExerciseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Seed Exercises
		$wetrain_exercises=factory(App\Exercise::class,250)->states('wetrain')->create();
		
        $wesweat_exercises=factory(App\Exercise::class,250)->states('wesweat')->create();
        
    }
}
