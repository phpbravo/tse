<?php 
return [ 
    'client_id' => env('PAYPAL_CLIENT_ID',''),
    'secret' => env('PAYPAL_SECRET',''),
    'settings' => array(
        'mode' => env('PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ),

    'nvp' => [
        'mode' => env('NVP_MODE','sandbox'),
        'acct1.UserName' => env('NVP_USERNAME'),
        'acct1.Password' => env('NVP_PASSWORD'),
        'acct1.Signature' => env('NVP_SIGNATURE'),
        'log.LogEnabled' => true,
        'log.FileName' => 'storage/logs/paypal_nvp.log',
        'log.LogLevel' => 'INFO',
    ],
];