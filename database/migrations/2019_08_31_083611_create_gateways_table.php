<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateways', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });

        //  Seed Gateways
        \App\Gateway::create([
            'name'=>'stripe'
        ]);

        \App\Gateway::create([
            'name'=>'paypal'
        ]);

        \App\Gateway::create([
            'name'=>'appstore'
        ]);

        \App\Gateway::create([
            'name'=>'playstore'
        ]);

        \App\Gateway::create([
            'name'=>'free'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateways');
    }
}
