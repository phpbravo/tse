<?php

use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		\App\Offer::create([
			'plan_id'=>1,
			'name'=>'forumle mensuelle',
			'title'=>'FORMULE MENSUELLE',
			'description'=>'FORMULE MENSUELLE',
			'price'=>'14.90',
			'tax_percent'=>'20',
			'duration'=>'1',
			'subscriber_limit'=>'0',
			'is_limited'=>false,
			'is_recurring'=>true,
			'is_lifetime'=>false,
			'is_public'=>true,
			'is_for_upgrade'=>false,
			'is_available'=>true,
			'is_stripe_available'=>true,
			'is_paypal_available'=>true,
		]);

		\App\Offer::create([
			'plan_id'=>1,
			'name'=>'forumle 3 mois',
			'title'=>'FORMULE 3 MOIS',
			'description'=>'FORMULE 3 MOIS',
			'price'=>'38.70',
			'tax_percent'=>'20',
			'duration'=>'3',
			'subscriber_limit'=>'0',
			'is_limited'=>false,
			'is_recurring'=>true,
			'is_lifetime'=>false,
			'is_public'=>true,
			'is_for_upgrade'=>true,
			'is_available'=>true,
			'is_stripe_available'=>true,
			'is_paypal_available'=>true,
		]);

		\App\Offer::create([
			'plan_id'=>1,
			'name'=>'forumle 6 mois',
			'title'=>'FORMULE 6 MOIS',
			'description'=>'FORMULE 6 MOIS',
			'price'=>'59.40',
			'tax_percent'=>'20',
			'duration'=>'6',
			'subscriber_limit'=>'0',
			'is_limited'=>false,
			'is_recurring'=>true,
			'is_lifetime'=>false,
			'is_public'=>true,
			'is_for_upgrade'=>true,
			'is_available'=>true,
			'is_stripe_available'=>true,
			'is_paypal_available'=>true,
		]);

		\App\Offer::create([
			'plan_id'=>1,
			'name'=>'forumle 1 an',
			'title'=>'FORMULE 1 AN',
			'description'=>'FORMULE 1 AN',
			'price'=>'96.00',
			'tax_percent'=>'20',
			'duration'=>'12',
			'subscriber_limit'=>'0',
			'is_limited'=>false,
			'is_recurring'=>true,
			'is_lifetime'=>false,
			'is_public'=>true,
			'is_for_upgrade'=>true,
			'is_available'=>true,
			'is_stripe_available'=>true,
			'is_paypal_available'=>true,
		]);

		\App\Offer::create([
			'plan_id'=>2,
			'name'=>'ebook',
			'title'=>'Nos Véritables Recettes Fitness (EBook)',
			'description'=>'Nos Véritables Recettes Fitness (EBook)',
			'price'=>'9.95',
			'tax_percent'=>'20',
			'duration'=>'0',
			'subscriber_limit'=>'0',
			'is_limited'=>false,
			'is_recurring'=>false,
			'is_lifetime'=>true,
			'is_public'=>false,
			'is_for_upgrade'=>false,
			'is_available'=>true,
			'is_stripe_available'=>true,
			'is_paypal_available'=>true,
		]);
			
	}
}
