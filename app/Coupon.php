<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable=[
        'name',
        'is_percent',
        'amount',
        'is_minimum',
        'minimum',
        'description',
        'is_public',
        'is_general',
        'is_available',
        'is_ending',
        'is_refund',
        'refund_token',
        'ends_on',
        'is_limited',
        'limit',
        'uses_per_user',
    ];
}
