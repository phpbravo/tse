<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeSectionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipe_section', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('recipe_id');
				$table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
			$table->unsignedBigInteger('section_id');
				$table->foreign('section_id')->references('id')->on('sections');
			$table->text('content');
			$table->integer('order_index')->default(0);
			$table->timestamps();
			$table->index('created_at');
			$table->index('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recipe_section');
	}
}
