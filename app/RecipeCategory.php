<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeCategory extends Model
{
    protected $dispatchesEvents=[
        'created' => Events\ResourceCreated::class,
    ];
}
