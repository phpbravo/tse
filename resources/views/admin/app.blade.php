<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@lang('admin.dashboard')</title>

        <link href="{{ mix('administrator/app.css', config('app.manifest_path')) }}" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    </head>
    <body>

    <div id="app">
        <app/>
    </div>

    <script src="/js/showdown.min.js"></script>
    <script src="{{ mix('administrator/app.js', config('app.manifest_path')) }}"></script>
    </body>
</html>
