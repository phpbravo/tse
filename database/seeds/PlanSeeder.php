<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		$main_plan = \App\Plan::create([
			'name'=>'accès au site'
		]);

		$main_plan->products()->sync([1,2,3,4]);

		$bikini_plan = \App\Plan::create([
			'name'=>'accès bikini'
		]);

		$ebook_plan = \App\Plan::create([
			'name'=>'accès ebook des recettes'
		]);

		$ebook_plan->products()->sync([5]);
			
	}
}
