<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
		'deleted' => Events\ResourceDeleted::class,
	];

	// attributes that are mass-assignable
	public $fillable = [
		'category_id',
		'subcategory_id',
		'name',
		'description',
		'content',
		'difficulty',
		'duration',
		'video_id'
	];

	// attributes that can be searched and filtered
	public $searchable_columns = [
		'name',
		'description',
	];

	public $filterable_columns = [
		'category_id',
		'subcategory_id',
		'difficulty',
		'duration',
	];

	public $permission_parents=[
		'category',
		'subcategory',
		'sessions',
	];

	public function getVideoNameAttribute(){
		return $this->video->vimeo_name??null;
	}

	public function category(){
		return $this->belongsTo('App\Category');
	}
	
	public function subcategory(){
		return $this->belongsTo('App\Subcategory');
	}

	public function sessions(){
		return $this->belongsToMany('App\Session');
	}

	public function video(){
		return $this->belongsTo('App\Video');
	}

	public function tags(){
		return $this->belongsToMany('App\Tag');
	}

	public function tag($tag_name){
		
		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first()?:Tag::create(['name'=>$tag_name]);

		$this->tags()->syncWithoutDetaching($tag);

		return $this;
	}

	public function untag($tag_name){

		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first();
		if(!$tag) return $this;

		$this->tags()->detach($tag);

		return $this;
	}

}
