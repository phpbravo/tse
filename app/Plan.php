<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
	protected $dispatchesEvents = [
		'created' => Events\PlanCreated::class,
	];

	protected $fillable = [
		'name'
	];

	protected $appends = [

	];

	protected $hidden = [

	];

	protected $casts = [

	];
	
	// Simple relations

	public function offer(){
		return $this->belongsTo('App\Offer');
	}

	public function plan_products(){
		return $this->hasMany('App\PlanProduct');
	}

	public function products(){
		return $this->belongsToMany('App\Product');
	}

	// Scoped relations

	public function resources(){
		return Resource::whereIn($this->products()->pluck('resource_id'))->get();
	}

	// Attributes

	public function getStripeTokenAttribute(){
		$gateway_plan=GatewayPlan::where('plan_id',$this->id)
		->where('gateway_id',1)
		->first();
		if (!$gateway_plan)
			return null;
		return $gateway_plan->gateway_token;
	}

	public function getPaypalTokenAttribute(){
		$gateway_plan=GatewayPlan::where('plan_id',$this->id)
		->where('gateway_id',2)
		->first();
		if (!$gateway_plan)
			return null;
		return $gateway_plan->gateway_token;
	}

	// Scopes

		// none

	// Instance methods

	public function createGateway(
		int $gateway_id,
		string $gateway_token
	): GatewayPlan {
		return GatewayPlan::create([
			'plan_id'=>$this->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
		]);
	}

}
