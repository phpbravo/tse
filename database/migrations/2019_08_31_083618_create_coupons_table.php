<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->boolean('is_percent')->default(false)->index();
            $table->integer('amount')->index();
            $table->boolean('is_minimum')->default(false)->index();
            $table->integer('minimum')->index();
            $table->string('description')->nullable();
            $table->boolean('is_public')->default(false)->index();
            $table->boolean('is_general')->default(false)->index();
            $table->boolean('is_available')->default(false)->index();
            $table->boolean('is_ending')->default(false)->index();
            $table->boolean('is_refund')->default(false)->index();
            $table->string('refund_token')->nullable()->index();
            $table->dateTime('ends_on')->nullable()->index();
            $table->boolean('is_limited')->default(false)->index();
            $table->integer('limit')->nullable()->index();
            $table->integer('uses_per_user')->default(1)->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
