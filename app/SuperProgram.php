<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperProgram extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
	];

	protected $fillable=[
		'name',
		'title',
		'description',
		'content',
	];

	public $filterable_columns=[
		'name',
		'title',
	];
    
	public function programs(){
		return $this->hasMany(Program::class);
	}

}
