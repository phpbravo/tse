@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
    @include('frontend.includes.navbar')




    <section class="faq-section" id="wetrainvids">
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-12 mt-5">
                   
                </div>
            </div>
          
            <!---- seance video list ----->
            <div class="row wetrainvideos-items mt-5 pb-0" id="wetrainvidscontainer">
                <div class="col-lg-12 col-md-12">
                    <div class="vidlists">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe
                                src="https://player.vimeo.com/video/{{$exercise->video->vimeo_id}}?title=0&amp;byline=0&amp;portrait=0"
                                width="540px" height="305px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 px-0">
                    <div class="materialouter">
                        <div class="materialinner">
                            <h3>{{ $exercise->name }}</h3>

                            @if ($exercise->description)
                                {!! $exercise->description !!}

                            @elseif($exercise->content)
                                {!! $exercise->content !!}
                            @endif
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection

    @section('footer')

</div>
@endsection
