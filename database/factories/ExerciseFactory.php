<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Exercise;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Exercise::class, function (Faker $faker) {
    return [
        'name'=>$faker->words($nb=$faker->numberBetween($min=1,$max=4), $asText=true),
        'description'=>$faker->sentence($nb=$faker->numberBetween($min=3,$max=8), $asText=true),
        'content'=>$faker->randomHtml(2,3),
        'video_link'=>$faker->randomElement([
            'https://player.vimeo.com/video/329959630',
            'https://player.vimeo.com/video/254597739',
            'https://player.vimeo.com/video/331591338',
        ]),
    ];
});

$factory->state(Exercise::class,'wetrain', function(Faker $faker){
    return [
        'category_id' => 1,
        'subcategory_id' => $faker->numberBetween($min=1, $max=5),
    ];
});

$factory->state(Exercise::class,'wesweat', function(Faker $faker){
    return [
        'category_id' => 2,
        'subcategory_id' => $faker->numberBetween($min=6, $max=12),
    ];
});