<?php

namespace Tests\API\v0;

use App\Gateway;
use App\Offer;
use App\Plan;
use App\Purchase;
use App\Subscription;
use App\Transaction;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SelfControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\User */
    private $user;

    /** @var \App\Plan */
    private $free_plan, $paid_plan;

    /** @var \App\Offer */
    private $free_offer, $paid_offer;

    private $username = "username";
    private $password = "password";

    private function createPlan(int $plan_id): Plan
    {
        return Plan::forceCreate([
            'id' => $plan_id,
            'name'=>'TEST PLAN',
        ]);
    }

    private function createOffer(Plan $plan): Offer
    {
        return Offer::create([
            'plan_id'=>$plan->id,
            'name'=>'TEST OFFER',
            'title'=>'TEST OFFER',
            'description'=>'This is a test offer. You should not see this.',
            'price'=>'14.99',
            'tax_percent'=>'20',
            'duration'=>1,
            'is_recurring'=>true,
            'appstore_id'=>'tse1month',
            'playstore_id'=>'tse1month'
        ]);
    }

    private function createTestModels()
    {
        Model::withoutEvents(function()
        {
            $this->user = User::forceCreate([
                'surname' =>  'doe',
                'email' => $this->username,
                'username' => $this->username,
                'agreed_terms' => true,
                'password' => bcrypt($this->password)
            ]);

            $this->free_plan = $this->createPlan(24);
            
            $this->paid_plan = $this->createPlan(1);
    
            $this->free_offer = $this->createOffer($this->free_plan);

            $this->paid_offer = $this->createOffer($this->paid_plan);
        });
    }

    private function subscribeUserToOffer(Offer $offer)
    {
        $this->subscription = Subscription::createActive(
            $this->user,
            $offer,
            1234,
            Gateway::FREE
        );

        $transaction = Transaction::createSucceededOfferSubscription(
            $this->subscription,
            54231,
            $offer->price,
            0,
            'EUR',
            now()->timestamp
        );
        
        Purchase::createFromTransaction(
            $transaction
        );
    }

    private function subscribeUserToFreeOffer()
    {
        $this->subscribeUserToOffer($this->free_offer);
    }

    private function subscribeUserToPaidOffer()
    {
        $this->subscribeUserToOffer($this->paid_offer);
    }

    /**
     * Test that a user with free plan cannot access content
     *
     * @return void
     */
    public function testNoAccessForFreeOffer()
    {
        $this->createTestModels();
        $this->subscribeUserToFreeOffer();

        $query = http_build_query([
            'option' => 'com_osmembership',
            'task' => 'api.login',
            'username' => $this->username,
            'password' => $this->password
        ]);

        $response = $this->get('/?'.$query);

        $response->assertOk();

        $response->assertJsonFragment([
            'access' => false
        ]);
    }
    /**
     * Test that a user with paid plan can access content
     *
     * @return void
     */
    public function testAccessForPaidOffer()
    {
        $this->createTestModels();
        $this->subscribeUserToPaidOffer();

        $query = http_build_query([
            'option' => 'com_osmembership',
            'task' => 'api.login',
            'username' => $this->username,
            'password' => $this->password
        ]);

        $response = $this->get('/?'.$query);

        $response->assertOk();

        $response->assertJsonFragment([
            'access' => true
        ]);
    }
}