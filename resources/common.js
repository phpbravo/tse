Number.prototype.centsToStringPlaces = function(){
	return (this/100).toFixed(2)
		.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
		.replace(/,/g,' ')
		.replace('.',',');
}

Number.prototype.toStringPlaces = function(){
	return this.toFixed(2)
		.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
		.replace(/,/g,' ')
		.replace('.',',');
}