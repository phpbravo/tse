@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
		<section class="same-wrapper definitions" id="top">
			<div class="container">
				<div class="row pb-5">
					
						<div class="col-lg-12">
							<h1 class="pages-heading">LES DÉFINITIONS</h1>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="inner-samecontent inner-bonussession">
								<h3 class="inner-contentheader">LES DEFINITIONS</h3>
								
								<p>
									Retrouve ici l'ensemble du jargon (définitions) sportif que l'on utilise.
								</p>

								<p>
									Pour voir toutes les définitions 
								</p>

								<p class="text-center">
									<a href="#" class="btn btn-primary custombtn"><span>CONNECTE TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								<p class="text-center font-weight-bold">
									OU
								</p>
								<p class="text-center">
									<a href="#" class="btn btn-primary custombtn"><span>INSCRIS TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								
							</div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="img-container">
								<img src="/assets/img/cc_definitions.jpg" class="img-fluid bg-fitnessimg">
							</div>
						</div>
					
				</div>
			</div>
		
		</section>
@include('frontend.includes.parallax-last')	
	<a href="#" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection