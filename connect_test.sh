#!/bin/bash

REPOSITORY_ADDRESS="git@bitbucket.org:phpbravo/tse.git"

TEST_SERVER_USER="ohmnewtse"
TEST_SERVER_ADDRESS="94.23.16.215"
TEST_SERVER_DIRECTORY="/var/www/trainsweateat.ohm-conception.com/web"

ssh $TEST_SERVER_USER@$TEST_SERVER_ADDRESS "\
cd $TEST_SERVER_DIRECTORY &&\
git clone $REPOSITORY_ADDRESS . ;\
git fetch &&\
git checkout master &&\
git reset origin/master --hard &&\
sh deploy_test.sh"