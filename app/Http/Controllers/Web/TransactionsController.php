<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{StripeHandler, PaypalHandler, ObjectHandler};

use Carbon\Carbon;

use App\{Transaction};

class TransactionsController extends Controller
{

	public function confirmTransaction(){
		$stripeHandler=new StripeHandler;
		$paypalHandler=new PaypalHandler;
		if (request()->get('stripe_payment_intent_token')){

			return $stripeHandler->confirmTransaction(request()->get('stripe_payment_intent_token'));

		}
	}

}
