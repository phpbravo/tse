@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="temoignages-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center">TÉMOIGNAGES</h1>
				</div>
			</div>
			<div class="row temoignagesrow">
				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">
						<img src="/assets/img/Avant-Apre`s Steven.png" class="card-img-top" alt="testimonial-image">
						<div class="card-body">
							<p class="card-text">
								Tu as changé ma vie ! Tes vidéos, tes conseils et ma volonté m'ont fait perdre 20 kilos et permis d'obtenir le corps athlétique que j'ai désormais !
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">
						<img src="/assets/img/lea.jpg" class="card-img-top" alt="testimonial-image">
						<div class="card-body">
							<p class="card-text">
								Grâce à tes vidéos j'ai réussi à perdre 12kg, à me mettre à la muscu et à adorer ça ! Et pour ça je te remercie vraiment ❤ Même ma mère s'y est mise!
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">
						<img src="/assets/img/temoignage-3.jpg" class="card-img-top" alt="testimonial-image">
						<div class="card-body">
							<p class="card-text">
								Je n'aurais jamais cru pouvoir me transformer comme ça !!! Je fais 1m60 et je suis passée de 59 kg à 53 en 1 mois 😍 on me l’aurais dit que je l’aurais pas cru !
							</p>
						</div>
					</div>
				</div>
			
				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Je viens de faire 3 séance WESWEAT de gainage et de HIIT! Je me sens tellement bien merci Sissy 💪❤ Ce projet est énorme
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Merci pour we sweat ! Les vidéos sont au top et ça fait du bien d'avoir un programme varié à faire à la maison ! Bravo à toi et Tini !
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Merci à toi sissy pour ces super séances we sweat! Grâce à toi j'ai découvert le goût de faire de la musculation et du cardio !
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Sarah : "Après une semaine de WeSweat je vois déjà les progrès notamment sur la résistance. Au début je ne comprenais pas comment c'était possible d'enchainer plusieurs trainings et aujourd'hui j'ai pu en faire deux d'affilé ! Merci beaucoup pour ton boulot, c'est pro et ca paye.
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Lalie : "Coucou Sissy, cela fait maintenant un mois que je suis inscrite sur TrainSweatEat et que j'ai commencé un rééquuilibrage alimentaire...voici un petit bilan... je ne pratique pas le sport en salle je fais donc les séances we sweat à un rythme de 3 fois  par semaine, chaque séance dure 1h environ car j'enchaine à chaque fois 4 vidéos...je suis ravie de constater que mon corps change déjà, je me sens moins lourde et toujours ultra motivée !!! Vivement les nouvelles vidéos!!! et merci encore!!!"
							</p>
						</div>
					</div>
				</div>

				<div class="col-xl-4 col-lg-12">
					<div class="card temoignagescards">	
						<div class="card-body">
							<p class="card-text">
								Vicky : "Bonjour, Vos vidéos sont au top... 1 semaine et demi de sport et de rééquilibrage alimentaire et déjà 4kg de perdu et mon Corp se redessine...
							</p>
							<p class="card-text">
								Merci pour tout et je ne lâche rien.
								Avoir eu 4 enfants n'excuse pas le fait que le Corp ne ressemble plus à rien. Merci merci merci
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	

@endsection

@section('footer')

<div/>

@endsection