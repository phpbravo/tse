<?php

namespace App\Handlers;

use App\AppstoreReceipt;
use App\FailedMobileSubscription;
use App\Gateway;
use App\Offer;
use App\Purchase;
use App\Subscription;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;

class LegacyAppstoreHandler
{
    public function renewSubscription(Subscription $subscription)
    {
    }

    public function cleanReceipt(object $receipt): object
    {
        if (isset($receipt->receipt))
        {
            unset($receipt->receipt);
        }

        if (
            isset($receipt->latest_receipt_info) &&
            gettype($receipt->latest_receipt_info) == 'array'
        ) {
            $temp_latest_receipt_info = $receipt->latest_receipt_info[0];
            $receipt->latest_receipt_info = $temp_latest_receipt_info;
        }

        return $receipt;
    }

    public function getReceipt(string $receipt, bool $live_mode = true): object
    {
        $response_body = null;
        $password = config('webhook.appstore.secret');
        $base_url = $live_mode ?
            'https://buy.itunes.apple.com' :
            'https://sandbox.itunes.apple.com';

        $client = new Client(
            [
                'base_uri' => $base_url,
            ]
        );
        $payload = [
            'json' => [
                'receipt-data' => $receipt,
                'password' => $password,
                'exclude-old-transactions' => true,
            ]
        ];

        while ($response_body == null)
        {
            try
            {
                $response = $client->post('/verifyReceipt', $payload);
                $response_body = json_decode(
                    $response->getBody()->getContents()
                );

                $response_body = $this->cleanReceipt($response_body);

            }
            catch(\Exception $e)
            {
                error_log($e->getMessage());
            }
        }

        $response_status = $response_body->status;

        if ($live_mode && $response_status == '21007')
        {
            return $this->getReceipt($receipt, false);
        }

        return $response_body;
    }

    public function findOrCreateReceipt(string $receipt_data): AppstoreReceipt
    {
        $appstore_receipt_data = $this->getReceipt($receipt_data);

        $raw_receipt_data = json_encode($appstore_receipt_data);

        $compressed_receipt =
            gzdeflate($raw_receipt_data);

        $vendor_token =
            $appstore_receipt_data
                ->latest_receipt_info
                ->unique_vendor_identifier
                ??
                null;

        $original_transaction_token = 
            $appstore_receipt_data
                ->latest_receipt_info
                ->original_transaction_id 
                ??
                null;

        $transaction_token = 
            $appstore_receipt_data
                ->latest_receipt_info
                ->transaction_id
                ??
                null;

        $appstore_receipt = AppstoreReceipt::updateOrCreate(
            [
                'original_transaction_token' => $original_transaction_token,
                'transaction_token' => $transaction_token
            ],
            [
                'content' => $compressed_receipt,
                'vendor_token' => $vendor_token,
                'original_transaction_token' => $original_transaction_token, 
                'transaction_token' => $transaction_token, 
            ]
        );

        return $appstore_receipt;
    }

    public function transferSubscriptionToUser(
        Subscription $subscription, User $user
    ){
        $subscription->update([
            'user_id' => $user->id
        ]);
    }

    public function createSubscriptionFromReceipt(
        AppstoreReceipt $appstore_receipt,
        User $user
    ){
        $latest_receipt_info =
            $appstore_receipt->parsed_content->latest_receipt_info;
        $original_transaction_token =
            $appstore_receipt->original_transaction_token;
        $transaction_token = $appstore_receipt->transaction_token;
        $original_purchase_date = $latest_receipt_info->original_purchase_date;
        $purchase_date = $latest_receipt_info->purchase_date;
        $offer_token = $latest_receipt_info->product_id;

        $offer = Offer::fromAppstoreToken($offer_token);
        $offer_price_integer = bcmul($offer->price, 100);

        $new_subscription = Subscription::createActive(
            $user,
            $offer,
            $original_transaction_token,
            Gateway::APPSTORE
        );

        $new_subscription->update([
            'subscribed_on' => $original_purchase_date
        ]);

        $new_transaction = Transaction::createSucceededOfferSubscription(
            $new_subscription,
            $transaction_token,
            $offer_price_integer,
            0,
            'EUR',
            now()->timestamp
        );

        $new_purchase = Purchase::createFromTransaction($new_transaction);
        $new_purchase->reschedule(Carbon::parse($purchase_date));

        $new_subscription->recalculateNextPaymentDate();
    }

    public function syncSubscription(string $receipt, int $user_id)
    {
        $appstore_receipt = $this->findOrCreateReceipt($receipt);
        $user = User::find($user_id);

        $existing_subscription = Subscription::getFromToken(
            $appstore_receipt->original_transaction_token
        );
        $existing_failed_subscription =
            FailedMobileSubscription::getFromVendorTokenNotRecovered(
                $appstore_receipt->vendor_token ?? ''
            );

        if ($existing_subscription)
        {
            $this->transferSubscriptionToUser($existing_subscription, $user);
        }
        elseif ($existing_failed_subscription)
        {
            $this->recoverSubscription(
                $existing_failed_subscription,
                $appstore_receipt
            );
        }
        else
        {
            $this->createSubscriptionFromReceipt($appstore_receipt, $user);
        }

    }

    public function assignReceiptToTransaction(
        AppstoreReceipt $appstore_receipt,
        Transaction $transaction
    ) {
        $appstore_receipt->update([
            'transaction_id' => $transaction->id
        ]);
    }

    public function recoverSubscription(
        FailedMobileSubscription $failed_subscription,
        AppstoreReceipt $appstore_receipt
    ) {
        $user = $failed_subscription->user;

        $this->createSubscriptionFromReceipt($appstore_receipt, $user);

        $failed_subscription->update([
            'appstore_receipt_id' => $appstore_receipt->id,
            'is_recovered' => true,
        ]);
    }

    public function initialBuy(int $appstore_receipt_id): void
    {
        /** @var \App\AppstoreReceipt */
        $appstore_receipt = AppstoreReceipt::find($appstore_receipt_id);
        $vendor_token = $appstore_receipt->vendor_token;
        $transaction_token =
            $appstore_receipt->transaction_token;

        $existing_transaction =
            Transaction::getFromToken($transaction_token);

        $failed_subscription =
            FailedMobileSubscription::getFromVendorTokenNotRecovered(
                $vendor_token
            );

        if ($existing_transaction)
        {
            $this->assignReceiptToTransaction(
                $appstore_receipt,
                $existing_transaction
            );
        }
        elseif ($failed_subscription)
        {
            $this->recoverSubscription(
                $failed_subscription,
                $appstore_receipt
            );
        }
    }

    public function handleEvent($event)
    {

        $event_type = $event['event_type'];
        $receipt_id = $event['receipt_id'];
        $event_info = $event['latest_receipt_info'];

        $subscription_token = $event_info['original_transaction_id'] ?? null;

        switch($event_type)
        {
            case 'legacy_INITIAL_BUY':
                $this->initialBuy($receipt_id);
                break;

            default:
                throw(new \Exception("Unsupported event: $event_type"));
                break;
        }

    }

}