<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;


/**
 * Class Plan
 *
 * A resource representing a plan.
 *
 * @package PayPal\Api2
 *
 * @property string id
 * @property string product_id
 * @property string name
 * @property string description
 * @property string status
 * @property \PayPal\Api2\BillingCycle[] billing_cycles
 * @property \PayPal\Api2\PaymentPreferences payment_preferences
 * @property \PayPal\Api2\Taxes taxes
 * @property bool quantity_supported
 * @property string create_time
 * @property string update_time
 */
class Plan extends PayPalResourceModel
{
    use Timestamps;

    /**
     * @var string $CREATED The plan was created. You cannot create 
     *  subscriptions for a plan in this state.
     */
    const CREATED = 'CREATED';

    /**
     * @var string $INACTIVE The plan is inactive.
     */
    const INACTIVE = 'INACTIVE';

    /**
     * @var string $ACTIVE The plan is active. You can only create 
     *  subscriptions for a plan in this state.
     */
    const ACTIVE = 'ACTIVE';

    /**
     * Identifier of the plan.
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Identifier of the plan.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Product ID for this plan.
     */
    public function setProductId(string $product_id): self
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * Product ID for this plan.
     */
    public function getProductId(): string
    {
        return $this->product_id;
    }

    /**
     * Name for this plan.
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Name for this plan.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Description for this plan.
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Description for this plan.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Status of the plan.
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Status of the plan.
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Billing cycles of the plan.
     * 
     * An array of billing cycles for trial and regular billing. A 
     * plan can have multiple billing cycles but only one trial billing 
     * cycle.
     * 
     * @param \PayPal\Api2\BillingCycle[] $billing_cycles
     */
    public function setBillingCycles(array $billing_cycles): self
    {
        $this->billing_cycles = $billing_cycles;
        return $this;
    }

    /**
     * Billing cycles of the plan.
     * 
     * An array of billing cycles for trial and regular billing. A 
     * plan can have multiple billing cycles but only one trial billing 
     * cycle.
     * 
     * @return \PayPal\Api2\BillingCycle[] $billing_cycles
     */
    public function getBillingCycles(): array
    {
        return $this->billing_cycles;
    }
    
    /**
     * Append billing cycles to the plan.
     */
    public function addBillingCycle(BillingCycle $item): self
    {
        if (!$this->getBillingCycles()) {
            return $this->setBillingCycles(array($item));
        } else {
            return $this->setBillingCycles(
                array_merge($this->getBillingCycles(), array($item))
            );
        }
    }

    /**
     * Remove billing cycles from the plan.
     */
    public function removeBillingCycle(BillingCycle $item): self
    {
        return $this->setBillingCycles(
            array_diff($this->getBillingCycles(), array($item))
        );
    }

    /**
     * Payment preferences of the plan.
     * 
     * An array of billing cycles for trial and regular billing. A 
     * plan can have multiple billing cycles but only one trial billing 
     * cycle.
     */
    public function setPaymentPreferences(PaymentPreferences $payment_preferences): self
    {
        $this->payment_preferences = $payment_preferences;
        return $this;
    }

    /**
     * Payment preferences of the plan.
     * 
     * An array of billing cycles for trial and regular billing. A 
     * plan can have multiple billing cycles but only one trial billing 
     * cycle.
     */
    public function getPaymentPreferences(): PaymentPreferences
    {
        return $this->payment_preferences;
    }

    /**
     * Tax details of the plan.
     */
    public function setTaxes(Taxes $taxes): self
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * Tax details of the plan.
     */
    public function getTaxes(): Taxes
    {
        return $this->taxes;
    }

    /**
     * Is quantity supported.
     * 
     * Indicates whether you can subscribe to this plan by providing a 
     * quantity for the goods or service.
     */
    public function setQuantitySupported(bool $quantity_supported): self
    {
        $this->quantity_supported = $quantity_supported;
        return $this;
    }

    /**
     * Is quantity supported.
     * 
     * Indicates whether you can subscribe to this plan by providing a 
     * quantity for the goods or service.
     */
    public function getQuantitySupported(): bool
    {
        return $this->quantity_supported;
    }

    public function create(
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        $payLoad = $this->toJSON();
        $json = self::executeCall(
            "/v1/billing/plans/",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    public static function get(
        string $plan_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate($plan_id, 'plan_id');
        $payLoad = "";
        $json = self::executeCall(
            "/v1/billing/plans/$plan_id",
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $ret = new Plan();
        $ret->fromJson($json);
        return $ret;
    }
}
