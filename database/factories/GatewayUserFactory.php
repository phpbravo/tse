<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\GatewayUser;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(GatewayUser::class, function (Faker $faker) {
    $gateway_id=$faker->boolean;
    return [
        'user_id' => $faker->unique()->numberBetween($min=1,$max=100),
        'gateway_id' => $gateway_id+1,
        'gateway_token' => $gateway_id?'QYR5Z8XDVJNXQ':'cus_FouNEgALEvcXpI',
    ];
});