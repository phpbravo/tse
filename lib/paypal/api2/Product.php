<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;


/**
 * Class Product
 *
 * A resource representing a product.
 *
 * @package PayPal\Api2
 *
 * @property string id
 * @property string name
 * @property string description
 * @property string type
 * @property string category
 * @property string image_url
 * @property string home_url
 * @property string create_time
 * @property string update_time
 */
class Product extends PayPalResourceModel
{
    use Timestamps;

    /**
     * Identifier of the product.
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Identifier of the product.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Name for this product.
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Name for this product.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Description for this product.
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Description for this product.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Type of the product.
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Type of the product.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Category of the product.
     */
    public function setCategory(string $category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Category of the product.
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * ImageUrl of the product.
     */
    public function setImageUrl(string $image_url): self
    {
        $this->image_url = $image_url;
        return $this;
    }

    /**
     * ImageUrl of the product.
     */
    public function getImageUrl(): string
    {
        return $this->image_url;
    }

    /**
     * HomeUrl of the product.
     */
    public function setHomeUrl(string $home_url): self
    {
        $this->home_url = $home_url;
        return $this;
    }

    /**
     * HomeUrl of the product.
     */
    public function getHomeUrl(): string
    {
        return $this->home_url;
    }

    public function create(
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        $payLoad = $this->toJSON();
        $json = self::executeCall(
            "/v1/catalogs/products/",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    public static function get(
        string $product_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate($product_id, 'product_id');
        $payLoad = "";
        $json = self::executeCall(
            "/v1/catalogs/products/$product_id",
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $ret = new Product();
        $ret->fromJson($json);
        return $ret;
    }

    public static function list(
        string $page,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ) {
        ArgumentValidator::validate($page, 'page');
        $payLoad = "";
        $json = self::executeCall(
            "/v1/payments/billing-plans/?page_size=20&total_required=true&page={$page}",
            "GET",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        // $ret = new Product();
        // $ret->fromJson($json);
        return json_decode($json);
    }
}
