<style>
	.dropdown>a{
		cursor:	default;
	}
</style>
<header class="header">
	<div class="header-wrapper">
		<nav class="navbar navbar-expand-lg bg-white navbar-dark">
		  	<a class="navbar-brand" href="{{ route('front.home') }}"><img src="/assets/img/logo.png" width="200" alt="train sweat eat"></a>

		  	<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
						<span class="sr-only">Ouvrir la barre de navigation</span>
		    	<span class="icon-bar bar1"></span>
		    	<span class="icon-bar bar2"></span>
		    	<span class="icon-bar bar3"></span>
		    	<span class="icon-bar bar4"></span>
		  	</button>

		  	<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
									<a href="{{ route('front.home') }}">
										<img src="/assets/img/picto-home.png" alt="">
										Accueil
									</a>
							</li>
							<li class="nav-item dropdown">
								<a>
									<img src="/assets/img/picto-concept.png" alt="">
									Concept
									<i class="fa fa-angle-down icon-angle"></i>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a class="dropdown-item js-activated bt-white" href="{{ route('front.concept') }}">
											Tout savoir
										</a>
									</li>
								</ul>
							</li>
					<li class="nav-item">
						<a href="{{ route('front.testimonies') }}">
							Témoignages
						</a>
					</li>
					<li class="nav-item">
						<a>
							<img src="/assets/img/picto-wepump.png" alt="">
							We train
							<i class="fa fa-angle-down icon-angle"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.program') }}">
									Programme du jour
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-train.bonus') }}">
									Séances bonus
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-train.videos') }}">
									Vidéos démo
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-train.faq') }}">
									FAQ & Définitions
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-train.concept') }}">
									Concept
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a>
							<img src="/assets/img/picto-wesweat.png" alt="">
							We sweat
							<i class="fa fa-angle-down icon-angle"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-sweat.videos') }}">
									Séances vidéo
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-sweat.faq') }}">
									FAQ
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-sweat.concept') }}">
									Concept
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a>
							<img src="/assets/img/picto-weeat.png" alt="">
							We eat
							<i class="fa fa-angle-down icon-angle"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.we-eat.recipes') }}">
									Mes recettes fit
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.ebook') }}">
									ebook
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a>
							<img src="/assets/img/picto-nous.png" alt="">
							Nous
							<i class="fa fa-angle-down icon-angle"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.nous.sissy') }}">
									Sissy
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.nous.tini') }}">
									Tini
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.nous.notre-recontre') }}">
									Notre rencontre
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="{{ route('front.ebook') }}">
							Ebook
						</a>
					</li>
					<li class="nav-item">
						<span>
							<img src="/assets/img/picto-loggin.png" alt="">
							Accès
							<i class="fa fa-angle-down icon-angle"></i>
						</span>
						<ul class="dropdown-menu">
							@if(auth()->guest())
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.login') }}">
									Connexion
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.register') }}">
									Inscription
								</a>
							</li>
							@else
							<li>
								<a class="dropdown-item js-activated bt-white" href="{{ route('front.account-subscriptions') }}">
									Mon Compte
								</a>
							</li>
							<li>
								<a class="dropdown-item js-activated bt-white" href="#" onclick="return logout()">
									Deconnexion
								</a>
							</li>
							@endif
						</ul>
					</li>
		    	</ul>
		  	</div>
		</nav>
		<hr class="hr-m-0">
	</div>
</header>