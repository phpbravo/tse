<?php

namespace App\Handlers;

class FilterHandler{

	private $modelClass;
	private $prototype;
	private $tableName;
	private $searchableColumns;
	private $filterableColumns;
	private $orderableColumns;
	private $searchableColumnDemutators;
	private $filterableColumnDemutators;
	private $orderableColumnDemutators;
	private $tableAttributes;
	private $tableRelations;
	private $query;

	public function __construct($modelClass){

		$this->modelClass=$modelClass;
		
		$this->prototype=with(new $modelClass);

		$this->tableName=$this->prototype->getTable();

		$this->searchableColumns=$this->prototype->searchable_columns?:[];
		$this->filterableColumns=$this->prototype->filterable_columns?:[];
		$this->orderableColumns=$this->prototype->orderable_columns?:[];

		$this->searchableColumnDemutators=
			method_exists($this->prototype,'getSearchableColumnDemutators')?
				$this->prototype->getSearchableColumnDemutators()
			:
				[];
		$this->filterableColumnDemutators=
			method_exists($this->prototype,'getFilterableColumnDemutators')?
				$this->prototype->getFilterableColumnDemutators()
			:
				[];
		$this->orderableColumnDemutators=
			method_exists($this->prototype,'getOrderableColumnDemutators')?
				$this->prototype->getOrderableColumnDemutators()
			:
				[];

		$this->query=$this->prototype->newQuery();
	}


	public function validateFilters(&$filters){

		if (!$filters) return;

		foreach ($filters as $index => $filter){
			
			$filters[$index]=json_decode($filter);
		}

		foreach ($filters as $index => $filter){
			
			if (in_array($filter->field,$this->filterableColumns))
				continue;

			if (array_key_exists($filter->field,$this->filterableColumnDemutators)){

				$demutated=$this->filterableColumnDemutators[$filter->field]($filter->value);

				if ($demutated){
					
					$filters[$index]->field=$demutated['column_name']();
					$filters[$index]->value=$demutated['column_value']();

					continue;
				}
			}

			array_splice($filters,array_search($filter,$filters,true),-1);

		}

	}

	
	public function orderBy($field,$ascending=false){

		if (!$field)
			return $this->query;
		
		$ascending=filter_var($ascending, FILTER_VALIDATE_BOOLEAN)?'asc':'desc';

		if ($field=='id'||in_array($field,$this->orderableColumns)){
			$this->query=$this->query->orderBy($field,$ascending);

		}elseif (array_key_exists($field,$this->orderableColumnDemutators)){
			$this->query=$this->orderableColumnDemutators[$field]($this->query,$ascending);
		}

		return $this->query;
		
	}
	
	public function getFilteredQuery($base_filters,$search,$orderBy=null,$ascending=false){
		
		$filters=$base_filters?array_filter($base_filters, function($value){
			return !is_null($value) && $value !== '{}';
		}):$base_filters;

		$this->validateFilters($filters);
		
		if ($filters){

			foreach ($filters as $filter){
				$this->query=$this->query->where($filter->field,'like',$filter->value);
			}   
		}

		if ($search){
			
			$search=trim($search);

			$this->query=$this->query->where(function($query) use ($search){
				$query_groups=explode(',',$search);
				foreach ($query_groups as $group_index => $query_string){
					$query_string=trim($query_string);
					if (!$query_string) continue;
					$query_string=explode(' ',$query_string);

					$stringSearch=function($query) use ($query_string){

						foreach ($query_string as $word_index => $query_word){
		
							$wordSearch= function ($query) use ($query_word){
						
								$query->where('id',$query_word);

								foreach ($this->searchableColumns as $column_name){
									$query->orWhere($column_name,'like','%'.$query_word.'%');
								}

								foreach ($this->searchableColumnDemutators as $column_demutator){
									$column_demutator($query,$query_word);
								}

							};
		
							$query->where($wordSearch);
						}
					};

					if ($group_index==0){
						$query->where($stringSearch);
					}else{
						$query->orWhere($stringSearch);
					}
				}
			});

		}

		$this->orderBy($orderBy,$ascending);

		return $this->query;

	}

	public function getQuery(){
		return $this->query;
	}

}