<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
	use Notifiable;

	protected $dispatchesEvents = [
		'created' => Events\UserCreated::class,
	];

	protected $fillable = [
		'email',
		'username',
		'password',
		'name',
		'surname',
		'organization',
		'address',
		'address2',
		'city',
		'postal_code',
		'country',
		'phone',
		'homepage_url',
		'agreed_terms',
		'agreed_newsletter',
		'created_at',
		'updated_at',
		'balance',
	];

	protected $hidden = [
		'password',
		'remember_token',
	];

	protected $casts = [
		'email_verified_at' => 'datetime',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

	// columns that can be searched
	public $searchable_columns = [
		'email',
		'name',
		'surname',
		'phone',
	];

	// columns that can be filtered
	public $filterable_columns = [
		'city',
		'postal_code',
		'country',
	];

	// columns that can be ordered
	public $orderable_columns = [
		'email',
		'name',
		'surname',
	];
	
	// return query builders to search through mutated columns
	public function getSearchableColumnDemutators()
	{
		return [

		];
	}

	// return query builders to filter mutated colummns
	public function getFilterableColumnDemutators()
	{
		return [
			
		];
	}

	// return query builders to sort by mutated columns
	public function getOrderableColumnDemutators()
	{
		return [
			'full_name'=>function($query,$ascending = 'asc')
			{
				return $query
					->orderBy('name',$ascending)
					->orderBy('surname',$ascending);
			},
			'formal_full_name'=>function($query,$ascending = 'asc')
			{
				return $query
				->orderBy('surname',$ascending)
				->orderBy('name',$ascending);
			}
		];
	}

	// Simple Relations

	public function gateways()
	{
		return $this->belongsToMany(Gateway::class);
	}

	public function gateway_users()
	{
		return $this->hasMany(GatewayUser::class);
	}

	public function purchases()
	{
		return $this->hasMany(Purchase::class);
	}

	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(Subscription::class);
	}

	public function pauses()
	{
		return $this->hasMany(Pause::class);
	}

	public function access_logs()
	{
		return $this->hasMany(UserAccessLog::class);
	}

	public function special_offers()
	{
		return $this->belongsToMany(
			Offer::class,
			'offer_whitelist'
		);
	}

	// Scoped Relations

	public function active_subscriptions()
	{
		return $this->subscriptions()->active();
	}

	public function inactive_subscriptions()
	{
		return $this->subscriptions()->inactive();
	}

	public function active_purchases()
	{
		return $this->purchases()->active();
	}

	public function inactive_purchases()
	{
		return $this->purchases()->inactive();
	}

	public function successful_transactions()
	{
		return $this->transactions()->succeeded();
	}
	
	public function pending_transactions()
	{
		return $this->transactions()->pending();
	}

	public function failed_transactions()
	{
		return $this->transactions()->failed();
	}

	// Attributes

	public function getFullNameAttribute()
	{
		return $this->name.' '.$this->surname;
	}

	public function getFormalNameAttribute()
	{
		return $this->surname.', '.$this->name;
	}

	public function getStripeTokenAttribute()
	{
		return GatewayUser::where('user_id',$this->id)
			->where('gateway_id',1)->first()->gateway_token;
	}

	public function getPaypalTokenAttribute()
	{
		$gateway_user = GatewayUser::where(
			'user_id',
			$this->id
		)->where('gateway_id',2)
		->first();

		if (!$gateway_user) return null;

		return $gateway_user->gateway_token;
	}
	
	public function getPlansAttribute()
	{
		return Plan::whereIn(
			'id',
			$this->purchases()->pluck('plan_id')
		)->get();
	}

	public function getActivePlansAttribute()
	{
		return Plan::whereIn(
			'id',
			$this->active_purchases()->pluck('plan_id')
		)->get();
	}

	public function getProductsAttribute()
	{
		return Product::findMany(
			PlanProduct::whereIn(
				'plan_id',
				$this->purchases()->pluck('plan_id')
			)->pluck('product_id')
		);
	}

	public function getActiveProductsAttribute()
	{
		return Product::findMany(
			PlanProduct::whereIn(
				'plan_id',
				$this->active_purchases()->pluck('plan_id')
			)->pluck('product_id')
		);
	}

	// Scopes
	
		// none

	// Instance Methods
	
	public function issueCredit(float $credit_amount): void
	{
		$this->update([
			'balance'=>bcadd($this->balance,$credit_amount,2)
		]);
	}

	public function chargeCredit(float $charge_amount): void
	{
		$this->update([
			'balance'=>bcsub($this->balance,$charge_amount,2)
		]);
	}

	public function hasOfferById(int $offer_id): bool
	{
		$active_count = $this->active_purchases()
			->where('offer_id',$offer->id)
			->count();

		return $active_count > 0;
	}

	public function hasPlanById(int $plan_id): bool
	{
		return ($this->active_plans->contains('id',$plan_id));
	}

	public function hasProductById(int $product_id): bool
	{
		return ($this->active_products->contains('id',$product_id));
	}

	public function getCurrentPurchaseOfOffer(
		Offer $offer
	): ?Purchase {
		$active_purchase = $this->active_purchases()
		->where('plan_id',$offer->plan_id)
		->first();

		return $active_purchase;
	}

	public function getCurrentSubscriptionToPlan(
		Plan $plan
	): ?Subscription {
		$active_subscription = $this->subscriptions()
			->whereHas('offer',function($query) use ($plan){
				$query->where('plan_id', $plan->id);
			})
			->active()
			->first();
		
			return $active_subscription;
	}

	//Static Methods
	
		// none

}