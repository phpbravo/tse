@echo off
REM php artisan make:model User -m &:: already prebuilt
php artisan make:model Admin -m
php artisan make:model Resource -m
php artisan make:model Gateway -m
php artisan make:model Coupon -m
php artisan make:model Tag -m
php artisan make:model Section -m
php artisan make:model Plan -m
php artisan make:model Group -m

:: needs User
php artisan make:model Pause -m

:: needs Resource
php artisan make:model Category -m
php artisan make:model Session -m
php artisan make:model Program -m
php artisan make:model Recipe -m
php artisan make:model Book -m
php artisan make:model Product -m

:: needs Resource, Category
php artisan make:model Subcategory -m

:: needs Resource, Category, Subcategory
php artisan make:model Exercise -m

:: needs User, Resource
php artisan make:model UserAccessLog -m

:: needs Plan
php artisan make:model Offer -m

:: needs Program
php artisan make:model ProgramSchedule -m

:: needs Offer
php artisan make:model OfferSchedule -m
php artisan make:model EarlyRenewalDiscount -m

:: needs User, Offer, Gateway
php artisan make:model Subscription -m

:: needs User, Gateway, Subscription, Coupon
php artisan make:model Transaction -m

:: needs User, Offer, Transaction
php artisan make:model Purchase -m




:::: composite pivot tables

::needs Gateway, User
php artisan make:model GatewayUser -pm
::needs Gateway, Offer
php artisan make:model GatewayOffer -pm
::needs Recipe, Section
php artisan make:model RecipeSection -pm




:::: simple pivot tables

:: needs Plan, Product
php artisan make:migration create_plan_product_pivot_table --table="plan_product"
:: needs Recipe, Tag
php artisan make:migration create_recipe_tag_pivot_table --table="recipe_tag"
:: needs Exercise, Tag
php artisan make:migration create_exercise_tag_pivot_table --table="exercise_tag"
:: needs Exercise, Session
php artisan make:migration create_exercise_session_pivot_table --table="exercise_session"
:: needs Program, Session
php artisan make:migration create_program_session_pivot_table --table="program_session"
:: needs Offer, User
php artisan make:migration create_offer_whitelist_pivot_table --table="offer_whitelist"
:: needs Coupon, User
php artisan make:migration create_coupon_whitelist_pivot_table --table="coupon_whitelist"
:: needs Coupon, Offer
php artisan make:migration create_coupon_offer_pivot_table --table="coupon_offer"
:: needs Pause, Subscription
php artisan make:migration create_pause_subscription_pivot_table --table="pause_subscription"
:: needs Pause, Purchase
php artisan make:migration create_pause_purchase_pivot_table --table="pause_purchase"
:: needs User, Group
php artisan make:migration create_user_group_pivot_table --table="user_group"
