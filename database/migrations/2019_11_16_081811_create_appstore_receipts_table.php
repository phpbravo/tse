<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppstoreReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appstore_receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
                $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('transaction_id')->nullable();
                $table->foreign('transaction_id')
                    ->references('id')
                    ->on('transactions');
            $table->string('vendor_token')->nullable()->index();
            $table->string('original_transaction_token')->nullable()->index();
            $table->string('transaction_token')->nullable()->index();
            $table->binary('content');
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appstore_receipts');
    }
}
