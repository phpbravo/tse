<?php

namespace App\Http\Controllers\Webhooks;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

use App\Handlers\PaypalHandler;
use App\WebhookEvent;

class PaypalController extends Controller
{
    public function handle()
    {
        $payload = json_decode(@file_get_contents('php://input'));
        $payload->type=$payload->event_type;
        $url=request()->url();
    
        WebhookEvent::create([
            'gateway_id'=>2,
            'url'=>$url,
            'data'=>$payload
        ]);
    
        return response()->json([
            'message'=>'success',
            'event'=>$payload->event_type
        ],200);
    }

    public function handle_legacy()
    {
        $url = request()->url();
        $raw_payload = @file_get_contents('php://input');

        $IPN = new \PayPal\IPN\PPIPNMessage(
            $raw_payload,
            ['mode'=>config('paypal.nvp.mode')]
        );

        $payload = $IPN->getRawData();

        $charset = $payload['charset'] ?? 'utf-8';

        array_walk_recursive($payload,function(&$item, $key) use ($charset){
            $item = mb_convert_encoding($item,'utf-8',$charset);
        });

        if ($IPN->validate())
        {
            $webhook_data = (object)[
                'type' => 'legacy_'.$payload['txn_type'],
                'payload' => $payload
            ];

            WebhookEvent::create([
                'gateway_id'=>2,
                'url'=>$url,
                'data'=>$webhook_data
            ]);
        }
        else
        {
            error_log('invalid IPN');    
        }

        http_response_code(200);
        die;
    }
}
