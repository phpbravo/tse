<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
		'deleted' => Events\ResourceDeleted::class,
    ];
    
    protected $fillable=[
        'name',
        'title',
        'slug',
        'content',
        'is_public',
        'is_published',
    ];

    public function page_groups(){
        return $this->belongsToMany('App\PageGroup');
    }
}
