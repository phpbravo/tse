<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KeysController extends Controller
{

	public function getPaypalPublicKey(){

		return config('api.paypal_public_key');

	}

	public function getStripePublicKey(){

		return config('api.stripe_public_key');

	}

}
