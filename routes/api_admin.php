<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/login','AdminAuth\LoginController@apiLogin');

Route::group(['middleware'=>['admin.auth']],function() {

	Route::post('/logout','LoginController@logout');

	Route::get('/configs','AdminAuth\LoginController@getConfig');

	Route::patch('/configs','AdminAuth\LoginController@updateConfig');

	Route::get('/currentUser',function(){
		$response=auth('admin')->user();
		$response->object='current_user';
		return $response;
	});

	Route::get('/purchases','Admin\PurchasesController@index');
	Route::post('/purchases','Admin\PurchasesController@store');
	Route::get('/purchases/{id}','Admin\PurchasesController@show');
	
	Route::get('/subscriptions','Admin\SubscriptionsController@index');
	Route::get('/subscriptions/{id}','Admin\SubscriptionsController@show');
	Route::get('/subscription/edit/{id}','Admin\SubscriptionsController@edit');
	Route::post('/subscription','Admin\SubscriptionsController@store');
	Route::put('/subscription/{id}','Admin\SubscriptionsController@update');
	
	Route::get('/transactions','Admin\TransactionsController@index');
	Route::get('/transactions/{id}','Admin\TransactionsController@show');

	Route::get('/offers/coupons',function(){

	});
	
	Route::get('/videos','Admin\VideosController@index');
	Route::post('/videos','Admin\VideosController@store');
	Route::get('videos/autocomplete','Admin\VideosController@autocomplete');
	Route::get('/videos/{id}','Admin\VideosController@show');
	Route::patch('/videos/{id}','Admin\VideosController@update');
	Route::delete('/videos/{id}','Admin\VideosController@destroy');
	Route::post('/videos/fetchVimeoVideos','Admin\VideosController@fetchVimeoVideos');
	
	Route::get('/pages','Admin\PagesController@index');
	Route::post('/pages','Admin\PagesController@store');
	Route::get('/pages/{id}','Admin\PagesController@show');
	Route::patch('/pages/{id}','Admin\PagesController@update');
	Route::delete('/pages/{id}','Admin\PagesController@destroy');

	Route::get('/page_groups/list','Admin\PagesController@pageGroupList');

	Route::get('/blog_posts', 'Admin\BlogPostsController@index');
	Route::post('/blog_post', 'Admin\BlogPostsController@store');
	Route::put('/blog_post/{id}', 'Admin\BlogPostsController@update');
	Route::delete('/blog_post/{id}', 'Admin\BlogPostsController@delete');

	Route::get('/offers','Admin\OffersController@index');
	Route::post('/offers','Admin\OffersController@store');
	Route::get('/offers/availableForUser/{id}','Admin\OffersController@getOffersAvailableForUser');
	Route::get('/offers/{id}','Admin\OffersController@show');
	Route::patch('/offers/{id}','Admin\OffersController@update');

	Route::get('/products','Admin\ProductsController@index');
	Route::get('/products/list','Admin\ProductsController@list');
	Route::post('/products','Admin\ProductsController@store');
	Route::get('/products/{id}','Admin\ProductsController@show');
	Route::patch('/products/{id}','Admin\ProductsController@update');
	Route::delete('/products/{id}','Admin\ProductsController@destroy');

	Route::get('/resources','Admin\ResourcesController@index');

	Route::get('/plans','Admin\PlansController@index');
	Route::post('/plans','Admin\PlansController@store');
	Route::get('/plans/autocomplete','Admin\PlansController@autocomplete');
	Route::get('/plans/list','Admin\PlansController@list');
	Route::get('/plans/{id}','Admin\PlansController@show');
	Route::patch('/plans/{id}','Admin\PlansController@update');
	Route::delete('/plans/{id}','Admin\PlansController@destroy');

	Route::get('/programs','Admin\ProgramsController@index');
	Route::post('/programs','Admin\ProgramsController@store');
	Route::get('/programs/autocomplete','Admin\ProgramsController@autocomplete');
	Route::get('/programs/{id}','Admin\ProgramsController@show');
	Route::patch('/programs/{id}','Admin\ProgramsController@update');
	Route::delete('/programs/{id}','Admin\ProgramsController@destroy');

	Route::get('/sessions','Admin\SessionsController@index');
	Route::post('/sessions','Admin\SessionsController@store');
	Route::get('/sessions/autocomplete','Admin\SessionsController@autocomplete');
	Route::get('/sessions/{id}','Admin\SessionsController@show');
	Route::patch('/sessions/{id}','Admin\SessionsController@update');
	Route::delete('/sessions/{id}','Admin\SessionsController@destroy');

	Route::get('/exercises','Admin\ExercisesController@index');
	Route::post('/exercises','Admin\ExercisesController@store');
	Route::get('/exercises/autocomplete','Admin\ExercisesController@autocomplete');
	Route::get('/exercises/{id}','Admin\ExercisesController@show');
	Route::patch('/exercises/{id}','Admin\ExercisesController@update');
	Route::delete('/exercises/{id}','Admin\ExercisesController@destroy');
	Route::post('/exercises/{id}/tags/{name}','Admin\ExercisesController@tag');
	Route::delete('/exercises/{id}/tags/{name}','Admin\ExercisesController@untag');

	Route::get('/recipes','Admin\RecipesController@index');
	Route::post('/recipes','Admin\RecipesController@store');
	Route::get('/recipes/autocomplete','Admin\RecipesController@autocomplete');
	Route::get('/recipes/{id}','Admin\RecipesController@show');
	Route::patch('/recipes/{id}','Admin\RecipesController@update');
	Route::delete('/recipes/{id}','Admin\RecipesController@destroy');
	Route::post('/recipes/{id}/tags/{name}','Admin\RecipesController@tag');
	Route::delete('/recipes/{id}/tags/{name}','Admin\RecipesController@untag');

	Route::get('/categories','Admin\CategoriesController@index');
	Route::get('/categories/list','Admin\CategoriesController@list');
	Route::post('/categories','Admin\CategoriesController@store');
	Route::get('/categories/autocomplete','Admin\CategoriesController@autocomplete');
	Route::get('/categories/{id}','Admin\CategoriesController@show');
	Route::patch('/categories/{id}','Admin\CategoriesController@update');
	Route::delete('/categories/{id}','Admin\CategoriesController@destroy');

	Route::post('/subcategories/{id}/orderAhead','Admin\SubcategoriesController@orderAhead');
	Route::post('/subcategories/{id}/orderBehind','Admin\SubcategoriesController@orderBehind');
	
	Route::post('/subcategories','Admin\SubcategoriesController@store');
	Route::delete('/subcategories/{id}','Admin\SubcategoriesController@destroy');
	Route::put('/subcategories/{id}','Admin\SubcategoriesController@updateAtomic');

	Route::get('/users','Admin\UsersController@index');
	Route::post('/users','Auth\LoginController@registerAdmin');
	Route::get('/users/autocomplete','Admin\UsersController@autocomplete');
	Route::get('/users/{id}','Admin\UsersController@show');
	Route::post('/users/{id}','Auth\LoginController@editAdmin');

	Route::get('/tags/autocomplete','Common\TagsController@autocomplete');

	Route::post('/files','Admin\FilesController@upload');

	Route::get('/gateways/endpoints','Admin\GatewaysController@getEndpoints');
	Route::get('/gateways/webhooks','Admin\GatewaysController@getGatewayWebhooks');
	Route::get('/gateways/events','Admin\GatewaysController@getEvents');
	Route::post('/gateways/webhooks','Admin\GatewaysController@createWebhook');
	Route::delete('/gateways/webhooks','Admin\GatewaysController@deleteWebhook');

	Route::get('/dashboard/user_stats','Admin\DashboardController@getUserStats');
	Route::get('/dashboard/purchase_stats','Admin\DashboardController@getPurchaseStats');
	Route::get('/dashboard/subscription_stats','Admin\DashboardController@getSubscriptionStats');
	Route::get('/dashboard/transaction_stats','Admin\DashboardController@getTransactionStats');
	Route::get('/dashboard/old_api_users','Admin\DashboardController@getOldApiUsageStats');

});
Route::get('{?url}',function(){
	abort(404);
})->where(['url'=>'.*']);