var we_eat_recipes_app= new Vue({
	el:'#recettesfit',
	data:{
		loading:true,
		subcategories:[],
		recipes:[],
		selected_category:0,
		selected_page:1,
		last_page:1,
		search:'',
	},
	methods:{
		changeCategory(category_id){
			this.selected_category=category_id;
			this.getRecipes()
		},
		changePage(page){
			if (page<1)
				page=1
			if (page>this.last_page)
				page=this.last_page
			this.selected_page=page
			this.getRecipes()
			this.$nextTick(()=>{
				this.selected_page=99;
				this.$nextTick(()=>{
					console.log(this.selected_page)
					this.selected_page=page
				})
			})
		},
		getRecipes(){
			filters=[];
			if (this.selected_category)
				filters.push({field:'recipe_category_id','value':this.selected_category})
			this.loading=true
			return axios.get('/api/we_eat',{params:{
				filters,
				search:this.search,
				page:this.selected_page
			}})
			.then(res=>{
				this.recipes=res.data.contents
				this.selected_page=res.data.current_page
				this.last_page=res.data.last_page
			})
			.finally(()=>{
				this.loading=false;
			})
		}
	},
	created(){
		this.getRecipes()
	}
})