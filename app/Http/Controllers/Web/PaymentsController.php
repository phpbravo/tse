<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Handlers\{
	StripeHandler,
	PaypalHandler,
	ObjectHandler,
	BailHandler,
	PaymentValidationHandler
};

use App\{
	User,
	Offer,
	Subscription,
	Transaction
};

class PaymentsController extends Controller
{
	/**
		* @var StripeHandler
	 */
	private $stripe_handler;

	/**
		* @var PaypalHandler
	 */
	private $paypal_handler;

	/**
		* @var BailHandler
	 */
	private $bail_handler;

	/**
		* @var PaymentValidationHandler
	 */
	private $payment_validation_handler;

	public function __construct()
	{
		$this->stripe_handler = new StripeHandler;
		$this->paypal_handler = new PaypalHandler;
		$this->bail_handler = new BailHandler;
		$this->payment_validation_handler = new PaymentValidationHandler;
	}

	public function checkIfUserCanAvailOffer(User $user, Offer $offer)
	{
		return (
			$offer->is_available
			// more checks
		);
	}

	public function checkIfUserShouldUpgrade(User $user, Offer $offer)
	{
		return $user->hasPlanById($offer->plan_id);
	}

	public function handlePurchase(
		User $user,
		Offer $offer,
		?string $token,
		string $gateway
	){
		$is_upgrade=$this->checkIfUserShouldUpgrade($user, $offer);

		if ($is_upgrade)
		{
			$current_purchase = $user->getCurrentPurchaseOfOffer($offer);
			$current_subscription = $user->getCurrentSubscriptionToPlan(
				$offer->plan
			);

			if ($offer->is_for_upgrade)
			{
				return $gateway == 'stripe' ?
					$this->stripe_handler->upgradePurchase(
						$user,
						$token,
						$offer,
						$current_purchase,
						$current_subscription
					) : // paypal
					$this->paypal_handler->upgradePurchase(
						$user,
						$offer,
						$current_purchase,
						$current_subscription
					);
			}
			else
				$this->bail_handler->bailOfferNotForUpgrade();
		}
		else
		{
			return $gateway == 'stripe' ?
				$this->stripe_handler->createPurchase(
					$user,
					$token,
					$offer
				) : // paypal
				$this->paypal_handler->createPurchase(
					$user,
					$offer
				);
		}
	}

	public function handleSubscription(
		User $user,
		Offer $offer,
		?string $token,
		string $gateway
	){
		$is_upgrade=$this->checkIfUserShouldUpgrade($user, $offer);

		if ($is_upgrade)
		{
			$current_purchase = $user->getCurrentPurchaseOfOffer($offer);
			$current_subscription = $user->getCurrentSubscriptionToPlan(
				$offer->plan
			);

			if ($offer->is_for_upgrade)
			{
				return $gateway == 'stripe' ?
					$this->stripe_handler->upgradeSubscription(
						$user,
						$token,
						$offer,
						$current_purchase,
						$current_subscription
					) : // paypal
					$this->paypal_handler->upgradeSubscription(
						$user,
						$offer,
						$current_purchase,
						$current_subscription
					);
			}
			else
				$this->bail_handler->bailOfferNotForUpgrade();
		}
		else
		{
			return $gateway == 'stripe' ?
				$this->stripe_handler->createSubscription(
					$user,
					$token,
					$offer
				) : // paypal
				$this->paypal_handler->createSubscription(
					$user,
					$offer
				);
		}
	}

	public function handlePayment()
	{
		$user = auth()->user();
		$offer_id = request()->get('offer_id');
		$offer=Offer::findOrFail($offer_id);
		$gateway = request()->get('gateway');
		$token=request()->get('token');

		$this->payment_validation_handler->validatePaymentRequest(
			$user,
			$offer,
			$gateway
		);

		if ($offer->is_recurring)
		{
			return $this->handleSubscription($user, $offer, $token, $gateway);
		}
		else
		{
			return $this->handlePurchase($user, $offer, $token, $gateway);
		}
	}

	public function confirmStripeSubscription(
		User $user,
		string $token
	){
		$subscription = Subscription::getFromToken($token);

		return $this->stripe_handler->confirmSubscription(
			$user,
			$subscription
		);
	}

	public function confirmPaypalSubscription(
		User $user,
		string $token
	){
		$subscription = Subscription::getFromToken($token);

		return $this->paypal_handler->confirmSubscription(
			$user,
			$subscription
		);
	}

	public function confirmStripePurchase(
		User $user,
		string $token
	){
		$transaction = Transaction::getFromToken($token);

		return $this->stripe_handler->confirmPurchase($user, $transaction);
	}

	public function confirmPaypalPurchase(
		User $user,
		string $token
	){
		return $this->paypal_handler->confirmPurchase($user, $token);
	}

	public function confirmPayment()
	{
		$user = auth()->user();
		$payment_type = request()->get('payment_type');
		$token = request()->get('token');
		$gateway = request()->get('gateway');

		$this->payment_validation_handler->validatePaymentConfirmationRequest(
			$payment_type,
			$token,
			$gateway
		);

		if ($gateway == 'stripe' && $payment_type == 'subscription')
			return $this->confirmStripeSubscription(
				$user,
				$token
			);
		elseif ($gateway == 'stripe' && $payment_type == 'purchase')
			return $this->confirmStripePurchase(
				$user,
				$token
			);
		elseif ($gateway == 'paypal' && $payment_type == 'subscription')
			return $this->confirmPaypalSubscription(
				$user,
				$token
			);
		elseif ($gateway == 'paypal' && $payment_type == 'purchase')
			return $this->confirmPaypalPurchase(
				$user,
				$token
			);
	}

	public function cancelSubscription(string $subscription_id)
	{
		$subscription = Subscription::findOrFail($subscription_id);

		$gateway_id = $subscription->gateway_id;
		$gateway = $gateway_id == 1 ? 'stripe' : 'paypal';

		switch($gateway)
		{
			case 'stripe':
				return $this->stripe_handler->cancelSubscription($subscription);
				break;
			case 'paypal':
				return $this->paypal_handler->cancelSubscription($subscription);
		}
	}
}
