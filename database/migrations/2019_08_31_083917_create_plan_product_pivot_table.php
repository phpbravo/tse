<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanProductPivotTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_product', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('plan_id');
				$table->foreign('plan_id')->references('id')->on('plans');
			$table->unsignedBigInteger('product_id');
				$table->foreign('product_id')->references('id')->on('products');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('plan_product', function (Blueprint $table) {
			//
		});
	}
}
