<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Admin</title>

        <link rel="stylesheet" href="{{ mix('administrator/app.css', config('app.manifest_path')) }}">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    </head>
    <body>
        
        <div class="container-fluid py-5">

            <div class="jumbotron col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 my-5">
                
                <h1>@lang('admin.login')</h1>

                <form action="/admin/login" method="POST" autocomplete="on">
                    @csrf
                    <div class="form-group">
                        <label for="username">@lang('validation.attributes.username')</label>
                        <input
                            type="text"
                            class="form-control"
                            name="username"
                            placeholder="@lang('validation.attributes.username')"
                        >
                    </div>
                    <div class="form-group">
                        <label for="password">@lang('validation.attributes.password')</label>
                        <input
                            type="password"
                            class="form-control"
                            name="password"
                            placeholder="@lang('validation.attributes.username')"
                            autocomplete="off"
                        >
                    </div>
                    <button class="float-right btn btn-primary">@lang('admin.login_button')</button>
                </form>

            </div>

        </div>

    </body>
</html>
