@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	<div class="registration-wrapper">
		<div class="container">
			<div class="registration-inner-wrapper">
					
				@if(count($offers))
				<h2>Select Plan</h2>
				<div class="row offers-tables-content">
				
								@foreach ($offers as $offer)
								<div class="col-lg-4 col-md-6 offers-column">
									<div class="offers-wrapper">
										<div class="offers-inner-wrapper pb-4">
										<div class="subscribe-button float-right">
											<a href="{{auth('web')->check()?route('front.payment',['id'=>$offer['id']]):route('front.register').'?next='.$offer['id']}}" class="custom-button">
												<span data-hover="S'inscrire=">
												S'inscrire 
												</span> 
											</a> 
										</div> 
										<div class="clearfix"></div>
											<div class="offers-header"> 
												<img src="/optimized/img/formule-black.png" alt="formule-black" class="hover"> 
												<h2>{{$offer['title']}}</h2> 
												<div class="offers-content"> 
													<span class="value">{{bcdiv($offer['price'],$offer['duration'],2)}}</span> 
													<span class="currency">€</span> 
													<span class="duration"> / mois</span> 
												</div> 
												<p>Facturé {{$offer['price']}}€{{$offer['is_recurring']?'/'.((int)$offer['duration']>1?(int)$offer['duration'].' mois':'moi'):' à l inscription'}}</p> 
											</div> 
											<div class="offers-body"> 
												<ul class="offers-features"> 
													<li class="boldEngagement"> 
														<strong>Sans aucun engagement</strong></li> 
													<strong class="plan-offers"> 
				
														@foreach ($offer['plan']['products'] as $product)
															<li> {{$product['description']}} </li>
														@endforeach
				
														<li>Et à toutes les prochaines surprises !</li> 
													</strong> 
												</ul> 
											</div> 
											<div class="offer-plan-bottom my-2"> 
												<div class="pictos-paiment"> 
				
													@if ($offer['is_paypal_available'])
													<img src="/optimized/img/paypal.svg" alt="paypal">
													@endif
				
													@if ($offer['is_stripe_available'])
													<img src="/optimized/img/stripe.svg" alt="cb">
													@endif
				
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
								@endforeach

				@endif
			</div>
		</div>
	</div>
@endsection
@section('footer')

</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LfMnMAUAAAAAOxx791zduXjx8UBh2cb_ZaZcnon', {action: 'homepage'}).then(function(token) {
    	// console.log(token);
    });
});
</script>
@endsection