<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler, FileHandler};

use App\{Page, PageGroup};

class PagesController extends Controller
{

	private function validatePage($fields,$editing=null){
		$objectHandler=new ObjectHandler(Page::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if (!isset($fields->title) || $fields->title==''){
			return $objectHandler->newError(
				'invalid_request',
				'Titre est obligatoire'
			);
		}

		if (!isset($fields->slug) || $fields->slug==''){
			return $objectHandler->newError(
				'invalid_request',
				'Slug est obligatoire'
			);
		}

		if (
			($editing && Page::where('slug',$fields->slug)->where('id','!=',$editing->id)->count()) ||
			(!$editing && Page::where('slug',$fields->slug)->count())
		){
			return $objectHandler->newError(
				'invalid_request',
				'Que slug est déjà pris'
			);
		}

		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Page::class);
		$pageHandler=new PageHandler(Page::class);
		$objectHandler=new ObjectHandler(Page::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('page_groups');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validatePage($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$page=Page::create([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'slug'=>$fields->slug,
			'content'=>$fields->content,
			'is_public'=>$fields->is_public??false,
			'is_published'=>$fields->is_published??false,
		]);

		if (isset($fields->page_groups)){

			$fields->page_groups=(array)json_decode(json_encode($fields->page_groups));

			$new_page_groups=array_filter(
				$fields->page_groups,
				function($page_group){
					return !isset($page_group->id);
				}
			);

			$page_group_ids=array_map(
				function($page_group){
					return $page_group->id;
				},
				array_filter(
					$fields->page_groups,
					function($page_group){
						return isset($page_group->id);
					}
				)
			);

			foreach ($new_page_groups as $new_page_group) {
				$page_group_ids[]=PageGroup::create([
					'name'=>$new_page_group->name
				])->id;
			}

			$page->page_groups()->attach($page_group_ids);
		}

		return $page;
	}

	public function update($id){

		$page=Page::find($id);

		$fields=(object)request()->all();

		$validation=$this->validatePage($fields,$page);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}


		if(!$page){
			return (new ObjectHandler(Page::class))->newError('not_found');
		}

		$page->update([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'slug'=>$fields->slug,
			'content'=>$fields->content,
			'is_public'=>$fields->is_public,
			'is_published'=>$fields->is_published,
		]);

		$page->page_groups()->sync([]);

		if (isset($fields->page_groups)){

			$fields->page_groups=(array)json_decode(json_encode($fields->page_groups));

			$new_page_groups=array_filter(
				$fields->page_groups,
				function($page_group){
					return !isset($page_group->id);
				}
			);

			$page_group_ids=array_map(
				function($page_group){
					return $page_group->id;
				},
				array_filter(
					$fields->page_groups,
					function($page_group){
						return isset($page_group->id);
					}
				)
			);

			foreach ($new_page_groups as $new_page_group) {
				$page_group_ids[]=PageGroup::create([
					'name'=>$new_page_group->name
				])->id;
			}

			$page->page_groups()->attach($page_group_ids);
		}

		return $page;
	}

	public function show($id){

		$page=Page::with(['sections','tags'])->find($id);

		if(!$page){
			return (new ObjectHandler(Page::class))->newError('not_found');
		}

		return $page;
	}

	public function destroy($id){

		$page=Page::find($id);

		if(!$page){
			return (new ObjectHandler(Page::class))->newError('not_found');
		}

		$page->delete();

		return (new ObjectHandler(Page::class))->newSuccess();
	}

	public function tag($id,$name){

		$page=Page::findOrFail($id);

		$page->tag($name);

		return $page;

	}

	public function untag($id,$name){

		$page=Page::findOrFail($id);

		$page->untag($name);

		return $page;

	}
	
	public function pageGroupList(){

		return PageGroup::all();

	}

}
