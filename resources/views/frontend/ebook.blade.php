@extends('frontend.includes.layout')
@section('header')

<link rel="stylesheet" type="text/css" href="/assets/css/lightslider.css">

@endsection()
@section('content')
@include('frontend.includes.navbar')

	<section class="ebook-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 verticalslides">
					   <div class="demo">
					   		<div class="item">            
					            <div class="clearfix">
					                <ul id="vertical">
					                    <li data-thumb="/assets/img/cover.jpg"> 
					                        <img src="/assets/img/cover.jpg" alt="Alt 1" id="cover" class="img-fluid">
					                     </li>
					                    <li data-thumb="/assets/img/brochettes-au-sesame-et-citron-vert.jpg"> 
					                        <img src="/assets/img/brochettes-au-sesame-et-citron-vert.jpg" alt="Alt 2" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/burger.jpg"> 
					                       <img src="/assets/img/burger.jpg" alt="Alt 3" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/pancakes-express.jpg"> 
					                        <img src="/assets/img/pancakes-express.jpg" alt="Alt 4" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/sissytella.jpg"> 
					                       <img src="/assets/img/sissytella.jpg" alt="Alt 5" class="img-fluid slidemeimg">
					                    </li>
					                    <li data-thumb="assets/img/smoothie-detox.jpg"> 
					                       <img src="/assets/img/smoothie-detox.jpg" alt="Alt 6" class="img-fluid slidemeimg">
					                    </li>
					                    <li data-thumb="/assets/img/tofu-au-lait-de-coco-et-petits-legumes.jpg"> 
					                        <img src="/assets/img/tofu-au-lait-de-coco-et-petits-legumes.jpg" alt="Alt 7" class="img-fluid slidemeimg">
					                    </li>
					                </ul>
					            </div>
					        </div>
					   </div>



				</div>

				<div class="col-md-12 horizontalslides">
					<ul id="adaptive">
						<li data-thumb="/assets/img/cover.jpg"> 
					                        <img src="/assets/img/cover.jpg" alt="Alt 1" id="cover2" class="img-fluid">
					                     </li>
					                    <li data-thumb="/assets/img/brochettes-au-sesame-et-citron-vert.jpg"> 
					                        <img src="/assets/img/brochettes-au-sesame-et-citron-vert.jpg" alt="Alt 2" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/burger.jpg"> 
					                       <img src="/assets/img/burger.jpg" alt="Alt 3" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/pancakes-express.jpg"> 
					                        <img src="/assets/img/pancakes-express.jpg" alt="Alt 4" class="img-fluid slidemeimg">
					                     </li>
					                    <li data-thumb="/assets/img/sissytella.jpg"> 
					                       <img src="/assets/img/sissytella.jpg" alt="Alt 5" class="img-fluid slidemeimg">
					                    </li>
					                    <li data-thumb="assets/img/smoothie-detox.jpg"> 
					                       <img src="/assets/img/smoothie-detox.jpg" alt="Alt 6" class="img-fluid slidemeimg">
					                    </li>
					                    <li data-thumb="/assets/img/tofu-au-lait-de-coco-et-petits-legumes.jpg"> 
					                        <img src="/assets/img/tofu-au-lait-de-coco-et-petits-legumes.jpg" alt="Alt 7" class="img-fluid slidemeimg">
					                    </li>
					</ul>
				</div>
				

				<div class="col-lg-4 mt-5" id="fitness-recipe">
					
						<h3 class="ebook-heading">Nos Véritables Recettes Fitness (EBook)</h3>
						<p class="para font-weight-bold">Vous n’êtes pas abonné au programme mais souhaitez accéder aux recettes ? Vous préférez une version PDF de vos recettes favorites ? Cet ebook est fait pour vous !
						</p>

						<p class="para">
							Vous aviez été nombreux à nous le réclamer, nous l’avons fait ! L’eBook regroupant toutes mes meilleures recettes We Eat est enfin là !
						</p>

						<p class="para">
							Retrouvez dans cet ebook mes <storng>30 recettes fitness spéciales summer</storng>, pour enfin atteindre vos objectifs tout en vous faisant plaisir !
						</p>
						@if (auth()->user() && auth()->user()->hasProductById(\App\Product::EBOOK_PRODUCT))
							<a download = "Nos Véritables Recettes Fitness.pdf" href="{{route('front.download-ebook',['id'=>'1','version'=>'desktop'])}}" class="btn btn-primary btn-block">TÉLÉCHARGER</a>
							<a download = "Nos Véritables Recettes Fitness (mobile).pdf" href="{{route('front.download-ebook',['id'=>'1','version'=>'mobile'])}}" class="btn btn-primary btn-block">TÉLÉCHARGER MOBILE</a>
						@else
							<h4 class="font-weight-bold" id="price"> PRIX: 9,95€</h4>
							<a href="{{auth('web')->check()?route('front.payment',['id'=>'10']):route('front.register').'?next='.'10'}}" class="btn btn-primary btn-block">ACHETER</a>
						@endif
				</div>
			
				<div class="col-lg-12" id="receipt-div">
					<h3 class="receipt-head">Les Recettes</h3>
					<p>
						Chaque recette est gourmande, tout en respectant un équilibre nutritionnel (nous vous indiquons tous les macro-nutriments) qui vous permettra d’atteindre vos objectifs plus vite que vous ne le pensez. La plupart des recettes sont protéinées, pour vous aider à obtenir votre quantité de protéines journalières en limitant la prise de compléments alimentaires. Elles sont aussi riches en vitamines et minéraux, et composées de bonnes calories ! Manger mieux, pas moins, est la clef de la réussite ! Les recettes sont toujours faciles et rapides à faire, pour que cuisiner ne soit pas une corvée, même quand on rentre tard de la salle !
					</p>

					<h3 class="receipt-head">Les Ingrédients</h3>

					<p>
						Les ingrédients sont peu nombreux, se trouvent facilement dans les commerces de proximité bio ou non et sont peu onéreux. Il est souvent possible de remplacer plusieurs d'entre eux en fonction de ce que vous avez sous le coude. L'objectif ? Vous fa-ci-li-ter la vie, pas vous la compliquer ! Alors n'hésitez pas à vous approporier nos recettes, pour créer les vôtres !
					</p>

					<h3 class="receipt-head">Notre Nutrition</h3>

					<p>
						Toutes les recettes ont été crées, réalisées, rédigées et photographiées par mes soins. Je les réalise au quotidien chez nous afin d’obtenir le meilleur des résultats possible ! Obtenir cet ebook, c’est l’assurance d’accéder à une véritable nutrition sportive, efficace et plaisir. Pas de recettes de magasine, ni de régime. Seulement nos véritables recettes.
					</p>
				</div>
			</div>
		</div>
	</section>

@endsection()
@section('footer')

<script type="text/javascript" src="/assets/js/lightslider.js"></script>

 <script type="text/javascript">
    	 $(document).ready(function() {
            $('#vertical').lightSlider({
                gallery:true,
                item:4,
                vertical:true,
                verticalHeight:100,
                vThumbWidth:50,
                adaptiveHeight:true,
                thumbItem:3,
                thumbMargin:4,
                mode:'bounce',
                slideMargin:0,
                loop:true,
                controls:false,
                vThumbWidth: 100,
                auto: true,
                speed: 500,
            });  
        });


    	$(document).ready(function() {
    		$('#adaptive').lightSlider({
		        adaptiveHeight:true,
		        item:1,
		        slideMargin:0,
		        loop:true,
		        auto: true,
                speed: 500, 
                 mode:'bounce'      
    });
});
    </script>
@endsection