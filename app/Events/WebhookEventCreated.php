<?php

namespace App\Events;

use App\Gateway;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Jobs\HandleLegacyPaypalEvent;
use App\Jobs\HandleLegacyPlaystoreEvent;
use App\Jobs\HandleLegacyAppstoreEvent;
use App\Jobs\HandleStripeEvent;
use App\Jobs\HandlePaypalEvent;

use App\WebhookEvent;

class WebhookEventCreated
{
	use Dispatchable, SerializesModels;

	/** @var WebhookEvent */
	public $webhook_event;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(WebhookEvent $webhook_event)
	{
		$gateway_id = $webhook_event->gateway_id;
		$event_type = $webhook_event->data['event_type'];

		if (substr($event_type,0,6) == 'legacy')
		{
			switch($gateway_id)
			{
				case Gateway::PAYPAL:
					dispatch(new HandleLegacyPaypalEvent($webhook_event));
					break;
				
				case Gateway::PLAYSTORE:
					dispatch(new HandleLegacyPlaystoreEvent($webhook_event));
					break;
				
				case Gateway::APPSTORE:
					dispatch(new HandleLegacyAppstoreEvent($webhook_event));
					break;

				default:
					break;
			}
		}
		else
		{
			switch($gateway_id)
			{
				case Gateway::STRIPE:
					dispatch(new HandleStripeEvent($webhook_event));
					break;

				case Gateway::PAYPAL:
					dispatch(new HandlePaypalEvent($webhook_event));
					break;

				default:
					break;
			}
		}
	}
}
