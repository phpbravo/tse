<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;

/**
 * Class PricingScheme
 *
 * The active pricing scheme for a billing cycle.
 *
 * @package PayPal\Api2
	* 
 *	@property int version
 *	@property \PayPal\Api2\Money fixed_price
 *	@property string create_time
 *	@property string update_time
	* 
 */
class PricingScheme extends PayPalModel
{
	use Timestamps;

	/**
		* The version of the pricing scheme.
		*/
	public function setVersion(int $given_name): self
	{
		$this->given_name = $given_name;
		return $this;
	}

	/**
		* The version of the pricing scheme.
		*/
	public function getVersion(): int
	{
		return $this->version;
	}
	
 /**
		* The fixed amount to charge for the subscription.
		*
		* For regular pricing, it is limited to a 20% increase from the
		* current amount and the change is applicable for both existing and
		* future subscriptions. For trial period pricing, there is no limit or
		* constraint in changing the amount and the change is applicable only 
		* on future subscriptions.
		* 
	 */
	public function setFixedPrice(Money $fixed_price): self
	{
		$this->fixed_price = $fixed_price;
		return $this;
	}

 /**
		* The fixed amount to charge for the subscription.
		*
		* For regular pricing, it is limited to a 20% increase from the
		* current amount and the change is applicable for both existing and
		* future subscriptions. For trial period pricing, there is no limit or
		* constraint in changing the amount and the change is applicable only 
		* on future subscriptions.
		* 
	 */
	public function getFixedPrice(): Money
	{
		return $this->fixed_price;
	}
}
