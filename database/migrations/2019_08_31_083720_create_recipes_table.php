<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipes', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('resource_id')->nullable();
				$table->foreign('resource_id')->references('id')->on('resources');
			$table->unsignedBigInteger('recipe_category_id');
				$table->foreign('recipe_category_id')->references('id')->on('recipe_categories');
			$table->string('name')->index();
			$table->string('description')->nullable();
			$table->string('preparation_time')->nullable();
			$table->string('cook_time')->nullable();
			$table->timestamps();
			$table->index('created_at');
			$table->index('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recipes');
	}
}
