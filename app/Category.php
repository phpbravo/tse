<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $dispatchesEvents=[
        'created' => Events\ResourceCreated::class,
    ];

    protected $fillable=[
        'name',
        'title',
        'created_at',
        'updated_at'
    ];

    protected $casts=[
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }
    
}
