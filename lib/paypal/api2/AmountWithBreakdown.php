<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

/**
 * Class AmountWithBreakdown
 *
 * Billing frequency details for a billing cycle
 *
 * @package PayPal\Api2
	* 
 *	@property \PayPal\Api2\Money $gross_amount
 *	@property \PayPal\Api2\Money $fee_amount
 *	@property \PayPal\Api2\Money $shipping_amount
 *	@property \PayPal\Api2\Money $tax_amount
 *	@property \PayPal\Api2\Money $net_amount
	* 
 */
class AmountWithBreakdown extends PayPalModel
{
	/**
		* The amount for this transaction.
		*/
	public function setGrossAmount(Money $gross_amount): self
	{
		$this->gross_amount = $gross_amount;
		return $this;
	}
	
	/**
		* The amount for this transaction.
		*
		* @return \PayPal\Api2\Money
		*/
	public function getGrossAmount(): Money
	{
		return $this->gross_amount;
	}

	/**
		* The fee details for the transaction.
		*/
	public function setFeeAmount(Money $fee_amount): self
	{
		$this->fee_amount = $fee_amount;
		return $this;
	}
	
	/**
		* The fee details for the transaction.
		*
		* @return \PayPal\Api2\Money
		*/
	public function getFeeAmount(): Money
	{
		return $this->fee_amount;
	}

	/**
		* The shipping amount for the transaction.
		*/
	public function setShippingAmount(Money $shipping_amount): self
	{
		$this->shipping_amount = $shipping_amount;
		return $this;
	}
	
	/**
		* The shipping amount for the transaction.
		*
		* @return \PayPal\Api2\Money
		*/
	public function getShippingAmount(): Money
	{
		return $this->shipping_amount;
	}

	/**
		* The tax amount for the transaction.
		*/
	public function setTaxAmount(Money $tax_amount): self
	{
		$this->tax_amount = $tax_amount;
		return $this;
	}
	
	/**
		* The tax amount for the transaction.
		*
		* @return \PayPal\Api2\Money
		*/
	public function getTaxAmount(): Money
	{
		return $this->tax_amount;
	}

	/**
		* The net amount that the payee receives for this transaction in their
		* PayPal account. The net amount is computed as gross_amount minus the
		* paypal_fee.
		*/
	public function setNetAmount(Money $net_amount): self
	{
		$this->net_amount = $net_amount;
		return $this;
	}
	
	/**
		* The net amount that the payee receives for this transaction in their
		* PayPal account. The net amount is computed as gross_amount minus the
		* paypal_fee.
		*
		* @return \PayPal\Api2\Money
		*/
	public function getNetAmount(): Money
	{
		return $this->net_amount;
	}
}
