const vx_actions={
    getCurrentUser(context,{email,password}){
        axios.get('/admin/api/current_user',{email,password}).then(
            response=>{
                let user=response.data;
                context.commit('login',user);
            }
        ).catch(
            response=>{
                console.error(response);
            }
        );
    }
}

export default vx_actions;