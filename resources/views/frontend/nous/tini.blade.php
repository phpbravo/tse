@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="concept-section tini" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section tini-section">
		<div class="container">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">TINI</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="sissy-inner">
						<h3 class="title-about">
							<strong>TINI</strong>
						</h3>
						<p>
							Hello les fitgens !<br>
							Je crois que c’est ici que je dois me présenter, même si je vais faire plus simple que Sissy !<br>
							Pour ceux d’entre vous qui ne me connaissent pas encore, je suis Tini, originaire de Tahiti.<br>
							Je baigne dans le sport depuis mon enfance puisque mon père a toujours été un grand sportif. Je ne le remercierai d’ailleurs jamais assez de m’avoir inculqué les valeurs du sport au quotidien. D’ailleurs, j’ai à peu près testé depuis mes 6 ans tous les sports individuels et collectifs avant d’en arriver au fitness.<br>
							Ma passion pour le fitness démarre lors d’une soirée étudiante pendant laquelle mes amis avaient décidé de lancer un concours de bras de fer. Etant le plus chétif de la bande avec mes 68 kg pour 1m80, je vous laisse imaginer mon classement. Ne me laissant pas abattre pour autant, j’ai demandé une revanche 1 mois plus tard. Je m’embarquais alors dans un régime de pompes quotidiennes pensant que j’allais développer la force nécessaire pour remporter mon pari. Vous vous doutez surement que 30 jours plus tard j’ai évidemment à nouveau essuyé la défaite. Déterminé, j’ai donc décidé d’intégrer aussi des tractions. Me voilà donc embarqué sur un programme pompes/tractions tous les jours.<br>
							Quelques semaines plus tard, me voila arrivé en Floride pour un semestre d’étude à l’étranger. Ma résidence proposant une salle de musculation à l’américaine, je pouvais ainsi continuer ma routine pompes/tractions. C’est ainsi que à 21 ans que je poussais pour la première fois les portes d’une salle de fitness.<br>
							Je me suis rapidement rendu compte que les hommes costaux de la salle ne faisaient pas que des pompes et tractions et c’est là que mon voyage dans l’odyssée du fit a commencé.<br>
							Je n’ai jamais retenté un bras de fer avec mon ami de l’époque mais ce n’est pas grave, car j’ai gagné bien plus encore…<br>
							10 ans plus tard, je me retrouve à écrire ces lignes pour vous.<br>
							Je suis passé à peu près par toutes les étapes du fitness et suis tombé dans tous les pièges aussi. J’ai beaucoup appris de mes erreurs. J’ai emmagasiné des heures de lectures de la presse spécialisée et de visionnages de vidéos Youtube, à la recherche du saint graal.<br>
							Au fil des années j’ai énormément appris et réalisé de nombreux tests sur moi pour mieux connaître mon corps et ce qui marche le mieux pour lui. Pour moi le fitness est devenu parti intégrante de ma vie et me fait autant de bien physiquement que mentalement.<br>
							Il s’agit d’un mode de vie et non d’une finalité, car en réalité la première clé du succès reste la pratique sur le long terme, la régularité et la progression, que l’on ne peut acquérir qu’en étant passionné.
						</p>

						

						<p>
							Je me rends aussi compte que beaucoup d’entre vous passent pour la première fois les portes d’une salle et se retrouvent, comme je l’ai été à mes début, perdu et hagards face à ces machines et ces poids, ne sachant pas trop quoi faire. C’est là que nous intervenons. Nous vous proposons de ne plus vous poser de questions et de gagner 10 ans de temps dans la pratique de ce sport. Joignez-vous tout simplement à nous quotidiennement et faites nos séances avec nous. Elles sont complètes, intenses et fun à la fois. Elles respectent les principes fondamentaux comme la fréquence d’entrainement, la progression, la congestion et intègrent la notion de variété pour vous permettre de progresser sans pour autant vous ennuyer.
							<br>
							Vous ne serez plus seul dans une salle, mais vous serez avec nous. Elle est pas belle la vie :)
						</p>
					</div>
				</div>
				<div class="col-lg-6 sissy-img-hide">
					<div class="sissy-rightimg">
						<img src="/assets/img/Tini.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section class="passion-section tini-notre">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
								
							</div>

							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE RENCONTRE</h1>
								<h3>TINI ET MOI</h3>
								<p class="notrep">
									La première fois que Tini et moi nous sommes rencontrés, c’était bel et bien dans une salle de sport, pendant les vacances ! Très cliché, mais pourtant vrai. Manque de pot pour moi, je finissais ma séance toute transpirée lorsqu’ il est arrivé. Je ne me suis, comme vous vous en doutez, pas attardée ! En revanche, nous nous sommes rapidement revus en France dès notre retour. Nous avions énormément de points communs et plein de choses à nous raconter !
								</p>

								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	
	

	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>TRAIN </span> SWEAT EAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="WE TRAIN">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>WE TRAIN</h2>
									<p>
										Quotidiennement, nous partageons avec vous nos <strong>entrainements</strong> en salle de musculation ! Ensemble, <strong>progressons chaque jour</strong> et restons <strong>surmotivés</strong> !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-heart" data-headline="WE SWEAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-heart">
									<h2>WE SWEAT</h2>
									<p>
										Retrouvez chaque mois en<strong> vidéo live</strong>, mes <strong>entrainements</strong> poids du corps favoris pour <strong>brûler un max de calories</strong> et <strong>sculpter son corps</strong> ! Transpirons ensembles dans votre salon !
									</p>
									<br>
									<p><a href{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-utensils" data-headline="WE EAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-utensils">
									<h2>WE EAT</h2>
									<p>
										Chaque mois, cuisinons ensembles de<strong> nouvelles recettes</strong> aussi fit que gourmandes ! Rapides et simples à réaliser, vous n’aurez plus d’excuses !
									</p>
									<br>
									<p><a href="{{ route('front.we-eat.recipes') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@include('frontend.includes.parallax-last')	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection