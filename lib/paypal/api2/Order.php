<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalResourceModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;

use PayPal\Traits\Timestamps;


/**
 * Class Order
 *
 * A resource representing an Order.
 *
 * @package PayPal\Api2
 *
 * @property string id
 * @property string intent
 * @property \PayPal\Api2\ApplicationContext application_context
 * @property string create_time
 * @property string update_time
 */
class Order extends PayPalResourceModel
{
    use Timestamps;

    /**
     * Identifier of the order.
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Identifier of the order.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Intent for this order.
     */
    public function setIntent(string $intent): self
    {
        $this->intent = $intent;
        return $this;
    }

    /**
     * Intent for this order.
     */
    public function getIntent(): string
    {
        return $this->intent;
    }

    /**
     * Get Approval Link
     */
    public function getApprovalLink(): ?string
    {
        return $this->getLink('approve');
    }

    public function create(
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        $payLoad = $this->toJSON();
        $json = self::executeCall(
            "/v2/checkout/orders",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    public function capture(
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        $payLoad = $this->toJSON();
        $json = self::executeCall(
            "/v2/checkout/orders/{$this->id}/capture",
            "POST",
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $this->fromJson($json);
        return $this;
    }

    public static function get(
        string $order_id,
        ApiContext $apiContext,
        ?PayPalRestCall $restCall = null
    ): self {
        ArgumentValidator::validate($order_id, 'order_id');
        $payLoad = "";
        $json = self::executeCall(
            "/v2/checkout/orders/$order_id",
            "GET",
            $payLoad,
            ['Prefer: return=representation'],
            $apiContext,
            $restCall
        );
        $ret = new Order();
        $ret->fromJson($json);
        return $ret;
    }
}
