@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
		<section class="same-wrapper meetingtoday" id="top">
			<div class="container">
				<div class="row pb-5">
					
						<div class="col-lg-12">
							<h1 class="pages-heading">TRAINING DU JOUR</h1>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="inner-samecontent inner-bonussession">
								<h3 class="inner-contentheader">LE <strong>TRAINING DU JOUR</strong></h3>
								
								<p>
									Nous te partageons ici chaque jour notre <strong>workout</strong>, comme l’exemple présenté.
								</p>

								<p>
									Tu auras accès à notre entraînement du jour et pourra suivre notre routine tout au long de l’année. Tu ne seras plus jamais seul et pourra progresser avec nous ! 
								</p>
								
								<p>
									<br>
								</p>

							@if(!auth('web')->check())
								<p class="text-center">
									<a href="{{ route('front.login') }}" class="btn btn-primary custombtn"><span>CONNECTE TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								<p class="text-center font-weight-bold">
									OU
								</p>
							@endif
								<p class="text-center">
									<a href="{{ !auth('web') ? route('front.login') : route('front.select-offer') }}" class="btn btn-primary custombtn"><span>INSCRIS TOI</span> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								</p>
								
							</div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="img-container">
								<img src="/assets/img/exemple_trainingjambes.jpg" class="img-fluid">
							</div>
						</div>
					
				</div>
			</div>
		
		</section>
@include('frontend.includes.parallax-last')	
	<a href="#" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection