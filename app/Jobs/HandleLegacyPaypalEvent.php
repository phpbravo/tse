<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Handlers\PaypalHandler;

use App\{
	Subscription,
	Purchase,
	Transaction
};

class HandleLegacyPaypalEvent implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $webhook_event;

	public $tries = 3;
	public $retryAfter = 3;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($webhook_event)
	{
		$this->webhook_event=$webhook_event;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(PaypalHandler $handler)
	{
		// $this->fail();
		$webhook_event = $this->webhook_event;
		$payload = $webhook_event->data['payload'] ?? null;
		
		$subscription_id = $payload['subscr_id'] ?? null;

		if ($subscription_id){
			$this->handleSubscription();
		}
		else
		{
			$this->fail(new \Exception('Only subscriptions supported for now'));
		}
	}

	// TODO: only renewals and cancellations supported for now
	public function handleSubscription()
	{
		$webhook_event = $this->webhook_event;
		$payload = $webhook_event->data['payload'] ?? null;

		$transaction_id  = $payload['txn_id'] ?? null;
		$subscription_id = $payload['subscr_id'] ?? null;
		$amount = bcmul($payload['mc_gross'],100) ?? 0;
		$fee = bcmul($payload['mc_fee'],100) ?? 0;
		$transaction_type = $payload['txn_type'] ?? null;

		$subscription = Subscription::getFromToken($subscription_id);

		switch ($transaction_type)
		{
			case 'subscr_payment':
				if ($subscription)
				{	// renewal
					$user = $subscription->user;
					$offer = $subscription->offer;
					

					$transaction = Transaction::createSucceededOfferSubscription(
						$subscription,
						$transaction_id,
						$amount,
						$fee,
						$payload['mc_currency'],
						$payload['payment_date']
					);

					Purchase::createFromTransaction($transaction);
				}
				else
				{	// new subscription
					$this->fail(new \Exception('New subscription not yet supported'));
				}
				break;

			case 'subscr_cancel':
				$subscription->cancel();
				break;
		}
	}

 
}
