<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Stripe\Event;

use App\Handlers\StripeHandler;

use App\{
	Subscription,
	Purchase,
	Transaction
};

class HandleStripeEvent implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $webhook_event;

	public $tries = 3;
	public $retryAfter = 3;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($webhook_event)
	{
		$this->webhook_event=$webhook_event;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(StripeHandler $handler)
	{
		$webhook_event = Event::retrieve($this->webhook_event->data['id']);
		$event_type = $webhook_event->type;

		if ($this->eventTypeIsSupported($event_type))
		{
			$handler->handleInvoice($webhook_event->data->object);
		}
		else
		{
			$this->fail(new \Exception('Unsupported event type'));
		}
	}

	public function eventTypeIsSupported(string $event_type): bool
	{
		$supported_events = config('webhook.stripe.supported_events');

		return in_array($event_type, $supported_events);
	}
}
