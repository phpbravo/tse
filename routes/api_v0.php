<?php

use Illuminate\Support\Facades\Route;

Route::get('/test',function(){
	return 'test';
})
->name('api.v0.test');

Route::any('/self','SelfController@getSelf')
->name('api.v0.self');

Route::any('/current_program','ProgramsController@getCurrentProgram')
->name('api.v0.current_program');

Route::any('/session/{id}','ProgramsController@getSession')
->name('api.v0.session');

Route::any('/we_train','ExercisesController@getWeTrainVideos')
->name('api.v0.we_train.index');

Route::any('/we_sweat','ExercisesController@getWeSweatVideos')
->name('api.v0.we_sweat.index');

Route::any('/we_flow','ExercisesController@getWeFlow')
->name('api.v0.we_flow');

Route::any('/we_eat','RecipesController@getWeEatRecipes')
->name('api.v0.we_eat.index');

Route::any('/bikini','ProgramsController@getBikiniCategories')
->name('api.v0.bikini.index');

Route::any('/bikini_videos','ExercisesController@getBikiniVideos')
->name('api.v0.bikini.videos');
