<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Handlers\LegacyAppstoreHandler;

use App\{
	Subscription,
	Purchase,
	Transaction
};

class HandleLegacyAppstoreEvent implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
 
	/** @var \App\WebhookEvent */
	protected $webhook_event;

	public $tries = 3;
	public $retryAfter = 3;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($webhook_event)
	{
		$this->webhook_event=$webhook_event;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(LegacyAppstoreHandler $handler)
	{
		$webhook_event = $this->webhook_event->data;
		$event_type = $webhook_event['event_type'];

		if ($this->eventTypeIsSupported($event_type))
		{
			$handler->handleEvent($webhook_event);
		}
		else
		{
			$this->fail(new \Exception('Unsupported event type'));
		}
	}

	public function eventTypeIsSupported(string $event_type): bool
	{
		$supported_events = config('webhook.appstore.supported_events');

		return in_array($event_type, $supported_events);
	}
}
