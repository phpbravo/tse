<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegacyRequest extends Model
{
    protected $fillable=[
        'endpoint',
        'ip',
    ];
}
