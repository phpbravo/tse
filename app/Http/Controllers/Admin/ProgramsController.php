<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Program, ProgramSchedule};

class ProgramsController extends Controller
{

	private function validateProgram($fields){
		$objectHandler=new ObjectHandler(Program::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if(isset($fields->program_schedules)){
			try{
				
				foreach ($fields->program_schedules as $program_schedule){
					$from=new Carbon($program_schedule['from'],'GMT+2');
					$to=new Carbon($program_schedule['to'],'GMT+2');
					if ($to<=$from){
						return $objectHandler->newError(
							'invalid_request',
							'La date à doit être postérieure à la date du'
						);
					}
				}
			}catch(\Exception $exception){
				return $objectHandler->newError(
					'invalid_request',
					'Calendrier de publication est invalide'
				);
			}
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Program::class);
		$pageHandler=new PageHandler(Program::class);
		$objectHandler=new ObjectHandler(Program::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page->items->makeHidden('content');

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateProgram($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$program=Program::create([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
		]);

		if (isset($fields->sessions) && is_array($fields->sessions)){

			$sessions=array_map(function($session){
				return $session['id'];
			},$fields->sessions);

			$program->sessions()->sync($sessions);

		}

		if (isset($fields->program_schedules) && is_array($fields->program_schedules)){

			foreach ($fields->program_schedules as $program_schedule){
				$now=now();

				if(!isset($program_schedule['is_custom'])){
					$program_schedule['is_custom']=false;
				}

				$from=new Carbon($program_schedule['from'],'GMT+2');
				$to=new Carbon($program_schedule['to'],'GMT+2');
				$is_published=($from<=$now)&&($to>=$now);
				$title=$program_schedule['is_custom']?$program_schedule['title']??'':'';
				$description=$program_schedule['is_custom']?$program_schedule['description']??'':'';

				ProgramSchedule::create([
					'program_id'=>$program->id,
					'is_published'=>$is_published,
					'from'=>$from,
					'to'=>$to,
					'title'=>$title,
					'description'=>$description,
				]);

			}

		}

		return $program;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateProgram($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$program=Program::find($id);

		if(!$program){
			return (new ObjectHandler(Program::class))->newError('not_found');
		}

		$program->update([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'content'=>$fields->content,
		]);

		if (isset($fields->sessions) && is_array($fields->sessions)){

			$sessions=array_map(function($session){
				return $session['id'];
			},$fields->sessions);

			$program->sessions()->sync($sessions);

		}

		$program->program_schedules()->delete();

		if (isset($fields->program_schedules) && is_array($fields->program_schedules)){

			foreach ($fields->program_schedules as $program_schedule){
				$now=now();

				if(!isset($program_schedule['is_custom'])){
					$program_schedule['is_custom']=false;
				}

				$from=new Carbon($program_schedule['from'],'GMT+2');
				$to=new Carbon($program_schedule['to'],'GMT+2');
				$is_published=($from<=$now)&&($to>=$now);
				$title=$program_schedule['is_custom']?$program_schedule['title']??'':'';
				$description=$program_schedule['is_custom']?$program_schedule['description']??'':'';

				ProgramSchedule::create([
					'program_id'=>$program->id,
					'is_published'=>$is_published,
					'from'=>$from,
					'to'=>$to,
					'title'=>$title,
					'description'=>$description,
				]);

			}

		}

		return $program;
	}

	public function show($id)
	{
		$program=Program::with(['sessions.exercise_sets.exercise_assignments.exercise','program_schedules'])->find($id);

		if(!$program)
		{
			return (new ObjectHandler(Program::class))->newError('not_found');
		}

		return $program;
	}

	public function destroy($id){

		$program=Program::find($id);

		if(!$program){
			return (new ObjectHandler(Program::class))->newError('not_found');
		}

		$program->delete();

		return (new ObjectHandler(Program::class))->newSuccess();
	}

}
