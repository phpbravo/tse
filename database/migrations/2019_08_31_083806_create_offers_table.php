<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('plan_id');
                $table->foreign('plan_id')->references('id')->on('plans')->nullable();
            $table->string('name')->index();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->decimal('price')->index();
            $table->decimal('tax_percent')->index();
            $table->char('duration',8)->index();
            $table->integer('subscriber_limit')->nullable()->index();
            $table->boolean('is_limited')->default(false)->index();
            $table->boolean('is_for_upgrade')->default(false)->index();
            $table->boolean('is_recurring')->default(false)->index();
            $table->boolean('is_lifetime')->default(false)->index();
            $table->boolean('is_public')->default(false)->index();
            $table->boolean('is_available')->default(false)->index();
            $table->boolean('is_stripe_available')->default(false)->index();
            $table->boolean('is_paypal_available')->default(false)->index();
            $table->string('appstore_id')->nullable()->index();
            $table->string('playstore_id')->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
