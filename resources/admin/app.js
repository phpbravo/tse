require('./bootstrap');

import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueApexCharts from 'vue-apexcharts'
import Timeselector from 'vue-timeselector'
import StarRating from 'vue-star-rating'

import TinyMCE from '@tinymce/tinymce-vue'

import vx_store from './store/store.js'
import vx_routes from './routes.js'


Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.use(Vuex)

Vue.prototype.window = window

window.moment = Vue.prototype.moment = require('moment')
window.buildFormData = function (formData, data, parentKey = null) {
	if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
		Object.keys(data).forEach(key => {
			buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
		});
	} else {
		let value = data == null ? '' : data;
		if (value === true)
			value = 1
		else if (value === false)
			value = 0

		formData.append(parentKey, value);
	}
}

window.uuid = function () {
	return Math.random().toString(16).replace('0.', '')
}

Vue.component('apexchart', VueApexCharts)
Vue.component('time-selector', Timeselector)
Vue.component('star-rating', StarRating)
Vue.component('tiny-mce', TinyMCE)

const files = require.context('./components/', true, /\.vue$/i);

files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const vuetify = new Vuetify({
	theme: {
		dark: false,
		themes: {
			light: {
				primary: '#aaaaaa',
				secondary: '#b0bec5',
				accent: '#8c9eff',
				error: '#b71c1c',
			},
			dark: {
				primary: '#aaaaaa',
				secondary: '#b0bec5',
				accent: '#8c9eff',
				error: '#b71c1c',
			},
		},
	},
	breakpoint: {
		thresholds: {
			xs: 600,
			sm: 900,
			md: 1200,
			lg: 1500,
		},
	}
});
const store = new Vuex.Store(vx_store);
const router = new VueRouter(vx_routes);

const app = new Vue({
	el: '#app',
	vuetify,
	router,
	store
});
