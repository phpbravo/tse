<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{
    AbstractPaymentHandler, 
    ObjectHandler, 
    FilterHandler, 
    PageHandler
};

use App\{
    Gateway,
    Offer,
    Purchase,
    Subscription,
    Transaction,
    User
};
use Carbon\Carbon;

class PurchasesController extends Controller
{
    public function index()
    {
        $filterHandler = new FilterHandler(Purchase::class);
        $pageHandler = new PageHandler(Purchase::class);
        $objectHandler = new ObjectHandler(Purchase::class);

        $filters = request()->filters;
        $search = request()->search;
        $perPage = request()->perPage;
        $page = request()->page;
        $orderBy = request()->orderBy;
        $ascending = request()->ascending;

        $filtered_query = $filterHandler
            ->getFilteredQuery($filters, $search, $orderBy, $ascending);

        $filtered_query->with([
            'offer' => function ($query){
                $query->select(['id','name']);
            },
            'plan' => function($query){
                $query->select(['id','name']);
            },
        ]);

        $page=$pageHandler
            ->getPage($filtered_query, $perPage, $page);

        $page_object=$objectHandler
            ->newPage(
                $page->items,
                $page->current_page,
                $page->last_page,
                $page->total_item_count
            );

        return $page_object;
    }

    public function show($id)
    {
        $purchase=Purchase::with([
            'transaction.gateway',
            'transaction.subscription',
            'user'
        ])->find($id);

		if(!$purchase){
			return (new ObjectHandler(Purchase::class))->newError('not_found');
		}

		return $purchase;
    }

    public function store(Request $request)
    {
        
        $offer = Offer::find($request->offer_id);
        $user = User::find($request->user_id);
        if ($request->gateway_id == 5) {
            Purchase::createFromOffer($offer, $user);
        }else {
            $handler = AbstractPaymentHandler::get_handler($request->gateway_id);
            $existing_transaction = Transaction::where('gateway_token', $request->token)->first();

            if (!$existing_transaction && $request->gateway_id != 2) {
                
                //check if transaction is verified
                $transaction_from_payment = $handler->getTransaction($request->token);
                $balance_transaction = $transaction_from_payment['charges']['data'][0]['balance_transaction'];
                
                //create local transaction
                $new_transaction = Transaction::create([
                    'user_id'=>$user->id,
                    'subscription_id'=>null,
                    'gateway_id'=>$request->gateway_id,
                    'gateway_token'=>$request->token,
                    'has_offer'=>true,
                    'offer_id'=>$offer->id,
                    'has_subscription'=>false,
                    'payment_status'=>'succeeded',
                    'net_amount'=>$balance_transaction['amount'],
                    'tax_percent'=>$offer->tax_percent,
                    'tax_amount'=>$balance_transaction['amount']*($offer->tax_percent/100),
                    'gateway_fee'=>$balance_transaction['fee'],
                    'gross_amount'=>
                        ($balance_transaction['amount']-$balance_transaction['fee'])-$balance_transaction['amount']*($offer->tax_percent/100),
                    'currency'=>$balance_transaction['currency'],
                    'discount'=>null,
                    'has_coupon'=>true,
                    'coupon_id'=>null,
                    'transaction_date' => Carbon::parse((int)$balance_transaction['created']) 
                ]);
                
                $new_purchase = Purchase::createFromTransaction($new_transaction);
                return $new_purchase;
            }
        }
    }

}
