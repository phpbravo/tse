<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler, FileHandler};

use App\Recipe;

class RecipesController extends Controller
{

	private function validateRecipe($fields){
		$objectHandler=new ObjectHandler(Recipe::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if (!isset($fields->recipe_category_id) || $fields->recipe_category_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Recipe Categorie est obligatoire'
			);
		}

		if(!\App\RecipeCategory::find($fields->recipe_category_id)){
			return $objectHandler->newError(
				'invalid_request',
				'Recipe La catégorie est invalide.'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Recipe::class);
		$pageHandler=new PageHandler(Recipe::class);
		$objectHandler=new ObjectHandler(Recipe::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateRecipe($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$recipe=Recipe::create([
			'recipe_category_id'=>$fields->recipe_category_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'preparation_time'=>$fields->preparation_time,
			'cook_time'=>$fields->cook_time,
		]);
		

		if (isset($fields->sections)){
			
			$sections=explode(',',$fields->sections);

			foreach ($sections as $id => $content) {

				if(!$id) continue;

				$recipe->sections()->attach([$id=>['content'=>$content]]);
			}

		}



		if (isset($fields->tags)){

			$tags=explode(',',$fields->tags);
			
			foreach ($tags as $tag){

				if (!is_string($tag)) continue;
				$recipe->tag($tag);
			}
			
		}

		if (
			isset($fields->image) && !is_null($fields->image) && $fields->image!='null'
			&& with(new \ReflectionClass($fields->image))->getShortName()=='UploadedFile'
		){
			with(new FileHandler)->store($fields->image,'recipes/'.$recipe->id.'/image','main_image');
		}

		return $recipe;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateRecipe($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$recipe=Recipe::find($id);

		if(!$recipe){
			return (new ObjectHandler(Recipe::class))->newError('not_found');
		}

		$recipe->update([
			'recipe_category_id'=>$fields->recipe_category_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'preparation_time'=>$fields->preparation_time,
			'cook_time'=>$fields->cook_time,
		]);

		$recipe->tags()->sync([]);
		$recipe->sections()->sync([]);

		if (isset($fields->sections)){
			
			$sections=explode(',',$fields->sections);

			foreach ($sections as $id => $content) {

				if(!$id) continue;

				$recipe->sections()->attach([$id=>['content'=>$content]]);
			}

		}

		if (isset($fields->tags)){

			$tags=explode(',',$fields->tags);
			
			foreach ($tags as $tag){

				if (!is_string($tag)) continue;
				$recipe->tag($tag);
			}
			
		}

		if (
			isset($fields->image) && !is_null($fields->image) && $fields->image!='null'
			&& with(new \ReflectionClass($fields->image))->getShortName()=='UploadedFile'
		){
			$files = glob(base_path('/storage/app/recipes/'.$recipe->id.'/image/*')); // get all file names
			foreach($files as $file){ // iterate files
				if(is_file($file))
					unlink($file); // delete file
			}
			with(new FileHandler)->store($fields->image,'recipes/'.$recipe->id.'/image','main_image');
		}

		return $recipe;
	}

	public function show($id){

		$recipe=Recipe::with(['sections','tags'])->find($id);

		if(!$recipe){
			return (new ObjectHandler(Recipe::class))->newError('not_found');
		}

		return $recipe;
	}

	public function destroy($id){

		$recipe=Recipe::find($id);

		if(!$recipe){
			return (new ObjectHandler(Recipe::class))->newError('not_found');
		}

		$recipe->delete();

		return (new ObjectHandler(Recipe::class))->newSuccess();
	}

	public function tag($id,$name){

		$recipe=Recipe::findOrFail($id);

		$recipe->tag($name);

		return $recipe;

	}

	public function untag($id,$name){

		$recipe=Recipe::findOrFail($id);

		$recipe->untag($name);

		return $recipe;

	}
	
	// returns an array of autocomplete matches
	public function autocomplete(){
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Recipe::where('name','like','%'.$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('id',$ignore)->take(5)->get();

		return $tags_matched;

  }

}
