<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapv0ApiRoutes();

        $this->mapAdminApiRoutes();

        $this->mapApiRoutes();

        $this->mapAdminWebRoutes();
        
        $this->mapWebhookRoutes();

        $this->mapWebRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }
    protected function mapAdminWebRoutes()
    {
        Route::prefix('admin')
            ->middleware('admin')
            ->namespace($this->namespace)
            ->group(base_path('routes/web_admin.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapv0ApiRoutes()
    {
        Route::any('/',function(){
            
            //RE-ROUTER FOR v0 API REQUEST
            if (request()->get('option')=='com_osmembership'){
                return app('App\Http\Controllers\API\v0\Router')->route();
            
            //ACTUAL HOMEPAGE
            }else {
                return app('App\Http\Controllers\Web\PagesController')->newhomepage();
            }
        })
        ->name('front.home');
        
        Route::prefix('api/v0')
        ->middleware('api.v0')
        ->namespace($this->namespace.'\API\v0')
        ->group(base_path('routes/api_v0.php'));
        
        Route::any('api/v0/register', $this->namespace.'\API\v0\SelfController@register')
             ->name('api.v0.register');
             
        Route::any('/we_sweat/categories',$this->namespace.'\API\v0\ProgramsController@getWeSweatCategories')
             ->name('api.v0.we_sweat.categories');

        Route::any('/we_flow_categories',$this->namespace.'\API\v0\ProgramsController@getWeFlowCategories')
             ->name('api.v0.we_flow_categories');

        Route::any('/we_eat_nutritions',$this->namespace.'\API\v0\RecipesController@getNutritions')
             ->name('api.v0.we_eat_nutritions');
             
        Route::any(
            '/'.config('webhook.mobile_legacy_endpoint'),
            $this->namespace.'\API\v0\MobileController@notify'
        )
        ->name('api.v0.mobile.notify');
    }

    protected function mapAdminApiRoutes()
    {
        Route::prefix('admin/api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api_admin.php'));
    }
    
    protected function mapWebhookRoutes()
    {
        Route::middleware('webhook')
            ->namespace($this->namespace)
            ->group(base_path('routes/webhook.php'));
    }
}
