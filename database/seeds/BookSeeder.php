<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		\App\Book::create([
			'name'=>'Nos Véritables Recettes Fitness (EBook)',
			'description'=>''
		]);
			
	}
}
