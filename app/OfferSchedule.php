<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class OfferSchedule extends Model
{
    protected $fillable=[
        'offer_id',
        'from',
        'to',
        'is_custom',
        'title',
        'description',
    ];

    public static function getActiveSchedules(){
        return self::where('to','>=',now())
        ->where('from','<=',now())
        ->orderBy('from','DESC')
        ->get();
    }

    public function offer(){
        return $this->belongsTo('App\Offer');
    }

}
