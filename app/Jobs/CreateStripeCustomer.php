<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Handlers\StripeHandler;

class CreateStripeCustomer implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $customer;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($customer)
	{
		$this->customer=$customer;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(StripeHandler $handler)
	{
		$handler->createCustomer($this->customer);
	}
}
