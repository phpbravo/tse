<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->timestamp('email_verified_at')->nullable()->index();
            $table->string('password')->nullable();
            $table->string('name')->nullable()->index();
            $table->string('surname')->nullable()->index();
            $table->string('organization')->nullable()->index();
            $table->string('address')->nullable()->index();
            $table->string('address2')->nullable()->index();
            $table->string('city')->nullable()->index();
            $table->string('postal_code')->nullable()->index();
            $table->string('country')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->string('homepage_url')->nullable()->index();
            $table->boolean('agreed_terms')->index();
            $table->boolean('agreed_newsletter')->nullable()->index();
            $table->string('profile_image_url')->nullable();
            $table->integer('balance')->nullable()->index();
            $table->string('api_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
            $table->rememberToken();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
