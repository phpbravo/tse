@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section seacevid" id="top">
			<div class="container">
			</div>
	</section>
	
	

	<section class="faq-section" id="wesweatvids">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">SÉANCES VIDÉO</h1>
					
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 px-0">

					<div class="materialouter" id="seancevid-intro">
						<hr>
						<div class="materialinner text-center" id="seancevid-content">

							<p class="materialinner-header"><strong>Alerte matériel !</strong></p>
							<p>
								Il est enfin l'heure d'introduire du matériel dans tes séances we sweat !<br>
								Après avoir progressé au poids du corps, je te recommande de te fournir le matériel suivant pour les prochaines vidéos : 
							</p>
							<div class="list-materiel">
								<p>- 1 paire d'haltères légères (1 à 2 kg)</p>
								<p>- 1 paire d'haltères intermédiaires (environ 3kg)</p>
								<p>- 1 paire d'haltéres lourdes (5kg environ)</p>
								<p>-1 paire de lestes chevilles (3 à 5kg)</p>
							</div>

							<p>
								Tu peux également investir plutôt dans deux petites barres et un jeu de disques que tu ajusteras à ta convenance.<br>
								C'est l'idéal lorsque l'on progresse. 
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="row categoriesbtn" id="wesweatcat">
				
				<div class="col-lg-12 text-center" id="recettesfitcol">
					<a
						@click.prevent="changeCategory(0)"
						href="#"
						class="btn btn-secondary recettesfitbtn"
					>
						<h4>Tous les trainings</h4>
					</a>
				</div>
				
				<div class="row grid-items">
					
					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(14)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Abdos</h4>
									<img src="/assets/img/abdos.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(25)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Bas du corps</h4>
									<img src="/assets/img/basducorps.png" class="img-fluid">
							</div>
										
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(15)"
							href="#"
						>
							<div class="categoriesbtn-item">
									<h4>Cardio</h4>
									<img src="/assets/img/cardio.png" class="img-fluid">
								</div>
							</a>
					</div>


					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(28)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Fitboxing</h4>
								<img src="/assets/img/fitboxing.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(19)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Full body</h4>
								<img src="/assets/img/fullbody.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(13)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Haut du corps</h4>
								<img src="/assets/img/hautducorps.png" class="img-fluid">
							</div>
						</a>
					</div>				


					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(26)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Hot Pilates</h4>
								<img src="/assets/img/hotpilates.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(27)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Salle</h4>
								<img src="/assets/img/salle.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(31)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>We Flow</h4>
								<img src="/assets/img/yogaenergy.png" class="img-fluid">
							</div>
						</a>
					</div>

					<div class="col recettescol">
						<a
							@click.prevent="changeCategory(33)"
							href="#"
						>
							<div class="categoriesbtn-item">
								<h4>Kettleburn</h4>
								<img src="/assets/img/yogaenergy.png" class="img-fluid">
							</div>
						</a>
					</div>


				</div>	
						
						

						


			</div>

		

			<div class="row searchrow">
				<div class="col-lg-12 searchcol">
					<div class="searhinner">
						<p>
						OU FAITES UNE RECHERCHE
						</p>
						<form>
							<div class="input-group mb-3">
							  <input type="text" class="form-control" placeholder="Cardio intense.." aria-label="Recipient's username" aria-describedby="basic-addon2">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary searchtbtn" type="button"><i class="fa fa-search"></i></button>
							  </div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div v-if="loading" class="d-flex" style="position:relative; width:100%; justify-content:center;" id="loading-icon">
				<div class="loader4"></div>
			</div>
				<!---- seance video list ----->
			<div class="row wetrainvideos-items">
				
				<div
					v-for="video in videos"
					:key="video.id"
					class="col-lg-6 col-md-12"
				>
					<div class="vidlists">
						<h4><a style="color:#060003;" :href="`/video/${video.id}`">@{{video.name}}</a></h4>
						<div class="embed-responsive embed-responsive-16by9">
							<iframe :src="`https://player.vimeo.com/video/${video.video.vimeo_id}?title=0&amp;byline=0&amp;portrait=0`" width="540px" height="305px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row paginationrow">
			<div class="col-lg-12">
				<nav aria-label="Page navigation" class="text-center">
						<ul class="pagination vidspagination" id="pagenos">
								<li class="page-item">
									<a
										@click.prevent="changePage(selected_page-1)"
										href="#"
										aria-label="Previous"
										class="page-link"
										>
												<span aria-hidden="true">&laquo;</span>
												<span class="sr-only">Previous</span>
										</a>
								</li>
								<li
									v-for="this_page in last_page"
									:key="this_page"
									class="page-item"
								><a @click.prevent="changePage(this_page)" class="page-link" style="cursor:pointer" :class="{'active':(this_page==selected_page)}">@{{this_page}}</a></li>
								<li class="page-item">
									<a
										@click.prevent="changePage(selected_page+1)"
										href="#"
										aria-label="Next"
										class="page-link"
										>
												<span aria-hidden="true">&raquo;</span>
												<span class="sr-only">Next</span>
										</a>
								</li>
						</ul>
				</nav>
			</div>
		</div>
		
	</section>

	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/we-sweat-videos.js?v={{filever('public/assets/js/we-sweat-videos.js')}}"></script>

@endsection