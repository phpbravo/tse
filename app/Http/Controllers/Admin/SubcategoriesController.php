<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Subcategory;

class SubcategoriesController extends Controller
{

	private function validateSubcategory($fields){
		$objectHandler=new ObjectHandler(Subcategory::class);

		if (!isset($fields->category_id) || $fields->category_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Catégorie est obligatoire'
			);
		}

		if(!\App\Category::find($fields->category_id)){
			return $objectHandler->newError(
				'invalid_request',
				'La catégorie est invalide.'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Subcategory::class);
		$pageHandler=new PageHandler(Subcategory::class);
		$objectHandler=new ObjectHandler(Subcategory::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$filtered_query->with('subcategories');

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateSubcategory($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$subcategory=Subcategory::create([
			'category_id'=>$fields->category_id,
			'order_index'=>Subcategory::min('order_index')-1,
			'name'=>"Nouvelle sous-catégorie",
		]);

		return $subcategory;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateSubcategory($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$subcategory=Subcategory::find($id);

		if(!$subcategory){
			return (new ObjectHandler(Subcategory::class))->newError('not_found');
		}

		$subcategory->update([
			'subcategory_id'=>$fields->subcategory_id,
			'subsubcategory_id'=>$fields->subsubcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_link'=>$fields->video_link,
			'api_video_link'=>$fields->api_video_link,
		]);

		$subcategory->tags()->sync([]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$subcategory->tag($tag);
			}
		}

		return $subcategory;
	}

	public function updateAtomic($id){

		$fields=request()->all();

		// maybe validation?

		$subcategory=Subcategory::find($id);

		if(!$subcategory){
			return (new ObjectHandler(Subcategory::class))->newError('not_found');
		}

		$subcategory->update($fields);

		return $subcategory;

	}

	public function show($id){

		$subcategory=Subcategory::with(['tags'])->find($id);

		if(!$subcategory){
			return (new ObjectHandler(Subcategory::class))->newError('not_found');
		}

		return $subcategory;
	}

	public function destroy($id){

		$subcategory=Subcategory::find($id);
		
		if(!$subcategory){
			return (new ObjectHandler(Subcategory::class))->newError('not_found');
		}

		$subcategory->delete();

		return (new ObjectHandler(Subcategory::class))->newSuccess();
	}

	public function orderAhead($id){

		$subcategory=Subcategory::findOrFail($id);

		$subcategory_ahead=Subcategory::where('category_id',$subcategory->category_id)->where('order_index','<=',$subcategory->order_index)->orderBy('order_index','desc')->skip(1)->first();

		if (!$subcategory_ahead) return (new ObjectHandler(Subcategory::class))->newError('invalid_request');

		$old_order=$subcategory->order_index;
		$new_order=$subcategory_ahead->order_index;

		if ($old_order==$new_order) $new_order--;

		$subcategory->update(['order_index'=>$new_order]);
		$subcategory_ahead->update(['order_index'=>$old_order]);

		return $subcategory;

	}

	public function orderBehind($id){

		$subcategory=Subcategory::findOrFail($id);

		$subcategory_behind=Subcategory::where('category_id',$subcategory->category_id)->where('order_index','>=',$subcategory->order_index)->orderBy('order_index','asc')->skip(1)->first();

		if (!$subcategory_behind) return (new ObjectHandler(Subcategory::class))->newError('invalid_request');

		$old_order=$subcategory->order_index;
		$new_order=$subcategory_behind->order_index;
		
		if ($old_order==$new_order) $new_order++;
		
		$subcategory->update(['order_index'=>$new_order]);
		$subcategory_behind->update(['order_index'=>$old_order]);

		return $subcategory;

	}
	
	// returns an array of autocomplete matches
	public function autocomplete(){
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Subcategory::where('name','like','%'.$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('id',$ignore)->take(5)->get();

		return $tags_matched;

  }

}
