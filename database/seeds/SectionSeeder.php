<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\Section::create([
            'name'=>'Ingrédients'
        ]);
        \App\Section::create([
            'name'=>'Préparation'
        ]);
        \App\Section::create([
            'name'=>'Astuce de sissy'
        ]);
        \App\Section::create([
            'name'=>'Informations Nutritionnelles'
        ]);
        
    }
}
