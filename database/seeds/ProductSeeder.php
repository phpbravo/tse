<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\Product::create([
            'resource_id'=>4,
            'name'=>'Tous les exercices',
            'description'=>'accès à tous les exercices en We Train et We Sweat',
        ]);
        \App\Product::create([
            'resource_id'=>5,
            'name'=>'Toutes les recettes',
            'description'=>'accès à tous les recettes fit',
        ]);
        \App\Product::create([
            'resource_id'=>8,
            'name'=>'Toutes les séances',
            'description'=>'accès à tous les séances',
        ]);
        \App\Product::create([
            'resource_id'=>7,
            'name'=>'Tous les programmes',
            'description'=>'accès à tous les programmes',
        ]);

        $recipes_book = \App\Book::find(1);

        \App\Product::create([
            'resource_id'=>$recipes_book->resource_id,
            'name'=>'Nos Véritables Recettes Fitness (EBook)',
            'description'=>'accès ebook',
        ]);
        
    }
}
