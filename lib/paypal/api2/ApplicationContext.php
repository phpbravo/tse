<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;


/**
 * Class ApplicationContext
 *
 * A resource representing an application context.
 *
 * @package PayPal\Api
 *
 * @property string brand_name
 * @property string locale
 * @property string shipping_preference
 * @property string user_action
 * @property string return_url
 * @property string cancel_url
 */
class ApplicationContext extends PayPalModel
{
    /**
     * Brand name.
     *
     * @param string $brand_name
     * 
     * @return $this
     */
    public function setBrandName($brand_name)
    {
        $this->brand_name = $brand_name;
        return $this;
    }

    /**
     * Brand name.
     *
     * @return string
     */
    public function getBrandName()
    {
        return $this->brand_name;
    }
    
    /**
     * Locale.
     *
     * @param string $locale
     * 
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Locale.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
    
    /**
     * Shipping preference
     *
     * @param string $shipping_preference
     * 
     * @return $this
     */
    public function setShippingPreference($shipping_preference)
    {
        $this->shipping_preference = $shipping_preference;
        return $this;
    }

    /**
     * Shipping preference
     *
     * @return string
     */
    public function getShippingPreference()
    {
        return $this->shipping_preference;
    }
    
    /**
     * User action
     *
     * @param string $user_action
     * 
     * @return $this
     */
    public function setUserAction($user_action)
    {
        $this->user_action = $user_action;
        return $this;
    }

    /**
     * User action
     *
     * @return string
     */
    public function getUserAction()
    {
        return $this->user_action;
    }
    
    /**
     * Return URL
     *
     * @param string $return_url
     * 
     * @return $this
     */
    public function setReturnURL($return_url)
    {
        $this->return_url = $return_url;
        return $this;
    }

    /**
     * Return URL
     *
     * @return string
     */
    public function getReturnURL()
    {
        return $this->return_url;
    }
    
    /**
     * Cancel URL
     *
     * @param string $cancel_url
     * 
     * @return $this
     */
    public function setCancelURL($cancel_url)
    {
        $this->cancel_url = $cancel_url;
        return $this;
    }

    /**
     * Cancel URL
     *
     * @return string
     */
    public function getCancelURL()
    {
        return $this->cancel_url;
    }

}
