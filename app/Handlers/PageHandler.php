<?php

namespace App\Handlers;

class PageHandler{

    private $modelClass;
    private $tableName;

    public function __construct($modelClass){
        $this->modelClass=$modelClass;
        $this->tableName=with(new $modelClass)->getTable();
    }

    public function getPage($query,$per_page=25,$page=1){

        $page=(int)$page;
        $per_page=(int)$per_page;

        $total_item_count=$query->count();

        $per_page=($per_page<1)?25:$per_page;

        $page_count=(int)ceil($total_item_count/$per_page);
        $page=($page>$page_count?$page_count:($page<1))?1:$page;
        $index=$page-1;
        $skip=$index*$per_page;
        $take=$per_page;

        $current_page=$page;
        $last_page=(int)$page_count?:1;
        $items=$query->skip($skip)->take($per_page)->get();

        return (object)compact([
            'current_page',
            'last_page',
            'items',
            'total_item_count',
        ]);
    }

}