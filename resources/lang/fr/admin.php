<?php

return [

    'login' => 'Connexion Admin',
    'login_button' => 'Connexion',
    'dashboard' => 'Accueil',
    'subscriptions' => 'Abonnements',
    'offers' => 'Offres',
    'coupons' => 'Coupons',
    'publishing' => 'Publier',
    'programs' => 'Programmes',
    'sessions' => 'Sessions',
    'exercises' => 'Sessions',
    'recipes' => 'Recettes',
    'users' => 'Utilisateurs',

];
