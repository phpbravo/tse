<?php

namespace PayPal\Traits;

trait Timestamps
{
	/**
	 * Date and time that this resource was created.
	 * Date format 'Y-m-d\TH:i:s\Z' (ISO8601Z)
	 */
	public function setCreateTime(string $create_time): self
	{
		$this->create_time = $create_time;
		return $this;
	}

	/**
	 * Date and time that this resource was created.
	 * Date format 'Y-m-d\TH:i:s\Z' (ISO8601Z)
	 */
	public function getCreateTime(): string
	{
		return $this->create_time;
	}

	/**
	 * Date and time that this resource was updated.
	 * Date format 'Y-m-d\TH:i:s\Z' (ISO8601Z)
	 */
	public function setUpdateTime(string $update_time): self
	{
		$this->update_time = $update_time;
		return $this;
	}

	/**
	 * Date and time that this resource was updated.
	 * Date format 'Y-m-d\TH:i:s\Z' (ISO8601Z)
	 */
	public function getUpdateTime(): string
	{
		return $this->update_time;
	}
}