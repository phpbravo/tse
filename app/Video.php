<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $fillable=[
		'vimeo_id',
		'vimeo_name',
		'thumbnails',
		'access_details',
		'last_modified_date',
		'vimeo_created_date',
	];

	// columns that can be searched
	public $searchable_columns = [
		'vimeo_id',
		'vimeo_name',
	];

	// columns that can be filtered
	public $filterable_columns = [
	];

	// columns that can be ordered
	public $orderable_columns = [
		'vimeo_id',
		'vimeo_name',
		'last_modified_date',
		'vimeo_created_date',
    ];
    
	protected $casts=[
		'thumbnails'=>'json',
		'access_details'=>'json',
		'last_modified_date'=>'date',
		'vimeo_created_date'=>'date',
	];
}
