<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\{
	User,
	Offer,
	Subscription,
	Coupon
};
use Carbon\Carbon;

class Transaction extends Model
{

	protected $dispatchesEvents = [

	];

	protected $fillable = [
		'user_id',
		'gateway_id',
		'gateway_token',
		'payment_status',
		'has_offer',
		'offer_id',
		'has_subscription',
		'subscription_id',
		'has_coupon',
		'coupon_id',
		'gross_amount',
		'discount',
		'gateway_fee',
		'tax_amount',
		'tax_percent',
		'net_amount',
		'currency',
		'payment_details',
		'transaction_date'
	];

	protected $hidden = [

	];

	protected $casts = [
		'payment_details'=>'json'
	];

	// Simple Relations

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function gateway()
	{
		return $this->belongsTo(Gateway::class);
	}

	public function offer()
	{
		return $this->belongsTo(Offer::class);
	}

	public function purchase()
	{
		return $this->hasOne(Purchase::class);
	}

	public function subscription()
	{
		return $this->belongsTo(Subscription::class);
	}

	public function coupon()
	{
		return $this->belongsTo(Coupon::class);
	}

	// Scoped Relations

		// none

	// Attributes

	public function getSucceededAttribute()
	{
		return $this->status == 'succeeded';
	}

	// Scopes

	public function scopeSucceeded($query)
	{
		return $query->where('payment_status','succeeded');
	}

	public function scopePending($query)
	{
		return $query->where('payment_status','pending');
	}

	public function scopeFailed($query)
	{
		return $query->where('payment_status','failed');
	}

	// Instance Methods

	public function updateSucceeded(
		int $net_amount,
		int $gateway_fee,
		string $currency,
		Coupon $coupon = null
	): bool {

		$payment_status = 'succeeded';
		$tax_amount = $this->has_offer?
			bcmul($net_amount,bcdiv($this->tax_percent,100,4),2):
			null;
		$gross_amount = ($net_amount-$gateway_fee)-$tax_amount;
		
		$discount = null;
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$discount = $coupon->amount;
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}

		return $this->update(compact(
			'payment_status',
			'net_amount',
			'gateway_fee',
			'tax_amount',
			'gross_amount',
			'discount',
			'has_coupon',
			'coupon_id'
		));
	}

	public function updateFailed(
		int $net_amount
	): bool {
		$payment_status = 'failed';
		
		return $this->update(compact(
			'payment_status',
			'net_amount'
		));
	}

	// Static Methods

	public static function getFromToken(
		string $subscription_token
	): ?Transaction {
		return self::where('gateway_token',$subscription_token)->first();
	}

	public static function createSucceededOfferPurchase(
		User $user,
		Offer $offer,
		int $net_amount,
		int $gateway_fee,
		string $currency,
		string $gateway_token,
		int $gateway_id,
		Coupon $coupon = null
	): Transaction {
		$tax_amount = 	bcmul($net_amount,bcdiv($offer->tax_percent,100,4));
		$gross_amount = ($net_amount-$gateway_fee)-$tax_amount;
		$discount = 0;
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$discount = $coupon->amount;
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}
		
		return self::create([
			'user_id'=>$user->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
			'has_offer'=>true,
			'offer_id'=>$offer->id,
			'has_subscription'=>false,
			'payment_status'=>'succeeded',
			'net_amount'=>bcmul($offer->price,100),
			'tax_percent'=>$offer->tax_percent,
			'has_coupon'=>$has_coupon,
			'coupon_id'=>$coupon_id,
			'gateway_fee'=>$gateway_fee,
			'tax_amount'=>$tax_amount,
			'gross_amount'=>$gross_amount,
			'discount'=>$discount,
		]);
	}

	public static function createPendingPurchase(
		User $user,
		Offer $offer,
		string $gateway_token,
		int $gateway_id,
		Coupon $coupon = null
	): Transaction {
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}
		
		return self::create([
			'user_id'=>$user->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
			'has_offer'=>true,
			'offer_id'=>$offer->id,
			'has_subscription'=>false,
			'payment_status'=>'pending',
			'net_amount'=>bcmul($offer->price,100),
			'tax_percent'=>$offer->tax_percent,
			'has_coupon'=>$has_coupon,
			'coupon_id'=>$coupon_id,
		]);
	}

	public static function createPendingOfferSubscription(
		User $user,
		Offer $offer,
		Subscription $subscription,
		string $gateway_token,
		Coupon $coupon = null
	): Transaction {
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}
		
		return self::create([
			'user_id'=>$user->id,
			'subscription_id'=>$subscription->id,
			'gateway_id'=>$subscription->gateway_id,
			'gateway_token'=>$gateway_token,
			'has_offer'=>true,
			'offer_id'=>$offer->id,
			'has_subscription'=>true,
			'payment_status'=>'pending',
			'net_amount'=>bcmul($offer->price,100),
			'tax_percent'=>$offer->tax_percent,
			'has_coupon'=>$has_coupon,
			'coupon_id'=>$coupon_id,
		]);
	}

	public static function createSucceededOfferSubscription(
		Subscription $subscription,
		string $gateway_token,
		int $amount_received,
		int $fee,
		string $currency,
		string $transaction_date,
		Coupon $coupon = null
	): Transaction {
		$user = $subscription->user;
		$offer = $subscription->offer;
		$discount = null;
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$discount = $coupon->amount;
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}
		
		return self::create([
			'user_id'=>$user->id,
			'subscription_id'=>$subscription->id,
			'gateway_id'=>$subscription->gateway_id,
			'gateway_token'=>$gateway_token,
			'has_offer'=>true,
			'offer_id'=>$offer->id,
			'has_subscription'=>true,
			'payment_status'=>'succeeded',
			'net_amount'=>$amount_received,
			'tax_percent'=>$offer->tax_percent,
			'tax_amount'=>
				round(
					$amount_received*($offer->tax_percent/100)
				),
			'gateway_fee'=>$fee,
			'gross_amount'=>
				round(
					($amount_received-$fee)-$amount_received*($offer->tax_percent/100)
				),
			'currency'=>$currency,
			'discount'=>$discount,
			'has_coupon'=>$has_coupon,
			'coupon_id'=>$coupon_id,
			'transaction_date' => Carbon::parse((int)$transaction_date) 
		]);
	}

	public static function createFailedOffer(
		User $user,
		Offer $offer,
		string $gateway_token,
		int $gateway_id,
		int $amount_requested,
		int $fee,
		string $currency,
		string $transaction_date,
		Coupon $coupon = null
	): Transaction {
		
		$coupon_id = null;
		$has_coupon = false;

		if ($coupon)
		{
			$coupon_id = $coupon->id;
			$has_coupon = true;
		}

		return self::create([
			'user_id'=>$user->id,
			'gateway_id'=>$gateway_id,
			'gateway_token'=>$gateway_token,
			'has_offer'=>true,
			'offer_id'=>$offer->id,
			'has_subscription'=>false,
			'has_coupon'=>$has_coupon,
			'coupon_id'=>$coupon_id,
			'payment_status'=>'failed',
			'net_amount'=>$amount_requested,
			'currency'=>$currency,
			'transaction_date' => Carbon::parse((int)$transaction_date) 
		]);
	}

}
