@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="imageslideup-section container" id="top">
		<div class="container" id="asd">
			
		</div>
	</section>
	
	

	<section class="faq-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">FAQ</h1>
					<hr>
				</div>
				<div class="col-xl-6 col-lg-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
				</div>
				<div class="col-xl-6 col-lg-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe src="//player.vimeo.com/video/22428395"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section class="definition-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-12">
					<h3 class="title-about">DÉFINITIONS</h3>
					<p><strong>L'ensemble des excerices est illustré dans la rubrique vidéos.</strong></p>
					<div class="definition-div">
						<img src="/assets/img/picto-serie.png">
						<h5>Serie:</h5>
						<p>ensemble des répétitions sans repos pour un exercice de musculation. Une série est faite de plusieurs répétitions.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-repetition.png">
						<h5>Répétition :</h5>
						<p>pour un exercice de musculation, une répétition correspond à un mouvement complet, une flexion et une extension.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-superset.png">
						<h5>Superset :</h5>
						<p>enchaînement de deux séries d’exercices sollicitant en général des muscles antagonistes sans temps de repos entre les séries.<br>
						<em>Exemple: </em> extension triceps curl biceps (repos).
						
						</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-superset.png">
						<h5>Biset :</h5>
						<p>Enchainement de deux séries de deux exercices différents, travaillant le même muscle sans temps de repos.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-triset.png">
						<h5>Triset :</h5>
						<p>enchainement de 4 séries ou plus de 4 exercices différents sur le même muscle sans temps de repos.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-giant.png">
						<h5>Giant set :</h5>
						<p>ensemble des répétitions sans repos pour un exercice de musculation. Une série est faite de plusieurs répétitions.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-pyramide.png">
						<h5>Pyramide set :</h5>
						<p>augmenter la charge à chaque série lorsque le nombre de répétitions diminue.
						<em>Exemple :</em> 
						20,15,12 ou 12,10,8,6
						</p>
					</div>

				</div>
				<div class="col-xl-6 col-lg-12">
					<p> &nbsp;</p>
					<div class="definition-div">
						<img src="/assets/img/picto-dropset.png">
						<h5>Dropset :</h5>
						<p>Diminuer le poids sur un exercice donné sans temps de repos pour une ou plusieurs séries.<br>
							<em>Exemple :</em><br>
							 8,8,8 drop 15 (effectuer 8 répétitions sur la série 3 puis diminuer le poids et effectuer 15 répétitions)
							<br>
							6 drop 15, 6 drop15, 6 drop 15 (Pour chaque série effectuer 6 répétition puis diminuer le poids et effectuer 15 répétitions)
						</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-drop.png">
						<h5>Drop :</h5>
						<p>Diminuer le poids sur un exercice donné par rapport à sa série précédente avec temps de repos.<br>
							<em>Exemple :</em>
							5,5,5,drop,drop
							
						</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-giant.png">
						<h5>AMRAP  :</h5>
						<p>Autant que possible tout en conservant une forme propre du mouvement.
						</p>
					</div>

					<div class="definition-div" id="tempo">
						<img src="/assets/img/picto-tempo.png">
						<h5>Tempo :</h5>
						<p>Rythme selon lequel exécuter un exercice. (1:2:3:4).
						</p>
						<p><span>Premier chiffre :</span> temps pour effectuer la portion négative du mouvement</p>
						<p><span>Deuxième chiffre :</span> temps de pause à l’étirement</p>
						<p><span>Troisième chiffre :</span> Temps pour effectuer la portion positive du mouvement</p>
						<p><span>Quatrième chiffre :</span> temps de pause à la contraction</p>
						<p><span>Portion négative :</span> corresponds à l’étirement du muscle</p>
						<p><span>Portion Positive :</span> Corresponds à la contraction du muscle</p>
						<p>
							<em>Exemple :</em> écarté incliné (4:0:1:0)
						</p>
						<p>4 secondes sur la portion négative, pas de pause, 1 secondes sur la portion positive, pas de pause à la contraction.</p>
					</div>

					<div class="definition-div">
						<img src="/assets/img/picto-repos.png">
						<h5>Repos :</h5>
						<p>Temps de repos entre deux séries. Lors que cela n’est pas précisé, prendre environ 1 :00 à 1:30 de repos, selon votre besoin. Nous pensons que le temps de repos n’est pas déterminant lorsqu’il reste raisonnable et nous préférons ainsi nous concentrer sur la forme d’un exercice, sa contraction et sa charge plutôt que sur le temps de repos. Néanmoins, respecter le temps de repos lorsque cela est précisé pour un exercice particulier est important.
						</p>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section class="section-collapsingfaq"> 
		<div class="container collapsingfaq-content">
			<div class="row collapsingfaq-content-row">
				<div class="col-lg-7 col-md-12">
					<h3 class="title-about">FAQ</h3>
					<div id="accordion">
						<div class="card faq-cards">
							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse1" aria-expanded="true">
									QUELS RÉSULTATS ATTENDRE AVEC WE TRAIN
								</a>
								</h4>
							</div>
							<div id="collapse1" class="collapse show" data-parent="#accordion">
								<div class="card-body">
									<p>
										We Train est notre véritable entrainement en salle, que nous faisons chaque jour, ensemble, Tini et moi. Nos trainings sont donc autant adaptés aux hommes, qu’aux femmes. Car contrairement aux croyances, nous sommes persuadés qu’il n’y a pas plusieurs trainings, en fonction du genre, de la morphologie ou des objectifs… mais plutôt un training « universel », qui travaillerait en profondeur l’intégralité des muscles avec variété des mouvements et des méthodes, afin d’obtenir une silhouette harmonieuse, tracée et galbée. Non les hommes n’auront pas un booty de femme, et non les femmes n’auront pas une carrure d’homme. Les hommes prendront en volume et en dessin, les femmes se galberont et seront plus toniques. Travailler l’ensemble du corps tout en adaptant les charges à ses capacités, en mixant les différentes méthodes de travail, est pour nous la meilleure façon de s’entrainer. En partant de ce postulat, il n’y a donc aucune contre-indication à s’entrainer en couple, ou à faire le même training qu’un homme ou une femme.
									</p>
								</div>
							</div>

							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse2" aria-expanded="false">
									POURQUOI TRAVAILLER TOUS LES MUSCLES DU CORPS
								</a>
								</h4>
							</div>

							<div id="collapse2" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Que vous soyez un homme ou une femme, vous avez tout intérêt à travailler l’ensemble de votre corps. Pourquoi travailler les jambes, vont nous demander les hommes ? Ou pourquoi travailler les pecs, vont nous demander les femmes ? Travailler l’ensemble du corps est indispensable pour plusieurs raisons. La première, que vous nous croyiez ou pas, est tout simplement l’harmonie de la silhouette. Les hommes doivent absolument travailler le bas de leurs corps, même si leur seul intérêt d’aller à la salle est d’obtenir de belles épaules carrées et de superbes pecs gonflés. En effet, un haut du corps musclé et un bas du corps négligé, comme on le voit trop souvent, mène tout simplement à une shape déséquilibrée que vous regretterez tôt ou tard, croyez-nous sur parole. Idem pour les femmes. J’ai fait l’erreur de faire l’impasse sur les pecs quand j’ai démarré la muscu. Après 3 ou 4 ans, j’ai commencé à trouver mon buste mal proportionné, les pectoraux étant trop faibles par rapport à mes épaules, mes bras ou mon dos. Je les ai enfin intégrés dans ma routine. Et comme pour les hommes qui prennent du retard sur les jambes, je peine à rattraper toutes ces années. Ensuite, travailler l’ensemble du corps est indispensable d’un point de vue santé et bien-être du corps. Pensez-vous sans danger de soulever 80kg au squat, alors que vous ne travaillez jamais votre dos ? Comment celui-ci va t’il être capable d’assumer de telles charges en toute sécurité ? Il y a deux issues. Soit vous vous blesserez, soit vous serez limitée au niveau du bas du corps. Ce n’est clairement pas ce que nous souhaitons. Et enfin, travailler l’ensemble du corps vous permettra d’augmenter considérablement votre masse musculaire. Et qui dit augmentation de la masse musculaire, dit par conséquent augmentation du métabolisme basal (énergie dépensée au repos). Ainsi, vous serez naturellement de plus en plus « sec » ou « sèche » car votre corps brulera toujours davantage d’énergie au repos. Adios les régimes farfelus et programmes de sèche drastique, votre masse grasse diminuera doucement, mais surement.
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse3" aria-expanded="false">
									DOIS-JE FAIRE TOUS LES TRAININGS ET TOUS LES EXERCICES
								</a>
								</h4>
							</div>

							<div id="collapse3" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Comme vous l’avez compris, sur We Train, nous vous partageons tous nos trainings. Ceux que nous faisons vraiment chaque jour, pas ces exercices « instagramables » qui n’apportent aucun résultat. Nous nous entrainons actuellement 6j sur 7, et sollicitons tous nos muscles deux fois par semaine. C’est à dire que nous faisons deux cycles de 3j par semaine, chaque cycle comportant 2j de trainings haut du corps, et un jour de training bas du corps. Les deux cycles s’enchainant du lundi au samedi. Le dimanche étant actuellement notre jour de repos. Mais êtes-vous obligés de suivre le même rythme ? Absolument pas !
										Nos trainings ne constituent pas LE programme parfait à suivre à la lettre. Et soyons clair, celui qui vous le promet est simplement un très bon menteur. Aucun training n’est parfait, et notre objectif et de vous aider à trouver le meilleur entrainement POUR VOUS. Notre training doit ainsi être source d’inspiration pour vous et nous vous conseillons donc vivement de l’adapter à votre emploi du temps et à votre rythme de vie. Si vous ne pouvez-vous entrainer que trois fois par semaine, il n’y a aucun souci à ne faire qu’un seul des deux cycles. Vous pouvez retenir le deuxième pour plus tard. Dans ce cas, nous vous conseillons de répartir autant que possible les séances dans la semaine. Si vous vous entrainer 4, vous pouvez faire deux séances haut du corps et deux séances bas du corps en piochant dans les trainings proposés. Si vous vous entrainez cinq fois, pourquoi ne pas faire 3 séances haut du corps et 2 séances jambes ? Le plus important est de ne pas vous entrainer systématiquement sous la contrainte, mais de trouver LE rythme qui VOUS convient ! En ce qui concerne les exercices, c’est à peu près la même chose. Vous n’avez aucune obligation à faire tous les trainings, même si nos entrainements sont clairement conçus pour être complets. Ainsi, si certains mouvements vous sont déconseillés à cause d’un souci de santé par exemple, il est évident qu’il vous faut passer les exercices concernés. En revanche, si vous pétez la forme et êtes pleinement aptes, faire des séances complètes optimisera vos résultats.
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse4" aria-expanded="false">
									COMMENT ADAPTER LE PLANNING
								</a>
								</h4>
							</div>

							<div id="collapse4" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Comme expliqué dans le point précédent, ce n’est pas parce que nous nous entrainons 6 fois par semaine que vous êtes obligés de le faire. Il est tout à fait envisageable de s’entrainer 3, 4 ou 5 fois. Dans ce cas-là, il est primordial de repartir au mieux ses séances haut du corps / Bas du corps et de travailler autant le haut que les jambes.
									</p>

									<p>
										Si vous vous entrainez 3 fois, nous vous suggérons de faire 3 half body, c’est à dire 2 séances haut du corps et une séance jambes. Vous n’avez cas faire le premier ou le deuxième cycle de notre semaine.
									</p>

									<p>
										Si vous vous entrainez 4 fois, nous vous conseillons de faire deux séances haut du corps et deux séances legs. Dans ce cas, pour le haut du corps, faites celles du premier ou du deuxième cycle, et complétez par les deux séances legs de la semaine.
									</p>

									<p>
										Enfin, si vous faites 5 séances par semaine, pourquoi ne pas faire trois séances haut du corps et deux séances jambes ? Vous pouvez dans ce cas-là piocher dans les différents trainings haut du corps que nous vous proposons et faire les deux séances legs de la semaine.
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse5" aria-expanded="false">
									COMMENT CHOISIR SES CHARGES
								</a>
								</h4>
							</div>

							<div id="collapse5" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Bien choisir ses charges n’est pas forcément chose aisée, surtout lorsque l’on débute. Tout est une question de ressenti. Si vous êtes supposé faire 10 répétitions, nous pensons que la charge choisie doit vous permettre d’en faire entre 8 et 12. Pas de quoi s’inquiéter si vous ne tombez pas sur le chiffre exact du premier coup. Le plus important étant de faire le nombre de rép dont vous êtes capables pour la charge sélectionnée. Ne vous arrêtez pas à 10 si vous êtes capables d’en faire 13 ! Faites en 13 et augmentez le poids à la prochaine série. Personnellement, avec l’expérience, je sens dès les premières répétitions si la charge choisie va me permettre de faire le nombre de répétitions que je suis censée faire.  Cela vient avec la pratique. Le ressenti est différent que vous soyez sur une série courte, ou longue. Sur une série très courte, l’effort paraît intense dès la première répétition, alors que sur une série plus longue, il nous faut tenir la distance ! Dans tous les cas, il est extrêmement important de toujours privilégier la forme du mouvement à la charge, et encore plus lorsque l’on débute. Nous faisons souvent l’erreur de surcharger, pour flatter notre égo, au détriment du mouvement et donc de notre santé ! Faites attention à ne pas tomber dans l’excès inverse. Sous charger ses poids ne vous fera pas progresser aussi vite que vous le pourriez. Ne vous sous estimez pas ! Pour ne pas vous influencer, nous avons volontairement choisi de ne pas vous donner nos charges. Elles n’ont aucune valeur pour vous et ne vous serons d’aucune utilité. Concentrez-vous sur VOS mouvement et VOS ressentis. Et n’oubliez pas dans tous les cas que nous avons tous commencé un jour. Il n’y a aucune honte à débuter léger, même à vide !
									</p>
								</div>
							</div>


							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse6" aria-expanded="false">
									COMMENT CONTINUER DE PROGRESSER
								</a>
								</h4>
							</div>

							<div id="collapse6" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Après quelques années de pratique, il est assez fréquent de se heurter à un pallier. Comment continuer à progresser, tant au niveau performance que visuellement ? Tout simplement en variant ses trainings et en introduisant diverses méthodes comme les programmes de force par exemple. Après 10 ans de pratique pour Tini et 5 ans pour moi, nous sommes obligés de casser la routine pour continuer à évoluer. C’est tout simplement la raison pour laquelle nos trainings sont aussi riches en exercices et méthodes différentes.
									</p>
								</div>
							</div>

							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse7" aria-expanded="false">
									ECHAUFFEMENTS / ETIREMENTS
								</a>
								</h4>
							</div>

							<div id="collapse7" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Nous ne précisons pas les échauffements et étirements que nous faisons avant et après chaque trainings, car ils sont propres à chacun d’entre nous et varient au fil des semaines, en fonction de ce que l’on souhaite travailler et ce que notre kinésithérapeute nous conseille. N’hésitez pas à consulter un professionnel de la santé pour savoir comment travailler votre mobilité, souplesse et flexibilité.
									</p>
								</div>
							</div>

							<div class="card-header">
								<h4>
									<a class="card-link" data-toggle="collapse" href="#collapse8" aria-expanded="false">
									 RAJOUTER DU CARDIO / ABDOS
								</a>
								</h4>
							</div>

							<div id="collapse8" class="collapse" data-parent="#accordion">
								<div class="card-body">
									<p>
										Nos trainings WE TRAIN ne comportent que nos exercices en salle de musculation. Mais si vous me suivez régulièrement, vous n’êtes pas sans savoir que j’aime beaucoup introduire des exercices de HIIT au début, à la fin, ou en parallèle de mes séances muscu, ainsi que des exercices d’abdominaux principalement constitués de gainage. Cela aide à booster mon métabolisme, à travailler mon cardio, à travailler mes muscles profonds et surtout à me sentir en super forme ! Retrouvez toutes mes séances préférées de HIIT et abdos dans la section WE SWEAT.
									</p>
								</div>
							</div>
							

						</div>
					</div>
				</div>

				<div class="col-lg-5 d-md-none d-lg-block rightsideimg">
					<div class="faq-rightimages">
						<img src="/assets/img/concept01.jpg" class="img-fluid">
					</div>

					<div class="faq-rightimages">
						<img src="/assets/img/wetrain-faq-derniere.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</section>

@include('frontend.includes.parallax')	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>WE</span> TRAIN</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="Training Du Jour">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>Training Du Jour</h2>
									<p>Quotidiennement, nous partageons avec vous nos <strong>entrainements</strong> en salle de musculation ! Ensemble, <strong>progressons chaque jour</strong> et restons <strong>surmotivés</strong> !</p>
									<br>
									<p><a href="{{ route('front.program') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-camera" data-headline="Vidéos">&nbsp;</span>
								<div class="services-box-item-content fa fa-camera">
									<h2>Vidéos</h2>
									<p>
										<strong>Tu ne connais pas un exercice</strong> ? Nous nous sommes filmés en train de <strong>le réaliser </strong> pour <strong>t'aider à le reproduire</strong> !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-calendar" data-headline="Scéances Bonus">&nbsp;</span>
								<div class="services-box-item-content fa fa-calendar">
									<h2>Scéances Bonus</h2>
									<p>
										Tu ne peux pas faire la <strong>séance du jour</strong> ? <strong>Pas de panique </strong>! Nous te proposons quelques<strong> séances bonus</strong> en <strong>remplacement.</strong>
									</p>
									<br>
									<p><a href="{{ route('front.we-train.bonus') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="passion-section tse-last-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container inner-parallax-section">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/rencontre.jpg" class="img-fluid ">
								
							</div>

							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h1>NOTRE PASSION</h1>
								<h3>POURQUOI TRAIN SWEAT EAT ?</h3>
								<p class="notrep">
									C'est l’aboutissement de notre volonté commune de partager notre motivation, notre savoir, et surtout, notre passion ! Nous avons ainsi uni nos compétences très complémentaires pour vous proposer un site communautaire jamais créé en France, d’une qualité à la hauteur de nos exigences. Très fiers que ce projet voit enfin le jour après plus de deux ans de travail et de réflexion, nous espérons que vous aimerez plus que tout partager ce lifestyle avec nous, et surtout que vous deviendrez la meilleure version de vous-même !
								</p>

								@include('frontend.includes.social-icons')
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<a href="#asd" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection