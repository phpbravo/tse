<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Tag;

class TagsController extends Controller
{
  // returns an array of autocomplete matches
  public function autocomplete(){
    $search=mb_strtolower(trim(request()->search));
    $ignore=request()->ignore?:[];

    if (!$search) return json_encode((object)[]);

    $tags_matched=Tag::where('name','like',$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('name',$ignore)->take(5)->get();

    return $tags_matched;

  }

}
