<?php

namespace App\Http\Controllers\Webhooks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Handlers\StripeHandler;
use App\WebhookEvent;

class StripeController extends Controller
{
  public function handle()
  {
    $payload = json_decode(@file_get_contents('php://input'));
    $url=request()->url();

    WebhookEvent::create([
      'gateway_id'=>1,
      'url'=>$url,
      'data'=>$payload
    ]);

    return response()->json([
      'message'=>'success',
      'event'=>$payload->type
    ],200);
  }
}
