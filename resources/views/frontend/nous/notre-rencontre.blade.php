@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="concept-section rencontre" id="top">
			<div class="container">
				
			</div>
	</section>
	
	

	<section class="faq-section rencontre-section">
		<div class="container">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">NOTRE RENCONTRE</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="sissy-inner">
						<h3 class="title-about">NOTRE 
							<strong>RENCONTRE</strong>
						</h3>
						<p>
							La première fois que Tini et moi nous sommes rencontrés, c’était bel et bien dans une salle de sport, pendant les vacances ! Très cliché, mais pourtant vrai. Manque de pot pour moi, je finissais ma séance toute transpirée lorsqu’ il est arrivé. Je ne me suis, comme vous vous en doutez, pas attardée ! En revanche, nous nous sommes rapidement revus en France dès notre retour. Nous avions énormément de points communs et plein de choses à nous raconter !
						</p>
						<p>
							Tous deux végétariens, passionnés de musculation, nous avions également fait les mêmes études, partagions la même ambition professionnelle et avions de nombreux traits en commun. Ayant tous deux un caractère assez explosif, notre histoire n’a pas été un long fleuve tranquille ! Mais 3 ans après notre rencontre, nous sommes plus unis que jamais, et prêts à nous lancer dans une nouvelle aventure !
						</p>

						<p>
							TRAIN SWEAT EAT est l’aboutissement de notre volonté commune de partager notre motivation, notre savoir, et surtout, notre passion ! Nous avons ainsi uni nos compétences très complémentaires pour vous proposer un site communautaire jamais créé en France, d’une qualité inégalée. Très fiers que ce projet voit enfin le jour après plus de deux ans de travail et réflexion, nous espérons que vous aimerez plus que tout vous entrainez avec nous, et surtout que vous deviendrez la meilleure version de vous même !
						</p>

					</div>
				</div>
				<div class="col-lg-6 sissy-img-hide">
					<div class="sissy-rightimg">
						<img src="/assets/img/nous-notre-rencontre-small.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
		
	</section>	

	@include('frontend.includes.parallax')	

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>TRAIN </span> SWEAT EAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="WE TRAIN">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>WE TRAIN</h2>
									<p>
										Quotidiennement, nous partageons avec vous nos <strong>entrainements</strong> en salle de musculation ! Ensemble, <strong>progressons chaque jour</strong> et restons <strong>surmotivés</strong> !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-heart" data-headline="WE SWEAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-heart">
									<h2>WE SWEAT</h2>
									<p>
										Retrouvez chaque mois en<strong> vidéo live</strong>, mes <strong>entrainements</strong> poids du corps favoris pour <strong>brûler un max de calories</strong> et <strong>sculpter son corps</strong> ! Transpirons ensembles dans votre salon !
									</p>
									<br>
									<p><a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-cutlery" data-headline="WE EAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-cutlery">
									<h2>WE EAT</h2>
									<p>
										Chaque mois, cuisinons ensembles de<strong> nouvelles recettes</strong> aussi fit que gourmandes ! Rapides et simples à réaliser, vous n’aurez plus d’excuses !
									</p>
									<br>
									<p><a href="{{ route('front.we-eat.recipes') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	


	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
@endsection

@section('footer')

<div/>

@endsection