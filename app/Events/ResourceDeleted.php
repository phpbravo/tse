<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ResourceDeleted
{
    use Dispatchable, SerializesModels;

    public $resourceable;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($resourceable)
    {
        
        $resource=\App\Resource::find($resourceable->resource_id);

        $resource->delete();
        
    }

}
