<?php

namespace PayPal\Api2;

use PayPal\Common\PayPalModel;
use PayPal\Core\PayPalConstants;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;


/**
 * Class Name
 *
 * A resource representing a name.
 *
 * @package PayPal\Api2
 *
 * @property string given_name
 * @property string surname
 */
class Name extends PayPalModel
{
    /**
     * Given name.
     */
    public function setGivenName(string $given_name): self
    {
        $this->given_name = $given_name;
        return $this;
    }

    /**
     * Given name.
     */
    public function getGivenName(): string
    {
        return $this->given_name;
    }

    /**
     * Surname.
     */
    public function setSurname(string $surname): self
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Surname.
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

}
