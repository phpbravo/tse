<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseAssignment extends Model
{
    protected $fillable=[
        'exercise_id',
        'exercise_set_id',
        'session_id',
        'order_index',
        'reps',
        'tempo',
        'description',
    ];

    public function exercise(){
        return $this->belongsTo('App\Exercise');
    }

    public function exercise_set(){
        return $this->belongsTo('App\ExerciseSet');
    }

    public function session(){
        return $this->belongsTo('App\Session');
    }
}
