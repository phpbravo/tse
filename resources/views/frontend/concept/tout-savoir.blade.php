@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="savoir-section toutsavoir" id="top">
		<div class="container">
			
		</div>
	</section>

	<section class="faq-section toutsavoircontent">
		<div class="container ">
			<div class="row ">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">LE CONCEPT</h1>
					<hr>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
						<h3 class="title-about">TRAIN SWEAT EAT</h3>
						
						<div class="savoir-innercontent">
							<p>
								Lorsque nous nous sommes rencontrés, une <strong>passion</strong> nous a vite réunis, le <strong>fitness</strong>. Etant deux accros à ce sport aussi passionnant qu’addictif, c’est tout naturellement que nous avons commencé à nous entrainer ensembles. Trois ans plus tard, nous sommes en couples et nous allons toujours à deux à la salle, faisons les mêmes séances et les mêmes exercices. Encore plus <strong>motivés</strong> qu’auparavant, nous avons néanmoins constaté que de nombreux fitgens se découragent rapidement dans <br>ce <strong> sport</strong>. En effet, beaucoup d’inscrits ne savent pas toujours comment s’entrainer en salle, ne savent pas quoi faire sans matériel ou encore quelle nutrition adopter. Ne voyant pas de résultats, ils finissent souvent par baisser les bras. Recevant de plus en plus de messages de notre communauté nous demandant de partager<br> nos <strong>entrainements</strong>, il nous est venu l’envie de créer <strong>TRAIN SWEAT EAT</strong>.
							</p>
							<p>
								<strong>TRAIN SWEAT EAT</strong> est un site communautaire où nous partageons chaque jour de la semaine notre<strong> workout journalier en salle, mais aussi tous les mois de nouvelles vidéos trainings en direct, ainsi que toutes nos recettes préférées !</strong>
							</p>
						</div>

						<div class="leconcept-innerimg" id="concept-bottomimg">
							<img src="/assets/img/concept2.jpg" class="img-fluid">
						</div>

						<div class="savoir-innercontent" id="saviorwesweatpart">
							<h3 class="title-about mb-2">WE SWEAT</h3>
							<p>
								« Vous n’allez pas en salle, et recherchez plutôt des séances au poids du corps ou avec peu de matériel ? Je vous propose dans<strong> WE SWEAT </strong> mes séances de<strong> sport à la maison</strong> redoutables pour <strong>sécher, se tonifier et se galber !</strong> Avec mes <strong> direct live</strong>, vous ne <strong>perdrez jamais la motivation</strong> et irez au bout de <strong>vos objectifs !</strong>
							</p>
							<p>
								Que vous ayez peu de temps pour vous entrainer, pas le budget pour aller en salle, ou juste envie de <strong>vous tonifier chez vous, WE SWEAT </strong> est fait pour vous ! Aimant les trainings rapides mais ultra intenses et efficaces, mes séances dépassent rarement 15min !
							</p>
							<p>
								WE SWEAT est composé de séance de renforcement musculaire, de cardio HIIT, de gainage, et de plein d’autres surprises qui arrivent prochainement. De quoi faire monter le cardio, brûler un max de calories, <strong>augmenter le métabolisme basal</strong>, tout en préservant sa masse musculaire.
							</p>
							<p>
								<strong>WE SWEAT</strong> peut être réalisé seul, ou comme pour moi en complément de WE TRAIN, en fonction des objectifs de chacun. Ainsi, vous pourrez adapter vos séances en fonction du temps dont vous disposez, de vos objectifs physiques, et du reste de votre routine.
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="leconcept-inner">
						<div class="leconcept-innerimg topimgs">
							<img src="/assets/img/Concept-trainsweateat.jpg" class="img-fluid">
						</div>
		
						<div class="savoir-innercontent" id="rightcontent">
							<h3 class="title-about mb-2">WE TRAIN</h3>
							<p>
								Sur <strong>WE TRAIN</strong>, vous découvrirez nos véritables entrainements, ceux qui nous font progresser, et qui vous donnera sans aucun doute autant de résultats. <strong>Tous les jours</strong>, vous avez accès à une<strong> nouvelle séance inédite en salle</strong>, constituée d’exercices de bases, comme d’exercices plus pointus, formant une séance redoutable, elle-même intégrée dans un programme structuré sur plusieurs mois.
							</p>
							<p>
								Vous ne connaissez pas un mouvement ? Pas de soucis, nous vous les avons tous filmés ! <strong>WE TRAIN</strong> nous permettra de toujours<strong> restés motivés ensembles</strong>, de nous<strong> challenger</strong> et surtout de<strong> ne jamais abandonner</strong>. Vous vous demandez sans doute si ce qui marche pour nous va forcément fonctionner pour vous. Sachez que nous n’avons jamais eu autant de résultats que depuis que nous nous entrainons ensembles.
							</p>
							<p>
								En effet, j’ai commencé la <strong>musculation</strong> comme la plupart des femmes, en faisant des impasses sur plusieurs muscles et en pensant que faire le booty à chaque séance me donnera le fessier de Jen Selter. J’ai vite compris en ne voyant aucun résultat qu’il était primordial pour les femmes comme pour les hommes de<strong> travailler le corps dans sa globalité.</strong>
							</p>

							<p>
								Oui, je parle aussi de vous, les <strong>fitguys </strong> qui faites les jambes aussi souvent que Booba. Notre programme commun, global et intensif apporte de véritables <strong>résultats visibles</strong> sur votre corps, pas seulement sur le papier !
							</p>
						</div>

						<div class="leconcept-innerimg" id="imgbottom">
							<img src="/assets/img/concept3.jpg" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<section class="passion-section toutsaviorparallax">
		<div class="row">
			<div class="col-lg-12">
				<div class="overlays">
					<div class="container">
						<div class="row">
							<div class="col-xl-5 col-lg-12 roundimg">
								<img src="/assets/img/weeat.jpg" class="img-fluid ">
							</div>
							<div class="col-xl-6 offset-xl-1 col-lg-12">
								<h3>WE EAT</h3>
								<p>
									Et enfin, comme un bon training va de pair avec une<strong> alimentation adaptée</strong>, nous vous proposons de découvrir dans<strong> WE EAT </strong> toutes nos meilleures <strong>recettes fitness, protéinées, healthy</strong> et surtout <strong>ultra gourmandes</strong>! Car oui il est tout à fait possible de se faire plaisir et de manger sain à la fois. Du brunch healthy, au diner exotique en passant par les collations protéinées, il y en a pour tous les goûts ! Découvrez tous les vendredis une nouvelle recette #vendrediveggieavecsissy et apprenez à <strong>cuisiner différemment</strong> pour enfin<strong> atteindre vos objectifs</strong> tout en prenant<strong> soin de votre santé</strong> !  Toutes nos recettes sont élaborées à base de<strong> végétaux</strong>, meilleures pour la santé et plus respectueuses de l’environnement, mais libre à vous de les adapter en version omnivore.
								</p>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="services" id="wetrainservice">
		<div class="container container">
			<div class="header-text text-center">
				<h1><span>TRAIN</span> SWEAT EAT</h1>
				<h4>REJOINS-NOUS !</h4>
			</div>
			<div class="divider text-center">
				<span class="outer-line"></span>
				<span class="fa fa-heart" aria-hidden="true"></span>
				<span class="outer-line"></span>
			</div>	
			<div class="row ">
				<div class="col-lg-12">
					<div class="st-inner">
						<div class="row services-box">
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-dumbbell" data-headline="WE TRAIN">&nbsp;</span>
								<div class="services-box-item-content fas fa-dumbbell">
									<h2>WE TRAIN</h2>
									<p>
										Découvrez chaque jour le training en salle de musculation réalisé par Sissy et Tini, adeptes et passionnés de musculation depuis<strong> 10 ans </strong>!
									</p>
									<p>
										Vous aussi, atteignez enfin vos objectifs en vous inspirant des<strong> meilleures méthodes de musculation</strong> existantes et en variant chaque jour votre training pour surprendre votre corps et ne jamais vous lasser !
									</p>
									<br>
									<p><a href="{{ route('front.we-train.videos') }}">Lire la suite</a></p>
								</div>
							</div> 
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fas fa-heart" data-headline="WE SWEAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-balance-scale">
									<h2>WE SWEAT</h2>
									<p>
										Accédez aux nombreuses vidéos à faire à la maison de la<strong> 1ere youtubeuse fitness française</strong> dont le précédent programme a été vu plus de<strong> 44 millions de fois</strong> !
									</p>
									<p>
										Grâce à son énergie, son expérience et surtout la qualité de ses séances, restez motivés et transformez-vous avec plaisir !
									</p>
									<br>
									<p id="servicelinkbottom"><a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-12 services-box-item">
								<span class="services-box-item-cover fa fa-cutlery" data-headline="WE EAT">&nbsp;</span>
								<div class="services-box-item-content fa fa-cutlery">
									<h2>WE EAT</h2>
									<p>
										Abs are made in the kitchen ! Pour optimiser vos résultats au maximum, Sissy et Tini vous ouvrent la porte de leur cuisine ! 
									</p>
									<p>
										<strong>Stop les régimes</strong> et recettes minceur, on adopte désormais une <strong>alimention fitness</strong>, riche en bons nutriments et <strong>protéines</strong>, et surtout ultra <strong>gourmandes </strong>! On oublie la privation et on fait du bien autant à son corps qu'à son esprit !
									</p>
									<br>
									<p><a href="{{ route('front.we-eat.recipes') }}">Lire la suite</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.includes.parallax-last')	

	<a href="#" class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></a>
	@endsection
	
	@section('footer')
	
	<div/>
	
	@endsection