<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\{
    Offer,
    Transaction
};

class Purchase extends Model
{

    protected $dispatchesEvents = [

    ];

    protected $fillable = [
        'user_id',
        'offer_id',
        'plan_id',
        'transaction_id',
        'is_active',
        'is_paused',
        'is_lifetime',
        'last_pause',
        'paused_total',
        'duration',
        'effective_on',
        'expires_on',
        'cancelled_on',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    // Simple Relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    // Scoped Relations

        // none

    // Attributes

    public function getIsExpiredAttribute()
    {
        $expiry_date = Carbon::parse($this->expires_on);
        $now = now();
        
        $is_expired = $expiry_date < $now;

        return $is_expired;
    }

    public function getIsNotExpiredAttribute()
    {
        $is_expired = $this->is_expired;

        return !$is_expired;
    }

    public function getIsNotActiveAttribute()
    {
        $is_active = $this->is_active;

        return !$is_active;
    }

    public function getTotalDurationInDaysAttribute()
    {
        $start_date = Carbon::parse($this->effective_on);
        $end_date = Carbon::parse($this->expires_on);

        $total_days = $start_date->diffInDays($end_date);

        return $total_days;
    }

    public function getRemainingDurationInDaysAttribute()
    {
        $now = now();
        $end_date = Carbon::parse($this->expires_on);

        $remaining_days = $now->diffInDays($end_date);

        if ($remaining_days < 0)
            $remaining_days = 0;

        return $remaining_days;
    }

    public function getRemainingDurationInPercentAttribute()
    {
        $remaining_percent = bcdiv(
            $this->remaining_duration_in_days,
            $this->total_duration_in_days,
            4
        );

        return $remaining_percent;
    }

    public function getHasTransactionAttribute()
    {
        $transaction = $this->transaction;

        if (!$transaction)
            return false;
        else
            return true;
    }

    public function getHasNoTransactionAttribute()
    {
        $has_transaction = $this->has_transaction;

        return !$has_transaction;
    }

    public function getRefundableAmountAttribute()
    {
        if (
            $this->is_not_active ||
            $this->is_expired ||
            $this->has_no_transaction
        )
            return 0;

        $credit_due = bcdiv(
            bcmul(
                $this->transaction->net_amount - $this->transaction->fee,
                $this->remaining_duration_in_percent,
                2
            ),
            100,
            2
        );
        
        return $credit_due;
    }


    // Scopes

    public function scopeActive($query)
    {
		return $query->where('is_active',true);
	}

    public function scopeInactive($query)
    {
		return $query->where('is_active',false);
	}

    // Instance Methods

    public function reschedule(Carbon $date)
    {
        $offer = $this->offer;
        $expiry = $offer->is_lifetime ? null :
            $date->copy()->addMonthsNoOverflow($this->duration);

        $this->update([
            'effective_on' => $date,
            'expires_on' => $expiry
        ]);
    }

    public function cancel()
    {
        $this->update([
            'is_active'=>false,
            'is_paused'=>false,
            'cancelled_on'=>now(),
        ]);
    }

    // Static Methods

    public static function createFromTransaction(
        Transaction $transaction
    ): self {
        $offer = $transaction->offer;
        $user = $transaction->user;

        $purchase = self::createFromOffer($offer, $user);

        $purchase->update([
            'transaction_id' => $transaction->id
        ]);

        return $purchase;
    }

    public static function createFromOffer(Offer $offer, User $user): self
    {
        $today = now();
        $expiry = $offer->is_lifetime ? null :
            now()->addMonthsNoOverflow($offer->duration);

        return self::create([
            'user_id'=>$user->id,
            'offer_id'=>$offer->id,
            'plan_id'=>$offer->plan_id,
            'transaction_id'=>null,
            'is_active'=>true,
            'is_paused'=>false,
            'is_lifetime'=>$offer->is_lifetime,
            'paused_total'=>'00000000',
            'duration'=>$offer->duration,
            'effective_on'=>$today,
            'expires_on'=>$expiry,
        ]);
    }
}
