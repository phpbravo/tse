<?php

namespace App\Http\Middleware\API\v0;

use Closure;
use App\User;

class Main
{

    public function authFailed($email,$password){
        return response()->json(
            [
                'success'=>false,
                'data'=>[
                    'errors'=>['username'=>$email,'password'=>$password]
                ]
            ],
            403);
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        auth()->shouldUse('api');
        $is_base64 = request()->get('base64');
        
        if ($is_base64) {
            $password=base64_decode(request()->get('password'));
        }else {
            $password=request()->get('password');
        }


        $email=request()->get('username');
        

        $user=User::where('email',$email)->orWhere('username',$email)->first();

        if (!$user)
            return $this->authFailed($email,'');
        // Authenticate

        if(app('hash')->check($password,$user->password)){
            // Auth passed
            auth()->setUser($user);
            return $next(request());
        }else{
            // Auth failed
            return $this->authFailed('',$password);
        }

    }
}
