<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailedMobileSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_mobile_subscriptions', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('gateway_id');
                $table->foreign('gateway_id')->references('id')->on('gateways');
            $table->unsignedBigInteger('appstore_receipt_id')->nullable();
                $table->foreign('appstore_receipt_id')
                    ->references('id')
                    ->on('appstore_receipts');
            $table->unsignedBigInteger('offer_id')->nullable();
                $table->foreign('offer_id')
                    ->references('id')
                    ->on('offers');
            $table->string('vendor_token')->nullable()->index();
            $table->text('error')->nullable();
            $table->boolean('is_recovered')->default(false)->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_mobile_subscriptions');
    }
}
