<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Jobs\CreatePaypalOffer;
use App\Jobs\CreateStripeOffer;

class OfferCreated
{
	use Dispatchable, SerializesModels;

	public $offer;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($offer)
	{
		
		dispatch(new CreatePaypalOffer($offer))->onConnection('sync');
		dispatch(new CreateStripeOffer($offer))->onConnection('sync');
		
	}

}
