<?php

namespace App\Http\Controllers\Admin;

use App\BlogPost;
use App\Handlers\FilterHandler;
use App\Handlers\ObjectHandler;
use App\Handlers\PageHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogPostsController extends Controller
{
    public function index()
    {
        $filterHandler = new FilterHandler(BlogPost::class);
        $pageHandler = new PageHandler(BlogPost::class);
        $objectHandler = new ObjectHandler(BlogPost::class);

        $filters = request()->filters;
        $search = request()->search;
        $perPage = request()->perPage;
        $page = request()->page;
        $orderBy = request()->orderBy;
        $ascending = request()->ascending;

        $filtered_query = $filterHandler
            ->getFilteredQuery($filters, $search, $orderBy, $ascending);


        $page=$pageHandler
            ->getPage($filtered_query, $perPage, $page);

        $page_object=$objectHandler
            ->newPage(
                $page->items,
                $page->current_page,
                $page->last_page,
                $page->total_item_count
            );

        return $page_object;
    }

    public function store(Request $request)
    {
        return BlogPost::create([
            'name' => $request->name,
            'title' => $request->title,
            'content' => $request->content,
        ]);

    }

    public function update(Request $request, $id)
    {
        $blog = BlogPost::find($id)->update([
            'name' => $request->name,
            'title' => $request->title,
            'content' => $request->content,
        ]);
    }

    public function delete($id)
    {
        $blog = BlogPost::find($id);
        $blog->delete();
    }
}
