<?php

if (! function_exists('filever')) {
	function filever($path) {
		$mtime=filemtime(base_path($path));
		$version=base_convert($mtime,10,36);

		return $version;
	}
}