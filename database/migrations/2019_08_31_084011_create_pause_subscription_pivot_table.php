<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePauseSubscriptionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pause_subscription', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pause_id');
                $table->foreign('pause_id')->references('id')->on('pauses');
            $table->unsignedBigInteger('subscription_id');
                $table->foreign('subscription_id')->references('id')->on('subscriptions');
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('pause_subscription');
        
    }
}
