<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\Admin::create([
            'name'=>'Administrator',
            'username'=>'admin',
            'email'=>'admin@example.org',
            'password'=>bcrypt('admin'),
        ]);

        //  Seed Resources
        $resources=[
            'All Categories'=>'Category',
            'All Subcategories'=>'Subcategory',
            'All Recipe Categories'=>'RecipeCategory',
            'All Exercises'=>'Exercise',
            'All Recipes'=>'Recipe',
            'All Books'=>'Book',
            'All Programs'=>'Program',
            'All Sessions'=>'Session',
            'All Pages'=>'Pages',
        ];
        foreach ($resources as $name => $type) {
            factory(\App\Resource::class)->create(['name'=>$name,'resource_object_type'=>$type]);
        }

        //  Seed Categories
        $we_train = \App\Category::create([
            'name'=>'We Train',
            'title'=>'We Train',
        ]);

        $we_sweat = \App\Category::create([
            'name'=>'We Sweat',
            'title'=>'We Sweat',
        ]);

        $bikini = \App\Category::create([
            'name'=>'Biki Avec Sissy',
            'title'=>'Biki Avec Sissy',
        ]);

        $we_flow = \App\Category::create([
            'name'=>'We Flow',
            'title'=>'We Flow',
        ]);

        $we_sweat_mobile = \App\Category::create([
            'name'=>'We Sweat Mobile',
            'title'=>'We Sweat',
        ]);
        
        // Seed Sections
        $this->call(SectionSeeder::class);

        // Seed Books
        $this->call(BookSeeder::class);

        // Seed Products
        $this->call(ProductSeeder::class);

        // Seed Plans
        $this->call(PlanSeeder::class);
    }
}
