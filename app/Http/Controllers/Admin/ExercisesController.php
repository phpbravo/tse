<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use App\Exercise;

class ExercisesController extends Controller
{

	private function validateExercise($fields){
		$objectHandler=new ObjectHandler(Exercise::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if (!isset($fields->category_id) || $fields->category_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Catégorie est obligatoire'
			);
		}

		if (!isset($fields->subcategory_id) || $fields->subcategory_id==''){
			return $objectHandler->newError(
				'invalid_request',
				'Sous-catégorie est obligatoire'
			);
		}

		if(!\App\Category::find($fields->category_id)){
			return $objectHandler->newError(
				'invalid_request',
				'La catégorie est invalide.'
			);
		}
		
		if(!\App\Subcategory::where('category_id',$fields->category_id)->find($fields->subcategory_id)){
			return $objectHandler->newError(
				'invalid_request',
				'La sous-catégorie est invalide.'
			);
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Exercise::class);
		$pageHandler=new PageHandler(Exercise::class);
		$objectHandler=new ObjectHandler(Exercise::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page->items->makeHidden('content');

		$page->items->each->append('video_name');

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateExercise($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$exercise=Exercise::create([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_id'=>$fields->video_id?:null,
		]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$exercise->tag($tag);
			}
		}

		return $exercise;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateExercise($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$exercise=Exercise::find($id);

		if(!$exercise){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		$exercise->update([
			'category_id'=>$fields->category_id,
			'subcategory_id'=>$fields->subcategory_id,
			'name'=>$fields->name,
			'description'=>$fields->description,
			'content'=>$fields->content,
			'difficulty'=>$fields->difficulty,
			'duration'=>$fields->duration,
			'video_id'=>$fields->video_id?:null,
		]);

		$exercise->tags()->sync([]);

		if (isset($fields->tags) && is_array($fields->tags)){

			foreach ($fields->tags as $tag){

				if (!is_string($tag)) continue;
				$exercise->tag($tag);
			}
		}

		return $exercise;
	}

	public function show($id){

		$exercise=Exercise::with(['tags','video'])->find($id);

		if(!$exercise){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		return $exercise;
	}

	public function destroy($id){

		$exercise=Exercise::with(['tags'])->find($id);
		
		if(!$exercise){
			return (new ObjectHandler(Exercise::class))->newError('not_found');
		}

		$exercise->delete();

		return (new ObjectHandler(Exercise::class))->newSuccess();
	}

	public function tag($id,$name){

		$exercise=Exercise::findOrFail($id);

		$exercise->tag($name);

		return $exercise;

	}

	public function untag($id,$name){

		$exercise=Exercise::findOrFail($id);

		$exercise->untag($name);

		return $exercise;

	}
	
	// returns an array of autocomplete matches
	public function autocomplete()
	{
		$search=mb_strtolower(trim(request()->search));
		$ignore=request()->ignore?:[];

		if (!$search) return json_encode((object)[]);

		$tags_matched=Exercise::where('name','like','%'.$search.'%')->orderBy(DB::raw('length(name)'))->whereNotIn('id',$ignore)->take(5)->get();

		return $tags_matched;
	}

}
