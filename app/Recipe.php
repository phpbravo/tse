<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
	];

	// attributes that are mass-assignable
	public $fillable = [
		'recipe_category_id',
		'name',
		'description',
		'preparation_time',
		'cook_time',
	];

	// attributes that can be searched and filtered
	public $filterable_columns = [
		'name',
		'description',
	];

	public function sections(){
		return $this->belongsToMany('App\Section')->withPivot('id','content','order_index');
	}

	public function recipe_category(){
		return $this->belongsTo('App\RecipeCategory');
	}

	public function tags(){
		return $this->belongsToMany('App\Tag');
	}

	public function tag($tag_name){
		
		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first()?:Tag::create(['name'=>$tag_name]);

		$this->tags()->syncWithoutDetaching($tag);

		return $this;
	}

	public function untag($tag_name){

		$tag_name=mb_strtolower(trim($tag_name));
		if($tag_name=='') return $this;

		$tag=Tag::where('name',$tag_name)->first();
		if(!$tag) return $this;

		$this->tags()->detach($tag);

		return $this;
	}
	
}
