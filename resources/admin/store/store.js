import vx_currentUser from './modules/currentUser.js';
import vx_actions from './actions.js';

const vx_store={
    modules:{
        vx_currentUser,
    },
    actions:vx_actions
};

export default vx_store;