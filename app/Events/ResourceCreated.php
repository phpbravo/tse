<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ResourceCreated
{
    use Dispatchable, SerializesModels;

    public $resourceable;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($resourceable)
    {
        
        $resource=\App\Resource::create([
            'name'=>$resourceable->name,
            'resource_object_type'=>(new \ReflectionClass($resourceable))->getShortName(),
            'resource_object_id'=>$resourceable->id,
            'created_at'=>$resourceable->created_at,
            'updated_at'=>$resourceable->updated_at,
        ]);

        $resourceable->resource_id=$resource->id;
        $resourceable->save();
        
    }

}
