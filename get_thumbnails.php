<?php
use Artisan;
use Carbon\Carbon;
use File;

$recipes_url = 'https://www.trainsweateat.com/index.php?option=com_osmembership&task=api.recipies&username=ohm.conception&password=ohmtest&api_key=tse2019&catid=2';


$path = storage_path('app/recipes');

//checking thumbnail files and directory
if (File::isDirectory($path)) {
    foreach (glob($path.'/*') as $folder) {
        foreach (glob($folder.'/*') as $imageFolder) {
            foreach (glob($imageFolder.'/*') as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
            rmdir($imageFolder);
        }
        rmdir($folder);
    }
}else {
    File::makeDirectory($path, 0777, true, true);
}
'thumbnails cleared';

$recipe_result = json_decode(file_get_contents($recipes_url)); 'recipe requested';

foreach ($recipe_result->data->list as $object) {
    $recipe = $object->item;
    $tags = $object->tags;

    $new_recipe = Recipe::find($recipe->id);

    //check if folder exists
    $thumbnail_path = $path.'/'.$new_recipe->id.'/image';
    $small_image_url = 'https://www.trainsweateat.com'.$recipe->thumbnail->small;
    $main_image_url = 'https://www.trainsweateat.com'.$recipe->thumbnail->large;
    if (!File::isDirectory($thumbnail_path)) {
        File::makeDirectory($thumbnail_path, 0777, true, true);
    }
    //save thumbnail
    $small_thumbnail = $thumbnail_path.'/0_main_thumb.jpg';
    $main_image = $thumbnail_path.'/0_main_image.jpg';
    file_put_contents($small_thumbnail, file_get_contents($small_image_url));
    file_put_contents($main_image, file_get_contents($main_image_url));
}
