@extends('frontend.includes.layout')
@section('content')
<div class="wrapper">
@include('frontend.includes.navbar')
	
	<section class="wesweat-section wetrainvid" id="top">
			<div class="container">
			</div>
	</section>
	
	

	<section class="faq-section" id="wetrainvids">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-center mb-5">VIDÉOS DÉMO</h1>
					<hr class="line2">	
				</div>
			</div>

	

			<div class="row categoriesbtn">
				
				<div class="col-lg-12 text-center" id="recettesfitcol">
					<a
						@click.prevent="changeCategory(0)"
						href="#"
						class="btn btn-secondary recettesfitbtn"
					>
						<h4>Toutes les vidéos</h4>
					</a>
				</div>

				<div class="col-xl-2 offset-xl-1 col-lg-2 offset-lg-1 col-md-3 col-sm-4 col-6  col-4 recettescol">
					<a
						@click.prevent="changeCategory(9)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Bras</h4>
							<img src="/assets/img/bras.png">
						</div>
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(12)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Dos</h4>
							<img src="/assets/img/dos.png">
						</div>
						
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(7)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Jambes</h4>
							<img src="/assets/img/jambes.png">
						</div>
					</a>
				</div>
				
				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(17)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Pecs</h4>
							<img src="/assets/img/pecs.png">
						</div>
					</a>
				</div>

				<div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-6 recettescol">
					<a
						@click.prevent="changeCategory(18)"
						href="#"
					>
						<div class="categoriesbtn-item">
							<h4>Épaules</h4>
							<img src="/assets/img/epaules.png">
						</div>
					</a>
				</div>


			</div>


			<div class="row searchrow">
				<div class="col-lg-12 searchcol">
					<div class="searhinner">
						<p>
						OU FAITES UNE RECHERCHE
						</p>
						<form>
							<div class="input-group mb-3">
							  <input type="text" class="form-control" placeholder="Bras biceps curl.." aria-label="Recipient's username" aria-describedby="basic-addon2">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary searchtbtn" type="button"><i class="fa fa-search"></i></button>
							  </div>
							</div>
						</form>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col-lg-12 px-0">
					<div class="materialouter">
						<div class="materialinner">
							<h3>VIDÉOS</h3>

							<div>
								<p>Tu ne connais pas un exercice ? Nous nous sommes filmés en train de le réaliser pour t'aider à le reproduire.</p>

								<p>
									Ici, tu retrouveras de courtes vidéos illustrant nos exercices les plus courants ! N’hésite pas à les consulter si tu ignores certains mouvements. Néanmoins, n’oublie pas que l’exécution et l’amplitude peuvent varier en fonction de la morphologie et de la flexibilité de chacun. N’hésite pas à demander conseil à un coach diplômé afin de vérifier ton placement.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div v-if="loading" class="d-flex" style="position:relative; width:100%; justify-content:center;" id="loading-icon">
				<div class="loader4"></div>
			</div>
			<!---- seance video list ----->
			<div class="row wetrainvideos-items" id="wetrainvidscontainer">
				
				<div
					v-for="exercise in videos"
					:key="exercise.id"
					class="col-lg-6 col-md-12"
				>
					<div class="vidlists">
						<h4> <a style="color:#060003;" :href="`/video/${exercise.id}`">@{{exercise.name}}</a></h4>
						<div class="embed-responsive embed-responsive-16by9">
							<iframe :src="`https://player.vimeo.com/video/${exercise.video.vimeo_id}?title=0&amp;byline=0&amp;portrait=0`" width="540px" height="305px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						</div>
					</div>
				</div>
				
			</div>

			<div class="row paginationrow">
				<div class="col-lg-12">
					<nav aria-label="Page navigation" class="text-center">
					  <ul class="pagination vidspagination" id="pagenos">
					    <li class="page-item">
										<a
											@click.prevent="changePage(selected_page-1)"
											href="#"
											aria-label="Previous"
											class="page-link"
											>
					        <span aria-hidden="true">&laquo;</span>
					        <span class="sr-only">Previous</span>
					      </a>
					    </li>
									<li
										v-for="this_page in last_page"
										:key="this_page"
										class="page-item"
									><a @click.prevent="changePage(this_page)" class="page-link" style="cursor:pointer" :class="{'active':(this_page==selected_page)}">@{{this_page}}</a></li>
					    <li class="page-item">
										<a
											@click.prevent="changePage(selected_page+1)"
											href="#"
											aria-label="Next"
											class="page-link"
											>
					        <span aria-hidden="true">&raquo;</span>
					        <span class="sr-only">Next</span>
					      </a>
					    </li>
					  </ul>
					</nav>
				</div>
			</div>
		</div>
		
	</section>

	

	

	

	<button class="btn btn-primary" id="mybtntop"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-up"></i></button>
	@endsection
	
	@section('footer')
	
	<div/>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="/assets/js/we-train-videos.js?v={{filever('public/assets/js/we-train-videos.js')}}"></script>
	
@endsection