interface data{
    is_loading: boolean
    loading_percent?: number
}

export default {
    data(): data{
        return {
            is_loading: false,
            loading_percent: null,
        }
    },
    methods:{
        startLoading(){
            this.is_loading = true;
        },
        stopLoading(){
            this.is_loading = false;
        }
    }

}