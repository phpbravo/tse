<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('exercise_id');
                $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->unsignedBigInteger('exercise_set_id');
                $table->foreign('exercise_set_id')->references('id')->on('exercise_sets')->onDelete('cascade');
            $table->unsignedBigInteger('session_id');
                $table->foreign('session_id')->references('id')->on('sessions')->onDelete('cascade');
            $table->integer('order_index')->default(0);
            $table->string('reps')->nullable();
            $table->string('tempo')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('exercise_assignment');
        
    }
}
