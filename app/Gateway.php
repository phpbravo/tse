<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    const
        STRIPE = 1, 
        PAYPAL = 2,
        APPSTORE = 3, 
        PLAYSTORE = 4, 
        FREE = 5;
        
    protected $fillable=[
        'name'
    ];
}
