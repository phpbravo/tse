var we_sweat_videos_app= new Vue({
	el:'#wesweatvids',
	data:{
		loading:true,
		subcategories:[],
		videos:[],
		selected_category:0,
		selected_page:1,
		last_page:1,
		search:'',
	},
	methods:{
		changeCategory(category_id){
			this.selected_category=category_id;
			this.getVideos()
		},
		changePage(page){
			if (page<1)
				page=1
			if (page>this.last_page)
				page=this.last_page
			this.selected_page=page
			this.getVideos()
			this.$nextTick(()=>{
				this.selected_page=99;
				this.$nextTick(()=>{
					console.log(this.selected_page)
					this.selected_page=page
				})
			})
		},
		getSubcategories(){
			return axios.get('/api/we_train/subcategories')
			.then(res=>{
				this.subcategories=res.data
			})
		},
		getVideos(){
			filters=[];
			if (this.selected_category)
				filters.push({field:'subcategory_id','value':this.selected_category})
			filters.push({field:'category_id','value':2})
			this.loading=true
			return axios.get('/api/we_train',{params:{
				filters,
				search:this.search,
				page:this.selected_page
			}})
			.then(res=>{
				this.videos=res.data.contents
				this.selected_page=res.data.current_page
				this.last_page=res.data.last_page
			})
			.finally(()=>{
				this.loading=false;
			})
		}
	},
	created(){
		this.getSubcategories()
		.then(()=>{
			this.getVideos()
		})
	}
})