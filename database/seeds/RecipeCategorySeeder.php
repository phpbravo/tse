<?php

use Illuminate\Database\Seeder;

class RecipeCategorySeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		// Seed Subcategories
		\App\RecipeCategory::create([
			'name'=>'Petits dej\' et brunchs',
		]);
		\App\RecipeCategory::create([
			'name'=>'Les classiques de sissy',
		]);
		\App\RecipeCategory::create([
			'name'=>'«chat meal» healthy',
		]);
		\App\RecipeCategory::create([
			'name'=>'Les exofit\'',
		]);
		\App\RecipeCategory::create([
			'name'=>'Collations healthy fit\'',
		]);
		\App\RecipeCategory::create([
			'name'=>'Desserts pour le plaisir',
		]);
			
	}
}
