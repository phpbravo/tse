var login_app= new Vue({
	el:'#login-app',
	data:{
		loading:false,
		errors:[],
		user:{
			login:'',
			password:'',
		},
	},
	methods:{
		login(){
			if (this.loading)
				return
			this.loading=true

			axios.post('/api/login',this.user)
			.then(response=>{
				routePush(response.data.homepage_url?response.data.homepage_url:"/")
			})
			.catch(error=>{
				this.loading=false
				let error_content=error.response.data.error_content
				if (!error_content)
					return
				this.errors.push(error_content.message)
			})

		}
	},
	mounted(){
		document.getElementById('login-app').className="login-wrapper"
	},
	created(){
	}
})