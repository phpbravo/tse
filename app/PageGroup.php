<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageGroup extends Model
{
	protected $dispatchesEvents=[
		'created' => Events\ResourceCreated::class,
		'deleted' => Events\ResourceDeleted::class,
	];

    protected $fillable=[
        'resource_id',
        'name'
    ];

    public function pages(){
        return $this->belongsToMany('App\Page');
    }
}
