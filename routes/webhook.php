<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Webhook Routes
|--------------------------------------------------------------------------
|
| These are the routes where webhooks can reach the application.
|
*/

Route::post('/'.config('webhook.stripe_endpoint'),'Webhooks\StripeController@handle');
Route::post('/'.config('webhook.paypal_endpoint'),'Webhooks\PaypalController@handle');

Route::post(
    '/'.config('webhook.paypal_legacy_endpoint'),
    'Webhooks\PaypalController@handle_legacy'
);

Route::post(
    '/'.config('webhook.appstore_legacy_endpoint'),
    'Webhooks\LegacyAppstoreController@handle'
);

Route::post(
    '/'.config('webhook.playstore_legacy_endpoint'),
    'Webhooks\LegacyPlaystoreController@handle'
);