<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Handlers\PaypalHandler;

class CreatePaypalPlan implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $plan;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($plan)
	{
		$this->plan=$plan;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(PaypalHandler $handler)
	{
		$handler->createGatewayPlan($this->plan);
	}
}
