<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{Offer, OfferSchedule, Plan, User};

class OffersController extends Controller
{

	private function validateOffer($fields){
		$objectHandler=new ObjectHandler(Offer::class);

		if (!isset($fields->name) || $fields->name==''){
			return $objectHandler->newError(
				'invalid_request',
				'Nom est obligatoire'
			);
		}

		if (!isset($fields->price) || $fields->price==''){
			return $objectHandler->newError(
				'invalid_request',
				'Prix est obligatoire'
			);
		}

		if (!isset($fields->tax_percent) || $fields->tax_percent==''){
			return $objectHandler->newError(
				'invalid_request',
				'Purrcentage de taxe est obligatoire'
			);
		}

		if(
			(!isset($fields->is_lifetime) || !$fields->is_lifetime) &&
			(!isset($fields->duration) || !intval($fields->duration,10))
		){
			return $objectHandler->newError(
				'invalid_request',
				'Durée est invalide'
			);
		}

		if (isset($fields->new_plan) && $fields->new_plan){
		
			if (!isset($fields->plan['name']) || $fields->plan['name']==''){
				return $objectHandler->newError(
					'invalid_request',
					'Nom du plan est obligatoire'
				);
			}
		
		}

		if(isset($fields->offer_schedules)){
			try{
				
				foreach ($fields->offer_schedules as $offer_schedule){
					$from=new Carbon($offer_schedule['from'],'GMT+2');
					$to=new Carbon($offer_schedule['to'],'GMT+2');
					if ($to<=$from){
						return $objectHandler->newError(
							'invalid_request',
							'La date à doit être postérieure à la date du'
						);
					}
				}
			}catch(\Exception $exception){
				return $objectHandler->newError(
					'invalid_request',
					'Calendrier est invalide'
				);
			}
		}
		
		return false;
	}

	public function index(){
		$filterHandler=new FilterHandler(Offer::class);
		$pageHandler=new PageHandler(Offer::class);
		$objectHandler=new ObjectHandler(Offer::class);

		$filters=request()->filters;
		$search=request()->search;
		$perPage=request()->perPage;
		$page=request()->page;
		$orderBy=request()->orderBy;
		$ascending=request()->ascending;

		$filtered_query=$filterHandler->getFilteredQuery($filters,$search,$orderBy,$ascending);

		$page=$pageHandler->getPage($filtered_query,$perPage,$page);

		$page_object=$objectHandler->newPage($page->items,$page->current_page,$page->last_page,$page->total_item_count);

		return $page_object;
	
	}

	public function store(){
		
		$fields=(object)request()->all();
		
		$validation=$this->validateOffer($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$offer=Offer::make([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'price'=>$fields->price,
			'tax_percent'=>$fields->tax_percent,
			'duration'=>$fields->duration ?? '0',
			'subscriber_limit'=>$fields->subscriber_limit,
			'is_limited'=>$fields->is_limited,
			'is_recurring'=>$fields->is_recurring && !$fields->is_lifetime,
			'is_lifetime'=>$fields->is_lifetime,
			'is_public'=>$fields->is_public,
			'is_for_upgrade'=>$fields->is_for_upgrade,
			'is_available'=>$fields->is_available,
			'is_stripe_available'=>$fields->is_stripe_available,
			'is_paypal_available'=>$fields->is_paypal_available,
		]);

		if (isset($fields->new_plan) && $fields->new_plan){

			$plan=Plan::create([
				'name'=>$fields->plan['name']
			]);

			$offer->plan_id=$plan->id;

			foreach ($fields->plan['products'] as $product){

				$plan->products()->attach($product['id']);

			}

		}else{

			$offer->plan_id=$fields->plan['id'];

		}

		$offer->save();

		if (
			isset($fields->offer_schedules) &&
			is_array($fields->offer_schedules) &&
			count($fields->offer_schedules)>0
		){

			$is_available=false;

			foreach ($fields->offer_schedules as $offer_schedule){
				$now=now();

				if(!isset($offer_schedule['is_custom'])){
					$offer_schedule['is_custom']=false;
				}

				$from=new Carbon($offer_schedule['from'],config('app.timezone'));
				$to=new Carbon($offer_schedule['to'],config('app.timezone'));
				$is_custom=$offer_schedule['is_custom'];
				$is_available=$is_available||($from<=$now)&&($to>=$now);
				$title=$is_custom?$offer_schedule['title']:'';
				$description=$is_custom?$offer_schedule['description']:'';

				OfferSchedule::create([
					'offer_id'=>$offer->id,
					'from'=>$from,
					'to'=>$to,
					'is_custom'=>$is_custom,
					'title'=>$title,
					'description'=>$description,
				]);

			}
				
			$offer->update(['is_available'=>$is_available]);

		}

		return $offer;
	}

	public function update($id){

		$fields=(object)request()->all();

		$validation=$this->validateOffer($fields);

		// if there were errors in validation, return them.
		if ($validation){
			return $validation;
		}

		$offer=Offer::find($id);

		$offer->update([
			'name'=>$fields->name,
			'title'=>$fields->title,
			'description'=>$fields->description,
			'price'=>$fields->price,
			'tax_percent'=>$fields->tax_percent,
			'duration'=>$fields->duration,
			'subscriber_limit'=>$fields->subscriber_limit,
			'is_limited'=>$fields->is_limited,
			'is_public'=>$fields->is_public,
			'is_for_upgrade'=>$fields->is_for_upgrade,
			'is_available'=>$fields->is_available,
		]);

		if (isset($fields->new_plan) && $fields->new_plan){

			$plan=Plan::create([
				'name'=>$fields->plan['name']
			]);

			$offer->update(['plan_id'=>$plan->id]);

			foreach ($fields->plan['products'] as $product){

				$plan->products()->attach($product['id']);

			}

		}else{

			$offer->update(['plan_id'=>$fields->plan['id']]);

		}

		$offer->offer_schedules()->delete();

		if (
			isset($fields->offer_schedules) &&
			is_array($fields->offer_schedules) &&
			count($fields->offer_schedules)>0
		){

			$is_available=false;

			foreach ($fields->offer_schedules as $offer_schedule){
				$now=now();

				if(!isset($offer_schedule['is_custom'])){
					$offer_schedule['is_custom']=false;
				}

				$from=new Carbon($offer_schedule['from'],config('app.timezone'));
				$to=new Carbon($offer_schedule['to'],config('app.timezone'));
				$is_custom=$offer_schedule['is_custom'];
				$is_available=$is_available||($from<=$now)&&($to>=$now);
				$title=$is_custom?$offer_schedule['title']:'';
				$description=$is_custom?$offer_schedule['description']:'';

				OfferSchedule::create([
					'offer_id'=>$offer->id,
					'from'=>$from,
					'to'=>$to,
					'is_custom'=>$is_custom,
					'title'=>$title,
					'description'=>$description,
				]);
			
			}

			$offer->update(['is_available'=>$is_available]);
			
		}

		return $offer;
	}

	public function show($id)
	{
		$offer=Offer::with(['plan.products','offer_schedules','gateway_offers'])->find($id);

		if(!$offer)
		{
			return (new ObjectHandler(Offer::class))->newError('not_found');
		}

		$offer->append([
			'active_subscription_count',
			'inactive_subscription_count',
			'subscription_count'
		]);

		return $offer;
	}

	public function list(){

		return Offer::all();

	}
	
	public function getOffersAvailableForUser($id)
	{
		$user = User::with('subscriptions.offer')->find($id);
		if (!$user) abort(404);
		
		$user_plans = [];
		foreach ($user->subscriptions as $subcription) {
			if ($subcription->offer) {
				array_push($user_plans, $subcription->offer->plan_id);
			}
		}

		$search=mb_strtolower(trim(request()->search));
		$one_time=request()->one_time?:false;
		$recurring=request()->recurring?:false;

		if (!$search) return json_encode((object)[]);

		$offers_matched=Offer::where('name','like','%'.$search.'%')
		->orderBy(DB::raw('length(name)'))
		->whereNotIn('plan_id',$user_plans);
		
		if ($recurring) 
			$offers_matched->recurring();

		if ($one_time)
			$offers_matched->oneTime();

		$offers_matched = $offers_matched->take(5)
		->get();
		
		return $offers_matched;
	}

}
