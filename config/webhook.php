<?php

return [

    'stripe_endpoint' => env('STRIPE_WEBHOOK_ENDPOINT'),

    'paypal_endpoint' => env('PAYPAL_WEBHOOK_ENDPOINT'),

    'paypal_legacy_endpoint' => env('PAYPAL_LEGACY_WEBHOOK_ENDPOINT'),

    'test_mode' => env('WEBHOOK_TEST_MODE'),

    'stripe' => [
        
        'supported_events' => [
    
            'invoice.payment_action_required',
            'invoice.payment_failed',
            'invoice.payment_succeeded',
    
        ]

    ],

    'playstore' => [

        'supported_events' => [

            'legacy_SUBSCRIPTION_RENEWED',
            'legacy_SUBSCRIPTION_CANCELED',
            'legacy_SUBSCRIPTION_REVOKED'

        ]

    ],

    'appstore' => [

        'secret' => env('APPSTORE_SECRET'),

        'supported_events' => [

            'legacy_INITIAL_BUY',

        ]

    ],

    'mobile_legacy_endpoint' => env('MOBILE_LEGACY_NOTIFICATION_ENDPOINT'),

    'appstore_legacy_endpoint' => env('APPSTORE_LEGACY_WEBHOOK_ENDPOINT'),
    'playstore_legacy_endpoint' => env('PLAYSTORE_LEGACY_WEBHOOK_ENDPOINT'),

];
