@extends('frontend.includes.layout')
@section('header')
<link rel="stylesheet" type="text/css" href="/assets/css/owl.carousel.min.css">
@endsection
@section('content')
@include('frontend.includes.navbar')
<div class="wrapper new-homepage">
	<section class="banner">
		<div class="banner-content">
			<div class="container-fluid">
				<div class="row banner-row">
					<div class="col-md-8 banner-column left">
						<div class="banner-text">
							<h3>L'application tse est là</h3>
							<h3>Are you ready ?</h3>
							<h2>Je découvre</h2>
							<p><span>Telechargement Gratuit</span></p>
							<div class="apps">
								<a rel="noopener" target="_blank" href="https://apps.apple.com/fr/app/train-sweat-eat/id1482080501" title="Téléchargez l'application sur l'App Store d'Apple.">
									<img src="optimized/img/app_store.svg" alt="apple app store logo">
								</a>
								<a rel="noopener" target="_blank" href="https://play.google.com/store/apps/details?id=com.ohmconception.trainsweateat" title="Téléchargez l'application sur la boutique Google Play.">
									<img src="optimized/img/play_store.svg" alt="google play store logo">
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 banner-column right">
						<div class="banner-image">
							<picture>
								<source type="image/webp" srcset="optimized/img/iphone_image.webp">
								<source type="image/png" srcset="optimized/img/iphone_image.png">
								<img src="optimized/img/iphone_image.png" alt="">
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section2">
		<div class="container-fluid">
			<div class="row section2-row">
				<div class="col-md-4 custom-column image">
					<h3>L'app fitness la plus complète!</h3>
					<div class="app-icon">
						<a rel="noopener" target="_blank" href="https://apps.apple.com/fr/app/train-sweat-eat/id1482080501" title="Téléchargez l'application sur l'App Store d'Apple."><img src="optimized/img/apple-logo.svg" alt="ios logo"></a>
						<a rel="noopener" target="_blank" href="https://play.google.com/store/apps/details?id=com.ohmconception.trainsweateat" title="Téléchargez l'application sur la boutique Google Play."><img src="optimized/img/android-os.svg" alt="android logo"></a>
					</div>
				</div>
				<div class="col-md-8 custom-column description">
					<p>Sculpte et muscle toi avec Trainsweateat et Bikini avec Sissy,<br> Le programme N°1 en France !<br> Booste ton métabolisme, développe tes muscles,<br> Régale-toi avec une alimentation saine et fit et deviens la meilleure version de toi-même !</p>
				</div>
			</div>
		</div>
	</section>
	<section class="we-sweat">
		<div class="container-fluid">
			<div class="row we-sweat-row1">
				<div class="col-md-6 custom-column image">
						<picture>
							<source type="image/webp" srcset="optimized/img/pilates_kick.webp">
							<source type="image/jpeg" srcset="optimized/img/pilates_kick.jpg">
							<img src="optimized/img/pilates_kick.jpg" alt="">
						</picture>
				</div>
				<div class="col-md-6 custom-column description">
					<h2>We <span>sweat</span></h2>
					<div class="new-divider">
						<span class="outer-line line-left"></span> 
						<span class="fa fa-heart"></span> 
						<span class="outer-line line-right"></span>
					</div>
					<h3>Les meilleurs Trainings Live !</h3>
					<p>Entraine-toi à la maison ou ailleurs avec Sissy, la plus motivante Des coach fitness ! avec elle pour te booster, tu atteindras enfin tes objectifs plus vite que prévu !</p>
					<p>Avec 1 nouveau programme chaque mois et plusieurs nouvelles vidéos chaque semaine,<br> Impossible d’abandonner ! Rejoins la team We Sweat et accède à déjà plus de 50 vidéos de tous niveaux !</p>
				</div>
			</div>
			<div class="row we-sweat-row2">
				<div class="col-md-6 custom-column description">
					<h3>De Nombreuses Nouvelles Disciplines !</h3>
					<p>Que tu aies envie de varier ton entrainement, de changer de routine ou de completer tes tranings, tu ne seras pas déçu !</p>
					<p>Découvres le Hotpilats pour travailler ta mobilité, <br>Le Fitboxing pour te muscler et te défouler, <br>Le Yoga pour te libérer, et encore plein d’autres disciplines à venir !</p>
					<div class="read-more">
						<a href="{{ route('front.we-sweat.videos') }}">Lire la suite</a>
					</div>
				</div>
				<div class="col-md-6 custom-column image">	
						<picture>
							<source type="image/webp" srcset="optimized/img/yoga.webp">
							<source type="image/jpeg" srcset="optimized/img/yoga.jpg">
							<img src="optimized/img/yoga.jpg" alt="">
						</picture>
				</div>
			</div>
		</div>
	</section>
	<section class="we-train">
		<div class="container-fluid">
			<div class="row we-train-row">
				<div class="col-md-6 custom-column image">
					<div class="we-train-img">
							<picture>
								<source type="image/webp" srcset="optimized/img/sissy_tini_facing_dumbell.webp">
								<source type="image/png" srcset="optimized/img/sissy_tini_facing_dumbell.png">
								<img src="optimized/img/sissy_tini_facing_dumbell.png" alt="">
							</picture>
					</div>
				</div>
				<div class="col-md-6 custom-column description">
					<h2>We <span>train</span></h2>
					<div class="new-divider">
						<span class="outer-line line-left"></span> 
						<span class="fas fa-dumbbell"></span> 
						<span class="outer-line line-right"></span>
					</div>
					<h3>Un programme inédit de musculation en salle.</h3>
					<p>Tu es plutôt accro à la salle et aux poids ? tu vas adorer les trainings We Train, préparés par Tini, spécialisé en musculation depuis plus de 10 ans !<br> Ce programme mixte, réalisé par Sissy et Tini eux même, te permettra de développer ta masse musculaire, de sécher ta masse grasse et surtout de kiffer tes entrainements ! En effet, avec une séance différente chaque jour, illustré en vidéo, impossible de se lasser !<br> Rejoins la team We Train et accède au meilleur programme accessible à tous.</p>
					<div class="read-more">
						<a href="{{ route('front.we-train.videos') }}">Lire la suite</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bikini-section">
		<div class="container-fluid">
			<div class="row bikini-section-row">
				<div class="col-md-6 custom-column description">
					<h2>Bikini <span>Avec Sissy</span></h2>
					<div class="new-divider">
						<span class="outer-line line-left"></span> 
						<span class="divider-icon"><img src="assets/img/vintage-bikini.png" alt=""></span> 
						<span class="outer-line line-right"></span>
					</div>
					<h3>Les légendaires bikini avec sissy !</h3>
					<p>Retrouve directement sur l’app les trainings iconiques Bikini avec Sissy ! Avec plus de<br> 23 millions de vues sur Youtube, il s’agit du training le plus populaire !<br> Accessibles gratuitement à partir de l’app, ces séances te préparerons à l’été comme jamais !</p>
				</div>
				<div class="col-md-6 custom-column image">
				</div>
			</div>
		</div>
	</section>
	<section class="we-eat">
		<div class="container-fluid">
			<div class="row we-eat-row">
				<div class="col-md-6 custom-column image">
					<div class="we-eat-img">
							<picture>
								<source type="image/webp" srcset="optimized/img/burger_smaller.webp">
								<source type="image/jpeg" srcset="optimized/img/burger_smaller.jpg">
								<img src="optimized/img/burger_smaller.jpg" alt="">
							</picture>
					</div>
				</div>
				<div class="col-md-6 custom-column description">
					<h2>We <span>eat</span></h2>
					<div class="new-divider">
						<span class="outer-line line-left"></span> 
						<span class="divider-icon"><img src="assets/img/vintage-bikini.png" alt=""></span> 
						<span class="outer-line line-right"></span>
					</div>
					<h3>Stops les régimes, oui à une alimentation fitness !</h3>
					<p>On arrête désormais de se priver et de compter les calories et <br> On adopte une alimentation fitness, saine et efficace !<br> Avec les véritables recettes et conseils de Sissy, tu mets toutes les chances de ton côté pour obtenir la shape de tes rêves !</p>
					<div class="read-more">
						<a href="{{ route('front.we-eat.recipes') }}">Lire la suite</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="temoignages-section">
		<div class="container-fluid">
			<div class="row temoignages-section-row">
				<div class="col-md-12 custom-column">
					<h2>Nos témoignages</h2>
					<div class="new-divider">
						<span class="outer-line line-left"></span> 
						<span class="fa fa-quote-right"></span> 
						<span class="outer-line line-right"></span>
					</div>
					<div class="testimonial-slider-wrapper">
						<div class="owl-testimonials owl-carousel owl-theme">
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@steven.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@steven.jpg">
													<img src="optimized/img/testimonials/@steven.jpg" alt="testimonial picture of @steven">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Tu as changé ma vie ! Tes vidéos, tes conseils et ma volonté m'ont fait perdre 20 kilos et permis d'obtenir le corps athlétique que j'ai désormais !
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@lea.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@lea.jpg">
													<img src="optimized/img/testimonials/@lea.jpg" alt="testimonial picture of @lea">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Grâce à tes vidéos j'ai réussi à perdre 12kg, à me mettre à la muscu et à adorer ça ! Et pour ça je te remercie vraiment ❤ Même ma mère s'y est mise!
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@julie.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@julie.jpg">
													<img src="optimized/img/testimonials/@julie.jpg" alt="testimonial picture of @julie">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Je n'aurais jamais cru pouvoir me transformer comme ça !!! Je fais 1m60 et je suis passée de 59 kg à 53 en 1 mois 😍 on me l’aurais dit que je l’aurais pas cru !
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@alizeesb.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@alizeesb.jpg">
													<img src="optimized/img/testimonials/@alizeesb.jpg" alt="testimonial picture of @alizeesb">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Hello ! Un message simple de remerciement pour toute l’équipe . 🤗 Un grand merci particulier à Sissy où j’ai commencé à me mettre au fitness il y a un peu plus d’un an . On ne change pas seulement de physique on apprend sur soi et on prend confiance en soi . ( surtout pour l’introvertie que je suis ) Un joli développement personnel qui fait du bien au corps mais avant tout à l’esprit ☺️ Aujourd’hui le sport est une priorité quotidienne . Un plaisir évidemment . Alors encore une fois merci !! 💪😌 Alizée S
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@chloealies2.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@chloealies2.jpg">
													<img src="optimized/img/testimonials/@chloealies2.jpg" alt="testimonial picture of @chloealies2">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Ce petit message juste pour te dire.. merci ! Merci pour tous Sissy vraiment.. tu m’accompagnes depuis des années maintenant j’ai fait et refais tous les BAS (j’attends le 6 avec impatience d’ailleurs). Merci d’être toujours là et de nous motiver comme tu le fais. Tu as changée ma vie ❤
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@debora_fit.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@debora_fit.jpg">
													<img src="optimized/img/testimonials/@debora_fit.jpg" alt="testimonial picture of @debora_fit">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													BILAN 3 MOIS PRISE DE MASSE ⏳ —————————————————- Photo de gauche: Août Photo de droite : Novembre —————————————————————— ⚠️ AUCUNE CONTRACTION NI DE COURBURE DU DOS NI RIEN ⚠️ Je suis relax à Max sur toutes les photos c’est trop facile de tricher de cambrer son dos pour bien ressortir ses fesses, ou de contracter à Max ses cuisses ... mais c’est pas mon but a moi ! 😌 ——————————————————————- + 2 cm tour de cuisse depuis Août et - 3cm tour de taille —————————————————————— Tout se passe très bien depuis début Août. J’ai perdu un peu de gras au niveau des jambes et du ventre et j’ai pris du muscle. Je suis hyper fière de moi, des résultats et tout ça quoi 😂... au début j’étais à 150g de glucides par repas maintenant je tourne autour des 200-210g j’augmente un peu près 10-15g chaque mois et j’ai pu remarquer qu’en mangeant plussss je stockais beaucoup moins !Sinon je respecte à la ligne ma diete. Je m’entraîne sans relâche 5 à 6 entraînements par semaine ! Je reste focus et déterminée ! Maintenant j’entame mon 4iem mois en augmentant d’ici 15j à 210g les glucides par repas. On se retrouve dans 1 mois pour voir quoi comment ✊🏽💪🏼 ————————————————————— Tout ça grâce au programme @teamtrainsweateat ! Merci à @sissymua et @fitwtini de m’avoir élue égérie 🙏🏼 cette transfo c’est une partie grâce à eux ❤️ Je vous kiffffff —————————————————————
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@h3ll0w.xox.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@h3ll0w.xox.jpg">
													<img src="optimized/img/testimonials/@h3ll0w.xox.jpg" alt="testimonial picture of @h3ll0w">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Coucou la team TSE 💪🏻, je souhaitais infiniment te remercier Sissy pour toutes ces chouettes séances que tu donnes qui m’ont vraiment permis de perdre des KG (93kg a 55 kg en 2ans avec stabilisation) certes mais modeler ma silhouette comme je le souhaitais également ainsi que me redonner une forme de fou car avant j’étais perpétuellement fatiguée et stressée 😩! Alors sincèrement tu mérites mes remerciements pour la patate que tu me donnes chaque fois que je fais les séances de #wesweat , ton investissement énorme ! Toujours à nous faire découvrir de nouvelles choses : Pilate, Fitboxing et bientôt le yoga ! Tu travailles dur et nous on a des résultats ! ❤️💪🏻🙏J’espère que tu continueras encore longtemps !!! Grâce à toi je suis devenu la meilleure version de moi-même et je continue ! Gros bisous 😘 Une fan belge 😂
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@juliette_mac.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@juliette_mac.jpg">
													<img src="optimized/img/testimonials/@juliette_mac.jpg" alt="testimonial picture of @juliette_mac">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Coucou Sissy et Tini j’espère que vous allez bien, je ne suis pas certaine que vous allez lire ce message mais je voulais partager mon expérience avec vous. Depuis quelques années je regarde les videos de Sissy et depuis mai 2018 je fais les BAS dans le but de perdre du poids car je me sentais grosse, j’ai perdu 10 kilos en moins d’1 an en combinant le sport une alimentation ultra saine. Aujourd’hui j’ai perdu 16 kilos et je me suis inscrite à la salle de sport depuis septembre dans le but de prendre de la masse musculaire car je ne dois plus perdre de poids cela pourrait devenir mauvais pour moi. Merci beaucoup pour tout, en l’espace d’1 an et demi j’ai appris tellement en matière de nutrition et dorénavant c’est un sujet qui me passionne. Ma transformation était de mai 2018 à fin août 2019
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item testimonials-wrapper">
								<div class="testimonials-inner-wrapper">
									<div class="row temoignages-inner-row">
										<div class="col-md-6 inner-column">
											<div class="img-wrapper">
												<picture>
													<source type="image/webp" srcset="optimized/img/testimonials/@lauraalvernhes.webp">
													<source type="image/jpeg" srcset="optimized/img/testimonials/@lauraalvernhes.jpg">
													<img src="optimized/img/testimonials/@lauraalvernhes.jpg" alt="testimonial picture of @lauraalvernhes">
												</picture>
											</div>
										</div>
										<div class="col-md-6 inner-column">
											<div class="testimonials-text">
												<p>
													Bonjour, je voulais partager avec vous mon évolution ! Au début avec le livre de Sissy et ensuite avec TSE en un peu plus de "définir le temps" 💪 Un grand merci à vous deux, vous êtes une source de motivation
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="home-video">
		<div class="container-fluid">
			<div class="row home-video-row">
				<div class="col-md-12">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe title="welcome video" src="https://player.vimeo.com/video/370605965?title=0&byline=0&portrait=0&autoplay=1&loop=1&badge=0&color=00adef" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
					</div><script src="https://player.vimeo.com/api/player.js"></script>
				</div>
			</div>
		</div>
	</section>

	@if(count($offers))
	<section class="offers-section">
		<div class="container-fluid">
			<h2>Abonne toi !</h2>
			<h4></h4>
			<div class="new-divider">
				<span class="outer-line line-left"></span> 
				<span class="fas fa-euro-sign"></span> 
				<span class="outer-line line-right"></span>
			</div>
			<div class="row offers-tables-content">

				@foreach ($offers as $offer)
				<div class="col-lg-4 col-md-6 offers-column">
					<div class="offers-wrapper">
						<div class="offers-inner-wrapper">
						<div class="subscribe-button">
							<a href="{{auth('web')->check()?route('front.payment',['id'=>$offer['id']]):route('front.register').'?next='.$offer['id']}}" class="custom-button">
								<span>
								S'inscrire 
								</span> 
							</a> 
						</div> 
							<div class="offers-header"> 
									<picture class="active-img">
										<source type="image/webp" srcset="optimized/img/formule-white.webp">
										<source type="image/png" srcset="optimized/img/formule-white.png">
										<img src="optimized/img/formule-white.png" alt="">
									</picture>
									<picture class="hover">
										<source type="image/webp" srcset="optimized/img/formule-black.webp">
										<source type="image/png" srcset="optimized/img/formule-black.png">
										<img src="optimized/img/formule-black.png" alt="">
									</picture>
								<h2>{{$offer['title']}}</h2> 
								<div class="offers-content"> 
									<span class="value">{{bcdiv($offer['price'],$offer['duration'],2)}}</span> 
									<span class="currency">€</span> 
									<span class="duration"> / mois</span> 
								</div> 
								<p>Facturé {{$offer['price']}}€{{$offer['is_recurring']?'/'.((int)$offer['duration']>1?(int)$offer['duration'].' mois':'moi'):' à l inscription'}}</p> 
							</div> 
							<div class="offers-body"> 
								<ul class="offers-features plan-offers"> 
									<li class="boldEngagement"> 
										<strong>Sans aucun engagement</strong>
									</li> 

										@foreach ($offer['plan']['products'] as $product)
											<li> <strong>{{$product['description']}} </strong></li>
										@endforeach

										<li>Et à toutes les prochaines surprises !</li> 
								</ul> 
							</div> 
							<div class="offer-plan-bottom"> 
								<div class="pictos-paiment"> 

									@if ($offer['is_paypal_available'])
									<img src="/optimized/img/paypal.svg" alt="can pay with paypal" title="Le paiement PayPal est disponible.">
									@endif

									@if ($offer['is_stripe_available'])
									<img src="/optimized/img/stripe.svg" alt="can pay with card" title="Le paiement par carte de crédit est disponible.">
									@endif

								</div> 
							</div> 
						</div> 
					</div> 
				</div>
				@endforeach

			</div>
		</div>
	</section>
	@endif

	<section class="they-talk-about-us">
		<div class="container-fluid">
			<h2>Ils parlent de nous</h2>
			<h4></h4>
			<div class="new-divider">
				<span class="outer-line line-left"></span> 
				<span class="fa fa-comments"></span> 
				<span class="outer-line line-right"></span>
			</div>
			<div class="row ttau-row">
				<div class="col-md-12">
					<ul>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/vital.webp">
								<source type="image/png" srcset="optimized/img/vital.png">
								<img src="optimized/img/vital.png" alt="vital logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/mens-fitness.webp">
								<source type="image/png" srcset="optimized/img/mens-fitness.png">
								<img src="optimized/img/mens-fitness.png" alt="men's fitness logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/biba.webp">
								<source type="image/png" srcset="optimized/img/biba.png">
								<img src="optimized/img/biba.png" alt="biba logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/le-figaro.webp">
								<source type="image/png" srcset="optimized/img/le-figaro.png">
								<img src="optimized/img/le-figaro.png" alt="le figaro logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/m6.webp">
								<source type="image/png" srcset="optimized/img/m6.png">
								<img src="optimized/img/m6.png" alt="m6 logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/elle.webp">
								<source type="image/png" srcset="optimized/img/elle.png">
								<img src="optimized/img/elle.png" alt="elle logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/tfi.webp">
								<source type="image/png" srcset="optimized/img/tfi.png">
								<img src="optimized/img/tfi.png" alt="tfi logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/glamour.webp">
								<source type="image/png" srcset="optimized/img/glamour.png">
								<img src="optimized/img/glamour.png" alt="glamour logo">
							</picture>
						</li>
						<li>
							<picture>
								<source type="image/webp" srcset="optimized/img/tonic.webp">
								<source type="image/png" srcset="optimized/img/tonic.png">
								<img src="optimized/img/tonic.png" alt="tonic logo">
							</picture>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="/assets/js/owl.carousel.min.js"></script>
@endsection