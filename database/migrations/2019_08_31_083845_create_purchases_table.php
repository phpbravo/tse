<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('offer_id');
                $table->foreign('offer_id')->references('id')->on('offers');
            $table->unsignedBigInteger('plan_id');
                $table->foreign('plan_id')->references('id')->on('plans');
            $table->unsignedBigInteger('transaction_id')->nullable();
                $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->boolean('is_active')->default(true)->index();
            $table->boolean('is_paused')->default(false)->index();
            $table->boolean('is_lifetime')->default(false)->index();
            $table->dateTime('last_pause')->nullable()->index();
            $table->char('paused_total',8)->nullable()->index();
            $table->char('duration',8)->nullable()->index();
            $table->dateTime('effective_on')->nullable()->index();
            $table->dateTime('expires_on')->nullable()->index();
            $table->dateTime('cancelled_on')->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
