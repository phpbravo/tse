<?php

return [

    'key' => env('API_KEY'),

    'stripe_public_key' => env('STRIPE_PUBLIC'),
    'stripe_secret_key' => env('STRIPE_SECRET'),

    'paypal_id' => env('PAYPAL_ID'),
    'paypal_secret_key' => env('PAYPAL_SECRET'),

];
