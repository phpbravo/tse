<?php

namespace App\Http\Controllers\API\v0;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Handlers\{ObjectHandler, FilterHandler, PageHandler};

use Carbon\Carbon;

use App\{BlogPost, Recipe};

class RecipesController extends Controller
{

	public function getWeEatRecipes(){

		$recipes=Recipe::with(['sections','tags','recipe_category'])->get()->map(function($recipe){
			$ingredients=$recipe->sections()->find(1);
			$preparation=$recipe->sections()->find(2);
			$tips=$recipe->sections()->find(3);
			$nutrition=$recipe->sections()->find(4);

			$specs=$recipe->tags->pluck('name')->reduce(function($specs,$tag){
				return $specs.$tag.' / ';
			},'');

			str_replace_last('/ ','',$specs);

			return 
			[
				'item'=>[
					'id'=>(string) $recipe->id,
					'title'=>$recipe->name,
					'extrafield'=>'',
					'ingredient'=>$ingredients?$ingredients->pivot->content:'',
					'description'=>$preparation?$preparation->pivot->content:'',
					'hits'=>0,
					'categoryname'=>$recipe->recipe_category->name,
					'specs'=>$specs,
					'extrafieldsearch'=>'',
					'preparation'=>$recipe->preparation_time ?bcdiv($recipe->preparation_time,60).' min':'',
					'cuisson'=>$recipe->cook_time?bcdiv($recipe->cook_time,60).' min':'',
					'tips'=>$tips?$tips->pivot->content:'',
					'intro' => null,
					'nutrition'=>$nutrition?$nutrition->pivot->content:'',
					'q'=>'sd',
					'thumbnail'=>[
						'large'=>'/image/recipes/'.$recipe->id.'/image/0_main_image.jpg',
						'small'=>'/image/recipes/'.$recipe->id.'/image/0_main_thumb.jpg',
					],
				],
				'tags'=>[],
			];

		});

		return response()->json([
			'success'=>true,
			'data'=>[
					'tags'=>[],
					'list'=>$recipes
				]
		],200);

	}

	public function getNutritions()
	{
		$nutritions = BlogPost::all();

		return response()->json([
			'success' => true,
			'data' => [
				'detail' => [
					'id' => '90',
					'name' => 'CONSEILS NUTRITIONNELS',
					'description' => '',
					'groupname' => 'Subscribers',
					'extra_fields_group' => null,
					'image' => config('app.url').'/'.'image/blog_posts/image/0_main_image.jpg'
				],
				'items' => $nutritions->map(function($nutrition) {
					return [
						'id' => (string) $nutrition->id,
						'title' => $nutrition->title,
						'extrafield' => [],
						'description' => $nutrition->content,
						'categoryname' => 'CONSEILS NUTRITIONNELS',
					];
				})
			]
		]);
	}

}