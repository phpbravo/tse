<?php
use Artisan;
use Carbon\Carbon;
// Artisan::call('migrate:fresh');
// Artisan::call('db:seed');


//ALL API ENDPOINTS
$we_train_url = 'https://www.trainsweateat.com/index.php?option=com_osmembership&task=api.videos&username=ohm.conception&password=ohmtest&api_key=tse2019&catid=6';
$we_sweat_url = 'https://www.trainsweateat.com/index.php?option=com_osmembership&task=api.videos&username=ohm.conception&password=ohmtest&api_key=tse2019&catid=15';
$recipes_url = 'https://www.trainsweateat.com/index.php?option=com_osmembership&task=api.recipies&username=ohm.conception&password=ohmtest&api_key=tse2019&catid=2';


//SAVE ALL VIDEOS
$videos = json_decode(file_get_contents('videos.json', true)); 'videos retrieved';
foreach ($videos as $key => $video) {
    $new_video = Video::forceCreate([
        'id' => $video->id,
        'vimeo_id' => $video->vimeo_id,
        'vimeo_name' => $video->vimeo_name,
        'thumbnails' => $video->thumbnails,
        'quality' => $video->quality,
        'profile_id' => $video->profile_id,
        'token' => $video->token,
        'last_modified_date' => $video->last_modified_date,
        'vimeo_created_date' => $video->vimeo_created_date,
    ]);
}

//WE TRAIN REQUEST
$we_train_results = json_decode(file_get_contents($we_train_url)); 'we train requested';

//Create sub category
foreach ($we_train_results->data->tags as $subcategory) {
    $new_subcategory = Subcategory::forceCreate([
        'id' => $subcategory->id,
        'category_id' => 1,
        'name' => $subcategory->name,
        'title' => $subcategory->name,
    ]);
}
'we train subcategories inserted';

//Create exercises
foreach ($we_train_results->data->list as $object) {
    
    $exercise = $object->item;
    $tags = $object->tags;

    if ($exercise->videoid == "367963906") {
        $exercise->videoid = '337688490';
    }
    if ($exercise->videoid == "367962078") {
        $exercise->videoid = '324795059';
    }
    $array_fails = [];
    $video_id = Video::where('vimeo_id', $exercise->videoid)->first();
    if (!$video_id) {
       $array_fails[] = $exercise;
       continue;
    }
    $new_exercise = Exercise::forceCreate([
        'id' => $exercise->id,
        'category_id' => 1,
        'subcategory_id' => $tags ? $tags[0]->id : null,
        'video_id' => $video_id->id,
        'name' => $exercise->title,
        'description' => $exercise->description,
        'duration' => (int)$exercise->time ? now()->diffInSeconds(Carbon::parse($exercise->time.'min')) : null,
    ]);
} 
$array_fails;
'we train exercises inserted';

//WE SWEAT REQUEST
$we_sweat_results = json_decode(file_get_contents($we_sweat_url)); 'we sweat requested';

//Create sub category
foreach ($we_sweat_results->data->tags as $subcategory) {
    $new_subcategory = Subcategory::forceCreate([
        'id' => $subcategory->id,
        'category_id' => 2,
        'name' => $subcategory->name,
        'title' => $subcategory->name,
    ]);
}
'we sweat subcategories inserted';

//Create exercises
foreach ($we_sweat_results->data->list as $object) {
    
    $exercise = $object->item;
    $tags = $object->tags;

    $array_fails = [];
    $video_id = Video::where('vimeo_id', $exercise->videoid)->first();
    if (!$video_id) {
       $array_fails[] = $exercise;
       continue;
    }
    $new_exercise = Exercise::forceCreate([
        'id' => $exercise->id,
        'category_id' => 2,
        'subcategory_id' => $tags ? $tags[0]->id : null,
        'video_id' => $video_id->id,
        'name' => $exercise->title,
        'description' => $exercise->description,
        'duration' => (int)$exercise->time ? now()->diffInSeconds(Carbon::parse($exercise->time)) : null,
    ]);
} 
$array_fails;
'we sweat exercises inserted';


//WE SWEAT REQUEST
$recipe_result = json_decode(file_get_contents($recipes_url)); 'recipe requested';

$names_array = [];
foreach ($recipe_result->data->list as $object) {
    $recipe = $object->item;
    array_push($names_array, $recipe->categoryname);
}
$unique_subcategory_names = array_unique($names_array); 'unique subcategory_names';
//save subcategory names
foreach ($unique_subcategory_names as $name) {
    $new_recipe_category = RecipeCategory::forceCreate([
        'name' => $name
    ]);
}
//create recipe
foreach ($recipe_result->data->list as $object) {
    $recipe = $object->item;
    $tags = $object->tags;

    $recipe_category = RecipeCategory::where('name', $recipe->categoryname)->first();

    //save recipe to database
    $new_recipe = Recipe::forceCreate([
        'id' => $recipe->id,
        'recipe_category_id' => $recipe_category->id,
        'name' => $recipe->title, 
        'preparation_time' =>  (int)$recipe->preparation ?  now()->diffInSeconds(Carbon::parse($recipe->preparation)) : null,
        'cook_time' => (int)$recipe->cuisson ? now()->diffInSeconds(Carbon::parse($recipe->cuisson)) : null,
    ]);

    //new recipe section
    $new_recipe->sections()->attach([
        1=>[
            'content'=> $recipe->ingredient
        ],
        2=>[
            'content'=>$recipe->description
        ],
        3=>[
            'content'=>$recipe->tips
        ],
        4=>[
            'content'=>$recipe->nutrition
        ],
    ]);

    //create tag for recipe
    foreach (json_decode($recipe->extrafield) as $tag) {
        if ($tag->id == 12) {
           foreach (explode('/', $tag->value) as $tag_string) {
               $new_recipe->tag($tag_string);
           }
        }
    }
}
