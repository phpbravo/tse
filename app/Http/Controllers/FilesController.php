<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


//  CONTROLLER IS API-ONLY

class FilesController extends Controller
{

	private static function recursive_mkdir($dest, $permissions=0755, $create=true){
		if(!is_dir(dirname($dest))){ self::recursive_mkdir(dirname($dest), $permissions, $create); }  
		elseif(!is_dir($dest)){ mkdir($dest, $permissions, $create); }
		else{return true;}
	}


	// HANDLES FILE UPLOAD
	public function upload(Request $request){

		$fileContainers=[];

		
		foreach($request as $key=>$value){

			if (strpos($key,'file')===0){
				$fileContainers[]=$value;
				break;
			}

		}

		foreach($fileContainers[0]->all() as $path=>$container){

			$subfolder=[];

			preg_match('/(?<=_).*(?=_)/', $path, $subfolder);

			//if there was a subfolder specified, set it up
			$subfolder=count($subfolder)>0?'/'.$subfolder[0]:'';

			self::recursive_mkdir(base_path('/storage/app/uploads/'.
			$request->target_entity_type.
			'/'.
			$request->target_entity_id.
			$subfolder.'/'));

			foreach($container as $file){

				$file->move(
					base_path('/storage/app/uploads/'.
					$request->target_entity_type.
					'/'.
					$request->target_entity_id.
					'/'.
					$subfolder.
					''),
				
					time().
					'_'.
					$file->getClientOriginalName()
				);

			}
		}

	}

	public function fetchGuarded($path){
		$path=base_path('storage/guarded/'.$path);
		if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');

		return response()->file($path);
	}
	
	//	FETCH RESOURCE FILE (CAN BE SRC'D)
	public function fetch(Request $request){
		$path=base_path('storage/app/'.$request->path);
		if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');

		return response()->file($path);
	}

	//	DELETE FILE
	public function delete(Request $request){
		$path=base_path('storage/app/'.$request->path);
		if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');
		
		$success=unlink($path);

		$data=(object)compact(
			'success'
		);

		return json_encode($data);
	}

	//  LISTS DIRECTORY
	public function ls(Request $request){
		$files=[];
		try{
			$files=array_slice(scandir(base_path('storage/app/'.$request->path)),2);
		}catch(\ErrorException $e){
			$files=[];
		}

		$data=(object)compact(
			'files'
		);

		return json_encode($data);
	}
	//  ALIAS OF _ls
	public function dir(Request $request){
		return ls($request);
	}


	//internal version that can be used by other controllers
	public function store(UploadedFile $file, $path){

	}

}
