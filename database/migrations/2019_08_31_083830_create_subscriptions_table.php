<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users');
            $table->unsignedBigInteger('offer_id');
                $table->foreign('offer_id')
                    ->references('id')
                    ->on('offers');
            $table->unsignedBigInteger('gateway_id');
                $table->foreign('gateway_id')
                    ->references('id')
                    ->on('gateways');
            $table->unsignedBigInteger('upgrade_from_subscription_id')
                ->nullable();
                $table->foreign('upgrade_from_subscription_id')
                ->references('id')
                ->on('subscriptions');
            $table->string('gateway_token')->index();
            $table->boolean('is_active')->default(false)->index();
            $table->boolean('is_paused')->default(false)->index();
            $table->boolean('is_upgrade')->default(false)->index();
            $table->integer('price')->index();
            $table->integer('tax_percent')->index();
            $table->dateTime('last_pause')->nullable()->index();
            $table->char('paused_total',8)->default('00000000')->index();
            $table->dateTime('subscribed_on')->nullable()->index();
            $table->dateTime('cancelled_on')->nullable()->index();
            $table->dateTime('next_payment_date')->nullable()->index();
            $table->boolean('is_legacy')->default(false)->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
